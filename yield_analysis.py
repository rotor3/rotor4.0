import copy

from db_connect.db_connect import get_dataframe_from_db
import pandas as pd
import numpy as np
from sympy import *
#from matplotlib import pyplot as plt

"""This module checks all yield equations for the turning point in order to cap yields at their max.
The derivative of the yield function is set to zero. SRIs maxima range from 67 to 150 (irrelevant).
In order to not get negative yields, the yield function is checked for = 0. The highest SRI of the minima is 18 thus far."""

sql_query = """SELECT t1.crop_id, t2.name_de, t1.a_1, t1.a_2, t1.a_3, t1.b_1, t1.b_2, t1.b_3, t1.c_1, t1.c_2, t1.c_3 FROM yield_crop_coefficients_tb t1 join (select crop_id, name_de from crops_tb) t2 on t1.crop_id = t2.crop_id"""


df = get_dataframe_from_db(sql_query, index_column='crop_id')

df.fillna(0, inplace=True)
print(df)

x = Symbol("x")
ackerzahl_min = []
for index, row in df.iterrows():
    print(row["name_de"])
    for i in range(1, 4):
        a = "a_" + str(i)
        b = "b_" + str(i)
        c = "c_" + str(i)
        # try:
        # print("row index", index, "row", row)
        y = row[a] * x**2 + row[b] * x + row[c]
        print("y(x) = ", y)
        y_derivative = y.diff(x)
        solved = solve(y_derivative)
        print("y'(x) = ", y_derivative)
        solve_y = solve(y)
        print("Ackerzahl minimum", solve_y)
        print("Ackerzahl max ", solved)
        ackerzahl_min.append(solve_y)
        print("min solve_y: ", min(ackerzahl_min))

print("Ackerzahl minima: ", ackerzahl_min)
        # except:
        #    pass
