import copy

from project_handling.project import Project
from db_connect.db_connect import execute_query_get_one
import json

from ui_logic.ui_status import UiStatus


class Dummy:
    def __init__(self, project_id):
        self.project = Project()
        self.load_project_from_db(project_id)


    def load_project_from_db(self, project_id):
        self.project = Project()
        self.project.project_id = project_id

        self.ui_status = UiStatus()
        print("project from db")

        json_sql = """SELECT projects_name, projects_user, project_data, ui_status
                            FROM projects_tb WHERE projects_id = %(project_id)s""" % {"project_id": project_id}

        recordset = execute_query_get_one(json_sql)

        self.project.project_name = recordset[0]
        self.project.project_user = recordset[1]
        project_data = json.loads(recordset[2])
        self.ui_status = json.loads(recordset[3])
        self.project.tab_general = project_data["tab_general"]
        self.project.tab_rotation = project_data["tab_rotation"]
        self.project.tab_epm = project_data["tab_epm"]
        self.project.tab_free_generation = project_data["tab_free_generation"]
        # TODO implement in project handling ---------------------------------------------------------------
        self.project.tab_manure = copy.deepcopy(project_data["tab_manure"])
        print("before key change", project_data["tab_manure"]["manure_values"])
        self.project.tab_manure["manure_values"] = {}
        self.project.tab_manure["manure_standard_values"] = {}
        for manure_key, manure_values in project_data["tab_manure"]["manure_values"].items():

            self.project.tab_manure["manure_values"][int(manure_key)] = copy.deepcopy(manure_values)
        for manure_key, manure_values in project_data["tab_manure"]["manure_standard_values"].items():

            self.project.tab_manure["manure_standard_values"][int(manure_key)] = copy.deepcopy(manure_values)
        # TODO ---------------------------------------------------------------------------------------------
        self.project.result_general = project_data["result_general"]
        self.project.results = project_data["results"]
        self.project.report = project_data["report"]

        print("PROJECT\n", self.project.tab_rotation)
        print("")
        print("Tab Manure, \n", self.project.tab_manure["m_n_manures_ids"])
        print("Tab Manure, \n", self.project.tab_manure["manure_values"])





        # return self.project

        # TODO: try construct for old projects that do not have calculations yet
        # try:
        #     rotor_project.results = json.loads(recordset[6])
        # finally:
        #     return rotor_project
