import os
from PySide2 import uic, pylupdate


uic.compileUiDir("../qtui_rotor")
#uic.compileUiDir("..\sqlbrowser")

"""
Alternativ, wenn das nicht functioniert im Terminal
cd Documents\02_HTW\000_BACHELORARBEIT\08_Rotor_40\02_Rotor40\qtui_rotor

pyuic5 -o mainwindow.py mainwindow.ui


"""

# build window
os.system('cmd/c "pyuic5 -o ..\qtui_rotor\mainwindow.py ..\qtui_rotor\mainwindow.ui"')
# update QTUI_rotor project for translations
os.system('cmd/c "pylupdate ..\qtui_rotor\QTUI_rotor.pro"')
"""if changes have been made to the GUI that require translation, open the app Linguist (from the Qt package), add translations,
Mark all "finished" - the green check mark - then go to File --> Release allbuildwindow.py"""

print("""if changes have been made to the GUI that require translation,\n- open the app Linguist (from the Qt package), add new items' translations,
\n- Mark all "finished" - the green check mark \n- go to File --> Release all \n- now execute buildwindow_pt2.py""")