from PySide2 import QtWidgets


class Version:
    @staticmethod
    def show_versions(widget, ui_status):
        if ui_status.settings["language"] == "de":
            QtWidgets.QMessageBox.about(widget, "Versionen und Updates", "Version 1.0 Originalversion veröffentlicht in "
                                                "Bachinger und Zander 2007, European Journal of Agronomy\nVersion 2.0 "
                                                "Wesentliche Änderungen der Struktur, der Algorithmen, der "
                                                "Standortdaten und der Benutzeroberflächen innerhalb des BERAS-Projekts"
                                                " 2012-2013\nVersion 2.8 Wesentliche Änderungen, die die "
                                                "Geschwindigkeit erhöhen, neue Fruchtarten und neue Kombinationen von "
                                                "Früchtefolgen hinzufügt\nVersion 2.9 Fehler im N-Modul wurden entfernt\n"
                                                "Version 3.0 Humusbilanzierung verbessert. Möglichkeit, sowohl Festmist"
                                                " als auch Gülle bei Winterkulturen, Mais und Leguminosengras zu "
                                                "düngen, einschließlich Herbstansaat Leguminosengras nach Getreide "
                                                "und Blattfrüchten.\nVersion3.1 enthält die Möglichkeit, Leguminosen "
                                                "in Untersaaten und in Stoppelsaaten zu integrieren. Und der Ertrag "
                                                "von beiden und ihre N2-Fixierung wird in einer Tabelle in "
                                                "Abhängigkeit der Vorauswahlen angezeigt\nVersion 3.1 Kultur 8 ist "
                                                "wieder wählbar.")

        elif ui_status.settings["language"] == "en":
            QtWidgets.QMessageBox.about(widget, "Versions/updates", "Version 1.0 Original version as published in "
                                               "Bachinger and Zander 2007, European Journal of Agronomy \nVersion 2.0 "
                                               "Major modifications to the structure, algorithms, site data, and user "
                                               "interfaces within the BERAS project 2012-2013\nVersion 2.8 Major"
                                               " modifications increasing the caculation speed, adding new crops and "
                                               "new combinations of crop sequences\nVersion 2.9 errors in the N module "
                                               "were removed\nVersion 3.0 Corg-balance improved, possibility of using "
                                               "both solod and liquid manure at winter crops, maize and legume grass "
                                               "included, legume grass harvest sown after cereals and leaf crops "
                                               "included.\nVersion3.1 includes the possibility to have legumes in "
                                               "undersown and in stubble sown cover crops. And the yield of both and "
                                               "their N2-fixation is displayed in an table")

        elif ui_status.settings["language"] == "fr":
            QtWidgets.QMessageBox.about(widget, "Versions / Mises à jour", "Version 1.0 Version originale publiée dans "
                                                "Bachinger and Zander 2007, Journal européen d’agronomie\nVersion 2.0 "
                                                "Modifications importantes de la structure, des algorithmes, des données"
                                                " de site et de l’interface utilisateur au sein du projet BERAS "
                                                "2012-2013\nVersion 2.8 Modifications importantes augmentant la vitesse "
                                                "de calcul, ajoutant de nouvelles cultures et de nouvelles séquences de "
                                                "cultures\nVersion 2.9 Des erreurs du module d'azote ont été rectifiés\n"
                                                "Version 3.0 permet de sélectionner à la fois de l’engrais solide et "
                                                "liquide avec les céréales d’hiver et certaines autres cultures. La "
                                                "proportion de légumineuses des différentes années de légumineuse "
                                                "fourragère peut être modifiée et il est désormais possible de "
                                                "sélectionner des légumineuses fourragères semées pour la récolte. "
                                                "La proportion de mélanges graines-légumineuses-céréales peut être "
                                                "sélectionnée.\nLa version 3.1 inclut la possibilité d'avoir des "
                                                "légumineuses dans les cultures de couverture directement sur les "
                                                "chaumes ou de semis direct sous abri au printemps. Et le rendement "
                                                "des deux et leur N fixation est affiché dans un tableau")
