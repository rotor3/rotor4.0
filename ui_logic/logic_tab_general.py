import copy

from db_connect.db_connect import execute_query_get_dictionary_list, execute_query_get_one, execute_query_get_recordset,\
        execute_query_get_dictionary_list

"""

"""
class LogicTabGeneral:


    @staticmethod
    def set_soil_type_from_soil_index(project):
        # self.logger.info("sb_soil_index enabled")
        if project.tab_general["soil_index"] < 25:
            project.tab_general["soil_index"] = 25
        elif project.tab_general["soil_index"] > 80:
            project.tab_general["soil_index"] = 80
        else:
            project.tab_general["soil_index"] = (project.tab_general["soil_index"] // 5) * 5
        # self.logger.info("Soil index after: ", soil_index)

        return project

    @staticmethod
    def get_cb_soil_type(project, ui_status):
        sql_query = """select soil_index, name_%(language)s from soil_quality_tb""" \
                                      % {"language": ui_status.settings["language"]}

        recordset = execute_query_get_recordset(sql_query)
        project.all_tabs_combobox_items["cb_soil_type"] = recordset

        return project

    @staticmethod
    def get_ui_status_assessment(project, ui_status):
        if project.tab_general["assessment_method"] in (1, 3):
            ui_status.tabs["tab_rotation_visible"] = True
            ui_status.tabs["tab_generator_visible"] = False
            print("ASSESSMENT METHOD", project.tab_general["assessment_method"])
            # TODO refactor to language
            if project.tab_general["assessment_method"]== 1:
                if ui_status.settings["language"] == "de":
                    ui_status.tabs_state["rotation_tab_name"] = "Intelligente Fruchtfolgeerstellung"
                elif ui_status.settings["language"] == "en":
                    ui_status.tabs_state["rotation_tab_name"] = "Guided generation"
                elif ui_status.settings["language"] == "fr":
                    ui_status.tabs_state["rotation_tab_name"] = "Génération guidée"

                print("ASSESSMENT METHODE GEFÜHRT")
            else:
                if project.tab_general["assessment_method"] == 1:
                    if ui_status.settings["language"] == "de":
                        ui_status.tabs_state["rotation_tab_name"] = "Fruchtfolgebewertung"
                    elif ui_status.settings["language"] == "en":
                        ui_status.tabs_state["rotation_tab_name"] = "Rotation assessment"
                    elif ui_status.settings["language"] == "fr":
                        ui_status.tabs_state["rotation_tab_name"] = "Évaluation d'une' rotation"

                print("ASSESSMENT METHOD BEWERTUNG")
        else:  # assessment_method == 2, free generation:
            ui_status.tabs["tab_rotation_visible"] = False
            ui_status.tabs["tab_generator_visible"] = True

        return project, ui_status

    @staticmethod
    def set_manure_tab_visible(project, ui_status, index):
        update_manure_tab = copy.copy(ui_status.tabs["tab_organic_manure_visible"])
        if True in ui_status.tab_rotation["sb_manure_amount_liquid_active"] or True in ui_status.tab_rotation[
            "sb_manure_amount_solid_active"]:
            ui_status.tabs["tab_organic_manure_visible"] = True
        else:
            False

        if update_manure_tab != ui_status.tabs["tab_organic_manure_visible"]:
            ui_status.tabs["update_tabs"] = True
        else:
            ui_status.tabs["update_tabs"] = False

        return ui_status
