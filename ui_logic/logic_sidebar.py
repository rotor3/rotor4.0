from project_handling.project import Project
# from rotor_calculations.yield_in_t import YieldInT
from ui_logic.logic_tab_manure import LogicTabManure
from ui_logic.logic_tab_yield import LogicTabYield
from ui_logic.ui_status import UiStatus
from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one, execute_query_get_dictionary_list


class LogicSidebar:

    @staticmethod
    def reset_all(project):
        temp_project = Project()
        temp_project.project_id = project.project_id
        temp_project.project_name = project.project_name
        temp_project.user_id = project.user_id

        ui_status = UiStatus()

        return temp_project, ui_status

    @staticmethod
    def reset_tab_general(project, ui_status):
        temp_project = Project()
        temp_status = UiStatus()

        project.tab_general = temp_project.tab_general
        ui_status.tab_general = temp_status.tab_general

        return project, ui_status

    @staticmethod
    def reset_crop_rotation(project, ui_status):
        temp_project = Project()
        temp_status = UiStatus()

        project.tab_rotation = temp_project.tab_rotation

        # ui_status.dict_tab_free_generation = temp_status.dict_tab_free_generation

        ui_status.tab_rotation = temp_status.tab_rotation
        ui_status.tabs["tab_organic_manure_visible"] = False
        ui_status.tabs["tab_epm_selection_visible"] = False
        ui_status.tabs["tab_report_visible"] = False
        ui_status.tabs_state["update_tabs"] = True

        return project, ui_status

    # @staticmethod
    # def reset_free_generator(project, ui_status):
    #     temp_project = Project()
    #     temp_status = UiStatus()
    #
    #     project.dict_tab_free_generator = temp_project.dict_tab_free_generator
    #     project.result_temp = temp_project.result_temp
    #     ui_status.dict_tab_free_generator = temp_status.dict_tab_free_generator
    #
    #     return project, ui_status

    @staticmethod
    def reset_manure(project, ui_status):
        # temp_project = Project()
        # temp_status = UiStatus()

        #TODO load default values
        for index in range(0, len(project.tab_manure["m_n_manures_ids"])):
            LogicTabManure.bt_load_standard_values(project, ui_status, index)

        return project, ui_status

    @staticmethod
    def reset_yield(project, ui_status):
        project, ui_status = LogicTabYield.get_all_yields(project, ui_status)

        return project, ui_status

