from db_connect.db_connect import execute_query_get_recordset
from ui_logic.logic_tab_rotation import LogicTabRotation


class LogicTabRotationIntelliGeneration(LogicTabRotation):
    import copy

    from PyQt5.uic.properties import QtWidgets

    from db_connect.db_connect import execute_query_get_recordset
    """
    This class is for methods, that work in all assessment methods, intelligent_generation, assessment and free_generation.
    Intelligent_generation and assessment share the same GUI!
    """

    class LogicTabRotation:

        @staticmethod
        def generate_crop_recordset(project, index, language):
            """
            in these selections only the previous_crop_delivery and prev_crop_type are considered. all other restrictions
            considering covercrop-selections and undersowing-selections would restrict the selection in GUI too much.
            """
            print("----generate_cb_crop_recordset", index)
            print(project.tab_rotation["crop_id"])
            previous_crop_id = project.tab_rotation["crop_id"][
                (index - 1) % project.tab_general["rotation_duration"]]
            next_crop_id = project.tab_rotation["crop_id"][
                (index + 1) % project.tab_general["rotation_duration"]]
            current_crop = project.tab_rotation["crop_id"][index]
            # print("PREV CRop", str(previous_crop_id), "Current crop:", current_crop, "NEXT CROP", str(next_crop_id))
            # print("PREV EARLY B-O check 1: ", project.dict_tab_crop["early_termination"])

            parameter_dict = {"language": language, "next_crop_id": next_crop_id,
                              "previous_crop_id": previous_crop_id}

            if previous_crop_id is None and next_crop_id is None or project.tab_general["assessment_method"] == 3:
                sql_query = """SELECT DISTINCT t1.crop_id, t1.name_%(language)s FROM crops_tb  t1 INNER JOIN
                                    crop_rotation_tb t2 ON t1.crop_id = t2.crop_id
                                    ORDER BY name_%(language)s;""" % parameter_dict

            # this case cannot occur with the select button system
            elif previous_crop_id is None and next_crop_id > 0:
                sql_query = """SELECT t1.crop_id, t1.name_%(language)s FROM crops_tb  t1 
                       INNER JOIN (SELECT DISTINCT t2.crop_id FROM crop_rotation_tb t2 INNER JOIN crop_rotation_tb t3 
                       ON (t2.n_delivery = t3.previous_crop_n_delivery)
                       AND (SELECT ct.crop_type_id from crops_tb ct 
                       WHERE ct.crop_id = t2.crop_id)= t3.previous_crop_type_id 
                       WHERE t3.crop_id = %(next_crop_id)s) t4 ON t1.crop_id = t4.crop_id
                       WHERE (SELECT sowing_time_id FROM crops_tb WHERE crop_id = %(next_crop_id)s) >= t1.harvest_time_id
                       ORDER BY name_%(language)s;""" % parameter_dict

            # TODO insert undersowing covercrop: if prev_crop_id undersowing is selected current must have covercrop = 3
            elif previous_crop_id > 0 and next_crop_id is None:

                sql_query = """SELECT a1.crop_id, a1.name_%(language)s from crops_tb a1
                        INNER JOIN (SELECT DISTINCT t2.crop_id FROM crop_rotation_tb t2 INNER JOIN crop_rotation_tb t3 
                        ON (t2.previous_crop_n_delivery = t3.n_delivery)
                        AND t2.previous_crop_type_id = (select ct.crop_type_id FROM crops_tb ct 
                        WHERE ct.crop_id = t3.crop_id)
                        WHERE t3.crop_id = %(previous_crop_id)s) t4 ON a1.crop_id = t4.crop_id
                        WHERE a1.sowing_time_id >= (select harvest_time_id from crops_tb WHERE crop_id = %(previous_crop_id)s) 
                        ORDER BY name_%(language)s;""" % parameter_dict

            else:
                sql_query = """SELECT DISTINCT a1.crop_id, a1.name_%(language)s FROM crops_tb  a1 
                       INNER JOIN (SELECT DISTINCT t1.crop_id, t1.n_delivery,
                       t1.previous_crop_n_delivery, 
                       t1.undersowing, t1.covercrop_id
                       FROM (SELECT DISTINCT t2.crop_id, t2.n_delivery, 
                       t2.previous_crop_n_delivery, t2.undersowing, t2.covercrop_id
                       FROM crop_rotation_tb t2 INNER JOIN crop_rotation_tb t3 
                       ON (t2.previous_crop_n_delivery = t3.n_delivery)
                       AND t2.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct WHERE ct.crop_id = t3.crop_id)
                       WHERE t3.crop_id = %(previous_crop_id)s) t1 INNER JOIN crop_rotation_tb p2 
                       ON t1.n_delivery = p2.previous_crop_n_delivery
                       AND (SELECT ct.crop_type_id from crops_tb ct 
                       WHERE ct.crop_id = t1.crop_id)= p2.previous_crop_type_id 
                       WHERE p2.crop_id = %(next_crop_id)s) prevnext ON a1.crop_id = prevnext.crop_id 
                       WHERE sowing_time_id >= (SELECT harvest_time_id FROM crops_tb WHERE crop_id = %(previous_crop_id)s)
                       AND (SELECT sowing_time_id FROM crops_tb 
                       WHERE crop_id = %(next_crop_id)s) >= harvest_time_id 
                       ORDER BY name_%(language)s""" % parameter_dict

            print("Generate Recordset", sql_query)
            recordset = execute_query_get_recordset(sql_query)

            # the recordset needs to be saved for later comparison
            project.rotation_temp["cb_crop_recordsets"][index] = recordset
            return project

        @staticmethod
        def get_status_of_none(project, ui_status, index):
            index_previous = (index - 1) % project.tab_general["rotation_duration"]
            project.tab_rotation["undersowing"][index] = False
            project.tab_rotation["covercrop"][index] = False

            project.rotation_temp["covercrop_ids"][index] = []
            project.rotation_temp["undersowing"][index] = []

            ui_status.tab_rotation["xb_undersowing_checkeable"][index] = True
            ui_status.tab_rotation["gb_undersowing_active"][index] = True

            if project.tab_rotation["undersowing"][index_previous] == False:
                ui_status.tab_rotation["xb_covercrop_checkeable"][index] = True
                ui_status.tab_rotation["gb_covercrop_active"][index] = True

            return project, ui_status

        @staticmethod
        def cb_crop_selected_changed(project, ui_status, index, language, *args):
            print("entering LogicTabRotation.cb_crop_selected_changed")
            rotation_duration = project.tab_general["rotation_duration"]

            # the list of crops in all other comboboxes gets renewed, starting withe the combobox AFTER the current
            recordset_changed = True
            print("Checkpoint 1 Recordset changed: ", recordset_changed, "Crops:", project.tab_rotation["crop_id"])
            # index_repopulate = copy.copy(index)
            # index_repopulate = (index_repopulate + 1) % rotation_duration

            project.rotation_temp["undersowing"][index] = []
            project.rotation_temp["covercrop_ids"][index] = []
            while recordset_changed == True:
                stored_recordset = copy.copy(project.rotation_temp["cb_crop_recordsets"][index])
                print("cb:crop_changed index", index)
                project = LogicTabRotation.generate_crop_recordset(project, index, language)
                # print("new_recordset \n", new_recordset)
                if stored_recordset == project.rotation_temp["cb_crop_recordsets"][index]:
                    recordset_changed = False
                    # print("Checkpoint 2 Recordset changed: ", recordset_changed)
                index = (index + 1) % rotation_duration

            return project

        @staticmethod
        def get_possible_undersowing_covercrop(project, ui_status, index):
            """all possible undersowings of CPAs and covercrops of crop are stored in project.rotation_temp"""
            print("entering LogicTabRotation.get_possible_undersowing_covercrop at index ", index)
            print(project.tab_rotation["crop_id"])

            index_previous = (index - 1) % project.tab_general["rotation_duration"]
            index_next = (index + 1) % project.tab_general["rotation_duration"]
            crop_id = project.tab_rotation["crop_id"][index]

            parameter_dict = {"crop_id": crop_id}

            sql_query_undersowing = """SELECT DISTINCT undersowing FROM crop_rotation_tb WHERE crop_id = %(crop_id)s""" \
                                    % parameter_dict

            sql_query_covercrop = """SELECT covercrop_id FROM m_n_crops_covercrop_tb WHERE crop_id = %(crop_id)s""" \
                                  % parameter_dict

            recorset_undersowing = execute_query_get_recordset(sql_query_undersowing)
            recordset_covercrop = execute_query_get_recordset(sql_query_covercrop)

            for undersowing in recorset_undersowing:
                project.rotation_temp["undersowing"][index].append(undersowing[0])
            for covercrop in recordset_covercrop:
                project.rotation_temp["covercrop_ids"][index].append(covercrop[0])

            project.rotation_temp["covercrop_ids"][index].sort()

            print("Possible undersowing", project.rotation_temp["undersowing"][index])
            print("Possible covervrop_ids", project.rotation_temp["covercrop_ids"][index])

            # _ create ui_status for SETTING UP items in year's selection_____________
            # ------------------------UNDERSOWING Status---------------------------------------------
            if project.rotation_temp["undersowing"][index] == [True]:
                print("Check XX 100")
                project.tab_rotation["undersowing"][index] = True
                ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
                ui_status.tab_rotation["gb_undersowing_active"][index] = True
                if 3 not in project.rotation_temp["covercrop_ids"][index_next] and \
                        project.rotation_temp["covercrop_ids"][index_next] != []:
                    print("3 NOT IN....")
                    ui_status.error["error"] = True
                    ui_status.error[
                        "errormessage"] = f"There is no combination for undersowing of year {(index + 1)} and covercrop of yeaar {(index_next + 1)}. Theerfore the selection is removed"
                    ui_status.error["remove_item"] = True

                elif 3 in project.rotation_temp["covercrop_ids"][index_next]:
                    project.tab_rotation["covercrop_id"][index_next] = 3
                    project.tab_rotation["covercrop"][index_next] = True
                    ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
                    ui_status.tab_rotation["gb_undersowing_active"][index] = True
                    ui_status.tab_rotation["xb_covercop_checkeable"][index_next] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index_next] = False

            elif project.rotation_temp["undersowing"][index] == [False]:

                print("CHECK X1")
                project.tab_rotation["undersowing"][index] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
                ui_status.tab_rotation["gb_undersowing_active"][index] = False
                if 2 not in project.rotation_temp["covercrop_ids"][index_next] and \
                        project.rotation_temp["covercrop_ids"][index_next] != []:
                    project.tab_rotation["covercrop"][index_next] = False
                    ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index_next] = False
            else:
                print("CHECK X2")
                ui_status.tab_rotation["xb_undersowing_checkeable"][index] = True
                ui_status.tab_rotation["gb_undersowing_active"][index] = True

            # ---------------COVERCROP--------------------------------
            if len(project.rotation_temp["covercrop_ids"][index]) == 1:
                print("CHECK X3")
                ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False

                project.tab_rotation["covercrop_id"][index] = project.rotation_temp["covercrop_ids"][index][0]

                if project.tab_rotation["covercrop_id"][index] == 1:
                    print("CHECK X4")
                    if False in project.rotation_temp["undersowing"][index_previous] or \
                            project.rotation_temp["undersowing"][index_previous] == []:
                        project.tab_rotation["undersowing"][index_previous] = False
                        ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False
                        ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False
                        project.tab_rotation["covercrop"][index] = False
                        ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                        ui_status.tab_rotation["gb_covercrop_active"][index] = False
                        print("CHECK X5")
                    else:
                        print("CHECK X6")
                        ui_status.error["error"] = True

                        ui_status.error[
                            "errormessage"] = f"The covercrop of year {(index + 1)} and undersowing of {(index_previous + 1)} do not match. Therefore the selected crop is removed"
                        ui_status.error["remove_item"] = True
                elif project.tab_rotation["covercrop_id"][index] == 2:
                    project.tab_rotation["covercrop"][index] = True
                    project.tab_rotation["covercrop_id"][index] = 2
                    ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index] = True

                    if False in project.rotation_temp["undersowing"][index_previous]:
                        project.tab_rotation["undersowing"][index_previous] = False
                        ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False
                        ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False

                else:
                    project.tab_rotation["covercrop"][index] = True
                    project.tab_rotation["covercrop_id"][index] = 3
                    ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index] = False
                    if True in project.rotation_temp["undersowing"][index_previous] or \
                            project.rotation_temp["undersowing"][index_previous] == []:
                        project.tab_rotation["undersowing"][index_previous] = True
                        ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False
                        ui_status.tab_rotation["gb_undersowing_active"][index_previous] = True

            # elif project.rotation_temp["covercrop_ids"][index]

            return project, ui_status

        @staticmethod
        def get_status_undersowing_toggled(project, ui_status, index):
            print("Entering LogicTabRotation.get_status_undersowing_toggled at index ", index)
            print(project.tab_rotation["crop_id"])
            # undersowing_toggled = project.dict_tab_rotation["undersowing"][index]

            index_next = (index + 1) % project.tab_general["rotation_duration"]

            # if next crop is already selected, its covercrop must be 3
            if project.tab_rotation["undersowing"][index]:
                if 3 in project.rotation_temp["covercrop_ids"][index_next] or \
                        project.rotation_temp["covercrop_ids"][index_next] == []:
                    project.tab_rotation["covercrop"][index_next] = True
                    project.tab_rotation["covercrop_id"][index_next] = 3

                    ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index_next] = False
                else:
                    # this triggeres a toggle, so nothing more is needed
                    project.tab_rotation["undersowing"][index] = False
                    ui_status.error["error"] = True
                    ui_status.error[
                        "errormessage"] = f"In the next year, covercrop as undersowing is not available. Therefore undersowing is unchecked."

                    # ui_status.dict_tab_rotation["xb_undersowing_checkeable"] = False
                    # ui_status.dict_tab_rotation["gb_undersowing_active"] = False
                    #
                    #     ui_status.error["error"] = True
                    #     ui_status.error["errormessage"] = f"In the next year, covercrop as undersowing is not available. Therefore undersowing is unchecked."
                    #     if True in project.rotation_temp["undersowing"][index]:
                    #         project.dict_tab_rotation["undersowing"][index] = True
                    #         ui_status.dict_tab_rotation["xb_undersowing_checkeable"] = False
                    #         ui_status.dict_tab_rotation["gb_undersowing_active"] = True

            # undersowing is False
            else:
                if project.rotation_temp["covercrop_ids"] == [3]:
                    ui_status.error["error"] = True
                    ui_status.error[
                        "errormessage"] = f"In the next year, covercrop as undersowing is not available. Therefore undersowing is unchecked."
                    #
                    # if True not in project.rotation_temp["undersowing"]:
                    #
                    # ui_status.dict_tab_rotation["gb_covercrop_active"][index_next] = False
                    #
                    #
                    #
                    # ui_status.dict_tab_rotation["xb_undersowing_checkeable"][index] = False
                    # ui_status.dict_tab_rotation["gb_undersowing_active"][index] = False
                    # ui_status.error["error"] = True
                    # ui_status.error["errormessage"] = f"""For the next crop in year {str((index + 2) % project.dict_tab_general['rotation_duration'])}, the
                    #                                     option of a covercrop as undersowing is not available, therefore undersowing is deselected."""

            return project, ui_status

        @staticmethod
        def get_status_covercrop_toggled(project, ui_status, index):
            print("Function LogicTabRotation.get_status_covercrop_toggled at index ", index)
            print(project.tab_rotation["crop_id"])
            rotation_duration = project.tab_general["rotation_duration"]
            index_previous = (index - 1) % rotation_duration

            if project.tab_rotation["covercrop"][index]:
                # the 2 impossible cases
                if len(project.rotation_temp["undersowing"][index_previous]) == 1 and (
                        (not project.tab_rotation["undersowing"][index_previous]
                         and project.rotation_temp["covercrop_ids"][index] == [3]) or \
                        (project.tab_rotation["undersowing"][index_previous] and \
                         3 not in project.rotation_temp["covercrop_ids"][index])):
                    ui_status.error["error"] = True
                    ui_status.error["errormessage"] = f"""For the crop in year {index + 1}, there is only the option of 
                    covercrop that has not been the undersowing of the previous crop. Therefore, the checkbox for undersowing 
                    of the previous year is unchecked and deactivated."""
                    project.tab_rotation["undersowing"][(index - 1) % rotation_duration] = False
                    ui_status.tab_rotation["xb_undersowing_checkeable"][(index - 1) % rotation_duration] = False
                    ui_status.tab_rotation["gb_undersowing_active"][(index - 1) % rotation_duration] = False

                    # removes selected crop from combobox
                    ui_status.error["remove_item"] = True

                # possible case but forced change in user's checkbox-selections: previous us is checked
                elif project.tab_rotation["undersowing"][index_previous] and \
                        3 not in project.rotation_temp["covercrop_ids"]:
                    # uncheck previious undersowing
                    project.tab_rotation["undersowing"][index_previous] = False
                    ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False
                    ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False

                    # set id for covercrop, defined by previous undersowing
                    project.tab_rotation["covercrop_id"][index] = 2
                    ui_status.error["error"] = True
                    ui_status.error[
                        "errormessage"] = f"""The undersowing for year {(index_previous + 1)} had to be unchecked."""

                # previous undersowing is not checked
                else:
                    if project.tab_rotation["undersowing"][index_previous] and \
                            3 in project.rotation_temp["covercrop_ids"]:
                        # covercrop is already uncheckeable from get_status_undersowing_toggled
                        project.tab_rotation["covercrop_id"] = 3
                    elif not project.tab_rotation["undersowing"][index_previous] and \
                            2 in project.rotation_temp["covercrop_ids"]:
                        project.tab_rotation["covercrop_id"] = 2


            else:
                project.tab_rotation["covercrop_id"][index] = 1

