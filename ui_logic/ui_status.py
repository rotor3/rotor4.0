class UiStatus:
    def __init__(self):
        self.load_project = True,
        self.new_project = True,
        self.tabs = {"tab_general_visible": True,
                     "tab_rotation_visible": True,
                     "tab_generator_visible": False,
                     "tab_organic_manure_visible": False,
                     "tab_yield_visible": False,
                     "tab_report_visible": False,
                     "tab_economy_visible": True}
        self.tabs_state = {"update_tabs": True,
                           "rotation_tab_name": "Intelligente Fruchtfolgeerstellung",
                           "visible_tabs_indices": [0, 1]}
        self.calculation_state = {"possible": False}

        self.settings = {"language": "de",
                         "changing_language": False}

        self.tab_general = {"gb_precipitation_winter_active": False}
                            #      "gb_precipitation_winter_selected": False,
                            #      "rb_rotation_intelligent_generation_checked": True,
                            #      "rb_rotation_free_genereration_checked": False,
                            #      "rb_rotation_assessment_checked": False,
                            # # "xb_manure_advanced_checked": False,
                            #      "rb_soil_index_checked": True,
                            #      "xb_red_area_checked": False,
                            # "gb_soil_parameters_advanced_checked": False}
        self.tab_rotation = {
            "qw_legumegras_visible": [True, True, True, True, True, True, True, True, True, True],
            "qf_covercrop_visible": [True, True, True, True, True, True, True, True, True, True],
            "xb_early_tillage_checkeable": [True, True, True, True, True, True, True, True, True, True],
            "xb_strawharvest_checkeable": [False, False, False, False, False, False, False, False, False,
                                           False],
            "xb_undersowing_checkeable": [False, False, False, False, False, False, False, False, False,
                                           False],
            "gb_undersowing_active": [True, True, True, True, True, True, True, True, True, True],
            "xb_covercrop_checkeable": [True, True, True, True, True, True, True, True, True, True],
            "gb_covercrop_active": [True, True, True, True, True, True, True, True, True, True],
            "cb_covercrop_winterhardiness_active": [True, True, True, True, True, True, True, True, True, True],

            "xb_covercrop_harvest_checkeable": [True, True, True, True, True, True, True, True, True, True],
            "undersowing_sync": [False, False, False, False, False, False, False, False, False, False],
            "covercrop_sync": [False, False, False, False, False, False, False, False, False, False],

            "qf_yield_adjustment_visible": [False, False, False, False, False, False, False, False,
                                            False, False],

            "qw_manure_visible": [[False, False, False, False], [False, False, False, False],
                                  [False, False, False, False], [False, False, False, False],
                                  [False, False, False, False], [False, False, False, False],
                                  [False, False, False, False], [False, False, False, False],
                                  [False, False, False, False], [False, False, False, False]],
            "cb_manure_source_visible": [[False, False, False, False], [False, False, False, False],
                                         [False, False, False, False], [False, False, False, False],
                                         [False, False, False, False], [False, False, False, False],
                                         [False, False, False, False], [False, False, False, False],
                                         [False, False, False, False], [False, False, False, False]],
            "cb_manure_specification_visible": [[False, False, False, False], [False, False, False, False],
                                                [False, False, False, False], [False, False, False, False],
                                                [False, False, False, False], [False, False, False, False],
                                                [False, False, False, False], [False, False, False, False],
                                                [False, False, False, False], [False, False, False, False]],

            "manure_unit_ton": [[True, True, True, True], [True, True, True, True], [True, True, True, True],
                            [True, True, True, True], [True, True, True, True], [True, True, True, True],
                            [True, True, True, True], [True, True, True, True], [True, True, True, True],
                            [True, True, True, True]]
        }

        self.tab_free_generation = {
            "gb_livestock_farming_checked": False,
            "qw_generate_crop_visible": [False, False, False, False, False, False, False, False, False, False],
            "new_recordset_for_crop": True}
        self.tab_manure = {
            "manure_visible": [False, False, False, False, False, False, False, False, False, False, False, False,
                               False, False, False, False, False, False, False, False],
            "manure_labels": None,
            "df_manure_labels":  None,
            "load_manures": True,
            "load_manure_values": [False, False, False, False, False, False, False, False,
                                       False, False, False, False, False, False, False, False, False, False,
                                       False, False],
            "manure_values_selected": [False, False, False, False, False, False, False, False,
                                       False, False, False, False, False, False, False, False, False, False,
                                       False, False]
        }


        # all caught errors are displayed in messageboxes
        self.error = {
            "error": False,
            "errormessage": "",
            "remove_item": [False, False, False, False, False, False, False, False, False, False]}
