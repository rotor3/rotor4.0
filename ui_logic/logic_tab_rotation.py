import copy

import pandas as pd
from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one, execute_query_get_dictionary_list
from project_handling.project import Project
from ui_logic.ui_status import UiStatus

"""
This class is for methods, that work in all assessment methods, intelligent_generation, assessment and free_generation.
Intelligent_generation and assessment share the same GUI!
"""

class LogicTabRotation:
    # ---------FILL COMBOBOXES, GET OPTIONS----------------------------------
    @staticmethod
    def generate_crop_recordset(project, ui_status, index):
        print("LogicTabRotation.generate_crop_recordset")
        """
        in these selections only the previous_crop_delivery and prev_crop_type are considered. all other restrictions
        considering covercrop-selections and undersowing-selections would restrict the selection in GUI too much.
        """
        print("----generate_cb_crop_recordset", index)
        print(project.tab_rotation["crop_id"])
        previous_crop_id = project.tab_rotation["crop_id"][
            (index - 1) % project.tab_general["rotation_duration"]]
        next_crop_id = project.tab_rotation["crop_id"][(index + 1) % project.tab_general["rotation_duration"]]
        current_crop = project.tab_rotation["crop_id"][index]
        # print("PREV CRop", str(previous_crop_id), "Current crop:", current_crop, "NEXT CROP", str(next_crop_id))
        # print("PREV EARLY B-O check 1: ", project.dict_tab_crop["early_tillage"])



        parameter_dict = {"language": ui_status.settings["language"], "next_crop_id": next_crop_id,
                          "previous_crop_id": previous_crop_id, "crop_type_id_where_clause_1": " WHERE crop_type_id IN (3, 4, 5) ",
                          "crop_type_id_where_clause_2": " AND crop_type_id IN (3, 4, 5) "}

        if index != 0 or project.tab_general["assessment_method"] == 3:
            parameter_dict["crop_type_id_where_clause_1"] = ""
            parameter_dict["crop_type_id_where_clause_2"] = ""


        if (previous_crop_id is None and next_crop_id is None) or project.tab_general["assessment_method"] == 3:
            sql_query = """SELECT DISTINCT t1.crop_id, t1.name_%(language)s FROM crops_tb  t1 INNER JOIN
                                 crop_production_activities_tb t2 ON t1.crop_id = t2.crop_id %(crop_type_id_where_clause_1)s
                                 ORDER BY name_%(language)s;""" % parameter_dict

        # this case cannot occur with the select button system
        elif previous_crop_id is None and next_crop_id > 0:
            sql_query = """SELECT t1.crop_id, t1.name_%(language)s FROM crops_tb t1 
                    INNER JOIN (SELECT DISTINCT t2.crop_id FROM crop_production_activities_tb t2 
                    INNER JOIN crop_production_activities_tb t3 
                    ON (t2.n_delivery = t3.previous_crop_n_delivery)
                    AND (SELECT ct.crop_type_id from crops_tb ct 
                    WHERE ct.crop_id = t2.crop_id)= t3.previous_crop_type_id 
                    WHERE t3.crop_id = %(next_crop_id)s) t4 ON t1.crop_id = t4.crop_id
                    WHERE (SELECT sowing_time_id FROM crops_tb WHERE crop_id = %(next_crop_id)s) >= t1.harvest_time_id
                    %(crop_type_id_where_clause_2)s 
                    ORDER BY name_%(language)s;""" % parameter_dict

        # TODO insert undersowing covercrop: if prev_crop_id undersowing is selected current must have covercrop = 3
        elif previous_crop_id > 0 and next_crop_id is None:

            sql_query = """SELECT a1.crop_id, a1.name_%(language)s FROM crops_tb  a1
                     INNER JOIN (SELECT DISTINCT t2.crop_id FROM crop_production_activities_tb t2 
                     INNER JOIN crop_production_activities_tb t3 
                     ON (t2.previous_crop_n_delivery = t3.n_delivery)
                     AND t2.previous_crop_type_id = (select ct.crop_type_id FROM crops_tb ct 
                     WHERE ct.crop_id = t3.crop_id)
                     WHERE t3.crop_id = %(previous_crop_id)s) t4 ON a1.crop_id = t4.crop_id
                     WHERE a1.sowing_time_id >= (select harvest_time_id from crops_tb WHERE crop_id = %(previous_crop_id)s) 
                     %(crop_type_id_where_clause_2)s 
                     ORDER BY name_%(language)s;""" % parameter_dict

        else:
            sql_query = """SELECT DISTINCT a1.crop_id, a1.name_%(language)s FROM crops_tb a1 
                    INNER JOIN (SELECT DISTINCT t1.crop_id, t1.n_delivery,
                    t1.previous_crop_n_delivery, 
                    t1.undersowing
                    FROM (SELECT DISTINCT t2.crop_id, t2.n_delivery, 
                    t2.previous_crop_n_delivery, t2.undersowing
                    FROM crop_production_activities_tb t2 INNER JOIN crop_production_activities_tb t3 
                    ON (t2.previous_crop_n_delivery = t3.n_delivery)
                    AND t2.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct WHERE ct.crop_id = t3.crop_id)
                    WHERE t3.crop_id = %(previous_crop_id)s) t1 INNER JOIN crop_production_activities_tb p2 
                    ON t1.n_delivery = p2.previous_crop_n_delivery
                    AND (SELECT ct.crop_type_id from crops_tb ct 
                    WHERE ct.crop_id = t1.crop_id)= p2.previous_crop_type_id 
                    WHERE p2.crop_id = %(next_crop_id)s) prevnext ON a1.crop_id = prevnext.crop_id 
                    WHERE sowing_time_id >= (SELECT harvest_time_id FROM crops_tb WHERE crop_id = %(previous_crop_id)s)
                    AND (SELECT sowing_time_id FROM crops_tb 
                    WHERE crop_id = %(next_crop_id)s) >= harvest_time_id %(crop_type_id_where_clause_2)s 
                    ORDER BY name_%(language)s""" % parameter_dict


        recordset = execute_query_get_recordset(sql_query)

        # the recordset needs to be saved for later comparison
        project.rotation_temp["cb_crop_recordsets"][index] = recordset
        return project

    @staticmethod
    def get_data_winterhardiness(project, ui_status):
        print("LogicTabRotation.get_data_winterhardiness")
        sql_query = "SELECT winterhardiness_value, name_%(language)s FROM winterhardiness_values_tb" \
                    % {"language": ui_status.settings["language"]}

        recordset = execute_query_get_recordset(sql_query)
        project.all_tabs_combobox_items["cb_winterhardiness"] = recordset

        return project


    @staticmethod
    def get_sb_manure_amount_unit(project, ui_status, index, index_2):
        print("LogicTabRotation.get_sb_manure_amount_unit")
        manure_type_id = project.tab_rotation["manure_type"][index][index_2]
        if manure_type_id:
            sql_query = """SELECT manure_unit_ton FROM manure_type_tb WHERE manure_type_id = %(manure_type_id)s""" % {"manure_type_id": manure_type_id}

            manure_unit_ton = execute_query_get_one(sql_query)[0]
            ui_status.tab_rotation["manure_unit_ton"][index][index_2] = manure_unit_ton

        return project, ui_status

    # -------------------------MANURE -------------------------------
    @staticmethod
    def bt_manure_delete(project, ui_status, index, index_2):
        ui_status.tab_rotation["qw_manure_visible"][index].pop(index_2)
        ui_status.tab_rotation["qw_manure_visible"][index].append(False)
        project.tab_rotation["manure_used"][index].pop(index_2)
        project.tab_rotation["manure_used"][index].append(False)
        print("List after pop and append", ui_status.tab_rotation["qw_manure_visible"][index])

        project.tab_rotation["m_n_manures_id"][index].pop(index_2)
        project.tab_rotation["m_n_manures_id"][index].append(0)
        project.tab_rotation["manure_type"][index].pop(index_2)
        project.tab_rotation["manure_type"][index].append(0)
        project.tab_rotation["manure_specification"][index].pop(index_2)
        project.tab_rotation["manure_specification"][index].append(0)
        project.tab_rotation["manure_amount"][index].pop(index_2)
        project.tab_rotation["manure_amount"][index].append(0)

        # TODO what's the difference? merge these two, clean up ui_status and project.tab_manure items

        if True not in project.tab_rotation["manure_used"][index]:
            project.tab_rotation["manure"][index] = False

        return project, ui_status

    @staticmethod
    def get_cb_manure_type(project, ui_status):
        print("LogicTabRotation.get_cb_manure_type")
        sql_query = """SELECT mt.manure_type_id, mt.name_%(language)s FROM manure_type_tb mt JOIN 
        m_n_production_systems_manure_types_tb mnps ON mt.manure_type_id = mnps.manure_type_id WHERE 
        production_system_id = %(production_system_id)s ORDER BY name_%(language)s""" % {"language": ui_status.settings["language"], "production_system_id": project.tab_general["production_system_id"]}
        # recordset = db.m_n_production_systems_manure_types_tb.read
        recordset = execute_query_get_recordset(sql_query)
        project.all_tabs_combobox_items["cb_manure_type"] = recordset
        print("Recordset manure_type: ", project.all_tabs_combobox_items["cb_manure_type"])
        return project

    @staticmethod
    def get_cb_manure_source(project, ui_status, index, index_2):
        print("LogicTabRotation.get_cb_manure_source")

        manure_type_id = project.tab_rotation["manure_type"][index][index_2]
        if manure_type_id:
            sql_query = """SELECT DISTINCT t1.manure_source_id, t1.name_%(language)s FROM (SELECT mst.manure_source_id, 
                             mst.name_%(language)s FROM (manure_source_tb mst JOIN m_n_manures_tb mnt ON 
                             mst.manure_source_id = mnt.manure_source_id) WHERE mnt.manure_type_id = %(manure_type_id)s) t1 
                             ORDER BY name_%(language)s""" % {"language": ui_status.settings["language"],
                                                             "manure_type_id": manure_type_id}

            recordset = execute_query_get_recordset(sql_query)
            project.all_tabs_combobox_items["cb_manure_source"][index][index_2] = recordset
            print("GET_CB_MANURE_SOURCE Recordset", recordset)

            # Visibility of source combobox:
            if recordset[0][0] is not 0:
                ui_status.tab_rotation["cb_manure_source_visible"][index][index_2] = True
            else:
                ui_status.tab_rotation["cb_manure_source_visible"][index][index_2] = False



        return project, ui_status

    @staticmethod
    def get_cb_manure_specification(project, ui_status, index, index_2):
        print("LogicTabRotation.get_cb_manure_specification")

        manure_type_id = project.tab_rotation["manure_type"][index][index_2]
        if manure_type_id:
            sql_query = """SELECT DISTINCT t1.manure_specification_id, t1.name_%(language)s FROM (SELECT mst.manure_specification_id, 
                                    mst.name_%(language)s FROM (manure_specification_tb mst JOIN m_n_manures_tb mnt ON 
                                    mst.manure_specification_id = mnt.manure_specification_id) WHERE mnt.manure_type_id = %(manure_type_id)s) t1
                                    ORDER BY name_%(language)s""" % {"language": ui_status.settings["language"], "manure_type_id": manure_type_id}
            recordset = execute_query_get_recordset(sql_query)
            project.all_tabs_combobox_items["cb_manure_specification"][index][index_2] = recordset

            if recordset[0][0] is not 0:
                ui_status.tab_rotation["cb_manure_specification_visible"][index][index_2] = True
            else:
                ui_status.tab_rotation["cb_manure_specification_visible"][index][index_2] = False

        return project, ui_status

    @staticmethod
    def get_m_n_manure_id(project, ui_status, index, index_2):
        print("LogicTabRotation.get_m_n_manure_id index",  " manure_used", project.tab_rotation["manure_used"])
        # for index in range(0, project.tab_general["rotation_duration"]):
        #     for index_2 in range(0, 4):

        if project.tab_rotation["manure_used"][index][index_2]:
            parameter_dict = {"manure_type_id": project.tab_rotation["manure_type"][index][index_2],
                            "manure_source_id": project.tab_rotation["manure_source"][index][index_2],
                            "manure_specification_id": project.tab_rotation["manure_specification"][index][index_2]}

            sql_query = """SELECT m_n_manures_id FROM m_n_manures_tb WHERE manure_type_id = %(manure_type_id)s AND
            manure_source_id = %(manure_source_id)s AND manure_specification_id = %(manure_specification_id)s"""\
            %parameter_dict
            try:
                m_n_manure_id = execute_query_get_one(sql_query)[0]
                print("m_n_manure_id", m_n_manure_id)

                project.tab_rotation["m_n_manures_id"][index][index_2] = m_n_manure_id
                print("m_n_manure_id: ", project.tab_rotation["m_n_manures_id"][index][index_2])
            except:
                pass


        return project, ui_status

    @staticmethod
    def get_manure_list(project, ui_status):
        print("LogicTabRotation.get_manure_list")
        set_of_all_manure_ids = set()
        print("Set1: ", set_of_all_manure_ids)
        for index in range(0, project.tab_general["rotation_duration"]):

            for index_2 in range(0, 4):
                #print("indices", index, index_2)
                if project.tab_rotation["manure_used"][index][index_2] and project.tab_rotation["m_n_manures_id"][index][index_2] != 0:

                    set_of_all_manure_ids.add(project.tab_rotation["m_n_manures_id"][index][index_2])
                    print("Set2: ", set_of_all_manure_ids)

        list_of_all_manure_ids = list(set_of_all_manure_ids)
        print("List of all manure ids: ", list_of_all_manure_ids)

        return project, ui_status

    @staticmethod
    def gb_manure_selected(project, ui_status, index):
        print("LogicTabRotation.gb_manure_selected")
        ui_status.tab_rotation["qw_manure_visible"][index][0] = project.tab_rotation["manure"][index]

        project.tab_rotation["manure_used"][index][0] = project.tab_rotation["manure"][index]
        print("project.tab_rotation['manure_used']", project.tab_rotation["manure_used"])
        if project.tab_rotation["manure_used"][index][0]:
            project, ui_status = LogicTabRotation.get_m_n_manure_id(project, ui_status, index, 0)


        # if all gb_manures are unchecked, the manure tab gets hidden
        if True not in project.tab_rotation["manure"]:
            ui_status.tabs["tab_organic_manure_visible"] = False
        else:
            ui_status.tabs["tab_organic_manure_visible"] = True


        return project, ui_status

    #-----------------tillage---------------------------------
    @staticmethod
    def get_cb_tillage(project, ui_status):
        sql_query = "SELECT tillage_id, name_%(language)s FROM tillage_tb" % {
            "language": ui_status.settings["language"]}
        recordset = execute_query_get_recordset(sql_query)
        project.all_tabs_combobox_items["cb_tillage"] = recordset

        return project


    #--------------UI STATUS-----------------------------------

    @staticmethod
    def get_possible_undersowing_covercrop(project, ui_status, index):
        """all possible undersowings of CPAs and covercrops of crop are stored in project.rotation_temp.
        Everything, that is already determined by the choices available - only concerning the crop selected - are set
        here.Everything concerning the undersowing before, determined by rhe covercrop, or the covercrop after this
        crop, determined by the undersowing of this crop,is done in get_status_covercrop_toggled
        and get_status_undersowing_toggled.
        Only early_tillage is completely set here."""

        print("LogicTabRotation.get_possible_undersowing_covercrop")
        # print("entering LogicTabRotation.get_possible_undersowing_covercrop at index ", index)
        # print(project.dict_tab_rotation["crop_id"])
        # print("xb_covercrop_checkeable", ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", ui_status.dict_tab_rotation["gb_covercrop_active"])

        index_previous = (index - 1) % project.tab_general["rotation_duration"]
        index_next = (index + 1) % project.tab_general["rotation_duration"]
        crop_id = project.tab_rotation["crop_id"][index]

        parameter_dict = {"crop_id": crop_id}

        sql_query_crop_type = """SELECT crop_type_id FROM crops_tb WHERE crop_id = %(crop_id)s""" % parameter_dict

        sql_query_undersowing = """SELECT DISTINCT undersowing FROM crop_production_activities_tb WHERE crop_id = %(crop_id)s""" \
                                % parameter_dict

        sql_query_covercrop = """SELECT covercrop_id FROM m_n_crops_covercrop_tb WHERE crop_id = %(crop_id)s""" \
                                % parameter_dict

        recordset_undersowing = execute_query_get_recordset(sql_query_undersowing)
        recordset_covercrop = execute_query_get_recordset(sql_query_covercrop)
        project.tab_rotation["crop_type_id"][index] = execute_query_get_one(sql_query_crop_type)[0]

        # ---------------------------storing all options in rotation_temp---------------------------
        project.rotation_temp["undersowing"][index] = []
        project.rotation_temp["covercrop_ids"][index] = []

        for undersowing in recordset_undersowing:
            project.rotation_temp["undersowing"][index].append(undersowing[0])
        for covercrop in recordset_covercrop:
            project.rotation_temp["covercrop_ids"][index].append(covercrop[0])

        project.rotation_temp["covercrop_ids"][index].sort()

        #-------------------------------------Legumegras-------------------------------------------


        # Legumegras can only be defined if the crop is a legumegras, duh!
        if project.tab_rotation["crop_type_id"][index] == 4:
            ui_status.tab_rotation["qw_legumegras_visible"][index] = True
            ui_status.tab_rotation["qf_covercrop_visible"][index] = False
        else:
            ui_status.tab_rotation["qw_legumegras_visible"][index] = False
            ui_status.tab_rotation["qf_covercrop_visible"][index] = True

            # ---early tillage: legume gras followed by winter rape or winter barley need to be terminated early ----
        # TODO hardcoded crop_ids!
        if crop_id == 8 or crop_id == 33:
            if project.tab_rotation["crop_type_id"][index_previous] == 4:
                project.tab_rotation["early_tillage"][index_previous] = True
                ui_status.tab_rotation["xb_early_tillage_checkeable"][index_previous] = False
        elif project.tab_rotation["crop_type_id"][index] == 4:
            if project.tab_rotation["crop_id"][index_next] in [8, 33]:
                project.tab_rotation["early_tillage"][index] = True
                ui_status.tab_rotation["xb_early_tillage_checkeable"][index] = False
            else:
                project.tab_rotation["early_tillage"][index] = False
                ui_status.tab_rotation["xb_early_tillage_checkeable"][index] = True
        else:
            project.tab_rotation["early_tillage"][index] = False
            ui_status.tab_rotation["xb_early_tillage_checkeable"][index] = False


        # -------------------------------------strawharvest-----------------------------------------------------
        # ----strawharvest is only available with cereals----------------------------------
        if project.tab_rotation["crop_type_id"][index] == 1:
            ui_status.tab_rotation["xb_strawharvest_checkeable"][index] = True
        else:
            ui_status.tab_rotation["xb_strawharvest_checkeable"][index] = False

        # ----------------------------undersowing and covercrop----------------------------------------
        # undersowing and covercrop temporary lists

        print("Possible undersowing", project.rotation_temp["undersowing"][index])
        print("Possible covervrop_ids", project.rotation_temp["covercrop_ids"][index])

        # ---------------create ui_status for SETTING UP items in year's selection_____________
        # ------------------------UNDERSOWING Status---------------------------------------------
        # print("get_possible.... undersowing")
        #
        # print("xb_covercrop_checkeable", ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", ui_status.dict_tab_rotation["gb_covercrop_active"])
        #TODO from here...
        if project.rotation_temp["undersowing"][index] == [True]:
            print("Check XX 100")
            project.tab_rotation["undersowing"][index] = True
            ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
            ui_status.tab_rotation["gb_undersowing_active"][index] = True

            ui_status.tab_rotation["sync_undersowing"][index] = True


        elif project.rotation_temp["undersowing"][index] == [False]:
            print("CHECK X1")
            project.tab_rotation["undersowing"][index] = False
            ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
            ui_status.tab_rotation["gb_undersowing_active"][index] = False

            ui_status.tab_rotation["undersowing_sync"][index] = False

        else:
            # no status for sync is set to not interfere with previously made choices
            print("CHECK X2 index", index)
            # project.dict_tab_rotation["undersowing"][index] = False
            ui_status.tab_rotation["xb_undersowing_checkeable"][index] = True
            ui_status.tab_rotation["gb_undersowing_active"][index] = True




        # ---------------COVERCROP--------------------------------
        # print("get_possible.... covercrop")
        #
        # print("xb_covercrop_checkeable", ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", ui_status.dict_tab_rotation["gb_covercrop_active"])
        if 3 in project.rotation_temp["covercrop_ids"][index] and True in project.rotation_temp["undersowing"][index_previous]:
            ui_status.tab_rotation["gb_undersowing_active"][index_previous] = True
            ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = True


        if len(project.rotation_temp["covercrop_ids"][index]) == 1:
            print("CHECK X3")
            ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
            project.tab_rotation["covercrop_id"][index] = project.rotation_temp["covercrop_ids"][index][0]

            print("get_possible.... covercrop vor if len(,,,")
            print("xb_covercrop_checkeable", ui_status.tab_rotation["xb_covercrop_checkeable"])
            print("gb_covercrop_active", ui_status.tab_rotation["gb_covercrop_active"])

            if project.rotation_temp["covercrop_ids"][index] == [1]:
                print("CHECK X4")
                print("get_possible.... ids = [1]")

                print("xb_covercrop_checkeable", ui_status.tab_rotation["xb_covercrop_checkeable"])
                print("gb_covercrop_active", ui_status.tab_rotation["gb_covercrop_active"])
                project.tab_rotation["covercrop"][index] = False
                project.tab_rotation["covercrop_id"][index] = 1
                ui_status.tab_rotation["gb_covercrop_active"][index] = False
                ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = False
                ui_status.tab_rotation["covercrop_sync"][index] = False

                project.tab_rotation["undersowing"][index_previous] = False
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False




            elif project.rotation_temp["covercrop_ids"][index] == [2]:
                print("get_possible.... covercrop ids = [2]")

                print("xb_covercrop_checkeable", ui_status.tab_rotation["xb_covercrop_checkeable"])
                print("gb_covercrop_active", ui_status.tab_rotation["gb_covercrop_active"])
                print("CHECK X5")
                project.tab_rotation["covercrop"][index] = True
                project.tab_rotation["covercrop_id"][index] = 2
                ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                ui_status.tab_rotation["gb_covercrop_active"][index] = True

                ui_status.tab_rotation["covercrop_sync"][index] = False

                project.tab_rotation["undersowing"][index_previous] = False
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False



            else: # covercrop = 3
                print("get_possible.... covercrop ids = [3]")

                print("xb_covercrop_checkeable", ui_status.tab_rotation["xb_covercrop_checkeable"])
                print("gb_covercrop_active", ui_status.tab_rotation["gb_covercrop_active"])
                print("CHECK X6")
                project.tab_rotation["covercrop"][index] = True
                project.tab_rotation["covercrop_id"][index] = 3
                ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                ui_status.tab_rotation["gb_covercrop_active"][index] = True
                ui_status.tab_rotation["covercrop_sync"][index] = True

                project.tab_rotation["undersowing"][index_previous] = True
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False



            print("get_possible.... covercrop ENDE")

            print("xb_covercrop_checkeable", ui_status.tab_rotation["xb_covercrop_checkeable"])
            print("gb_covercrop_active", ui_status.tab_rotation["gb_covercrop_active"])

        else:
            # if undersowing in the previous year has been selected, the covercrop is already set to  3
            if project.tab_rotation["covercrop_id"][index] == 3:
                if 3 in project.rotation_temp["covercrop_ids"][index]:

                    ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                    # ui_status.tab_rotation["gb_covercrop_active"][index] = False
                else:
                    if False in project.rotation_temp["undersowing"][index_previous]:
                        project.tab_rotation["undersowing"][index_previous] = False
                        # TODO maybe "if 1/2 in covercrop_ids?
                        project.tab_rotation["covercrop_id"][index] = 1

                        ui_status.error["error"] = True
                        ui_status.error[
                            "errormessage"] = f"1 Undersowing in year {(index_previous + 1)} had to be unchecked. There is no undersowing as covercrop available."
                    else:
                        ui_status.error["error"] = True
                        ui_status.error[
                            "errormessage"] = f"2 There is no possible combination with undersowing of year {(index_previous + 1)}. Please select a different crop!"
                        ui_status.error["remove_item"][index] = True
            else:
                ui_status.tab_rotation["xb_covercrop_checkeable"][index] = True





        print(ui_status.tab_rotation)

        return project, ui_status


    @staticmethod
    def get_ui_status_undersowing_toggled(project, ui_status, index):
        print("LogicTabRotation.get_ui_status_undersowing_toggled")
        """When undersowing is True, Covercrop as undersowing in the next year MUST be available OR The next selection is still None.
        Otherwise undersowing must be false. The undersowing checkbox is not deactivated though. """
        index_next = (index + 1) % project.tab_general["rotation_duration"]
        if project.tab_rotation["undersowing"][index]:
            # an undersowing has to be winterhardy, so the next covercrop's winterhardiness is set to "high"
            project.tab_rotation["covercrop_winterhardiness"][index_next] = 0
            ui_status.tab_rotation["cb_covercrop_winterhardiness_active"][index_next] = False

            if 3 in project.rotation_temp["covercrop_ids"][index_next] \
                    or project.rotation_temp["covercrop_ids"][index_next] == []:
                print("undersowing toggled CHECKPOINT 0")
                ui_status.tab_rotation["undersowing_sync"][index] = True

                project.tab_rotation["covercrop"][index_next] = True
                ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = False

                project.tab_rotation["covercrop_id"][index_next] = 3
                ui_status.tab_rotation["covercrop_sync"][index_next] = True

            elif 3 not in project.rotation_temp["covercrop_ids"][index_next] and project.rotation_temp["covercrop_ids"][index_next] != []:
                project.tab_rotation["undersowing"][index] = False
                ui_status.tab_rotation["gb_undersowing_active"][index] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
                ui_status.error["error"] = True
                ui_status.error[
                    "errormessage"] = f"""Undersowing is unavailable due to the covercrop of year {(index_next + 1)}."""
            else:
                project.tab_rotation["covercrop"][index_next] = True
                ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = False

        else: # if undersowing is false
            ui_status.tab_rotation["undersowing_sync"][index] = False
            ui_status.tab_rotation["covercrop_sync"][index_next] = False
            if project.tab_rotation["covercrop"][index_next] is True:
                if project.rotation_temp["covercrop_ids"][index_next] == []:
                    project.tab_rotation["covercrop"][index_next] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index_next] = True
                    ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = True
                elif 2 in project.rotation_temp["covercrop_ids"][index_next]:
                    project.tab_rotation["covercrop_id"][index_next] = 2
                    ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = True
                    ui_status.tab_rotation["gb_covercrop_active"][index_next] = True
                elif 1 in project.rotation_temp["covercrop_ids"][index_next]:
                    project.tab_rotation["covercrop_id"][index_next] = 1
                    project.tab_rotation["covercrop"][index_next] = False

            ui_status.tab_rotation["cb_covercrop_winterhardiness_active"][index_next] = True

            # if 2 not in project.rotation_temp["covercrop_ids"][index_next] and project.rotation_temp["covercrop_ids"][index_next] != []:
            #     pass



        return project, ui_status

    @staticmethod
    def get_crop_methods_from_hay(project, index):
        project.tab_rotation["silage"][index] = 100 - project.tab_rotation["hay"][index] - project.tab_rotation["green_fodder"][index]
        return project

    @staticmethod
    def get_crop_methods_from_silage(project, index):
        project.tab_rotation["green_fodder"][index] = 100 - project.tab_rotation["silage"][index] - project.tab_rotation["hay"][index]
        return project

    @staticmethod
    def get_crop_methods_from_green_fodder(project, index):
        project.tab_rotation["hay"][index] = 100 - project.tab_rotation["green_fodder"][index] - project.tab_rotation["silage"][index]
        return project

    @staticmethod
    def get_status_covercrop_toggled(project, ui_status, index):
        print("Function LogicTabRotation.get_status_covercrop_toggled at index ", index)
        print(project.tab_rotation["crop_id"])
        rotation_duration = project.tab_general["rotation_duration"]
        index_previous = (index - 1) % rotation_duration

        # ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = project.tab_rotation["covercrop"][index]


        if project.tab_rotation["covercrop"][index]:
            # the 2 impossible cases
            # if there is only one option for the previous undersowing (false) and current covercrop can only be covercrop as undersowing
            # or the opposite, previous undersowing is true and undersowing as covercrop is not available
            # if len(project.rotation_temp["undersowing"][index_previous]) == 1 and \
            #         ((not project.tab_rotation["undersowing"][index_previous] \
            #         and project.rotation_temp["covercrop_ids"][index] == [3]) or \
            #         (project.tab_rotation["undersowing"][index_previous] and \
            #          3 not in project.rotation_temp["covercrop_ids"][index])):
            if project.rotation_temp["undersowing"][index_previous] == [True] and 3 not in project.rotation_temp["covercrop_ids"][index]:
                ui_status.error["error"] = True
                ui_status.error["errormessage"] = f"""6 For the crop in year {index + 1}, there is only the option of
                covercrop that has not been the undersowing of the previous crop. Therefore, the checkbox for undersowing
                of the previous year is unchecked and deactivated."""
                project.tab_rotation["undersowing"][index_previous] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False

                # removes selected crop from combobox
                ui_status.error["remove_item"][index] = True

            # possible case but forced change in user's checkbox-selections: previous us is checked
            elif project.tab_rotation["undersowing"][index_previous] and \
                3 not in project.rotation_temp["covercrop_ids"][index] and project.rotation_temp["covercrop_ids"][index] != []:
                # uncheck previous undersowing
                project.tab_rotation["undersowing"][index_previous] = False
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False

                #set id for covercrop, defined by previous undersowing
                project.tab_rotation["covercrop_id"][index] = 2
                ui_status.error["error"] = True
                ui_status.error["errormessage"] = f"""7 The undersowing for year {(index_previous + 1)} had to be unchecked."""

            # previous undersowing is not checked
            else:
                if project.tab_rotation["undersowing"][index_previous] and \
                        (3 in project.rotation_temp["covercrop_ids"] or project.rotation_temp["covercrop_ids"] == []):
                    # covercrop is already uncheckeable from get_status_undersowing_toggled
                    project.tab_rotation["covercrop_id"] = 3

                    ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
                    ui_status.tab_rotation["gb_covercrop_active"][index] = True
                    # ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = True


                elif not project.tab_rotation["undersowing"][index_previous] and \
                2 in project.rotation_temp["covercrop_ids"]:
                    project.tab_rotation["covercrop_id"] = 2

            # # if undersowing in the previous year is selected, covercrop must stay checked and be uncheckeable
            # if project.tab_rotation["undersowing"][index_previous]:
            #     ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
            #     ui_status.tab_rotation["gb_covercrop_active"][index] = True
            #     ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = True
            #
            #     project.tab_rotation["covercrop"][index] = True



        else:
            # covercrop is not possible as [1,2], only as [1, 2, 3] or [1, 3] or [1], [2] or [3]
            if project.tab_rotation["undersowing"][index_previous] and False in project.rotation_temp["undersowing"][index_previous]:
                project.tab_rotation["undersowing"][index_previous] = False
                ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous] = False
                ui_status.tab_rotation["gb_undersowing_active"][index_previous] = False

                project.tab_rotation["covercrop_id"][index] = 1
                project.tab_rotation["covercrop_harvest"][index] = False

            # else:
            #     ui_status.error["error"] = True
            #     ui_status.error[
            #         "errormessage"] = f"""7 The covercrop for year {(index + 1)} had to be unchecked."""



        return project, ui_status

    # this function deals with everything for ONLY the year of index
    # TODO THIS FUNCTION IS OBSOLETE
    @staticmethod
    def get_ui_status_crop_year(project, ui_status, index):
        print("Entering LogicTabRotation.get_status_undersowing_toggled at index ", index)
        print(project.tab_rotation["crop_id"])

        index_next = (index + 1) % project.tab_general["rotation_duration"]
        index_previous = (index - 1) % project.tab_general["rotation_duration"]

        # if undersowing and the next crop is already selected, its covercrop must be 3
        if project.tab_rotation["undersowing"][index]:
            print("undersowing toggled CHECKPOINT -1")
            if 3 in project.rotation_temp["covercrop_ids"][index_next] \
                    or project.rotation_temp["covercrop_ids"][index_next] == []:
                print("undersowing toggled CHECKPOINT 0")
                ui_status.tab_rotation["undersowing_sync"][index] = True

                project.tab_rotation["covercrop"][index_next] = True
                ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = False
                project.tab_rotation["covercrop_id"][index_next] = 3
                ui_status.tab_rotation["covercrop_sync"][index_next] = True

            else:
                # this triggeres a toggle, so nothing more is needed
                print("undersowing toggled CHECKPOINT 1")
                project.tab_rotation["undersowing"][index] = False
                ui_status.tab_rotation["covercrop_sync"][index_next] = False
                ui_status.tab_rotation["undersowing_sync"][index] = False
                ui_status.error["error"] = True
                ui_status.error["errormessage"] = f"3 In year {(index_next + 1)}, covercrop as undersowing is not available. Therefore undersowing is unchecked."



        # undersowing is False
        else:
            print("undersowing toggled CHECKPOINT 2")
            if 1 not in project.rotation_temp["covercrop_ids"][index_next] and project.rotation_temp["covercrop_ids"][index_next] != []:
                print("undersowing toggled CHECKPOINT 3")
                if True in project.rotation_temp["undersowing"][index]:
                    project.tab_rotation["undersowing"][index] = True
                    ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False
                    ui_status.error["error"] = True
                    ui_status.error["errormessage"] = \
                        f"5 In the next year, a covercrop is required. Therefore undersowing is checked."
                else:
                    ui_status.error["error"] = True
                    ui_status.error["errormessage"] = \
                        f"5 In the next year, a covercrop is required. This year does not allow for undersowing. Therefore, the crop is removed from the selection."
                    ui_status.error["remove_item"][index] = True
            else:
                ui_status.tab_rotation["xb_covercrop_checkeable"][index_next] = True

        # if undersowing in the previous year is selected, covercrop must stay checked and be uncheckeable
        # resetting checkboxes
        if project.tab_rotation["undersowing"][index_previous] == False:
            ui_status.tab_rotation["xb_covercrop_checkeable"][index] = True
            ui_status.tab_rotation["gb_covercrop_active"][index] = True
            # ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = True

        else:
            ui_status.tab_rotation["xb_covercrop_checkeable"][index] = False
            ui_status.tab_rotation["gb_covercrop_active"][index] = True
            # ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = True

            project.tab_rotation["covercrop"][index] = True

        print("undersowing temp previous:", project.rotation_temp["undersowing"][index_previous] )
        if True in project.rotation_temp["undersowing"][index_previous]: # and 3 in project.rotation_temp["covercrop_ids"][index]:
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            ui_status.tab_rotation["gb_undersowing_active"][index_previous] = True
            if project.rotation_temp["undersowing"][index_previous] == [True]:
                print("222222222222!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                ui_status.tab_rotation["xb_undersowing_checkeable"][index] = False


        return project, ui_status

    # if the crop is none: this is a reset of all undersowig and covercrop related data and states
    @staticmethod
    def get_ui_status_of_none(project, ui_status, index):
        print("LogicTabRotation.get_ui_status_of_none")
        index_previous = (index -1) % project.tab_general["rotation_duration"]
        index_next = (index + 1) % project.tab_general["rotation_duration"]

        project.rotation_temp["covercrop_ids"][index] = []
        project.rotation_temp["undersowing"][index] = []

        # if project.rotation_temp["covercrop_ids"][index_next] == [3]:
        #     project.tab_rotation["undersowing"][index] = True
        # else:
        #     project.tab_rotation["undersowing"][index] = False

        ui_status.tab_rotation["xb_undersowing_checkeable"][index] = True
        ui_status.tab_rotation["gb_undersowing_active"][index] = True

        ui_status.tab_rotation["xb_covercrop_checkeable"][index] = True
        ui_status.tab_rotation["qf_covercrop_visible"][index] = True
        ui_status.tab_rotation["gb_covercrop_active"][index] = True
        # ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index] = True

        # if project.tab_rotation["undersowing"][index_previous]:
        project.tab_rotation["covercrop"][index] = project.tab_rotation["undersowing"][index_previous]
        ui_status.tab_rotation["xb_covercrop_checkeable"][index] = not project.tab_rotation["undersowing"][index_previous]


        return project, ui_status

    @staticmethod
    def reset_values_for_remove_item(project, ui_status, index):
        print("entering LogicTabRotation.reset_values_for_remove_item")
        temp_ui_status = UiStatus()
        temp_project = Project()
        # teh recordsets foe the comboboxes need to stay the same, otherwise the removed item would be replaced
        temp_project.rotation_temp["cb_crop_recordsets"] = copy.copy(project.rotation_temp["cb_crop_recordsets"])

        # reset values of removed item to default
        for key in temp_project.tab_rotation:
            print(key)
            print(project.tab_rotation[key], temp_project.tab_rotation[key])
            project.tab_rotation[key][index] = temp_project.tab_rotation[key][index]
        for key in temp_project.rotation_temp:
            project.rotation_temp[key][index] = temp_project.rotation_temp[key][index]

        for key in temp_ui_status.tab_rotation:
            ui_status.tab_rotation[key][index] = temp_ui_status.tab_rotation[key][index]

        return project, ui_status
