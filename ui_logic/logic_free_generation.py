import copy

from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one
from project_handling.project import Project
from ui_logic.ui_status import UiStatus

"""
This class is for methods, that work only in the assessment_method free_generation.
Intelligent_generation and assessment share the same GUI! Free_generation has its own.
"""

class LogicFreeRotationGeneration:

    # recordsets for the selection boxes
    @staticmethod
    def get_recordset_cb_crops(project, ui_status, index):
        print("entering LogicFreeGeneration.get_recordset_cb_crops")
        crop_type_id = project.tab_free_generation["crop_type_selection"][index]
        print("Crop type selectiion ", project.tab_free_generation["crop_type_selection"][index])
        parameter_dict = {"language": ui_status.settings["language"], "where_clause": ""}

        if crop_type_id in [1, 2, 3, 4]:
            where_clause = f" WHERE t1.crop_type_id = {crop_type_id}"
        else:
            where_clause = ""

            # for the first crop combobox only legumes are selectable
            if index == 0:
                where_clause = "WHERE t1.crop_type_id IN (3, 4)"

        parameter_dict["where_clause"] = where_clause

        sql_query = """SELECT DISTINCT t1.crop_id, t1.name_%(language)s FROM crops_tb  t1 INNER JOIN
                                                   crop_production_activities_tb t2 ON t1.crop_id = t2.crop_id 
                                                   %(where_clause)s ORDER BY name_%(language)s;""" % parameter_dict

        recordset = execute_query_get_recordset(sql_query)
        print("recordset", recordset)

        project.free_generation_temp["cb_crop_recordsets"][index] = recordset

            # ui_status.dict_tab_free_generation["new_recordset_for_crop"] = False


        return project

    @staticmethod
    def get_recordset_cb_crop_types(project, ui_status, index):
        print("entering LogicFreeGeneration.get_recordset_cb_crop_types")
        if index == 0:
            sql_query = """SELECT crop_type_id, name_%(language)s FROM crop_type_tb  
                                               WHERE crop_type_id IN (0, 3, 4) ORDER BY crop_type_id;""" \
                    % {"language": ui_status.settings["language"]}
        else:
            # crop_type_id < 5 necessary because of the expansion of the database table crop_type_tb
            sql_query = """SELECT crop_type_id, name_%(language)s FROM crop_type_tb WHERE crop_type_id < 5 ORDER BY crop_type_id;""" % {"language": ui_status.settings["language"]}

        recordset = execute_query_get_recordset(sql_query)

        project.free_generation_temp["cb_crop_type_recordsets"][index] = recordset
        print("CROP TYPE RECORDSETS", project.free_generation_temp["cb_crop_type_recordsets"][index])


        return project

    @staticmethod
    def crop_selected(project, index):
        print("entering LogicFreeGeneration.crop_selected")
        # not None is needed, because on the initializing the UI the ID is None
        if project.tab_free_generation["crop_id"][index] is not None:
            crop_id = project.tab_free_generation["crop_id"][index]
            sql_query = "SELECT crop_type_id FROM crops_tb WHERE crop_id = %(crop_id)s" % {"crop_id": crop_id}

            crop_type_id = execute_query_get_one(sql_query)[0]

            project.tab_free_generation["crop_type_id"][index] = crop_type_id

        return project


    @staticmethod
    def crop_type_selected(project, ui_status, index):
        print("entering LogicFreeGeneration.crop_type_selected")
        project.tab_free_generation["crop_type_selection"][index]

        if project.tab_free_generation["crop_type_id"][index] != 0:
            ui_status.tab_free_generation["new_recordset_for_crop"] = True

            pass
        # this is a workaround to not have to change anything in intelligent generation, but:
        else:
            project.tab_free_generation["crop_type_id"][index] = None

        return project, ui_status

    # TODO not in use
    @staticmethod
    def reset_tab_free_generation(project, ui_status):
        print("entering LogicFreeGeneration.reset_tab_free_generation")
        temp_project = Project()
        temp_ui_status = UiStatus()
        project.tab_free_generation = temp_project.tab_free_generation
        project.free_generation_temp = temp_project.free_generation_temp
        ui_status.tab_free_generation = temp_ui_status.tab_free_generation

        for index in range(0, project.tab_general["rotation_duration"]):
            project = LogicFreeRotationGeneration.get_recordset_cb_crops(project, ui_status, index)
            project = LogicFreeRotationGeneration.get_recordset_cb_crop_types(project, ui_status, index)

        return project, ui_status