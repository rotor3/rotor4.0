from db_connect.db_connect import execute_query_get_dictionary_list, execute_query_get_one, execute_query_get_recordset,\
        execute_query_get_dictionary_list
class LogicGeneral:

    @staticmethod
    def save_project_to_db():
        pass

    @staticmethod
    def verify_project(project, ui_status):
        print("Function verify_project")

        # self.logger.info("Project modified", self.project)

        for index in range(0, project.tab_general["rotation_duration"]):
            if project.tab_general["assessment_method"] in [1, 3] and project.tab_rotation["crop_id"][index] is None:
                ui_status.calculation_state["possible"] = False

                break
            elif project.tab_general["assessment_method"] == 2 and project.tab_free_generation["crop_id"][index] is None:
                ui_status.calculation_state["possible"] = False

                break
            else:
                ui_status.calculation_state["possible"] = True


        return project, ui_status

