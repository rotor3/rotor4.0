import copy

from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one, execute_query_get_dictionary_list, \
    get_dataframe_from_db
# from rotor_calculations.yield_in_t import YieldInT
from sympy import *
import pandas as pd


class LogicTabYield:
    @staticmethod
    def get_all_crop_factors(project, ui_status):
        sql_query = """SELECT * FROM yield_crop_coefficients_tb"""

        df = get_dataframe_from_db(sql_query, index_column='crop_id')
        crop_coefficients_dictionary = df.T.to_dict()

        project.tab_yield["yield_crop_coefficients"] = crop_coefficients_dictionary

    @staticmethod
    def get_names_tab_yield(project, ui_status, index):
        print("Entering LogicTabYield.get_names_tab_yield")
        parameter_dict = {"language": ui_status.settings["language"], "crop_id": project.tab_rotation["crop_id"][index]}
        # set name
        sql_query = "SELECT name_%(language)s FROM crops_tb WHERE crop_id = %(crop_id)s" % parameter_dict

        name = execute_query_get_one(sql_query)[0]
        project.tab_rotation["crop_name"][index] = name

        return project, ui_status

    @staticmethod
    def reset_one_yield(project, ui_status, index):
        print("Entering LogicTabYield.reset_yield")
        project, ui_status = LogicTabYield.get_yield_main_crop(project, ui_status, index)
        project, ui_status = LogicTabYield.get_yield_catch_crop(project, ui_status, index)

        return project, ui_status

    @staticmethod
    def get_yield_main_crop(project, ui_status, index):
        print("LogicTabYield.get_yield_main_crop")
        print("Pec_Factor", project.result_general["precipitation_factor"])
        precipitation_factor = project.result_general["precipitation_factor"]
        soil_index = project.tab_general["soil_index"]
        crop_id = project.results["crop_rotation"][index]["crop_id"]

        crop_factors = project.tab_yield["yield_crop_coefficients"][crop_id]
        print("crop_factors", crop_factors)

        # select coefficients for yield equation according to N-level
        if project.results["crop_rotation"][index]["n_level"] == 1:
            a = crop_factors["a_1"]
            b = crop_factors["b_1"]
            c = crop_factors["c_1"]
        elif project.results["crop_rotation"][index]["n_level"] == 2:
            a = crop_factors["a_2"]
            b = crop_factors["b_2"]
            c = crop_factors["c_2"]
        else:  # if ef == 3 or actually also 4
            a = crop_factors["a_3"]
            b = crop_factors["b_3"]
            c = crop_factors["c_3"]

        ############################################################################################################
        #
        # the yields are parabolas that are open at the bottom. Their maximumum (solve(y_derivative)) sets the max
        # for the soil rating index. If the index is higher, the yields decrease. Therefore the turning point
        # defines the max sri. Over this sri the yields are assumed to be constant.
        # Theoretically, the yields can be negative under a certain sri. This is determined by setting y = 0,
        # cutting the function off below this sri. For the existing yield coefficiants, this min sri is 18 at the max.
        # This implementation is just a safety measure.
        #
        #############################################################################################################
        # Yield (y), max and min sri
        x = Symbol("x")
        y = a * x ** 2 + b * x + c
        y_derivative = 2 * a * x + b
        if y != 0:  # y = 0 for crops that do not have yield coefficients such as leafcrops

            soil_index_max = solve(y_derivative)[0]
            print("MAX: ", soil_index_max)

            # soil index is limited to the range between its minimum, where yield = 0 and its max, after which yields decrease
            soil_index = min(soil_index, soil_index_max)
            print("If soil Index greater...SOIL INDEX:", soil_index)

        print("If soil Index greater...SOIL INDEX after if:", soil_index)
        # calculate the yield of each crop
        base_yield = a * soil_index ** 2 + b * soil_index + c

        # adjusted by 80% for crops with undersowing
        if project.tab_rotation["crop_type_id"][index] == 4 and \
                project.tab_rotation["undersowing"][index]:
            yield_correction_factor_undersowing = .8
        else:
            yield_correction_factor_undersowing = 1

        yield_with_manure = base_yield * (0.8 + precipitation_factor) * yield_correction_factor_undersowing + \
                            project.results["crop_rotation"][index]["n_amount_from_manure"] / \
                            project.results["crop_rotation"][index]["n_need_crop_per_t_fresh_mass_yield_main"]

        # lower limit yield is 0:
        yield_with_manure = max(0, yield_with_manure)

        if yield_with_manure == 0:
            ui_status.error["error"] = True
            ui_status.error["errormessage"] = "Please fill in a yield estimation where the yield is 0!"

        project.tab_yield["yield_amount_t"][index] = yield_with_manure

        return project, ui_status

    @staticmethod
    def get_yield_catch_crop(project, ui_status, index):
        print("LogicTabYield.get_yield_catch_crop")
        print("Pec_Factor", project.result_general["precipitation_factor"])
        precipitation_factor = project.result_general["precipitation_factor"]
        soil_index = project.tab_general["soil_index"]
        crop_id = 120
        # untersaat = crop_id 120
        # cc = 80% crop_id 120
        if project.results["crop_rotation"][index]["covercrop"]:
            crop_factors = project.tab_yield["yield_crop_coefficients"][crop_id]
            print("crop_factors", crop_factors)
            a = crop_factors["a_3"]
            b = crop_factors["b_3"]
            c = crop_factors["c_3"]

            # Yield (y), max and min sri
            x = Symbol("x")

            y_derivative = 2 * a * x + b

            soil_index_max = solve(y_derivative)[0]
            # soil index is limited to the range between its minimum, where yield = 0 and its max, after which yields decrease
            soil_index = min(project.tab_general["soil_index"], soil_index_max)

            # calculate the yield of catch crop
            base_yield_cc = a * soil_index ** 2 + b * soil_index + c

            # adjustment for precipitation class
            yield_cc = base_yield_cc * (0.8 + precipitation_factor)

            if project.results["crop_rotation"][(index - 1) % project.tab_general["rotation_duration"]]["undersowing"]:
                yield_cc = yield_cc * 0.8
            project.tab_yield["yield_amount_catch_crop_t"][index] = yield_cc



        # Faktor LKG * 0.2 für Zwischenfrucht und LKG * 0.25 für Untersaat
        # TODO Etablierungswahrscheinlichkeit
        # TODO Anteile für Erntemethoden Heu Silage Grünschnitt einrechnen

        return project, ui_status

    @staticmethod
    def get_all_yields(project, ui_status):
        print("Entering LogicTabYield.get_all_yields")
        LogicTabYield.get_all_crop_factors(project, ui_status)
        for index in range(0, project.tab_general["rotation_duration"]):
            print(project.tab_rotation["crop_id"][index], project, ui_status, index)
            project, ui_status = LogicTabYield.get_yield_main_crop(project, ui_status, index)
            project, ui_status = LogicTabYield.get_yield_catch_crop(project, ui_status, index)

            ui_status.tabs["tab_yield_visible"] = True
            ui_status.tabs_state["update_tabs"] = True

        return project, ui_status


if __name__ == "__main__":
    # print("Dummy...")
    # dummy = Dummy(13)
    # print("Dummy results...")
    # project = dummy.load_project_from_db(122)
    # dummy.results = Project().results
    # print("Dummy calculations", dummy.results)

    LogicTabYield.get_all_crop_factors()
