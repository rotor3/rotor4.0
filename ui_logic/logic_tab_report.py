
from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one, execute_query_get_dictionary_list
from project_handling.project import Project


class LogicTabReport:
    @staticmethod
    def setup_report_table(project, ui_status):
        print("FROM REPORT:\nPROJECT result_general:\n", project.result_general)
        print("FROM REPORT:\nPROJECT results:\n", project.results)
        print(". By setting 'position' in db to 0, the item is not displayed")
        '''This gets all Names for the columns of the displayed report. By setting 'position' in db to 0, the item is not displayed'''
        print("Crop_rotation:", project.results["crop_rotation"])
        sql_query = """SELECT project_key, name_%(language)s FROM report_rows_tb WHERE position > 0 ORDER BY position"""\
                    % {"language": ui_status.settings["language"]}

        report_rows_names_list = execute_query_get_recordset(sql_query)
        print("List report_rows_names_list \n", report_rows_names_list)
        report_rows = []

        for pair in report_rows_names_list:
            # adds the name at the beginning of each list
            # report_row = []
            # crop name
            report_row = [pair[1]]
            i = 1
            print("Report row", report_row, "index", i)
            # TODO!!!! Spalten in db.report_table an Bezeichnungen anpassen!!!!
            for cpa in project.results["crop_rotation"]:
                if pair[0] != "crop_name":

                    report_row.append(cpa[pair[0]])
                    print(pair[0], cpa[pair[0]])
                else:
                    crop_id = cpa["crop_id"]
                    sql_query = """SELECT name_%(language)s  FROM crops_tb WHERE crop_id = %(crop_id)s""" % {
                        "language": ui_status.settings["language"], "crop_id": crop_id}

                    report_row.append(execute_query_get_one(sql_query)[0])

                    i += 1
            report_rows.append(report_row)
        print("REPORTROWS Dictionary", report_rows)
        project.report["item_list"] = report_rows

        # project.report["item_list"] = [["Name 1", "value 1 1", "value 1 2", "value 1 3"], ["Name 2", "value 2 1", "value 2 2", "value 2 3"]]

        return project, ui_status