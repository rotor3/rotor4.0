import copy
from operator import itemgetter
import pandas as pd

from pip._internal.utils.misc import enum

from db_connect.db_connect import execute_query_get_recordset, execute_query_get_one, execute_query_get_dictionary_list, get_dataframe_from_db
from project_handling.project import Project



class LogicTabManure:
    #--------------set up GUI--------------------------------------------


    @staticmethod
    def update_manure_ids(project, ui_status):
        print("LogicTabManure.update_manure_ids")
        if project.tab_general["assessment_method"] in (1, 3):
            list_old = copy.copy(project.tab_manure["m_n_manures_ids"])
            list_new = [manure_id for list in project.tab_rotation["m_n_manures_id"] for manure_id in list if manure_id != 0]

        set_of_manure_ids_old = set(list_old)
        set_of_manure_ids = set(list_new)
        print("BEFORE")
        print("list_old:", list_old)
        print("set_of_manure_ids_old: ", set_of_manure_ids_old)
        print("list_new: ", list_new)
        print("set_of_manure_ids: ", set_of_manure_ids)


        add_ids = set_of_manure_ids.difference(set_of_manure_ids_old)
        remove_ids = set_of_manure_ids_old.difference(set_of_manure_ids)
        # both sets are empty
        if add_ids == remove_ids:
            ui_status.tab_manure["load_manures"] = False
            print("SAME SAME")
        else:
            ui_status.tab_manure["load_manures"] = True
            print("CHANGE MANURES")
            for id in remove_ids:
                project.tab_manure["m_n_manures_ids"].remove(id)
                print("REMOVING", id)
            for id in add_ids:
                project.tab_manure["m_n_manures_ids"].append(id)
                print("ADDING", id)

        print("AFTER")
        print("list_old:", list_old)
        print("set_of_manure_ids_old: ", set_of_manure_ids_old)
        print("list_new: ", list_new)
        print("set_of_manure_ids: ", set_of_manure_ids)
        return project, ui_status



    @staticmethod
    def get_df_manure_all_standarvalues(project, ui_status):
        print("LogicTabManure.get_df_manure_all_standarvalues")
        sql_query = """SELECT * FROM m_n_manures_tb"""

        project.tab_manure["df_manure_standard_values"] = get_dataframe_from_db(sql_query, index_column='m_n_manures_id')

        # fill dattaframe with 0 where NULL value
        project.tab_manure["df_manure_values"] = project.tab_manure["df_manure_standard_values"].fillna(0, inplace=True)
        project.tab_manure["manure_standard_values"] = copy.deepcopy(project.tab_manure["df_manure_standard_values"].T.to_dict())
        if ui_status.new_project:

            project.tab_manure["manure_values"] = copy.deepcopy(project.tab_manure["manure_standard_values"])
            ui_status.new_project = False

        print("ALL STANDARDVALUES!!")
        print(project.tab_manure["manure_standard_values"])

        return project, ui_status

    @staticmethod
    def get_df_manure_labels(project, ui_status):
        print("LogicTabManure.get_df_manure_labels")
        """A dataframe for all manures is created"""

        sql_query = """select t4.m_n_manures_id, t1.name_%(language)s, t2.name_%(language)s, t3.name_%(language)s from (((manure_type_tb t1 join m_n_manures_tb t4 
                    on t1.manure_type_id = t4.manure_type_id) join manure_source_tb t2 on t2.manure_source_id = 
                    t4.manure_source_id) join manure_specification_tb t3 on t3.manure_specification_id = 
                    t4.manure_specification_id)""" % {"language": ui_status.settings["language"]}

        df_manure_labels = get_dataframe_from_db(sql_query)
        print("MANURE DF", df_manure_labels)

        def func(row):
            manure_label = []
            for i in range(1, 4):
                if row[i] != "--":
                    manure_label.append(row[i])

            label = ', '.join(manure_label)
            # print(label)
            return label


        df_manure_labels['label'] = df_manure_labels.apply(func, axis=1, raw=False)
        print("DF MANURE LABELS!!!!\n", df_manure_labels)
        ui_status.tab_manure["df_manure_labels"] = df_manure_labels
        # write dataframe to dictionary
        ui_status.tab_manure["manure_labels"] = dict(zip(df_manure_labels.index, df_manure_labels['label']))
        # print("Manure label dict", ui_status.tab_manure["manure_labels"])
        return ui_status
#



    @staticmethod
    def bt_load_standard_values(project, ui_status, index):
        print("MainWindow.bt_load_standard_values", index)


        m_n_manures_id = project.tab_manure["m_n_manures_ids"][index]
        project.tab_manure["manure_values"][m_n_manures_id] = copy.deepcopy(project.tab_manure["manure_standard_values"][m_n_manures_id])
        ui_status.tab_manure["load_manures"] = True
        return project, ui_status





