import json

from PySide2 import QtWidgets

from db_connect.db_connect import execute_query_get_recordset, execute_query_get_dictionary_list
from qtui_rotor.openprojectwindow import Ui_OpenProjectWindow
from ui_logic.logic_sidebar import LogicSidebar
from ui_logic.ui_status import UiStatus


class LogicOpenProjectWindow(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.open_project_window = Ui_OpenProjectWindow()
        self.open_project_window.setupUi(self)


    @staticmethod
    def get_project_list():
        print("entering LogicOpenProjectWindow.get_project_list")


        sql_query = """SELECT projects_id, projects_name, project_last_changed FROM projects_tb ORDER BY projects_name"""

        recordset = execute_query_get_recordset(sql_query)

        return recordset

    @staticmethod
    def load_project_from_db(project):
        print("entering LogicOpenProjectWindow.load_project_from_db")
        project, ui_status = LogicSidebar.reset_all(project)

        ui_status = UiStatus()


        print("project from db")

        json_sql = """SELECT *
                        FROM projects_tb WHERE projects_id = %(project_id)s""" % {"project_id": project.project_id}

        db_project = execute_query_get_dictionary_list(json_sql)[0]


        # This is a basic try-construct for future updates/upgrades,
        # in order for ROTOR to load projects from previous versions
        try:
            project.project_name = db_project["projects_name"]
            project.user_id = db_project["projects_user"]

            project.tab_general = json.loads(db_project["dict_tab_general"])
            project.tab_rotation = json.loads(db_project["dict_tab_rotation"])
            project.tab_free_generation = json.loads(db_project["dict_tab_free_generation"])
            project.dict_tab_manure_2 = json.loads(db_project["dict_tab_manure"])
            project.tab_epm = json.loads(db_project["dict_tab_epm"])

            project.result_general = json.loads(db_project["result_general"])
            project.results = json.loads(db_project["results"])

            ui_status.tabs = json.loads(db_project["ui_status_tabs"])


            project.results = json.loads(db_project["results"])

            ui_status.tabs = json.loads(db_project["ui_status_tabs"])
        except Exception as e:
            ui_status.error["error"] = True
            ui_status.error["errormessage"] = str(e)

        finally:
            return project, ui_status
    @staticmethod
    def test():
        print("TEST")

if __name__ == '__main__':
    recordset = LogicOpenProjectWindow.get_project_list(False, False, False)
    window = LogicOpenProjectWindow()
    window.show()
    print(recordset)