<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="de_DE">
<context>
    <name>DialogWindow</name>
    <message>
        <location filename="dialogwindow.ui" line="16"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="26348"/>
        <source>ROTOR 4.0</source>
        <translation>ROTOR 4.0</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26360"/>
        <source>Dauer der Fruchtfolge</source>
        <translation>Rotation duration</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26831"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26832"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26833"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26834"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26835"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26836"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26837"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26838"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26370"/>
        <source>Jahre</source>
        <translation>years</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26395"/>
        <source>Allgemein</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26372"/>
        <source>Art der Berechnung</source>
        <translation>Type of assessment</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26373"/>
        <source>Fruchtfolge bewerten</source>
        <translation>Assess crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26880"/>
        <source>Fruchtfolge generieren</source>
        <translation>Generate crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26375"/>
        <source>intelligente Fruchtfolgeerstellung</source>
        <translation>Guided generation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <source>Niederschläge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26377"/>
        <source>Winterniederschlag</source>
        <translation>Winter precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26378"/>
        <source>Jahresniederschlag</source>
        <translation>Annual precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26380"/>
        <source>Bodenparameter</source>
        <translation>Soil parameters</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26381"/>
        <source>Erweiterte Bodenparameter</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26382"/>
        <source>Steingehalt [%] (Partikel &gt; 2mm)</source>
        <translation>Stone ratio (particles &gt; 2mm)</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26383"/>
        <source>organische Substanz [% C in TM]</source>
        <translation>Organic substance</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26826"/>
        <source>Intelligente Fruchtfolgenerstellung</source>
        <translation>Guided generation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26385"/>
        <source>Oberboden [cm]</source>
        <translation>Top soil</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26386"/>
        <source>Trockenrohdichte im Oberboden [g/cm3]</source>
        <translation>Bulk density</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1149"/>
        <source>C-N Verhältnis der OBS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26389"/>
        <source>Bodenart</source>
        <translation>Soil type</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26390"/>
        <source>Ackerzahl</source>
        <translation>Soil rating index</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Atmosphärischer Stickstoffeintrag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26392"/>
        <source>kg/ha und Jahr</source>
        <translation>kg/ ha /a</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26393"/>
        <source>Stickstoffemissionskarte des UBA</source>
        <translation>Nitrogen emission map</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26394"/>
        <source>&quot;Rotes Gebiet&quot;, hohe Nitratbelastung des Grundwassers</source>
        <translation>High concentration of nitrate in ground water (&gt;50mg/ l)</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27006"/>
        <source>1. Jahr</source>
        <translation>1st year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26793"/>
        <source>---select crop---</source>
        <translation>--select crop--</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="9608"/>
        <source>früher Umbruch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27901"/>
        <source>Strohernte</source>
        <translation>Strawharvest</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27858"/>
        <source>Bodenbearbeitung</source>
        <translation>Tillage</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26802"/>
        <source>cm Pflugtiefe</source>
        <translation>cm depth of tillage</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26803"/>
        <source>Untersaat</source>
        <translation>Undersowing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="9420"/>
        <source>Winterhärte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26787"/>
        <source>hoch</source>
        <translation>high</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26788"/>
        <source>mittel</source>
        <translation>medium</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26789"/>
        <source>niedrig</source>
        <translation>low</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26805"/>
        <source>Leguminosenanteil</source>
        <translation>Legume portion</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27857"/>
        <source>Zwischenfrucht</source>
        <translation>Cover crop</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26792"/>
        <source>Zwischenfrucht wird geerntet</source>
        <translation>Cover crop harvest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39639"/>
        <source>Düngung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10119"/>
        <source>Frühjahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26823"/>
        <source>Herbst</source>
        <translation>Autumn</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26824"/>
        <source>t/ ha</source>
        <translation>t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40508"/>
        <source>Löschen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10074"/>
        <source>Weiterer Dünger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9050"/>
        <source>TM-Aufwuchs dt/ha</source>
        <translation type="obsolete">dm of yield t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9049"/>
        <source>N2-Fixierung kg N/ha</source>
        <translation type="obsolete">N fixed</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27100"/>
        <source>2. Jahr</source>
        <translation>2nd year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27194"/>
        <source>3. Jahr</source>
        <translation>3rd year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27288"/>
        <source>4. Jahr</source>
        <translation>4th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27382"/>
        <source>5. Jahr</source>
        <translation>5th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27476"/>
        <source>6. Jahr</source>
        <translation>6th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27570"/>
        <source>7. Jahr</source>
        <translation>7th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27664"/>
        <source>8. Jahr</source>
        <translation>8th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27758"/>
        <source>9. Jahr</source>
        <translation>9th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27852"/>
        <source>10. Jahr</source>
        <translation>10th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26828"/>
        <source>Maximale Anzahl der Ergebnisse </source>
        <translation>Maximum results </translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26829"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26830"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26840"/>
        <source>Betrieb mit Viehhaltung</source>
        <translation>Livestock farming</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26841"/>
        <source>Auswahl der Tierart</source>
        <translation>Livestock type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10419"/>
        <source>Auswahl der Düngerarten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10455"/>
        <source>Auswahl der Feldfrüchte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26844"/>
        <source>Feldfrucht</source>
        <translation>Crop</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26845"/>
        <source>Fruchtgruppe</source>
        <translation>Crop type</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26846"/>
        <source>Feldrucht 1</source>
        <translation>crop 1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26874"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26849"/>
        <source>Feldrucht 2</source>
        <translation>crop 2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26852"/>
        <source>Feldrucht 3</source>
        <translation>crop 3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26855"/>
        <source>Feldrucht 4</source>
        <translation>crop 4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26858"/>
        <source>Feldrucht 5</source>
        <translation>crop 5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26861"/>
        <source>Feldrucht 6</source>
        <translation>crop 6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26864"/>
        <source>Feldrucht 7</source>
        <translation>crop 7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26867"/>
        <source>Feldrucht 8</source>
        <translation>crop 8</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26870"/>
        <source>Feldrucht 9 </source>
        <translation>crop 9 </translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26873"/>
        <source>Feldrucht 10</source>
        <translation>crop 10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40621"/>
        <source>Zurück</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11130"/>
        <source>Fruchtfolge zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27950"/>
        <source>Weiter</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27951"/>
        <source>Berechnen</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11171"/>
        <source>Dünger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27928"/>
        <source>TextLabel</source>
        <translation>label</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12179"/>
        <source>N-Verfügbarkeit %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26915"/>
        <source>N-Ausbringungsverluste %</source>
        <translation>N loss</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26890"/>
        <source>K-Gehalt kg/t FM</source>
        <translation>K-Content</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26909"/>
        <source>Trockenmasse</source>
        <translation>Dry mass</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26885"/>
        <source>P-Gehalt kg/t FM</source>
        <translation>P content</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9212"/>
        <source>Standardwerte nutzen</source>
        <translation type="obsolete">Use standard values</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26916"/>
        <source>N-Gehalt in kg/t FM</source>
        <translation>N content</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26981"/>
        <source>Kultur1</source>
        <translation>crop1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27005"/>
        <source>Bericht</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27002"/>
        <source>New Column</source>
        <translation>n c</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27004"/>
        <source>Saldo</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40588"/>
        <source>Tab zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40601"/>
        <source>alles zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27952"/>
        <source>Speichern</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27953"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27954"/>
        <source>Sprache</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27955"/>
        <source>Hilfe</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40714"/>
        <source>Über Rotor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26350"/>
        <source>Speichern als</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26351"/>
        <source>Deutsch</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26352"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26353"/>
        <source>Francais</source>
        <translation>Francais</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26354"/>
        <source>Handbuch</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40760"/>
        <source>Projekt öffnen</source>
        <translation type="unfinished">Open project</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26356"/>
        <source>Projekt exportieren</source>
        <translation>Export project</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26357"/>
        <source>Lizenz</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26358"/>
        <source>Versionen</source>
        <translation>Versions</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26359"/>
        <source>Projekt importieren</source>
        <translation>Import project</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26926"/>
        <source>Standardwerte laden</source>
        <translation>Load default values</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="24559"/>
        <source>ErtragsabschÃ¤tzung</source>
        <translation type="obsolete">Yield assessment</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26979"/>
        <source>dt/ ha</source>
        <translation>t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26980"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="24537"/>
        <source>GeschÃ¤tzte ErtrÃ¤ge:</source>
        <translation type="obsolete">Estimated yields:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1113"/>
        <source>jährliche Mineralisierungsrate OBS in %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="9539"/>
        <source>Grünschnitt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26796"/>
        <source>Heu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26797"/>
        <source>Silage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15605"/>
        <source>Ökonomie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27943"/>
        <source>m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27874"/>
        <source>Weitere Bodenbearbeitung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27927"/>
        <source>Menge: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27861"/>
        <source>Aussaat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27864"/>
        <source>Ernte Zwischenfrucht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27867"/>
        <source>Transport Zwischenfrucht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27870"/>
        <source>Hauptkultur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27871"/>
        <source>Bodenbearbeitung Hauptkultur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27878"/>
        <source>Aussaat Hauptkultur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27884"/>
        <source>Ernte Hauptkultur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27889"/>
        <source>Ernte Hauptkultur 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27894"/>
        <source>Ernte Hauptkultur 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27897"/>
        <source>Transport Hauptkultur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27907"/>
        <source>Stoppelbearbeitung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27931"/>
        <source>Pflege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27940"/>
        <source>Weitere Pflege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27942"/>
        <source>Arbeitsbreite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40062"/>
        <source>Kapazität</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27881"/>
        <source>Aussaat Untersaat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27892"/>
        <source>Weitere Erntemethode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27904"/>
        <source>Strohernte Transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27930"/>
        <source>t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40471"/>
        <source>Anzahl der Durchgänge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14499"/>
        <source>Geschätzte Erträge Zwischenfrucht:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14414"/>
        <source>Ertragsabschätzung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14898"/>
        <source>Geschätzte Erträge:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26355"/>
        <source>Projekt u00f6ffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26376"/>
        <source>Niederschlu00e4ge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26384"/>
        <source>ju00e4hrliche Mineralisierungsrate OBS in %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26387"/>
        <source>C-N Verhu00e4ltnis der OBS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26391"/>
        <source>Atmosphu00e4rischer Stickstoffeintrag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26786"/>
        <source>Winterhu00e4rte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26795"/>
        <source>Gru00fcnschnitt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26798"/>
        <source>fru00fcher Umbruch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27910"/>
        <source>Du00fcngung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26822"/>
        <source>Fru00fchjahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27945"/>
        <source>Lu00f6schen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26821"/>
        <source>Weiterer Du00fcnger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26842"/>
        <source>Auswahl der Du00fcngerarten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26843"/>
        <source>Auswahl der Feldfru00fcchte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27949"/>
        <source>Zuru00fcck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26877"/>
        <source>Fruchtfolge zuru00fccksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26896"/>
        <source>N-Verfu00fcgbarkeit %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26928"/>
        <source>Du00fcnger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26931"/>
        <source>Geschu00e4tzte Ertru00e4ge Zwischenfrucht:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26954"/>
        <source>Geschu00e4tzte Ertru00e4ge:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="26982"/>
        <source>Ertragsabschu00e4tzung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27929"/>
        <source>Kapazitu00e4t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27944"/>
        <source>Anzahl der Durchgu00e4nge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27946"/>
        <source>u00d6konomie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27947"/>
        <source>Tab zuru00fccksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27948"/>
        <source>alles zuru00fccksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="27956"/>
        <source>u00dcber Rotor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenProjectWindow</name>
    <message>
        <location filename="openprojectwindow.ui" line="14"/>
        <source>Projekt &#xc3;&#xb6;ffnen</source>
        <translation type="unfinished">Open project</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="67"/>
        <source>New Column</source>
        <translation>new column</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="72"/>
        <source>Projekt</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="77"/>
        <source>Letzte &#xc3;&#x84;nderung</source>
        <translation type="unfinished">Last changed</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="100"/>
        <source>&#xc3;&#x96;ffnen</source>
        <translation type="unfinished">Open</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="107"/>
        <source>Abbrechen</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>ReportWindow</name>
    <message>
        <location filename="reportwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="30"/>
        <source>Fruchtfolgebilanzen</source>
        <translation>Balances</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="59"/>
        <source>Beenden</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="79"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="90"/>
        <source>PDF Exportieren</source>
        <translation>Export PDF</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="95"/>
        <source>Textdatei exportieren</source>
        <translation>Export text file</translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="100"/>
        <source>Drucken</source>
        <translation>print</translation>
    </message>
</context>
</TS>
