<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>DialogWindow</name>
    <message>
        <location filename="dialogwindow.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="8678"/>
        <source>ROTOR 4.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8679"/>
        <source>Dauer der Fruchtfolge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9151"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9152"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9153"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9154"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9155"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9156"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9157"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9158"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8688"/>
        <source>Jahre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8715"/>
        <source>Allgemein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8689"/>
        <source>Art der Berechnung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8690"/>
        <source>Fruchtfolge bewerten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9189"/>
        <source>Fruchtfolge generieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8692"/>
        <source>intelligente Fruchtfolgeerstellung</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="8693"/>
        <source>Niederschläge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8694"/>
        <source>Winterniederschlag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8695"/>
        <source>Jahresniederschlag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8696"/>
        <source>Bodenparameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8697"/>
        <source>Erweiterte Bodenparameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8698"/>
        <source>Steingehalt [%] (Partikel &gt; 2mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8699"/>
        <source>organische Substanz [% C in TM]</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="8700"/>
        <source>jährliche Mineralisierungsrate OBS in %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8701"/>
        <source>Oberboden [cm]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8702"/>
        <source>Trockenrohdichte im Oberboden [g/cm3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="8703"/>
        <source>C-N Verhältnis der OBS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8705"/>
        <source>Bodenart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8706"/>
        <source>Ackerzahl</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="8707"/>
        <source>Atmosphärischer Stickstoffeintrag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8708"/>
        <source>kg/ha und Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8709"/>
        <source>Stickstoffemissionskarte des UBA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8710"/>
        <source>&quot;Rotes Gebiet&quot;, hohe Nitratbelastung des Grundwassers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8711"/>
        <source>Humusbilanzierungsmethode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8712"/>
        <source>nach Leithold, 1997</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8713"/>
        <source>nach VDLUFA, 2014</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8714"/>
        <source>nach Mosab, 2020</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9146"/>
        <source>Intelligente Fruchtfolgenerstellung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9250"/>
        <source>1. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9104"/>
        <source>---select crop---</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9105"/>
        <source>früher Umbruch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9106"/>
        <source>Strohernte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9246"/>
        <source>Bodenbearbeitung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9108"/>
        <source>cm Pflugtiefe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9109"/>
        <source>Ertragsanpassung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9110"/>
        <source>dt/ha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9111"/>
        <source>Untersaat</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9118"/>
        <source>Winterhärte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9119"/>
        <source>hoch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9120"/>
        <source>mittel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9121"/>
        <source>niedrig</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9122"/>
        <source>Leguminosenanteil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9117"/>
        <source>Zwischenfrucht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9123"/>
        <source>Zwischenfrucht wird geerntet</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9124"/>
        <source>Düngung</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9140"/>
        <source>Frühjahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9141"/>
        <source>Herbst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9142"/>
        <source>t/ ha</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9143"/>
        <source>Löschen</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9139"/>
        <source>Weiterer Dünger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9145"/>
        <source>TM-Aufwuchs dt/ha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9144"/>
        <source>N2-Fixierung kg N/ha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9252"/>
        <source>2. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9254"/>
        <source>3. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9256"/>
        <source>4. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9258"/>
        <source>5. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9260"/>
        <source>6. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9262"/>
        <source>7. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9264"/>
        <source>8. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9266"/>
        <source>9. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9103"/>
        <source>10. Jahr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9148"/>
        <source>Maximale Anzahl der Ergebnisse </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9149"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9150"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9159"/>
        <source>Betrieb mit Viehhaltung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9160"/>
        <source>Auswahl der Tierart</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9161"/>
        <source>Auswahl der Düngerarten</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9162"/>
        <source>Auswahl der Feldfrüchte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9163"/>
        <source>Feldfrucht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9164"/>
        <source>Fruchtgruppe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9165"/>
        <source>Feldrucht 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9184"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9167"/>
        <source>Feldrucht 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9169"/>
        <source>Feldrucht 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9171"/>
        <source>Feldrucht 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9173"/>
        <source>Feldrucht 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9175"/>
        <source>Feldrucht 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9177"/>
        <source>Feldrucht 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9179"/>
        <source>Feldrucht 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9181"/>
        <source>Feldrucht 9 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9183"/>
        <source>Feldrucht 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9274"/>
        <source>Zurück</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9186"/>
        <source>Fruchtfolge zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9275"/>
        <source>Weiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9276"/>
        <source>Berechnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9217"/>
        <source>Dünger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9216"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9198"/>
        <source>N-Verfügbarkeit %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9199"/>
        <source>N-Ausbringungsverluste %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9204"/>
        <source>K-Gehalt kg/t FM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9207"/>
        <source>Trockenmasse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9211"/>
        <source>P-Gehalt kg/t FM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9212"/>
        <source>Standardwerte nutzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9215"/>
        <source>N-Gehalt in kg/t FM</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9248"/>
        <source>Umweltschutzmaßnahmen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9245"/>
        <source>Kultur1</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9247"/>
        <source>Umweltschutzmaßnahme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9271"/>
        <source>Bericht</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9268"/>
        <source>New Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9270"/>
        <source>Saldo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9272"/>
        <source>Tab zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9273"/>
        <source>alles zurücksetzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9282"/>
        <source>Speichern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9278"/>
        <source>Datei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9279"/>
        <source>Sprache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9280"/>
        <source>Hilfe</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9281"/>
        <source>Über Rotor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9283"/>
        <source>Speichern als</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9284"/>
        <source>Deutsch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9285"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9286"/>
        <source>Francais</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9287"/>
        <source>Handbuch</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.py" line="9288"/>
        <source>Projekt öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9289"/>
        <source>Projekt exportieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9290"/>
        <source>Lizenz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9291"/>
        <source>Versionen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9292"/>
        <source>Projekt importieren</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenProjectWindow</name>
    <message encoding="UTF-8">
        <location filename="openprojectwindow.ui" line="14"/>
        <source>Projekt öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="67"/>
        <source>New Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="72"/>
        <source>Projekt</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="openprojectwindow.ui" line="77"/>
        <source>Letzte Änderung</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="openprojectwindow.ui" line="100"/>
        <source>Öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="107"/>
        <source>Abbrechen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReportWindow</name>
    <message>
        <location filename="reportwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="30"/>
        <source>Fruchtfolgebilanzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="59"/>
        <source>Beenden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="79"/>
        <source>Datei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="90"/>
        <source>PDF Exportieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="95"/>
        <source>Textdatei exportieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="100"/>
        <source>Drucken</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
