<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="8678"/>
        <source>ROTOR 4.0</source>
        <translation>ROTOR 4.0</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8679"/>
        <source>Dauer der Fruchtfolge</source>
        <translation>Durée de la rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9151"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9152"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9153"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9154"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9155"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9156"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9157"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9158"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8688"/>
        <source>Jahre</source>
        <translation>années</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8715"/>
        <source>Allgemein</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8689"/>
        <source>Art der Berechnung</source>
        <translation>Type de calcul</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8690"/>
        <source>Fruchtfolge bewerten</source>
        <translation>Evaluer une rotation des cultures</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9189"/>
        <source>Fruchtfolge generieren</source>
        <translation>Créer une rotation des cultures</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8692"/>
        <source>intelligente Fruchtfolgeerstellung</source>
        <translation>Création guidée d&apos;une rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <source>Niederschläge</source>
        <translation type="obsolete">Précipitations</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8694"/>
        <source>Winterniederschlag</source>
        <translation>Précipitations hivernales</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8695"/>
        <source>Jahresniederschlag</source>
        <translation>Précipitations annuelles</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8696"/>
        <source>Bodenparameter</source>
        <translation>Paramètres du sol</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8697"/>
        <source>Erweiterte Bodenparameter</source>
        <translation>Paramètres du sol avancés</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8698"/>
        <source>Steingehalt [%] (Partikel &gt; 2mm)</source>
        <translation>Contenue en pièrres</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8699"/>
        <source>organische Substanz [% C in TM]</source>
        <translation>Matière organique</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1113"/>
        <source>jährliche Mineralisierungsrate OBS in %</source>
        <translation type="obsolete">Taux de minéralisation annuel</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8701"/>
        <source>Oberboden [cm]</source>
        <translation>Terre végétale</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8702"/>
        <source>Trockenrohdichte im Oberboden [g/cm3]</source>
        <translation>Densité de la terre végétale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1149"/>
        <source>C-N Verhältnis der OBS</source>
        <translation type="obsolete">Rapport C/ N</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8705"/>
        <source>Bodenart</source>
        <translation>Type du sol</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8706"/>
        <source>Ackerzahl</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Atmosphärischer Stickstoffeintrag</source>
        <translation type="obsolete">Azoté atmosphérique</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8708"/>
        <source>kg/ha und Jahr</source>
        <translation>kg/ ha par an</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8709"/>
        <source>Stickstoffemissionskarte des UBA</source>
        <translation>Carte des émission d&apos;azoté de l&apos;Allemgne</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8710"/>
        <source>&quot;Rotes Gebiet&quot;, hohe Nitratbelastung des Grundwassers</source>
        <translation>Teneur en nitrate élevée de &apos;eau souterraine (&gt;50mg/l)</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8711"/>
        <source>Humusbilanzierungsmethode</source>
        <translation>Bilan d&apos;humus</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8712"/>
        <source>nach Leithold, 1997</source>
        <translation>Selon Leithold, 1997</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8713"/>
        <source>nach VDLUFA, 2014</source>
        <translation>Selon VDLUFA, 2014</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8714"/>
        <source>nach Mosab, 2020</source>
        <translation>Selon Halwani, 2020</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9146"/>
        <source>Intelligente Fruchtfolgenerstellung</source>
        <translation>Génération guidée</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9250"/>
        <source>1. Jahr</source>
        <translation>1re anée</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9104"/>
        <source>---select crop---</source>
        <translation>--sélectionner--</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10196"/>
        <source>früher Umbruch</source>
        <translation type="obsolete">Enfouir tôt</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9106"/>
        <source>Strohernte</source>
        <translation>Récolte de paille</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9246"/>
        <source>Bodenbearbeitung</source>
        <translation>Travail du sol</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9108"/>
        <source>cm Pflugtiefe</source>
        <translation>Profondeur du labourage</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9109"/>
        <source>Ertragsanpassung</source>
        <translation>Adjustement de racolte</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9110"/>
        <source>dt/ha</source>
        <translation>t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9111"/>
        <source>Untersaat</source>
        <translation>Culture ous semise</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10496"/>
        <source>Winterhärte</source>
        <translation type="obsolete">Résistance au froid</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9119"/>
        <source>hoch</source>
        <translation>haut</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9120"/>
        <source>mittel</source>
        <translation>moyen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9121"/>
        <source>niedrig</source>
        <translation>bas</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9122"/>
        <source>Leguminosenanteil</source>
        <translation>Taux de légumineuses</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9117"/>
        <source>Zwischenfrucht</source>
        <translation>Culture dérobée</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9123"/>
        <source>Zwischenfrucht wird geerntet</source>
        <translation>Racolte de la culture dérobée</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10579"/>
        <source>Düngung</source>
        <translation type="obsolete">Fertilisation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10908"/>
        <source>Frühjahr</source>
        <translation type="obsolete">Printemps</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9141"/>
        <source>Herbst</source>
        <translation>Automne</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9142"/>
        <source>t/ ha</source>
        <translation>t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10946"/>
        <source>Löschen</source>
        <translation type="obsolete">Effacer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10863"/>
        <source>Weiterer Dünger</source>
        <translation type="obsolete">Autre fertilisation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9145"/>
        <source>TM-Aufwuchs dt/ha</source>
        <translation>Racolte en matière sèche</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9144"/>
        <source>N2-Fixierung kg N/ha</source>
        <translation>Azote fixé</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9252"/>
        <source>2. Jahr</source>
        <translation>2de année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9254"/>
        <source>3. Jahr</source>
        <translation>3e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9256"/>
        <source>4. Jahr</source>
        <translation>4e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9258"/>
        <source>5. Jahr</source>
        <translation>5e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9260"/>
        <source>6. Jahr</source>
        <translation>6e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9262"/>
        <source>7. Jahr</source>
        <translation>7e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9264"/>
        <source>8. Jahr</source>
        <translation>8e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9266"/>
        <source>9. Jahr</source>
        <translation>9e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9103"/>
        <source>10. Jahr</source>
        <translation>10e année</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9148"/>
        <source>Maximale Anzahl der Ergebnisse </source>
        <translation>Nombre maximale des résultats</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9149"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9150"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9159"/>
        <source>Betrieb mit Viehhaltung</source>
        <translation>Entreprise agricole avec élevage</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9160"/>
        <source>Auswahl der Tierart</source>
        <translation>Type de bétail</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11298"/>
        <source>Auswahl der Düngerarten</source>
        <translation type="obsolete">Type d&apos;engrais</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11334"/>
        <source>Auswahl der Feldfrüchte</source>
        <translation type="obsolete">Type de culture</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9163"/>
        <source>Feldfrucht</source>
        <translation>Culture</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9164"/>
        <source>Fruchtgruppe</source>
        <translation>Type de culture</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9165"/>
        <source>Feldrucht 1</source>
        <translation>culture 1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9184"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9167"/>
        <source>Feldrucht 2</source>
        <translation>culture2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9169"/>
        <source>Feldrucht 3</source>
        <translation>culture3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9171"/>
        <source>Feldrucht 4</source>
        <translation>culture4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9173"/>
        <source>Feldrucht 5</source>
        <translation>culture5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9175"/>
        <source>Feldrucht 6</source>
        <translation>culture6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9177"/>
        <source>Feldrucht 7</source>
        <translation>culture7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9179"/>
        <source>Feldrucht 8</source>
        <translation>culture7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9181"/>
        <source>Feldrucht 9 </source>
        <translation>culture9</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9183"/>
        <source>Feldrucht 10</source>
        <translation>culture10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16231"/>
        <source>Zurück</source>
        <translation type="obsolete">Arrière</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12009"/>
        <source>Fruchtfolge zurücksetzen</source>
        <translation type="obsolete">Remettre la rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9275"/>
        <source>Weiter</source>
        <translation>avant</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9276"/>
        <source>Berechnen</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12050"/>
        <source>Dünger</source>
        <translation type="obsolete">Fumier</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9216"/>
        <source>TextLabel</source>
        <translation>label</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="13487"/>
        <source>N-Verfügbarkeit %</source>
        <translation type="obsolete">N disponibilté %</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9199"/>
        <source>N-Ausbringungsverluste %</source>
        <translation>Perte d&apos;azote</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9204"/>
        <source>K-Gehalt kg/t FM</source>
        <translation>Teneur en K</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9207"/>
        <source>Trockenmasse</source>
        <translation>Matière sèche</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9211"/>
        <source>P-Gehalt kg/t FM</source>
        <translation>Teneur en P</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9212"/>
        <source>Standardwerte nutzen</source>
        <translation>Utiliser défauts</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9215"/>
        <source>N-Gehalt in kg/t FM</source>
        <translation>Teneur de N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15196"/>
        <source>Umweltschutzmaßnahmen</source>
        <translation type="obsolete">Mesures environnementales</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9245"/>
        <source>Kultur1</source>
        <translation>culture1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16016"/>
        <source>Umweltschutzmaßnahme</source>
        <translation type="obsolete">Mesure environnementale</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9271"/>
        <source>Bericht</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9268"/>
        <source>New Column</source>
        <translation>New column</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9270"/>
        <source>Saldo</source>
        <translation>Solde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16198"/>
        <source>Tab zurücksetzen</source>
        <translation type="obsolete">Remettre l&apos;onglet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16211"/>
        <source>alles zurücksetzen</source>
        <translation type="obsolete">Remettre tout</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9282"/>
        <source>Speichern</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9278"/>
        <source>Datei</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9279"/>
        <source>Sprache</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9280"/>
        <source>Hilfe</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16324"/>
        <source>Über Rotor</source>
        <translation type="obsolete">Sur ROTOR</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9283"/>
        <source>Speichern als</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9284"/>
        <source>Deutsch</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9285"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9286"/>
        <source>Francais</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9287"/>
        <source>Handbuch</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16370"/>
        <source>Projekt öffnen</source>
        <translation type="obsolete">Ouvrir projet</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9289"/>
        <source>Projekt exportieren</source>
        <translation>Exporter projet</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9290"/>
        <source>Lizenz</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9291"/>
        <source>Versionen</source>
        <translation>Versions</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9292"/>
        <source>Projekt importieren</source>
        <translation>Importer projet</translation>
    </message>
</context>
</TS>
