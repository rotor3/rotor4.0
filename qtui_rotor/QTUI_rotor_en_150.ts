<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="8678"/>
        <source>ROTOR 4.0</source>
        <translation>ROTOR 4.0</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8679"/>
        <source>Dauer der Fruchtfolge</source>
        <translation>Rotation duration</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9151"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9152"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9153"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9154"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9155"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9156"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9157"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9158"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8688"/>
        <source>Jahre</source>
        <translation>years</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8715"/>
        <source>Allgemein</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8689"/>
        <source>Art der Berechnung</source>
        <translation>Assessment type</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8690"/>
        <source>Fruchtfolge bewerten</source>
        <translation>Assess existing crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9189"/>
        <source>Fruchtfolge generieren</source>
        <translation>Free generation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8692"/>
        <source>intelligente Fruchtfolgeerstellung</source>
        <translation>Intelligent generation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <source>Niederschläge</source>
        <translation type="obsolete">Precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8694"/>
        <source>Winterniederschlag</source>
        <translation>Winter precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8695"/>
        <source>Jahresniederschlag</source>
        <translation>Annual precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8696"/>
        <source>Bodenparameter</source>
        <translation>Soil parameters</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8697"/>
        <source>Erweiterte Bodenparameter</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8698"/>
        <source>Steingehalt [%] (Partikel &gt; 2mm)</source>
        <translation>Stone ratio (particles &gt;2mm) </translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8699"/>
        <source>organische Substanz [% C in TM]</source>
        <translation>Organic substance </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1113"/>
        <source>jährliche Mineralisierungsrate OBS in %</source>
        <translation type="obsolete">Annual rate of mineralization</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8701"/>
        <source>Oberboden [cm]</source>
        <translation>Topsoil</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8702"/>
        <source>Trockenrohdichte im Oberboden [g/cm3]</source>
        <translation>Bulkdensity of topsoil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1149"/>
        <source>C-N Verhältnis der OBS</source>
        <translation type="obsolete">C:N ratio</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8705"/>
        <source>Bodenart</source>
        <translation>Soil type</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8706"/>
        <source>Ackerzahl</source>
        <translation>Soil rating index</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Atmosphärischer Stickstoffeintrag</source>
        <translation type="obsolete">Atmospheric nitrogen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8708"/>
        <source>kg/ha und Jahr</source>
        <translation>kg/ha per year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8709"/>
        <source>Stickstoffemissionskarte des UBA</source>
        <translation>Nitrogen map of Germany</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8710"/>
        <source>&quot;Rotes Gebiet&quot;, hohe Nitratbelastung des Grundwassers</source>
        <translation>&quot;Red area&quot;, nitrate concentration in ground water &gt; 50mg/l</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8711"/>
        <source>Humusbilanzierungsmethode</source>
        <translation>Method of humus balance</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8712"/>
        <source>nach Leithold, 1997</source>
        <translation>after Leithold, 1997</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8713"/>
        <source>nach VDLUFA, 2014</source>
        <translation>after VDLUFA, 2014</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="8714"/>
        <source>nach Mosab, 2020</source>
        <translation>after Halwani, 2020</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9146"/>
        <source>Intelligente Fruchtfolgenerstellung</source>
        <translation>Intelligent generation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9250"/>
        <source>1. Jahr</source>
        <translation>1st year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9104"/>
        <source>---select crop---</source>
        <translation>--select crop--</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10196"/>
        <source>früher Umbruch</source>
        <translation type="obsolete">Early termination</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9106"/>
        <source>Strohernte</source>
        <translation>Strawharvest</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9246"/>
        <source>Bodenbearbeitung</source>
        <translation>Tillage</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9108"/>
        <source>cm Pflugtiefe</source>
        <translation>tillage depth in cm</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9109"/>
        <source>Ertragsanpassung</source>
        <translation>Yield adjustment</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9110"/>
        <source>dt/ha</source>
        <translation>t/ha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9111"/>
        <source>Untersaat</source>
        <translation>Undersowing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10496"/>
        <source>Winterhärte</source>
        <translation type="obsolete">Winterhardiness</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9119"/>
        <source>hoch</source>
        <translation>high</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9120"/>
        <source>mittel</source>
        <translation>medium</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9121"/>
        <source>niedrig</source>
        <translation>low</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9122"/>
        <source>Leguminosenanteil</source>
        <translation>Legume portion</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9117"/>
        <source>Zwischenfrucht</source>
        <translation>Covercrop</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9123"/>
        <source>Zwischenfrucht wird geerntet</source>
        <translation>Covercrop harvest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10579"/>
        <source>Düngung</source>
        <translation type="obsolete">Manure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10908"/>
        <source>Frühjahr</source>
        <translation type="obsolete">Spring</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9141"/>
        <source>Herbst</source>
        <translation>Autumn</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9142"/>
        <source>t/ ha</source>
        <translation>t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10946"/>
        <source>Löschen</source>
        <translation type="obsolete">Delete</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10863"/>
        <source>Weiterer Dünger</source>
        <translation type="obsolete">Add manure</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9145"/>
        <source>TM-Aufwuchs dt/ha</source>
        <translation>dry-mass yield</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9144"/>
        <source>N2-Fixierung kg N/ha</source>
        <translation>N2 fixed in kg N/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9252"/>
        <source>2. Jahr</source>
        <translation>2nd year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9254"/>
        <source>3. Jahr</source>
        <translation>3rd year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9256"/>
        <source>4. Jahr</source>
        <translation>4th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9258"/>
        <source>5. Jahr</source>
        <translation>5th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9260"/>
        <source>6. Jahr</source>
        <translation>6th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9262"/>
        <source>7. Jahr</source>
        <translation>7th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9264"/>
        <source>8. Jahr</source>
        <translation>8th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9266"/>
        <source>9. Jahr</source>
        <translation>9th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9103"/>
        <source>10. Jahr</source>
        <translation>10th year</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9148"/>
        <source>Maximale Anzahl der Ergebnisse </source>
        <translation>Maximum number of results</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9149"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9150"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9159"/>
        <source>Betrieb mit Viehhaltung</source>
        <translation>Livestock holding</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9160"/>
        <source>Auswahl der Tierart</source>
        <translation>Livestock</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11298"/>
        <source>Auswahl der Düngerarten</source>
        <translation type="obsolete">Manure types</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11334"/>
        <source>Auswahl der Feldfrüchte</source>
        <translation type="obsolete">Selection of crops</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9163"/>
        <source>Feldfrucht</source>
        <translation>Crop</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9164"/>
        <source>Fruchtgruppe</source>
        <translation>Crop type</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9165"/>
        <source>Feldrucht 1</source>
        <translation>Crop 1</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9184"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9167"/>
        <source>Feldrucht 2</source>
        <translation>Crop 2</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9169"/>
        <source>Feldrucht 3</source>
        <translation>Crop 3</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9171"/>
        <source>Feldrucht 4</source>
        <translation>Crop 4</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9173"/>
        <source>Feldrucht 5</source>
        <translation>Crop 5</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9175"/>
        <source>Feldrucht 6</source>
        <translation>Crop 6</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9177"/>
        <source>Feldrucht 7</source>
        <translation>Crop 7</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9179"/>
        <source>Feldrucht 8</source>
        <translation>Crop 8</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9181"/>
        <source>Feldrucht 9 </source>
        <translation>Crop 9</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9183"/>
        <source>Feldrucht 10</source>
        <translation>Crop 10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16231"/>
        <source>Zurück</source>
        <translation type="obsolete">Back</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12009"/>
        <source>Fruchtfolge zurücksetzen</source>
        <translation type="obsolete">Reset crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9275"/>
        <source>Weiter</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9276"/>
        <source>Berechnen</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12050"/>
        <source>Dünger</source>
        <translation type="obsolete">Manure</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9216"/>
        <source>TextLabel</source>
        <translation>label</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="13487"/>
        <source>N-Verfügbarkeit %</source>
        <translation type="obsolete">N-availability %</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9199"/>
        <source>N-Ausbringungsverluste %</source>
        <translation>N-loss %</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9204"/>
        <source>K-Gehalt kg/t FM</source>
        <translation>K-content in kg K/ t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9207"/>
        <source>Trockenmasse</source>
        <translation>Dry mass</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9211"/>
        <source>P-Gehalt kg/t FM</source>
        <translation>P-content in kg/t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9212"/>
        <source>Standardwerte nutzen</source>
        <translation>Use standardvalues</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9215"/>
        <source>N-Gehalt in kg/t FM</source>
        <translation>N-content in kg/t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15196"/>
        <source>Umweltschutzmaßnahmen</source>
        <translation type="obsolete">Encironmental protection measures</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9245"/>
        <source>Kultur1</source>
        <translation>crop1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16016"/>
        <source>Umweltschutzmaßnahme</source>
        <translation type="obsolete">Environmental protection measure</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9271"/>
        <source>Bericht</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9268"/>
        <source>New Column</source>
        <translation>New column</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9270"/>
        <source>Saldo</source>
        <translation>Balance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16198"/>
        <source>Tab zurücksetzen</source>
        <translation type="obsolete">Reset tab</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16211"/>
        <source>alles zurücksetzen</source>
        <translation type="obsolete">Reset all</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9282"/>
        <source>Speichern</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9278"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9279"/>
        <source>Sprache</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9280"/>
        <source>Hilfe</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16324"/>
        <source>Über Rotor</source>
        <translation type="obsolete">About Rotor</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9283"/>
        <source>Speichern als</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9284"/>
        <source>Deutsch</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9285"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9286"/>
        <source>Francais</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9287"/>
        <source>Handbuch</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16370"/>
        <source>Projekt öffnen</source>
        <translation type="obsolete">Open project</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9289"/>
        <source>Projekt exportieren</source>
        <translation>Export project</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9290"/>
        <source>Lizenz</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9291"/>
        <source>Versionen</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="9292"/>
        <source>Projekt importieren</source>
        <translation>Import project</translation>
    </message>
</context>
<context>
    <name>OpenProjectWindow</name>
    <message>
        <location filename="openprojectwindow.ui" line="14"/>
        <source>Projekt öffnen</source>
        <translation type="obsolete">Open project</translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="67"/>
        <source>New Column</source>
        <translation type="obsolete">New column</translation>
    </message>
</context>
<context>
    <name>ReportWindow</name>
    <message>
        <location filename="reportwindow.ui" line="79"/>
        <source>Datei</source>
        <translation type="obsolete">File</translation>
    </message>
</context>
</TS>
