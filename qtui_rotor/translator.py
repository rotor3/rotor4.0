# #######################################################################################################################
#
# This class is an extension of the QTranslator. The translation files, mainwindow_en.qm or mainwindow_fr.qm are loaded.
# The system language is the default language if available. If a translation is added, do it in
# Qt creator in the C++ code. perform a
#  >>> pylupdate5 QTUI_rotor.pro
# then modify the translation  in the .ts  file, preferably in Qt Linguist, perform an lrelease,
# then move the binaries (.qm) into the resources folder. Update the resource with
# C:\Users\...resources>pyrcc5 resource.qrc resource.py
#  Instructions at
#  https://blog.finxter.com/python-application-translation-with-qt-linguist/#Compilation_of_Translation_Files
#
#########################################################################################################################
import locale
import sys

from PySide2.QtCore import QTranslator




class Translator(QTranslator):
    def __init__(self):
        QTranslator.__init__(self)
        print("Translator __init__: ")
        try:
            if len(sys.argv) == 1:
                # the splitting gets only e.g. the 'en' of 'en_us'
                language = locale.getdefaultlocale()[0].split('_')[0]
                self.load(':/language/resources/' + "mainwindow_" + language)
                print("Getting locale ", language)
            else:
                self.load(':/language/resources/' + 'mainwindow_' + sys.argv[1])
                print("Loading default language German")

        except Exception as e:
            print("Could not load language" + e)
