import copy
import json

from PyQt5.QtCore import QFileInfo
from PyQt5.QtWidgets import QButtonGroup, QTableWidgetItem, QMessageBox
from qtpy import QtWidgets
from PyQt5 import QtCore, QtWidgets
from PyQt5.Qt import QDesktopServices

from dialog_window import DialogWindow
from project_handling.json_serialize import json_encode_project, json_decode_project
from project_handling.project import Project
from project_handling.project_handling import ProjectHandling
from qtui_connect.OpenProjectWindow import OpenProjectWindow
from rotation_generation.crop_rotations import CropRotation
from rotor_calculations.calculate_2 import Calculate2
from ui_logic.logic_free_generation import LogicFreeRotationGeneration
from ui_logic.logic_general import LogicGeneral

from ui_logic.logic_sidebar import LogicSidebar
from ui_logic.logic_tab_yield import LogicTabYield
from ui_logic.logic_tab_general import LogicTabGeneral
from ui_logic.logic_tab_manure import LogicTabManure
from ui_logic.logic_tab_report import LogicTabReport
from ui_logic.logic_tab_rotation import LogicTabRotation
from qtui_rotor.mainwindow import Ui_MainWindow
from ui_logic.ui_status import UiStatus
import logging
import os


# TODO neu einsortieren!!!! Alles manure
def set_manure_line_visible(self, index_manure, visible):
    self.lb_manure_list[index_manure].setVisible(visible)
    self.sb_manure_dry_mass_list[index_manure].setVisible(visible)
    self.sb_manure_n_content_list[index_manure].setVisible(visible)
    self.sb_manure_n_availability_list[index_manure].setVisible(visible)
    self.sb_manure_n_loss_list[index_manure].setVisible(visible)
    self.sb_manure_p_content_list[index_manure].setVisible(visible)
    self.sb_manure_k_content_list[index_manure].setVisible(visible)


def set_ui_status_manure(self, index, index_2):
    self.cb_manure_source_list[index][index_2].setVisible(
        self.ui_status.tab_rotation["cb_manure_source_visible"][index][index_2])
    self.cb_manure_specification_list[index][index_2].setVisible(
        self.ui_status.tab_rotation["cb_manure_specification_visible"][index][index_2])
    if self.ui_status.tab_rotation["manure_unit_ton"][index][index_2]:
        self.lb_manure_amount_list[index][index_2].setText("t/ha")
    else:
        self.lb_manure_amount_list[index][index_2].setText("m<sup>3</sup>/ha")


def cb_manure_type_selected(self, index, index_2):
    print("Entering MainWindow.cb_manure_type_changed")
    self.project.tab_rotation["manure_type"][index][index_2] = self.cb_manure_type_list[index][index_2].currentData()
    self.project, self.ui_status = LogicTabRotation.get_cb_manure_source(self.project, self.ui_status, index, index_2)
    self.project, self.ui_status = LogicTabRotation.get_cb_manure_specification(self.project, self.ui_status, index,
                                                                                index_2)
    self.project, self.ui_status = LogicTabRotation.get_sb_manure_amount_unit(self.project, self.ui_status, index,
                                                                              index_2)
    self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_source"][index][index_2],
                           self.cb_manure_source_list[index][index_2])
    self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_specification"][index][index_2],
                           self.cb_manure_specification_list[index][index_2])

    self.set_ui_status_manure(index, index_2)


def cb_manure_source_changed(self, index, index_2):
    print("Entering MainWindow.cb_manure_type_changed")
    self.project.tab_rotation["manure_source"][index][index_2] = self.cb_manure_source_list[index][
        index_2].currentData()
    self.project, self.ui_status = LogicTabRotation.get_cb_manure_source(self.project, self.ui_status, index, index_2)


def cb_manure_specification_selected(self, index, index_2):
    print("Entering MainWindow.cb_manure_specification_changed")
    self.project.tab_rotation["manure_specification"][index][index_2] = self.cb_manure_specification_list[index][
        index_2].currentData()
    self.project, self.ui_status = LogicTabRotation.get_cb_manure_specification(self.project, self.ui_status, index,
                                                                                index_2)


def cb_manure_source_selected(self, index, index_2):
    print("Entering MainWindow.cb_manure_source_changed")
    self.project.tab_rotation["manure_source"][index][index_2] = self.cb_manure_source_list[index][
        index_2].currentData()
    self.project, self.ui_status = LogicTabRotation.get_cb_manure_source(self.project, self.ui_status, index,
                                                                         index_2)


def bt_manure_delete(self, index, index_2):
    print("List after pop and append", self.ui_status.tab_rotation["qw_manure_visible"][index])
    self.ui_status.tab_rotation["qw_manure_visible"][index].pop(index_2)
    self.ui_status.tab_rotation["qw_manure_visible"][index].append(False)
    print("List after pop and append", self.ui_status.tab_rotation["qw_manure_visible"][index])

    self.project.tab_rotation["manure_id"][index].pop(index_2)
    self.project.tab_rotation["manure_id"][index].append(0)
    self.project.tab_rotation["manure_type"][index].pop(index_2)
    self.project.tab_rotation["manure_type"][index].append(0)
    self.project.tab_rotation["manure_spec"][index].pop(index_2)
    self.project.tab_rotation["manure_spec"][index].append(0)
    self.project.tab_rotation["manure_amount"][index].pop(index_2)
    self.project.tab_rotation["manure_amount"][index].append(0)
    self.set_visibility_manure_widgets()


def bt_next_manure(self, index, index_qw):
    self.ui_status.tab_rotation["qw_manure_visible"][index][index_qw + 1] = True

    self.set_visibility_manure_widgets()


def gb_manure_toggled(self, index):
    checked = self.gb_manure_list[index].isChecked()
    self.project.tab_rotation["manure"][index] = checked
    self.ui_status.tab_rotation["qw_manure_visible"][index][0] = checked
    print("CHECKED: ", checked)

    for i in range(0, len(self.qw_manure_list[index])):
        if checked:
            print("Index, i:", index, i, self.ui_status.tab_rotation["qw_manure_visible"][index][i])
            self.qw_manure_list[index][i].setVisible(self.ui_status.tab_rotation["qw_manure_visible"][index][i])
        else:
            self.qw_manure_list[index][i].setVisible(False)

    # tab manure visibility
    self.project, self.ui_status = LogicTabRotation.gb_manure_selected(self.project, self.ui_status)

    self.set_ui_status_tabs()


def sb_manure_amount(self, index, index_qw):
    self.project.tab_rotation["manure_amount"][index][index_qw] = self.sb_manure_amount_list[index][index_qw]


# ------------------GENERAL--------------------------------------------------------

def populate_combobox(self, recordset, combobox):
    print("Function populate_combobox", combobox.objectName())
    combobox.clear()
    if combobox in self.cb_crop_year_list:
        combobox.addItem("----select crop----")
    for row in recordset:
        # row[1] is name displayed, row[0] is item-data
        combobox.addItem(row[1], row[0])


def remove_item_from_combobox(self, index):
    print("entering remove_item_from_combobox")
    # self.ui_status.error["error"] = False
    # self.ui_status.error["remove_item"][index] = False
    self.project, self.ui_status = LogicTabRotation.reset_values_for_remove_item(self.project, self.ui_status,
                                                                                 index)
    self.cb_crop_year_list[index].removeItem(self.cb_crop_year_list[index].findData(
        self.project.tab_rotation["crop"][index]))


def setup_ui(self):
    print("entering setup_ui")
    # tabs are set in/ visible
    self.set_ui_status_tabs()

    self.setup_tab_general()
    self.setup_tabs_for_rotation_duration()
    '''the tab_rotation and the tab for the free generation are setup in self.setup_tabs_for_rotation_duration()'''
    # self.setup_tab_rotation()
    # self.setup_tab_free_generation()
    self.setup_tab_manure()
    # self.load_project_to_ui()
    self.load_project = False


def setup_tab_manure(self):
    print("entering setup_tab_manure")
    self.project = LogicTabManure.get_recordset_manure_type(self.project, self.language)
    self.project = LogicTabManure.get_recordset_solid_rotting(self.project, self.language)
    # self.populate_combobox(self.project.dict_all_tabs_combobox_items["cb_manure_type"], self.QTUI_rotor.cb_manure_solid_type)
    # self.populate_combobox(self.project.dict_all_tabs_combobox_items["cb_manure_type"], self.QTUI_rotor.cb_manure_liquid_type)
    #
    # self.populate_combobox(self.project.dict_all_tabs_combobox_items["cb_manure_solid_rotting"], self.QTUI_rotor.cb_manure_solid_rotting)


def setup_tab_epm(self):
    print("entering setup_tab_epm")
    # tillage is the same for any selection
    self.setting_up_tab_epm = True
    self.project = LogicTabYield.get_tillage_tab_epm(self.project, self.language)

    for index in range(0, self.rotation_duration):
        self.project = LogicTabYield.get_names_tab_yield(self.project, index, self.language)
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_epm_tillage"],
                               self.cb_tillage_list[index])
        self.lb_yield_amount_main_list[index].setText(self.project.tab_rotation["crop_name"][index])
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_epm"][index],
                               self.sb_yield_amount_main_list[index])
    self.setting_up_tab_epm = False


def setup_tab_general(self):
    print("entering setup_tab_general")
    # selcts the radiobutton for the assessment method
    self.rb_assessment_method_group.button(self.project.tab_general["assessment_method"]).setChecked(True)
    # comboboxes tab_general
    self.project = LogicTabGeneral.get_cb_soil_type(self.project, self.language)
    self.populate_combobox(self.project.all_tabs_combobox_items["cb_soil_type"], self.QTUI_rotor.cb_soil_type)


# sets the chosen number of crop GroupBoxes as active in the tabs Generation and Free Generation and epm
def setup_tabs_for_rotation_duration(self):
    print("entering setup_tabs_for_rotation_duration")
    for index in range(0, self.MAX_DURATION):
        self.gb_rotation_list[index].setHidden(True)
        self.qw_free_generation_crop_list[index].setHidden(True)
        self.qf_epm_list[index].setHidden(True)
    for index in range(0, self.rotation_duration):
        self.gb_rotation_list[index].setHidden(False)
        self.qw_free_generation_crop_list[index].setHidden(False)
        self.qf_epm_list[index].setHidden(False)
    if self.project.tab_general["assessment_method"] == 2:
        self.setup_tab_free_generation()
    else:
        # self.setup_tab_rotation_crop_year_comboboxes()
        self.setup_tab_rotation()


def setup_tab_rotation_crop_year_comboboxes(self):
    for index in range(0, self.rotation_duration):
        # Tab Generation: filling all comboboxes
        self.populate_crop_year_combobox(index)


def setup_tab_rotation_manure_widgets(self):
    for index in range(0, self.rotation_duration):
        for index_qw in range(0, 4):

            # turn qwidgets on or off according to status
            qw_visible = self.ui_status.tab_rotation["qw_manure_visible"][index][index_qw]
            self.qw_manure_list[index][index_qw].setVisible(qw_visible)
            if index_qw < 3:
                self.bt_next_manure_list[index][index_qw].setVisible(not qw_visible)
            if qw_visible is False:
                if index_qw == 0:
                    self.project.tab_rotation["manure"][index] = False
                    self.gb_manure_list[index].setChecked(False)
                    self.bt_next_manure_list[index][index_qw].setVisible(True)
                else:
                    self.bt_next_manure_list[index][index_qw - 1].setVisible(True)


def setup_tab_rotation(self):
    print("entering setup_tab_rotation")
    temp_project = copy.copy(self.project)
    self.project = LogicTabRotation.get_data_winterhardiness(self.project, self.language)

    self.project = LogicTabRotation.get_cb_manure_type(self.project, self.ui_status)
    for index in range(0, self.rotation_duration):
        # Tab Generation: filling all comboboxes
        self.populate_crop_year_combobox(index)
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_winterhardiness"],
                               self.cb_undersowing_winterhardiness_list[index])
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_winterhardiness"],
                               self.cb_covercrop_winterhardiness_list[index])
        # manure combobox
        # self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_liquid_solid_type"],
        #                        self.cb_manure_liquid_solid_list[index])
        # self.cb_manure_liquid_solid_list[index].setCurrentIndex(0)  # sets default to "no manure"
        # for qf in self.qf_yield_adjustment_list:
        self.qf_yield_adjustment_list[index].setVisible(self.QTUI_rotor.rb_rotation_assessment.isChecked())
        for index_2 in range(0, 4):
            # fill comboboxes - only the type needs to be filled, all others fill automatically
            self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_type"],
                                   self.cb_manure_type_list[index][index_2])

    self.project = temp_project

    # Tab Free Generation. filling all comboboxes


def setup_tab_free_generation(self):
    print("entering setup_tab_free_generation")
    # self.load_project = True
    for index in range(0, self.rotation_duration):
        self.project = LogicFreeRotationGeneration.get_recordset_cb_crop_types(self.project, index, self.language)
        self.project = LogicFreeRotationGeneration.get_recordset_cb_crops(self.project, index, self.language)
        self.populate_combobox(self.project.free_generation_temp["cb_crop_type_recordsets"][index],
                               self.cb_free_generation_crop_type_list[index])
        print("Cb name", self.cb_free_generation_crop_type_list[index].objectName())
        print("CURRENT INDEX CROP TYPE", self.cb_free_generation_crop_type_list[index].currentIndex())
        # self.cb_free_generation_crop_type_list[index].setCurrentIndex(2)
        self.populate_free_generation_crop_combobox(index)

    #     self.cb_free_generation_crop_selected(index)
    # # self.load_project = False
    self.load_project_to_tab_free_generation()


def setup_tab_report(self):
    print("entering setup_tab_report")
    self.QTUI_rotor.tw_report.clear()
    for index in range(1, (self.MAX_DURATION + 1)):
        if index <= self.rotation_duration:
            hidden = False
        else:
            hidden = True
        self.QTUI_rotor.tw_report.setColumnHidden(index, hidden)

    # populationg rows and columns
    self.project, self.ui_status = LogicTabReport.setup_report_table(self.project, self.ui_status, self.language)

    self.QTUI_rotor.tw_report.setRowCount(len(self.project.report["item_list"]))

    self.QTUI_rotor.tw_report.setItem(2, 2, QTableWidgetItem("test"))
    for row in range(0, len(self.project.report["item_list"])):
        for column in range(0, (self.rotation_duration + 1)):
            print("TABLE ITEM", self.project.report["item_list"][row][column])
            self.QTUI_rotor.tw_report.setItem(row, column,
                                              QTableWidgetItem(str(self.project.report["item_list"][row][column])))


def set_ui_status_calculation_possible(self):
    if self.ui_status.calculation_state["possible"]:
        self.QTUI_rotor.bt_calculate.setStyleSheet("background-color: #66dd00")
        self.QTUI_rotor.bt_calculate.setEnabled(True)
    else:
        self.QTUI_rotor.bt_calculate.setStyleSheet("")
        self.QTUI_rotor.bt_calculate.setEnabled(False)


def set_ui_status_tabs(self):
    print("entering set_ui_status_tabs")
    # visible_tabs_indices is needed for the previous and next buttons
    index = 0
    self.QTUI_rotor.tabs_rotor.setTabText(1, self.ui_status.tabs_state["rotation_tab_name"])
    visible_tabs_indices = []
    for key in self.ui_status.tabs:

        print("set_ui_status_tabs", index, key, self.ui_status.tabs[key])

        self.QTUI_rotor.tabs_rotor.setTabVisible(index, self.ui_status.tabs[key])
        if self.ui_status.tabs[key]:
            visible_tabs_indices.append(index)
        print("visible_tabs_indices", visible_tabs_indices)
        index += 1
    self.ui_status.tabs_state["update_tabs"] = False
    self.ui_status.tabs_state["visible_tabs_indices"] = visible_tabs_indices


def set_ui_status_undersowing(self, index):
    print("entering set_ui_status_undersowing index", index)
    # print("VORHER")
    #
    # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
    # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])

    index_next = (index + 1) % self.rotation_duration
    self.xb_undersowing_list[index].setChecked(self.project.tab_rotation["undersowing"][index])
    self.xb_undersowing_list[index].setEnabled(self.ui_status.tab_rotation["xb_undersowing_checkeable"][index])
    self.gb_undersowing_list[index].setEnabled(self.ui_status.tab_rotation["gb_undersowing_active"][index])

    self.xb_covercrop_list[index_next].setChecked(self.project.tab_rotation["covercrop"][index_next])
    self.xb_covercrop_list[index_next].setEnabled(
        self.ui_status.tab_rotation["xb_covercrop_checkeable"][index_next])
    self.gb_covercrop_list[index_next].setEnabled(
        self.ui_status.tab_rotation["gb_covercrop_active"][index_next])
    # self.xb_covercrop_harvest_list[index_next].setEnabled(self.ui_status.dict_tab_rotation["xb_covercrop_harvest_checkeable"][index_next])
    # the removal triggers a repopulation of the comboboxes, where remove_item is set to False
    # self.display_ui_status_errormessage(index)
    if self.ui_status.error["remove_item"][index]:
        self.ui_status.error["remove_item"][index] = False
        self.remove_item_from_combobox(index)

    self.display_ui_status_errormessage(index)
    # print("Ende set_ui_status_undersowing")
    #
    # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
    # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])


def set_ui_status_covercrop(self, index):
    print("entering set_ui_status_covercrop index", index)
    # print("VORHER")
    #
    # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
    # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])
    index_previous = (index - 1) % self.rotation_duration

    self.xb_covercrop_list[index].setChecked(self.project.tab_rotation["covercrop"][index])
    self.xb_covercrop_list[index].setEnabled(self.ui_status.tab_rotation["xb_covercrop_checkeable"][index])
    self.gb_covercrop_list[index].setEnabled(self.ui_status.tab_rotation["gb_covercrop_active"][index])

    self.xb_undersowing_list[index_previous].setChecked(
        self.project.tab_rotation["undersowing"][index_previous])
    self.xb_undersowing_list[index_previous].setEnabled(
        self.ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous])
    self.gb_undersowing_list[index_previous].setEnabled(
        self.ui_status.tab_rotation["gb_undersowing_active"][index_previous])
    # if an impossible combination occurs, the current crop is removed from the combobox
    # the removal triggers a repopulation of the comboboxes, where remove_item is set to False
    # self.display_ui_status_errormessage(index)
    if self.ui_status.error["remove_item"][index]:
        self.ui_status.error["remove_item"][index] = False
        self.remove_item_from_combobox(index)

    self.xb_covercrop_harvest_list[index].setEnabled(
        self.ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index])
    self.xb_covercrop_harvest_list[index].setChecked(self.project.tab_rotation["covercrop_harvest"][index])

    self.display_ui_status_errormessage(index)
    # print("Nachher")
    #
    # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
    # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])


def set_ui_status_strawharvest(self, index):
    print("entering set_ui_status_strawharvest")
    self.xb_strawharvest_list[index].setEnabled(
        self.ui_status.tab_rotation["xb_strawharvest_checkeable"][index])


def set_ui_status_early_termination(self, index):
    print("entering set_ui_early_termination")
    self.xb_early_tillage_list[index].setChecked(self.project.tab_rotation["early_termination"][index])
    self.xb_early_tillage_list[index].setEnabled(
        self.ui_status.tab_rotation["xb_early_termination_checkeable"][index])


def display_ui_status_errormessage(self, *args):
    print("entering display_ui_status_errormessage")
    # args is the index
    if self.ui_status.error["error"]:
        if args:
            print("args", args[0])
            year = args[0] % self.rotation_duration + 1
            messagebox_header = f"Information year {year}"

        else:
            messagebox_header = "Information"

        QtWidgets.QMessageBox.information(self, messagebox_header,
                                          self.ui_status.error["errormessage"], QtWidgets.QMessageBox.Ok)
        self.ui_status.error["error"] = False
        self.ui_status.error["errormessage"] = ""
    else:
        pass


def set_one_ui_status_tab_generation(self, index):
    print("entering set_one_ui_status_tab_generation index", index)
    self.qw_free_generation_crop_list[index].setVisible(self.ui_status.tab_rotation["qf_yield_adjustment_visible"])


def set_manure_tab_visible(self):
    self.ui_status.tabs["tab_organic_manure_visible"] = self.QTUI_rotor.gb_livestock_farming.isChecked()
    self.set_ui_status_tabs()


# --------------------MENUBAR FUNCTIONS AND SLOTS--------------------------------------------
def save_project_as(self):
    self.project.project_id = None
    self.save_project()


def save_project(self):
    print("Function save_project")
    self.logger.info("SAVING PROJECT...")
    # if self.verify_project() is False:
    #     return

    # project has not yet been saved to the db and therefore does not have an id yet
    if self.project.project_id is None:
        self.logger.info("ID is None")
        # change default project name and save

        # TODO: NEEDS TO BE VERIFIED THAT Project Name does not yet exist (Unique Constraint)

        save_window = DialogWindow()
        self.project = save_window.save_project_window(self.project)

    self.project, self.ui_status = ProjectHandling.save_project_to_db(self.project, self.ui_status)

    self.setWindowTitle("ROTOR 4.0  --  %(projectname)s" % {"projectname": self.project.project_name})
    self.display_ui_status_errormessage()


def open_project(self):
    print("Function  open_project")

    open_project_window = OpenProjectWindow()
    # open_project_window.show()
    open_project_window.exec_()

    if open_project_window.project.project_id is not None:
        self.project = open_project_window.project
        self.ui_status = open_project_window.ui_status

        self.display_ui_status_errormessage()
        self.load_project_to_ui()


def export_project(self):
    print("entering export_project")

    user_path = os.path.expanduser('~')
    documents_path = os.path.join(user_path, "Documents")

    export_name = QtWidgets.QFileDialog.getSaveFileName(self, "Export Rotor project", documents_path,
                                                        "ROTOR file (*.rotor)")
    print("JSON_EXPORT", export_name[0], "\n")
    print("Checkpoint 1")
    # try:
    file = open(export_name[0], 'w')
    # get project name from file dialog and set as projectname
    filename = QFileInfo(export_name[0]).fileName()
    self.project.project_name = filename.split(".")[0]
    print("Export; project", self.project)
    # json_project = ProjectHandling.json_export(self.project)
    json_project = json.dumps(self.project, default=json_encode_project)
    print("Export; json_project", json_project)
    print("Export; str(json_project)", str(json_project))
    file.write(str(json_project))
    file.close()
    # except:
    #     pass
    print("FILE CLOSED")


# TODO refactor import to project_handling
def import_project(self):
    print("entering import_project")
    user_path = os.path.expanduser('~')
    documents_path = os.path.join(user_path, "Documents")

    import_name = QtWidgets.QFileDialog.getOpenFileName(self, "Import Rotor project", documents_path,
                                                        "ROTOR file (*.rotor)")
    try:
        print("IMPORT FILENAME", import_name)
        file = open(import_name[0], 'r')
        print("FILE", file)
        project = Project()
        json_import = file.read()
        print("json_import", json_import)

        self.project = json_decode_project(json_import)
        print(project.project_name)
        file.close()

        self.load_project_to_ui()
    except:
        pass


def set_language(self, language):
    print("entering set_language")
    self.language = language

    self.ui_status.settings["changing_language"] = True
    temp_project = copy.copy(self.project)
    temp_ui_status = copy.copy(self.ui_status)

    self.setup_ui()
    self.project = temp_project
    self.ui_status = temp_ui_status
    self.set_ui_status_tabs()
    self.load_project_to_ui()

    self.ui_status.settings["changing_language"] = False


def show_manual(self):
    pass


# ---------------LOWER BAR FUNCTIONS AND SLOTS---------------------------------------------------------

def tab_forward(self):
    print("entering tab forward")
    current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()
    current_index = self.ui_status.tabs_state["visible_tabs_indices"].index(current_tab)
    next_tab = self.ui_status.tabs_state["visible_tabs_indices"][
        (current_index + 1) % len(self.ui_status.tabs_state["visible_tabs_indices"])]
    self.QTUI_rotor.tabs_rotor.setCurrentIndex(next_tab)


def tab_back(self):
    print("entering tab back")
    current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()

    current_index = self.ui_status.tabs_state["visible_tabs_indices"].index(current_tab)
    next_tab = self.ui_status.tabs_state["visible_tabs_indices"][
        (current_index - 1) % len(self.ui_status.tabs_state["visible_tabs_indices"])]
    self.QTUI_rotor.tabs_rotor.setCurrentIndex(next_tab)

    # TODO implement free generation reset


def reset_current_tab(self):
    print("entering reset_current_tab")
    current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()
    if current_tab == 0:
        self.reset_tab_general()
    elif current_tab == 1:
        self.reset_tabs_rotation()
    elif current_tab == 2:
        self.reset_tab_free_generation()
    elif current_tab == 3:
        self.reset_tab_manure()
    elif current_tab == 4:
        self.reset_tab_yield()


def reset_tab_manure(self):
    print("entering reset_tab_manure")
    self.project, self.ui_status = LogicSidebar.reset_manure(self.project, self.ui_status)
    self.load_project_to_tab_manure()


def reset_epm_selection(self):
    print("entering reset_epm_selection")
    self.project, self.ui_status = LogicSidebar.reset_epm_selection(self.project, self.ui_status)

    for index in range(0, self.rotation_duration):
        self.sb_yield_amount_main_list[index].setCurrentIndex(0)
        self.cb_tillage_list[index].setCurrentIndex(0)


def reset_tab_general(self):
    print("entering reset_tab_general")
    self.project, self.ui_status = LogicSidebar.reset_tab_general(self.project, self.ui_status)

    self.load_project_to_tab_general()


def bt_calculate_generate_rotations(self):
    print("Function calculate")
    '''This function generates the crop rotations and hands them over to the calculations for yield, N, P, K and humus'''
    self.save_project()
    if not self.project.tab_general["assessment_method"] == 1:
        QMessageBox.question(self, "Bis hierhin und nicht weiter...", "Hier fehlt noch ein bißchen was- sorry!",
                             QMessageBox.Ok | QMessageBox.Cancel)

        return

    if self.ui_status.tabs["tab_epm_selection_visible"] is False:
        print("Calculate: epm is False")
        crop_rotation = CropRotation(self.project, self.ui_status)
        if crop_rotation.ui_status.error["error"]:
            self.ui_status = crop_rotation.ui_status
        else:
            self.project = crop_rotation.project
            self.ui_status = crop_rotation.ui_status

            self.ui_status.tabs["tab_epm_selection_visible"] = True
            self.ui_status.tabs_state["update_tabs"] = True
            self.set_ui_status_tabs()
            self.QTUI_rotor.tabs_rotor.setCurrentIndex(4)
            self.setup_tab_yield()
            self.load_project_to_tab_yield()

    else:
        print("Calculate: epm is True")
        result = Calculate2(self.project, self.ui_status)
        self.project = result.project
        self.ui_status = result.ui_status

        self.ui_status.tabs["tab_report_visible"] = True
        self.ui_status.tabs_state["update_tabs"] = True
        self.set_ui_status_tabs()
        self.QTUI_rotor.tabs_rotor.setCurrentIndex(5)
        self.setup_tab_report()
        self.load_project_to_tab_report()

    self.display_ui_status_errormessage()
    print("Dict_tab_rotation", self.project.tab_rotation)
    print("RESULT", self.project.results)


# ----------------GENERAL TAB SLOTS--------------------------------------
def gb_precipitation_winter_toggled(self, value):
    self.project.tab_general["precipitation_winter_selected"] = value


def sb_precipitation_value_changed(self, value):
    self.project.tab_general["precipitation"] = value
    self.QTUI_rotor.sb_precipitation_winter.setMaximum(value)


def sb_precipitation_winter_value_changed(self, value):
    self.project.tab_general["precipitation_winter"] = value


def open_url(self):
    print("entering open_url")
    try:
        url = QtCore.QUrl('https://gis.uba.de/website/depo1/')
        QDesktopServices.openUrl(url)

    except Exception as e:
        QtWidgets.QMessageBox.warning(self, 'Open Url', 'Could not open URL.\n Error:\n{}'.format(str(e)))


def xb_red_area_toggled(self, red_area):
    # TODO §13a DüV, red areas should be considered in the calculations, so far, this bool has no effect
    self.project.tab_general["red_area"] = red_area


def get_rotation_duration(self):
    print("entering get_rotation_duration")
    # cast is necessary, because the combobox only knows strings
    self.rotation_duration = int(self.QTUI_rotor.cb_rotation_duration.currentText())
    self.project.tab_general["rotation_duration"] = self.rotation_duration
    self.setup_tabs_for_rotation_duration()

    self.logger.info("get_rotation_duration(): Rotation_duration changed to", self.rotation_duration)


def sb_atmospheric_n_input_value_changed(self, value):
    self.project.tab_general["atmospheric_n_input"] = value


def gb_soil_parameters_advanced_toggled(self, value):
    self.project.tab_general["soil_advanced_parameters_selected"] = value


def sb_soil_mineralisation_value_changed(self, value):
    self.project.tab_general["soil_mineralisation"] = value


def sb_soil_organic_matter_value_changed(self, value):
    self.project.tab_general["soil_organic_matter"] = value


def sb_soil_c_n_ratio_value_changed(self, value):
    self.project.tab_general["soil_c_n_ratio"] = value


def sb_soil_dry_density_value_changed(self, value):
    self.project.tab_general["soil_bulk_density"] = value


def sb_soil_stoneratio_value_changed(self, value):
    self.project.tab_general["soil_stoneratio"] = value


def sb_soil_topsoil_depth_value_changed(self, value):
    self.project.tab_general["soil_topsoil_depth"] = value


def rb_soc_selection(self, soc_method):
    self.project.tab_general["soc_method"] = soc_method


def rb_soil_index_toggled(self, value):
    self.project.tab_general["soil_quality_by_soil_index"] = value


# sets the soil type according to the soil rating index (Ackerzahl) - can be changed in database soil_quality_tb)
def set_cb_soil_type_from_soil_index(self, value):
    self.project.tab_general["soil_index"] = value
    if self.QTUI_rotor.sb_soil_index.isEnabled():
        LogicTabGeneral.set_soil_type_from_soil_index(self.project)
        self.QTUI_rotor.cb_soil_type.setCurrentIndex(
            self.QTUI_rotor.cb_soil_type.findData(self.project.tab_general["soil_index"]))


# sets the soil rating index according to the soil type - can be changed in database soil_quality_tb)
def set_sb_soil_index_from_soil_type(self):
    if self.QTUI_rotor.cb_soil_type.isEnabled():
        self.logger.info("cb_soil_type enabled")

        self.QTUI_rotor.sb_soil_index.setValue(self.QTUI_rotor.cb_soil_type.currentData())


def rb_assessment_selection(self, assessment_method):
    print("entering rb_assessment_selection")
    self.project.tab_general["assessment_method"] = assessment_method
    if self.project.tab_general["assessment_method"] == 1:
        self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)
        self.setup_tab_rotation_crop_year_comboboxes()
    elif self.project.tab_general["assessment_method"] == 3:
        self.setup_tab_rotation_crop_year_comboboxes()
    else:
        self.setup_tab_free_generation()
    print("ASSESSMENT METHOD ", self.project.tab_general["assessment_method"])
    if self.project.tab_general["assessment_method"] in [1, 3]:
        self.load_project_to_tab_rotation()
    else:
        self.load_project_to_tab_free_generation()

    self.project, self.ui_status = LogicTabGeneral.get_ui_status_assessment(self.project, self.ui_status)

    self.set_ui_status_tabs()


# -------------------ROTATION TAB FUNCTIONS-(Assessment and Intelligent Generation)-------------------------------

def cb_crop_selected(self, index):
    print("entering cb_crop_selected index", index)
    print("current data: ", self.cb_crop_year_list[index].currentData())
    '''this function writes the selected crop to the project. In the intelligent generation, every selection 
    requires the crop comboboxes to be repopulated. This leads to a renewed seelction and triggers this function - 
    therefore the bool self.generation_repopulating.'''

    if self.project.tab_general["assessment_method"] == 1:

        print('self.project.dict_tab_rotation["crop"][index]:', index, 'Crop:',
              self.project.tab_rotation["crop"][index])
        print("current data: ", self.cb_crop_year_list[index].currentData())

        if self.project.tab_rotation["crop"][index] != self.cb_crop_year_list[index].currentData() \
                and self.generation_repopulating is False:
            self.project.tab_rotation["crop"][index] = self.cb_crop_year_list[index].currentData()

            print("REPOPULATING ", index)

            index_range = (copy.copy(index) + 1) % self.rotation_duration
            print("INDEX_RANGE", index_range)

            for i in range(index_range, (index_range + self.rotation_duration)):
                # self.generation_repopulating = True
                index_repopulate = i % self.rotation_duration
                print("Repopulate Loop", index_repopulate)

                self.populate_crop_year_combobox(index_repopulate)
                # self.generation_repopulating = False

            if self.project.tab_rotation["crop"][index] is None:
                self.project, self.ui_status = LogicTabRotation.get_ui_status_of_none(self.project,
                                                                                      self.ui_status,
                                                                                      index)
            else:
                self.project, self.ui_status = LogicTabRotation.get_possible_undersowing_covercrop(self.project,
                                                                                                   self.ui_status,
                                                                                                   index)
                self.set_ui_status_covercrop(index)
                self.set_ui_status_undersowing(index)
                self.set_ui_status_strawharvest(index)
                self.set_ui_status_early_tillage(index)

    # assessment method assessment
    else:
        self.project.tab_rotation["crop"][index] = self.cb_crop_year_list[index].currentData()
        print('self.project.dict_tab_rotation["crop"][index]', self.project.tab_rotation["crop"][index])

        if self.project.tab_rotation["crop"][index] is None:
            self.project, self.ui_status = LogicTabRotation.get_ui_status_of_none(self.project, self.ui_status,
                                                                                  index)
        else:
            self.project, self.ui_status = LogicTabRotation.get_possible_undersowing_covercrop(self.project,
                                                                                               self.ui_status,
                                                                                               index)
        self.set_ui_status_covercrop(index)
        self.set_ui_status_undersowing(index)
        self.set_ui_status_strawharvest(index)
        self.set_ui_status_early_tillage(index)

    '''If a calculation is possible, the calculate button turns active and green'''
    self.project, self.ui_status = LogicGeneral.verify_project(self.project, self.ui_status)
    self.set_ui_status_calculation_possible()


def xb_early_termination_toggled(self, index):
    print("Function xb_early_termination_toggled")
    self.project.tab_rotation["early_termination"][index] = self.xb_early_tillage_list[index].isChecked()


def xb_strawharvest_toggled(self, index):
    print("Function xb_strawharvest_toggled")
    self.project.tab_rotation["strawharvest"][index] = self.xb_strawharvest_list[index].isChecked()

    self.logger.info("strawharvest", self.project.tab_rotation["strawharvest"])


def xb_covercrop_toggled(self, index):
    print("Function toggle_covercrop")
    self.project.tab_rotation["covercrop"][index] = self.xb_covercrop_list[index].isChecked()
    # self.project, self.ui_status = LogicTabRotation.get_ui_status_crop_year(self.project, self.ui_status, index)
    # TODO why was it not here?
    self.project, self.ui_status = LogicTabRotation.get_status_covercrop_toggled(self.project, self.ui_status, index)

    self.set_ui_status_covercrop(index)


# this function links the inputs of undersowing and covercrop_id 3, undersowing as covercrop
def sync_gb_undersowing_gb_covercrop(self, index):
    print("Function sync_covercrop")
    index_next = (index + 1) % self.rotation_duration
    index_previous = (index - 1) % self.rotation_duration
    if self.ui_status.tab_rotation["undersowing_sync"][index]:
        self.cb_covercrop_winterhardiness_list[index_next].setCurrentIndex(
            self.cb_undersowing_winterhardiness_list[index].currentIndex())
        self.sb_covercrop_legume_ratio_list[index_next].setValue(
            self.sb_undersowing_legume_ratio_list[index].value())
    elif self.ui_status.tab_rotation["covercrop_sync"][index]:
        self.cb_undersowing_winterhardiness_list[index_previous].setCurrentIndex(
            self.cb_covercrop_winterhardiness_list[index].currentIndex())
        self.sb_undersowing_legume_ratio_list[index_previous].setValue(
            self.sb_covercrop_legume_ratio_list[index].value())


def xb_undersowing_toggled(self, index):
    print("Function toggle_undersowing")
    self.project.tab_rotation["undersowing"][index] = self.xb_undersowing_list[index].isChecked()
    # self.project, self.ui_status = LogicTabRotation.get_ui_status_crop_year(self.project, self.ui_status, index)
    self.project, self.ui_status = LogicTabRotation.get_ui_status_undersowing_toggled(self.project, self.ui_status,
                                                                                      index)
    if self.project.tab_rotation["undersowing"][index]:
        self.sync_gb_undersowing_gb_covercrop(index)

    self.set_ui_status_undersowing(index)
    # if covercrop of the next year has already been adjusted, undersowing is synchronized


def activate_sb_manure_amount(self, index):
    print("Function activate_manure_amount_sb")
    # manure type is saved
    self.project.tab_rotation["manure_liquid_solid_type_id"][index] = self.cb_manure_liquid_solid_list[
        index].currentData()
    self.project, self.ui_status = LogicTabRotation.activate_manure_amount(self.project, self.ui_status, index,
                                                                           self.language)

    self.set_ui_status_manure_sb(index)
    self.set_ui_status_tabs()


def set_ui_status_manure_sb(self, index):
    print("enetering set_ui_status_manure_sb")
    # this is only for the tab_rotation, so assessment and
    # self.ui_status = LogicTabRotation.activate_manure_amount(self.project, self.ui_status, index, self.language)
    self.sb_manure_amount_liquid_list[index].setEnabled(
        self.ui_status.tab_rotation["sb_manure_amount_liquid_active"][index])
    self.sb_manure_amount_solid_list[index].setEnabled(
        self.ui_status.tab_rotation["sb_manure_amount_solid_active"][index])


def populate_crop_year_combobox(self, index):
    print("Function populate_crop_year_combobox")
    # recordsets are created in cb_crop_selected
    self.generation_repopulating = True
    self.project = LogicTabRotation.generate_crop_recordset(self.project, index, self.language)
    recordset = self.project.rotation_temp["cb_crop_recordsets"][index]
    self.populate_combobox(recordset, self.cb_crop_year_list[index])

    # sets the combobox to previously selected crop
    if self.project.tab_rotation["crop"][index] is not None:
        print("FROM CROP YEAR CB, crop at current index is not none")
        # asking if crop_id is in the recordset. This is necessary if the assessment method is switched from assessment to Intelligeneration
        if self.project.tab_rotation["crop"][index] in (item for sublist in recordset for item in sublist):
            self.cb_crop_year_list[index].setCurrentIndex(
                self.cb_crop_year_list[index].findData(self.project.tab_rotation["crop"][index]))
            print("if index...", index, "crop_id", self.project.tab_rotation["crop"][index])
        else:
            self.cb_crop_year_list[index].setCurrentIndex(0)  # Index 0 is ---select crop---

    self.generation_repopulating = False


# ------------FREE GENERATION TAB-------------------------------------
def cb_free_generation_crop_selected(self, index):
    print("entering cb_free_generation_crop_selected")
    if self.project.tab_general["assessment_method"] == 2:  # and self.load_project is False:
        if self.cb_free_generation_crop_list[index].currentData() is not None:
            self.project.tab_free_generation["crop"][index] = self.cb_free_generation_crop_list[index].currentData()
        print("Current Crop", self.project.tab_free_generation["crop"][index])

        self.project, self.ui_status = LogicGeneral.verify_project(self.project, self.ui_status)
        self.set_ui_status_calculation_possible()


def cb_free_generation_crop_type_selected(self, index):
    print("entering cb_free_generation_crop_type_selected",
          self.cb_free_generation_crop_type_list[index].currentIndex())
    # if self.load_project is False:
    self.project.tab_free_generation["crop_type_selection"][index] = self.cb_free_generation_crop_type_list[
        index].currentData()

    old_crop_id = copy.copy(self.project.tab_free_generation["crop"][index])

    self.populate_free_generation_crop_combobox(index)
    # if save crop id is available in the selected crop type, the crop remains, otherwise the first item in combobox is selected
    if old_crop_id in (item for row in self.project.free_generation_temp["cb_crop_recordsets"][index] for item in row):
        print("crop_type try")
        self.cb_free_generation_crop_list[index].setCurrentIndex(
            self.cb_free_generation_crop_list[index].findData(old_crop_id))


def populate_free_generation_crop_combobox(self, index):
    print("Function populate_free_generation_crop_combobox")
    self.project = LogicFreeRotationGeneration.get_recordset_cb_crops(self.project, index, self.language)
    self.populate_combobox(self.project.free_generation_temp["cb_crop_recordsets"][index],
                           self.cb_free_generation_crop_list[index])


# --------------------MANURE TAB---------------------------------------------------------------------
def set_manure_solid_defaults(self):
    print("Function set_manure_solid_defaults, load project is", self.load_project)
    if self.load_project is False:
        self.project.dict_tab_manure_2["manure_solid_type"] = self.QTUI_rotor.cb_manure_solid_type.currentData()
        self.project.dict_tab_manure_2["manure_solid_rotting"] = self.QTUI_rotor.cb_manure_solid_rotting.currentData()

        self.project, self.ui_status = LogicTabManure.set_tab_manure_solid_defaults(self.project, self.ui_status)
        self.load_project_to_tab_manure()
        self.display_ui_status_errormessage()
    else:
        pass


def set_manure_liquid_defaults(self):
    print("Function set_manure_liquid_defaults, load project is", self.load_project)
    if self.load_project is False:
        self.project.dict_tab_manure_2["manure_liquid_type"] = self.QTUI_rotor.cb_manure_liquid_type.currentData()

        self.project, self.ui_status = LogicTabManure.set_tab_manure_liquid_defaults(self.project, self.ui_status)
        self.load_project_to_tab_manure()
        self.display_ui_status_errormessage()
    else:
        pass


# -----------------TAB EPM----------------------------------------------------------------
def cb_epm_selected(self, index):
    if self.setting_up_tab_epm is False:
        self.project.tab_epm["environmental_protection_measures_id"][index] = self.sb_yield_amount_main_list[
            index].currentData()
        self.project, self.ui_status = LogicTabYield.get_correction_factor(self.project, self.ui_status, index)


def cb_tillage_selected(self, index):
    if self.setting_up_tab_epm is False:
        self.project.tab_epm["tillage_id"][index] = self.cb_tillage_list[index].currentData()


# ------------------------- RESETS----------------------------------------------------------

def reset_all(self):
    print("Function reset_all")
    self.project, self.ui_status = LogicSidebar.reset_all(self.project)
    self.set_ui_status_tabs()

    self.load_project_to_ui()


def reset_tabs_rotation(self):
    print("Function bt_reset_crop_rotation")
    self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)
    print("Crop list after reset in LogicSidebar", self.project.tab_rotation)
    self.setup_tab_rotation()
    self.load_project_to_tab_rotation()
    self.set_ui_status_tabs()


def reset_tab_free_generation(self):
    self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)
    self.setup_tab_free_generation()
    self.load_project_to_tab_free_generation()


def reset_manure(self):
    print("Function reset_manure")
    self.project, self.ui_status = LogicSidebar.reset_manure(self.project, self.ui_status)

    self.load_project_to_tab_manure()

    # reset comboboxes to their default values
    # self.QTUI_rotor.cb_manure_solid_type.setCurrentIndex(0)
    # self.QTUI_rotor.cb_manure_solid_rotting.setCurrentIndex(0)
    # self.QTUI_rotor.cb_manure_liquid_type.setCurrentIndex(0)


# ----------------------LOAD PROJECT---------------------------------------------------------
def load_project_to_ui(self):
    print("Function load_project_to_ui")
    self.logger.info("PROJECT in LOAD_PROJECT_TO_UI", self.project.tab_general, self.project.tab_rotation,
                     self.project.dict_tab_manure_2)
    # self.load_project = True to keep warnings from popping up
    self.load_project = True
    # self.setup_rotation_tab()
    # rotor_project = self.project
    self.setWindowTitle("ROTOR 4.0  --  %(projectname)s" % {"projectname": self.project.project_name})

    self.load_project_to_tab_general()
    if self.project.tab_general["assessment_method"] in [1, 3]:
        self.load_project_to_tab_rotation()
    else:
        self.load_project_to_tab_free_generation()

    if self.ui_status.tabs["tab_epm_selection_visible"]:
        self.setup_tab_yield()
        self.load_project_to_tab_yield()
    if self.ui_status.tabs["tab_organic_manure_visible"]:
        self.setup_tab_manure()
        self.load_project_to_tab_manure()
    if self.ui_status.tabs["tab_report_visible"]:
        self.load_project_to_tab_report()
    self.load_project = False


def load_project_to_tab_general(self):
    self.rotation_duration = self.project.tab_general["rotation_duration"]

    # adds all attributes from rotor_project to UI
    # if the duration is changed by reloading, the tabs generation and free generation are automatically setup
    self.QTUI_rotor.cb_rotation_duration.setCurrentText(str(self.project.tab_general["rotation_duration"]))
    self.QTUI_rotor.sb_precipitation.setValue(self.project.tab_general["precipitation"])

    self.QTUI_rotor.gb_precipitation_winter.setChecked(self.project.tab_general["precipitation_winter_selected"])
    self.QTUI_rotor.sb_precipitation_winter.setValue(self.project.tab_general["precipitation_winter"])

    self.rb_assessment_method_group.button(self.project.tab_general["assessment_method"]).setChecked(True)

    self.QTUI_rotor.sb_soil_index.setValue(self.project.tab_general["soil_index"])
    self.QTUI_rotor.rb_soil_index.setChecked(self.project.tab_general["soil_quality_by_soil_index"])
    self.QTUI_rotor.rb_soil_type.setChecked(not self.project.tab_general["soil_quality_by_soil_index"])

    self.QTUI_rotor.gb_soil_parameters_advanced.setChecked(
        self.project.tab_general["soil_advanced_parameters_selected"])
    if self.project.tab_general["soil_advanced_parameters_selected"]:
        self.QTUI_rotor.sb_soil_adv_mineralisation.setValue(self.project.tab_general["soil_mineralisation"])
        self.QTUI_rotor.sb_soil_adv_organic_matter.setValue(self.project.tab_general["soil_organic_matter"])
        self.QTUI_rotor.sb_soil_adv_c_n_ratio.setValue(self.project.tab_general["soil_c_n_ratio"])
        self.QTUI_rotor.sb_soil_adv_topsoil_depth.setValue(self.project.tab_general["soil_topsoil_depth"])
        self.QTUI_rotor.sb_soil_adv_stoneratio.setValue(self.project.tab_general["soil_stoneratio"])
        self.QTUI_rotor.sb_soil_adv_dry_density.setValue(self.project.tab_general["soil_bulk_density"])

    self.QTUI_rotor.sb_atmospheric_n_input.setValue(self.project.tab_general["atmospheric_n_input"])
    self.QTUI_rotor.xb_red_area.setChecked(self.project.tab_general["red_area"])

    self.rb_soc_method_group.button(self.project.tab_general["soc_method"]).setChecked(True)

    # ----------------TAB ROTATION-----------------------------------------


def load_project_to_tab_rotation(self):
    print("entering load_project_to_tab_rotation")
    # TODO Check if the rotation tab needs resetting first
    # self.setup_tab_rotation()
    # since the project comes with the selected crops, the comboboxes are automatically reloaded
    # if self.project.dict_tab_general["assessment_method"] in (1, 3):

    for index in range(0, self.rotation_duration):
        self.cb_crop_year_list[index].setCurrentIndex((self.cb_crop_year_list[index].findData(
            self.project.tab_rotation["crop"][index])))
        self.xb_early_tillage_list[index].setChecked(self.project.tab_rotation["early_termination"][index])
        self.xb_strawharvest_list[index].setChecked(self.project.tab_rotation["strawharvest"][index])
        self.xb_undersowing_list[index].setChecked(self.project.tab_rotation["undersowing"][index])
        self.cb_undersowing_winterhardiness_list[index].setCurrentIndex(
            self.project.tab_rotation["undersowing_winterhardiness"][index])
        self.sb_undersowing_legume_ratio_list[index].setValue(
            self.project.tab_rotation["undersowing_legume_ratio"][index])
        self.xb_covercrop_list[index].setChecked(self.project.tab_rotation["covercrop"][index])
        self.cb_covercrop_winterhardiness_list[index].setCurrentIndex(
            self.project.tab_rotation["covercrop_winterhardiness"][index])
        self.sb_covercrop_legume_ratio_list[index].setValue(
            self.project.tab_rotation["covercrop_legume_ratio"][index])
        self.xb_covercrop_harvest_list[index].setChecked(self.project.tab_rotation["covercrop_harvest"][index])

        if self.project.tab_rotation["manure"][index]:
            for index_2 in range(4):
                if self.project.tab_rotation["m_n_manure_values_id"] == 0:
                    break
                else:
                    self.cb_manure_type_list[index][index_2].setCurrentIndex(
                        self.cb_manure_type_list[index][index_2].findData(
                            self.project.tab_rotation["manure_type"][index][index_2]))
                    self.cb_manure_source_list[index][index_2].setCurrentIndex(
                        self.cb_manure_source_list[index][index_2].findData(
                            self.project.tab_rotation["manure_source"][index][index_2]))
                    self.cb_manure_specification_list[index][index_2].setCurrentIndex(
                        self.cb_manure_specification_list[index][index_2].findData(
                            self.project.tab_rotation["manure_specification"][index][index_2]))

        # self.sb_manure_amount_solid_list[index].setValue(self.project.tab_rotation["manure_amount_solid"][index])
        # self.sb_manure_amount_liquid_list[index].setValue(self.project.tab_rotation["manure_amount_liquid"][index])

        self.sb_yield_adjustment_list[index].setValue(self.project.tab_rotation["yield_adjustment"][index])


def load_project_to_tab_free_generation(self):
    print("entering load_project_to_tab_free_generation")
    # if self.load_project = True
    # TODO loading project to free rotation
    self.QTUI_rotor.gb_livestock_farming.setChecked(self.project.tab_free_generation["livestock_farming"])
    for index in range(0, self.rotation_duration):
        print("Current index crop_type", self.cb_free_generation_crop_type_list[index].findData(
            self.project.tab_free_generation["crop_type_selection"]))
        self.cb_free_generation_crop_type_list[index].setCurrentIndex(
            self.cb_free_generation_crop_type_list[index].findData(
                self.project.tab_free_generation["crop_type_selection"][index]))
        if self.project.tab_free_generation["crop"][index] is not None:
            self.cb_free_generation_crop_list[index].setCurrentIndex(
                self.cb_free_generation_crop_list[index].findData(self.project.tab_free_generation["crop"][index]))

    # self.load_project = False


def load_project_to_tab_epm(self):
    # the tab is only visible, if the project is verified and has proceeded to that selection
    if self.ui_status.tabs["tab_epm_selection_visible"]:
        # self.QTUI_rotor.tab_epm_selection.setVisible(True)
        for i in range(0, self.rotation_duration):
            self.sb_yield_amount_main_list[i].setCurrentIndex(self.sb_yield_amount_main_list[i].findData(
                self.project.tab_epm["environmental_protection_measures_id"][i]))

            self.cb_tillage_list[i].setCurrentIndex(
                self.cb_tillage_list[i].findData(str(self.project.tab_epm["tillage_id"][i])))


def load_project_to_tab_manure(self):
    # solid manure, the choice of manure type does not have any effect on values
    if self.ui_status.tabs["tab_organic_manure_visible"]:
        for i in len(self.project.tab_manure["m_n_manure_values_id"]):
            self.project, self.ui_status = LogicTabManure.get_manure_type_source_spec_by_id(self.project,
                                                                                            self.ui_status)

            self.xb_manure_default_list[i].setChecked(self.project.tab_rotation["use_std_manure_value"][i])

        # self.QTUI_rotor.cb_manure_solid_type.setCurrentIndex(
        #     self.QTUI_rotor.cb_manure_solid_type.findData(str(self.project.dict_tab_manure_2["manure_solid_type"])))
        # self.QTUI_rotor.cb_manure_solid_rotting.setCurrentIndex(
        #     self.QTUI_rotor.cb_manure_solid_rotting.findData(self.project.dict_tab_manure_2["manure_solid_rotting"]))

        # self.QTUI_rotor.sb_manure_solid_dry_matter.setValue(self.project.dict_tab_manure_2["manure_solid_dry_matter"])
        # self.QTUI_rotor.sb_manure_solid_n_content.setValue(self.project.dict_tab_manure_2["manure_solid_n_content"])
        # self.QTUI_rotor.sb_manure_solid_n_availability_percent.setValue(
        #     self.project.dict_tab_manure_2["manure_solid_n_availability_percent"])
        # self.QTUI_rotor.sb_manure_solid_n_loss_percent.setValue(
        #     self.project.dict_tab_manure_2["manure_solid_n_loss_percent"])
        # self.QTUI_rotor.sb_manure_solid_p_content.setValue(self.project.dict_tab_manure_2["manure_solid_p_content"])
        # self.QTUI_rotor.sb_manure_solid_k_content.setValue(self.project.dict_tab_manure_2["manure_solid_k_content"])
        #
        # # liquid manure, the choice of manure type does not have any effect on values
        # self.QTUI_rotor.cb_manure_liquid_type.setCurrentIndex(
        #     self.QTUI_rotor.cb_manure_liquid_type.findData(str(self.project.dict_tab_manure_2["manure_liquid_type"])))
        # self.QTUI_rotor.sb_manure_liquid_dry_matter.setValue(self.project.dict_tab_manure_2["manure_liquid_dry_matter"])
        # self.QTUI_rotor.sb_manure_liquid_n_content.setValue(self.project.dict_tab_manure_2["manure_liquid_n_content"])
        # self.QTUI_rotor.sb_manure_liquid_n_availability_percent.setValue(
        #     self.project.dict_tab_manure_2["manure_liquid_n_availability_percent"])
        # self.QTUI_rotor.sb_manure_liquid_n_loss_percent.setValue(
        #     self.project.dict_tab_manure_2["manure_liquid_n_loss_percent"])
        # self.QTUI_rotor.sb_manure_liquid_p_content.setValue(self.project.dict_tab_manure_2["manure_liquid_p_content"])
        # self.QTUI_rotor.sb_manure_liquid_k_content.setValue(self.project.dict_tab_manure_2["manure_liquid_k_content"])


def load_project_to_tab_report(self):
    if self.ui_status.tabs["tab_report_visible"]:
        pass
