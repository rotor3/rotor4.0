#ifndef OPENPROJECTWINDOW_H
#define OPENPROJECTWINDOW_H

#include <QDialog>

namespace Ui {
class OpenProjectWindow;
}

class OpenProjectWindow : public QDialog
{
    Q_OBJECT

public:
    explicit OpenProjectWindow(QWidget *parent = nullptr);
    ~OpenProjectWindow();

private:
    Ui::OpenProjectWindow *ui;
};

#endif // OPENPROJECTWINDOW_H
