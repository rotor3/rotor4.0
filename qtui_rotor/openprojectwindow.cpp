#include "openprojectwindow.h"
#include "ui_openprojectwindow.h"

OpenProjectWindow::OpenProjectWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenProjectWindow)
{
    ui->setupUi(this);
}

OpenProjectWindow::~OpenProjectWindow()
{
    delete ui;
}
