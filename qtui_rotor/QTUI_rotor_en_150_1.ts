<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB" sourcelanguage="en">
<context>
    <name>DialogWindow</name>
    <message>
        <location filename="dialogwindow.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <source>ROTOR 4.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <source>Dauer der Fruchtfolge</source>
        <translation type="unfinished">Rotation duration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.ui" line="11196"/>
        <source>3</source>
        <translation type="unfinished">3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.ui" line="11201"/>
        <source>4</source>
        <translation type="unfinished">4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.ui" line="11206"/>
        <source>5</source>
        <translation type="unfinished">5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.ui" line="11211"/>
        <source>6</source>
        <translation type="unfinished">6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.ui" line="11216"/>
        <source>7</source>
        <translation type="unfinished">7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <location filename="mainwindow.ui" line="11221"/>
        <source>8</source>
        <translation type="unfinished">8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="290"/>
        <location filename="mainwindow.ui" line="11226"/>
        <source>9</source>
        <translation type="unfinished">9</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="295"/>
        <location filename="mainwindow.ui" line="11231"/>
        <source>10</source>
        <translation type="unfinished">10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <source>Jahre</source>
        <translation type="unfinished">years</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="415"/>
        <source>Allgemein</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="509"/>
        <source>Art der Berechnung</source>
        <translation type="unfinished">Assessment type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="536"/>
        <source>Fruchtfolge bewerten</source>
        <translation type="unfinished">Assess existing crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="552"/>
        <location filename="mainwindow.ui" line="11092"/>
        <location filename="mainwindow.ui" line="11107"/>
        <source>Fruchtfolge generieren</source>
        <translation type="unfinished">Free generation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="584"/>
        <source>intelligente Fruchtfolgeerstellung</source>
        <translation type="unfinished">Intelligent generation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <source>Niederschläge</source>
        <translation type="unfinished">Precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <source>Winterniederschlag</source>
        <translation type="unfinished">Winter precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="753"/>
        <source>Jahresniederschlag</source>
        <translation type="unfinished">Annual precipitation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="881"/>
        <source>Bodenparameter</source>
        <translation type="unfinished">Soil parameters</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="932"/>
        <source>Erweiterte Bodenparameter</source>
        <translation type="unfinished">Advanced</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1002"/>
        <source>Steingehalt [%] (Partikel &gt; 2mm)</source>
        <translation type="unfinished">Stone ratio (particles &gt;2mm) </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1014"/>
        <source>organische Substanz [% C in TM]</source>
        <translation type="unfinished">Organic substance </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1113"/>
        <source>jährliche Mineralisierungsrate OBS in %</source>
        <translation type="unfinished">Annual rate of mineralization</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1125"/>
        <source>Oberboden [cm]</source>
        <translation type="unfinished">Topsoil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1137"/>
        <source>Trockenrohdichte im Oberboden [g/cm3]</source>
        <translation type="unfinished">Bulkdensity of topsoil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1149"/>
        <source>C-N Verhältnis der OBS</source>
        <translation type="unfinished">C:N ratio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1178"/>
        <location filename="mainwindow.ui" line="1205"/>
        <source>Bodenart</source>
        <translation type="unfinished">Soil type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1280"/>
        <source>Ackerzahl</source>
        <translation type="unfinished">Soil rating index</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Atmosphärischer Stickstoffeintrag</source>
        <translation type="unfinished">Atmospheric nitrogen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1358"/>
        <source>kg/ha und Jahr</source>
        <translation type="unfinished">kg/ha per year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1374"/>
        <source>Stickstoffemissionskarte des UBA</source>
        <translation type="unfinished">Nitrogen map of Germany</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1393"/>
        <source>&quot;Rotes Gebiet&quot;, hohe Nitratbelastung des Grundwassers</source>
        <translation type="unfinished">&quot;Red area&quot;, nitrate concentration in ground water &gt; 50mg/l</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1428"/>
        <source>Humusbilanzierungsmethode</source>
        <translation type="unfinished">Method of humus balance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1460"/>
        <source>nach Leithold, 1997</source>
        <translation type="unfinished">after Leithold, 1997</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1478"/>
        <source>nach VDLUFA, 2014</source>
        <translation type="unfinished">after VDLUFA, 2014</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1499"/>
        <source>nach Mosab, 2020</source>
        <translation type="unfinished">after Halwani, 2020</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1542"/>
        <source>Intelligente Fruchtfolgenerstellung</source>
        <translation type="unfinished">Intelligent generation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1598"/>
        <location filename="mainwindow.ui" line="16097"/>
        <source>1. Jahr</source>
        <translation type="unfinished">1st year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1647"/>
        <location filename="mainwindow.ui" line="2596"/>
        <location filename="mainwindow.ui" line="3545"/>
        <location filename="mainwindow.ui" line="4494"/>
        <location filename="mainwindow.ui" line="5443"/>
        <location filename="mainwindow.ui" line="6392"/>
        <location filename="mainwindow.ui" line="7341"/>
        <location filename="mainwindow.ui" line="8290"/>
        <location filename="mainwindow.ui" line="9239"/>
        <location filename="mainwindow.ui" line="10188"/>
        <source>---select crop---</source>
        <translation type="unfinished">--select crop--</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1655"/>
        <location filename="mainwindow.ui" line="2604"/>
        <location filename="mainwindow.ui" line="3553"/>
        <location filename="mainwindow.ui" line="4502"/>
        <location filename="mainwindow.ui" line="5451"/>
        <location filename="mainwindow.ui" line="6400"/>
        <location filename="mainwindow.ui" line="7349"/>
        <location filename="mainwindow.ui" line="8298"/>
        <location filename="mainwindow.ui" line="9247"/>
        <location filename="mainwindow.ui" line="10196"/>
        <source>früher Umbruch</source>
        <translation type="unfinished">Early termination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1668"/>
        <location filename="mainwindow.ui" line="2617"/>
        <location filename="mainwindow.ui" line="3566"/>
        <location filename="mainwindow.ui" line="4515"/>
        <location filename="mainwindow.ui" line="5464"/>
        <location filename="mainwindow.ui" line="6413"/>
        <location filename="mainwindow.ui" line="7362"/>
        <location filename="mainwindow.ui" line="8311"/>
        <location filename="mainwindow.ui" line="9260"/>
        <location filename="mainwindow.ui" line="10209"/>
        <source>Strohernte</source>
        <translation type="unfinished">Strawharvest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1675"/>
        <location filename="mainwindow.ui" line="2624"/>
        <location filename="mainwindow.ui" line="3573"/>
        <location filename="mainwindow.ui" line="4522"/>
        <location filename="mainwindow.ui" line="5471"/>
        <location filename="mainwindow.ui" line="6420"/>
        <location filename="mainwindow.ui" line="7369"/>
        <location filename="mainwindow.ui" line="8318"/>
        <location filename="mainwindow.ui" line="9267"/>
        <location filename="mainwindow.ui" line="10216"/>
        <location filename="mainwindow.ui" line="15268"/>
        <location filename="mainwindow.ui" line="15355"/>
        <location filename="mainwindow.ui" line="15441"/>
        <location filename="mainwindow.ui" line="15498"/>
        <location filename="mainwindow.ui" line="15584"/>
        <location filename="mainwindow.ui" line="15699"/>
        <location filename="mainwindow.ui" line="15785"/>
        <location filename="mainwindow.ui" line="15842"/>
        <location filename="mainwindow.ui" line="15936"/>
        <location filename="mainwindow.ui" line="15994"/>
        <source>Bodenbearbeitung</source>
        <translation type="unfinished">Tillage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1711"/>
        <location filename="mainwindow.ui" line="2660"/>
        <location filename="mainwindow.ui" line="3609"/>
        <location filename="mainwindow.ui" line="4558"/>
        <location filename="mainwindow.ui" line="5507"/>
        <location filename="mainwindow.ui" line="6456"/>
        <location filename="mainwindow.ui" line="7405"/>
        <location filename="mainwindow.ui" line="8354"/>
        <location filename="mainwindow.ui" line="9303"/>
        <location filename="mainwindow.ui" line="10252"/>
        <source>cm Pflugtiefe</source>
        <translation type="unfinished">tillage depth in cm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1735"/>
        <location filename="mainwindow.ui" line="2684"/>
        <location filename="mainwindow.ui" line="3633"/>
        <location filename="mainwindow.ui" line="4582"/>
        <location filename="mainwindow.ui" line="5531"/>
        <location filename="mainwindow.ui" line="6480"/>
        <location filename="mainwindow.ui" line="7429"/>
        <location filename="mainwindow.ui" line="8378"/>
        <location filename="mainwindow.ui" line="9327"/>
        <location filename="mainwindow.ui" line="10276"/>
        <source>Ertragsanpassung</source>
        <translation type="unfinished">Yield adjustment</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1758"/>
        <location filename="mainwindow.ui" line="2707"/>
        <location filename="mainwindow.ui" line="3656"/>
        <location filename="mainwindow.ui" line="4605"/>
        <location filename="mainwindow.ui" line="5554"/>
        <location filename="mainwindow.ui" line="6503"/>
        <location filename="mainwindow.ui" line="7452"/>
        <location filename="mainwindow.ui" line="8401"/>
        <location filename="mainwindow.ui" line="9350"/>
        <location filename="mainwindow.ui" line="10299"/>
        <source>dt/ha</source>
        <translation type="unfinished">t/ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1777"/>
        <location filename="mainwindow.ui" line="2726"/>
        <location filename="mainwindow.ui" line="3675"/>
        <location filename="mainwindow.ui" line="4624"/>
        <location filename="mainwindow.ui" line="5573"/>
        <location filename="mainwindow.ui" line="6522"/>
        <location filename="mainwindow.ui" line="7471"/>
        <location filename="mainwindow.ui" line="8420"/>
        <location filename="mainwindow.ui" line="9369"/>
        <location filename="mainwindow.ui" line="10318"/>
        <source>Untersaat</source>
        <translation type="unfinished">Undersowing</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1822"/>
        <location filename="mainwindow.ui" line="1955"/>
        <location filename="mainwindow.ui" line="2771"/>
        <location filename="mainwindow.ui" line="2904"/>
        <location filename="mainwindow.ui" line="3720"/>
        <location filename="mainwindow.ui" line="3853"/>
        <location filename="mainwindow.ui" line="4669"/>
        <location filename="mainwindow.ui" line="4802"/>
        <location filename="mainwindow.ui" line="5618"/>
        <location filename="mainwindow.ui" line="5751"/>
        <location filename="mainwindow.ui" line="6567"/>
        <location filename="mainwindow.ui" line="6700"/>
        <location filename="mainwindow.ui" line="7516"/>
        <location filename="mainwindow.ui" line="7649"/>
        <location filename="mainwindow.ui" line="8465"/>
        <location filename="mainwindow.ui" line="8598"/>
        <location filename="mainwindow.ui" line="9414"/>
        <location filename="mainwindow.ui" line="9547"/>
        <location filename="mainwindow.ui" line="10363"/>
        <location filename="mainwindow.ui" line="10496"/>
        <source>Winterhärte</source>
        <translation type="unfinished">Winterhardiness</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1839"/>
        <location filename="mainwindow.ui" line="1969"/>
        <location filename="mainwindow.ui" line="2788"/>
        <location filename="mainwindow.ui" line="2918"/>
        <location filename="mainwindow.ui" line="3737"/>
        <location filename="mainwindow.ui" line="3867"/>
        <location filename="mainwindow.ui" line="4686"/>
        <location filename="mainwindow.ui" line="4816"/>
        <location filename="mainwindow.ui" line="5635"/>
        <location filename="mainwindow.ui" line="5765"/>
        <location filename="mainwindow.ui" line="6584"/>
        <location filename="mainwindow.ui" line="6714"/>
        <location filename="mainwindow.ui" line="7533"/>
        <location filename="mainwindow.ui" line="7663"/>
        <location filename="mainwindow.ui" line="8482"/>
        <location filename="mainwindow.ui" line="8612"/>
        <location filename="mainwindow.ui" line="9431"/>
        <location filename="mainwindow.ui" line="9561"/>
        <location filename="mainwindow.ui" line="10380"/>
        <location filename="mainwindow.ui" line="10510"/>
        <source>hoch</source>
        <translation type="unfinished">high</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1844"/>
        <location filename="mainwindow.ui" line="1974"/>
        <location filename="mainwindow.ui" line="2793"/>
        <location filename="mainwindow.ui" line="2923"/>
        <location filename="mainwindow.ui" line="3742"/>
        <location filename="mainwindow.ui" line="3872"/>
        <location filename="mainwindow.ui" line="4691"/>
        <location filename="mainwindow.ui" line="4821"/>
        <location filename="mainwindow.ui" line="5640"/>
        <location filename="mainwindow.ui" line="5770"/>
        <location filename="mainwindow.ui" line="6589"/>
        <location filename="mainwindow.ui" line="6719"/>
        <location filename="mainwindow.ui" line="7538"/>
        <location filename="mainwindow.ui" line="7668"/>
        <location filename="mainwindow.ui" line="8487"/>
        <location filename="mainwindow.ui" line="8617"/>
        <location filename="mainwindow.ui" line="9436"/>
        <location filename="mainwindow.ui" line="9566"/>
        <location filename="mainwindow.ui" line="10385"/>
        <location filename="mainwindow.ui" line="10515"/>
        <source>mittel</source>
        <translation type="unfinished">medium</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1849"/>
        <location filename="mainwindow.ui" line="1979"/>
        <location filename="mainwindow.ui" line="2798"/>
        <location filename="mainwindow.ui" line="2928"/>
        <location filename="mainwindow.ui" line="3747"/>
        <location filename="mainwindow.ui" line="3877"/>
        <location filename="mainwindow.ui" line="4696"/>
        <location filename="mainwindow.ui" line="4826"/>
        <location filename="mainwindow.ui" line="5645"/>
        <location filename="mainwindow.ui" line="5775"/>
        <location filename="mainwindow.ui" line="6594"/>
        <location filename="mainwindow.ui" line="6724"/>
        <location filename="mainwindow.ui" line="7543"/>
        <location filename="mainwindow.ui" line="7673"/>
        <location filename="mainwindow.ui" line="8492"/>
        <location filename="mainwindow.ui" line="8622"/>
        <location filename="mainwindow.ui" line="9441"/>
        <location filename="mainwindow.ui" line="9571"/>
        <location filename="mainwindow.ui" line="10390"/>
        <location filename="mainwindow.ui" line="10520"/>
        <source>niedrig</source>
        <translation type="unfinished">low</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1867"/>
        <location filename="mainwindow.ui" line="1997"/>
        <location filename="mainwindow.ui" line="2816"/>
        <location filename="mainwindow.ui" line="2946"/>
        <location filename="mainwindow.ui" line="3765"/>
        <location filename="mainwindow.ui" line="3895"/>
        <location filename="mainwindow.ui" line="4714"/>
        <location filename="mainwindow.ui" line="4844"/>
        <location filename="mainwindow.ui" line="5663"/>
        <location filename="mainwindow.ui" line="5793"/>
        <location filename="mainwindow.ui" line="6612"/>
        <location filename="mainwindow.ui" line="6742"/>
        <location filename="mainwindow.ui" line="7561"/>
        <location filename="mainwindow.ui" line="7691"/>
        <location filename="mainwindow.ui" line="8510"/>
        <location filename="mainwindow.ui" line="8640"/>
        <location filename="mainwindow.ui" line="9459"/>
        <location filename="mainwindow.ui" line="9589"/>
        <location filename="mainwindow.ui" line="10408"/>
        <location filename="mainwindow.ui" line="10538"/>
        <source>Leguminosenanteil</source>
        <translation type="unfinished">Legume portion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1910"/>
        <location filename="mainwindow.ui" line="2859"/>
        <location filename="mainwindow.ui" line="3808"/>
        <location filename="mainwindow.ui" line="4757"/>
        <location filename="mainwindow.ui" line="5706"/>
        <location filename="mainwindow.ui" line="6655"/>
        <location filename="mainwindow.ui" line="7604"/>
        <location filename="mainwindow.ui" line="8553"/>
        <location filename="mainwindow.ui" line="9502"/>
        <location filename="mainwindow.ui" line="10451"/>
        <source>Zwischenfrucht</source>
        <translation type="unfinished">Covercrop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2025"/>
        <location filename="mainwindow.ui" line="2974"/>
        <location filename="mainwindow.ui" line="3923"/>
        <location filename="mainwindow.ui" line="4872"/>
        <location filename="mainwindow.ui" line="5821"/>
        <location filename="mainwindow.ui" line="6770"/>
        <location filename="mainwindow.ui" line="7719"/>
        <location filename="mainwindow.ui" line="8668"/>
        <location filename="mainwindow.ui" line="9617"/>
        <location filename="mainwindow.ui" line="10566"/>
        <source>Zwischenfrucht wird geerntet</source>
        <translation type="unfinished">Covercrop harvest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2038"/>
        <location filename="mainwindow.ui" line="2987"/>
        <location filename="mainwindow.ui" line="3936"/>
        <location filename="mainwindow.ui" line="4885"/>
        <location filename="mainwindow.ui" line="5834"/>
        <location filename="mainwindow.ui" line="6783"/>
        <location filename="mainwindow.ui" line="7732"/>
        <location filename="mainwindow.ui" line="8681"/>
        <location filename="mainwindow.ui" line="9630"/>
        <location filename="mainwindow.ui" line="10579"/>
        <source>Düngung</source>
        <translation type="unfinished">Manure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2091"/>
        <location filename="mainwindow.ui" line="2183"/>
        <location filename="mainwindow.ui" line="2275"/>
        <location filename="mainwindow.ui" line="2367"/>
        <location filename="mainwindow.ui" line="3040"/>
        <location filename="mainwindow.ui" line="3132"/>
        <location filename="mainwindow.ui" line="3224"/>
        <location filename="mainwindow.ui" line="3316"/>
        <location filename="mainwindow.ui" line="3989"/>
        <location filename="mainwindow.ui" line="4081"/>
        <location filename="mainwindow.ui" line="4173"/>
        <location filename="mainwindow.ui" line="4265"/>
        <location filename="mainwindow.ui" line="4938"/>
        <location filename="mainwindow.ui" line="5030"/>
        <location filename="mainwindow.ui" line="5122"/>
        <location filename="mainwindow.ui" line="5214"/>
        <location filename="mainwindow.ui" line="5887"/>
        <location filename="mainwindow.ui" line="5979"/>
        <location filename="mainwindow.ui" line="6071"/>
        <location filename="mainwindow.ui" line="6163"/>
        <location filename="mainwindow.ui" line="6836"/>
        <location filename="mainwindow.ui" line="6928"/>
        <location filename="mainwindow.ui" line="7020"/>
        <location filename="mainwindow.ui" line="7112"/>
        <location filename="mainwindow.ui" line="7785"/>
        <location filename="mainwindow.ui" line="7877"/>
        <location filename="mainwindow.ui" line="7969"/>
        <location filename="mainwindow.ui" line="8061"/>
        <location filename="mainwindow.ui" line="8734"/>
        <location filename="mainwindow.ui" line="8826"/>
        <location filename="mainwindow.ui" line="8918"/>
        <location filename="mainwindow.ui" line="9010"/>
        <location filename="mainwindow.ui" line="9683"/>
        <location filename="mainwindow.ui" line="9775"/>
        <location filename="mainwindow.ui" line="9867"/>
        <location filename="mainwindow.ui" line="9959"/>
        <location filename="mainwindow.ui" line="10632"/>
        <location filename="mainwindow.ui" line="10724"/>
        <location filename="mainwindow.ui" line="10816"/>
        <location filename="mainwindow.ui" line="10908"/>
        <source>Frühjahr</source>
        <translation type="unfinished">Spring</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2101"/>
        <location filename="mainwindow.ui" line="2193"/>
        <location filename="mainwindow.ui" line="2285"/>
        <location filename="mainwindow.ui" line="2377"/>
        <location filename="mainwindow.ui" line="3050"/>
        <location filename="mainwindow.ui" line="3142"/>
        <location filename="mainwindow.ui" line="3234"/>
        <location filename="mainwindow.ui" line="3326"/>
        <location filename="mainwindow.ui" line="3999"/>
        <location filename="mainwindow.ui" line="4091"/>
        <location filename="mainwindow.ui" line="4183"/>
        <location filename="mainwindow.ui" line="4275"/>
        <location filename="mainwindow.ui" line="4948"/>
        <location filename="mainwindow.ui" line="5040"/>
        <location filename="mainwindow.ui" line="5132"/>
        <location filename="mainwindow.ui" line="5224"/>
        <location filename="mainwindow.ui" line="5897"/>
        <location filename="mainwindow.ui" line="5989"/>
        <location filename="mainwindow.ui" line="6081"/>
        <location filename="mainwindow.ui" line="6173"/>
        <location filename="mainwindow.ui" line="6846"/>
        <location filename="mainwindow.ui" line="6938"/>
        <location filename="mainwindow.ui" line="7030"/>
        <location filename="mainwindow.ui" line="7122"/>
        <location filename="mainwindow.ui" line="7795"/>
        <location filename="mainwindow.ui" line="7887"/>
        <location filename="mainwindow.ui" line="7979"/>
        <location filename="mainwindow.ui" line="8071"/>
        <location filename="mainwindow.ui" line="8744"/>
        <location filename="mainwindow.ui" line="8836"/>
        <location filename="mainwindow.ui" line="8928"/>
        <location filename="mainwindow.ui" line="9020"/>
        <location filename="mainwindow.ui" line="9693"/>
        <location filename="mainwindow.ui" line="9785"/>
        <location filename="mainwindow.ui" line="9877"/>
        <location filename="mainwindow.ui" line="9969"/>
        <location filename="mainwindow.ui" line="10642"/>
        <location filename="mainwindow.ui" line="10734"/>
        <location filename="mainwindow.ui" line="10826"/>
        <location filename="mainwindow.ui" line="10918"/>
        <source>Herbst</source>
        <translation type="unfinished">Autumn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2122"/>
        <location filename="mainwindow.ui" line="2214"/>
        <location filename="mainwindow.ui" line="2306"/>
        <location filename="mainwindow.ui" line="2398"/>
        <location filename="mainwindow.ui" line="3071"/>
        <location filename="mainwindow.ui" line="3163"/>
        <location filename="mainwindow.ui" line="3255"/>
        <location filename="mainwindow.ui" line="3347"/>
        <location filename="mainwindow.ui" line="4020"/>
        <location filename="mainwindow.ui" line="4112"/>
        <location filename="mainwindow.ui" line="4204"/>
        <location filename="mainwindow.ui" line="4296"/>
        <location filename="mainwindow.ui" line="4969"/>
        <location filename="mainwindow.ui" line="5061"/>
        <location filename="mainwindow.ui" line="5153"/>
        <location filename="mainwindow.ui" line="5245"/>
        <location filename="mainwindow.ui" line="5918"/>
        <location filename="mainwindow.ui" line="6010"/>
        <location filename="mainwindow.ui" line="6102"/>
        <location filename="mainwindow.ui" line="6194"/>
        <location filename="mainwindow.ui" line="6867"/>
        <location filename="mainwindow.ui" line="6959"/>
        <location filename="mainwindow.ui" line="7051"/>
        <location filename="mainwindow.ui" line="7143"/>
        <location filename="mainwindow.ui" line="7816"/>
        <location filename="mainwindow.ui" line="7908"/>
        <location filename="mainwindow.ui" line="8000"/>
        <location filename="mainwindow.ui" line="8092"/>
        <location filename="mainwindow.ui" line="8765"/>
        <location filename="mainwindow.ui" line="8857"/>
        <location filename="mainwindow.ui" line="8949"/>
        <location filename="mainwindow.ui" line="9041"/>
        <location filename="mainwindow.ui" line="9714"/>
        <location filename="mainwindow.ui" line="9806"/>
        <location filename="mainwindow.ui" line="9898"/>
        <location filename="mainwindow.ui" line="9990"/>
        <location filename="mainwindow.ui" line="10663"/>
        <location filename="mainwindow.ui" line="10755"/>
        <location filename="mainwindow.ui" line="10847"/>
        <location filename="mainwindow.ui" line="10939"/>
        <source>t/ ha</source>
        <translation type="unfinished">t/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2129"/>
        <location filename="mainwindow.ui" line="2221"/>
        <location filename="mainwindow.ui" line="2313"/>
        <location filename="mainwindow.ui" line="2405"/>
        <location filename="mainwindow.ui" line="3078"/>
        <location filename="mainwindow.ui" line="3170"/>
        <location filename="mainwindow.ui" line="3262"/>
        <location filename="mainwindow.ui" line="3354"/>
        <location filename="mainwindow.ui" line="4027"/>
        <location filename="mainwindow.ui" line="4119"/>
        <location filename="mainwindow.ui" line="4211"/>
        <location filename="mainwindow.ui" line="4303"/>
        <location filename="mainwindow.ui" line="4976"/>
        <location filename="mainwindow.ui" line="5068"/>
        <location filename="mainwindow.ui" line="5160"/>
        <location filename="mainwindow.ui" line="5252"/>
        <location filename="mainwindow.ui" line="5925"/>
        <location filename="mainwindow.ui" line="6017"/>
        <location filename="mainwindow.ui" line="6109"/>
        <location filename="mainwindow.ui" line="6201"/>
        <location filename="mainwindow.ui" line="6874"/>
        <location filename="mainwindow.ui" line="6966"/>
        <location filename="mainwindow.ui" line="7058"/>
        <location filename="mainwindow.ui" line="7150"/>
        <location filename="mainwindow.ui" line="7823"/>
        <location filename="mainwindow.ui" line="7915"/>
        <location filename="mainwindow.ui" line="8007"/>
        <location filename="mainwindow.ui" line="8099"/>
        <location filename="mainwindow.ui" line="8772"/>
        <location filename="mainwindow.ui" line="8864"/>
        <location filename="mainwindow.ui" line="8956"/>
        <location filename="mainwindow.ui" line="9048"/>
        <location filename="mainwindow.ui" line="9721"/>
        <location filename="mainwindow.ui" line="9813"/>
        <location filename="mainwindow.ui" line="9905"/>
        <location filename="mainwindow.ui" line="9997"/>
        <location filename="mainwindow.ui" line="10670"/>
        <location filename="mainwindow.ui" line="10762"/>
        <location filename="mainwindow.ui" line="10854"/>
        <location filename="mainwindow.ui" line="10946"/>
        <source>Löschen</source>
        <translation type="unfinished">Delete</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2138"/>
        <location filename="mainwindow.ui" line="2230"/>
        <location filename="mainwindow.ui" line="2322"/>
        <location filename="mainwindow.ui" line="3087"/>
        <location filename="mainwindow.ui" line="3179"/>
        <location filename="mainwindow.ui" line="3271"/>
        <location filename="mainwindow.ui" line="4036"/>
        <location filename="mainwindow.ui" line="4128"/>
        <location filename="mainwindow.ui" line="4220"/>
        <location filename="mainwindow.ui" line="4985"/>
        <location filename="mainwindow.ui" line="5077"/>
        <location filename="mainwindow.ui" line="5169"/>
        <location filename="mainwindow.ui" line="5934"/>
        <location filename="mainwindow.ui" line="6026"/>
        <location filename="mainwindow.ui" line="6118"/>
        <location filename="mainwindow.ui" line="6883"/>
        <location filename="mainwindow.ui" line="6975"/>
        <location filename="mainwindow.ui" line="7067"/>
        <location filename="mainwindow.ui" line="7832"/>
        <location filename="mainwindow.ui" line="7924"/>
        <location filename="mainwindow.ui" line="8016"/>
        <location filename="mainwindow.ui" line="8781"/>
        <location filename="mainwindow.ui" line="8873"/>
        <location filename="mainwindow.ui" line="8965"/>
        <location filename="mainwindow.ui" line="9730"/>
        <location filename="mainwindow.ui" line="9822"/>
        <location filename="mainwindow.ui" line="9914"/>
        <location filename="mainwindow.ui" line="10679"/>
        <location filename="mainwindow.ui" line="10771"/>
        <location filename="mainwindow.ui" line="10863"/>
        <source>Weiterer Dünger</source>
        <translation type="unfinished">Add manure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2472"/>
        <location filename="mainwindow.ui" line="3421"/>
        <location filename="mainwindow.ui" line="4370"/>
        <location filename="mainwindow.ui" line="5319"/>
        <location filename="mainwindow.ui" line="6268"/>
        <location filename="mainwindow.ui" line="7217"/>
        <location filename="mainwindow.ui" line="8163"/>
        <location filename="mainwindow.ui" line="9157"/>
        <location filename="mainwindow.ui" line="10103"/>
        <location filename="mainwindow.ui" line="11055"/>
        <source>TM-Aufwuchs dt/ha</source>
        <translatorcomment>!!Stimmt nicht, war &quot;Aufwuchs&quot;</translatorcomment>
        <translation type="unfinished">dry-mass yield</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2514"/>
        <location filename="mainwindow.ui" line="3463"/>
        <location filename="mainwindow.ui" line="4412"/>
        <location filename="mainwindow.ui" line="5361"/>
        <location filename="mainwindow.ui" line="6310"/>
        <location filename="mainwindow.ui" line="7259"/>
        <location filename="mainwindow.ui" line="8205"/>
        <location filename="mainwindow.ui" line="9109"/>
        <location filename="mainwindow.ui" line="10055"/>
        <location filename="mainwindow.ui" line="11007"/>
        <source>N2-Fixierung kg N/ha</source>
        <translation type="unfinished">N2 fixed in kg N/ ha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2547"/>
        <location filename="mainwindow.ui" line="16102"/>
        <source>2. Jahr</source>
        <translation type="unfinished">2nd year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3496"/>
        <location filename="mainwindow.ui" line="16107"/>
        <source>3. Jahr</source>
        <translation type="unfinished">3rd year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4445"/>
        <location filename="mainwindow.ui" line="16112"/>
        <source>4. Jahr</source>
        <translation type="unfinished">4th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5394"/>
        <location filename="mainwindow.ui" line="16117"/>
        <source>5. Jahr</source>
        <translation type="unfinished">5th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6343"/>
        <location filename="mainwindow.ui" line="16122"/>
        <source>6. Jahr</source>
        <translation type="unfinished">6th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7292"/>
        <location filename="mainwindow.ui" line="16127"/>
        <source>7. Jahr</source>
        <translation type="unfinished">7th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="8241"/>
        <location filename="mainwindow.ui" line="16132"/>
        <source>8. Jahr</source>
        <translation type="unfinished">8th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="9190"/>
        <location filename="mainwindow.ui" line="16137"/>
        <source>9. Jahr</source>
        <translation type="unfinished">9th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="10139"/>
        <source>10. Jahr</source>
        <translation type="unfinished">10th year</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11160"/>
        <source>Maximale Anzahl der Ergebnisse </source>
        <translation type="unfinished">Maximum number of results</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11186"/>
        <source>1</source>
        <translation type="unfinished">1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11191"/>
        <source>2</source>
        <translation type="unfinished">2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11244"/>
        <source>Betrieb mit Viehhaltung</source>
        <translation type="unfinished">Livestock holding</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11284"/>
        <source>Auswahl der Tierart</source>
        <translation type="unfinished">Livestock</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11298"/>
        <source>Auswahl der Düngerarten</source>
        <translation type="unfinished">Manure types</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11334"/>
        <source>Auswahl der Feldfrüchte</source>
        <translation type="unfinished">Selection of crops</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11374"/>
        <source>Feldfrucht</source>
        <translation type="unfinished">Crop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11400"/>
        <source>Fruchtgruppe</source>
        <translation type="unfinished">Crop type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11425"/>
        <source>Feldrucht 1</source>
        <translation type="unfinished">Crop 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11467"/>
        <location filename="mainwindow.ui" line="11520"/>
        <location filename="mainwindow.ui" line="11573"/>
        <location filename="mainwindow.ui" line="11626"/>
        <location filename="mainwindow.ui" line="11679"/>
        <location filename="mainwindow.ui" line="11732"/>
        <location filename="mainwindow.ui" line="11788"/>
        <location filename="mainwindow.ui" line="11841"/>
        <location filename="mainwindow.ui" line="11894"/>
        <location filename="mainwindow.ui" line="11947"/>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11481"/>
        <source>Feldrucht 2</source>
        <translation type="unfinished">Crop 2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11534"/>
        <source>Feldrucht 3</source>
        <translation type="unfinished">Crop 3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11587"/>
        <source>Feldrucht 4</source>
        <translation type="unfinished">Crop 4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11640"/>
        <source>Feldrucht 5</source>
        <translation type="unfinished">Crop 5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11693"/>
        <source>Feldrucht 6</source>
        <translation type="unfinished">Crop 6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11746"/>
        <source>Feldrucht 7</source>
        <translation type="unfinished">Crop 7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11802"/>
        <source>Feldrucht 8</source>
        <translation type="unfinished">Crop 8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11855"/>
        <source>Feldrucht 9 </source>
        <translation type="unfinished">Crop 9</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11908"/>
        <source>Feldrucht 10</source>
        <translation type="unfinished">Crop 10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="11996"/>
        <location filename="mainwindow.ui" line="16231"/>
        <source>Zurück</source>
        <translation type="unfinished">Back</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12009"/>
        <source>Fruchtfolge zurücksetzen</source>
        <translation type="unfinished">Reset crop rotation</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12016"/>
        <location filename="mainwindow.ui" line="16238"/>
        <source>Weiter</source>
        <translation type="unfinished">Next</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12036"/>
        <location filename="mainwindow.ui" line="16271"/>
        <source>Berechnen</source>
        <translation type="unfinished">Start</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12050"/>
        <source>Dünger</source>
        <translation type="unfinished">Manure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="12101"/>
        <location filename="mainwindow.ui" line="12230"/>
        <location filename="mainwindow.ui" line="12494"/>
        <location filename="mainwindow.ui" line="12810"/>
        <location filename="mainwindow.ui" line="12886"/>
        <location filename="mainwindow.ui" line="13365"/>
        <location filename="mainwindow.ui" line="13372"/>
        <location filename="mainwindow.ui" line="13455"/>
        <location filename="mainwindow.ui" line="13640"/>
        <location filename="mainwindow.ui" line="13754"/>
        <location filename="mainwindow.ui" line="13818"/>
        <location filename="mainwindow.ui" line="13825"/>
        <location filename="mainwindow.ui" line="14022"/>
        <location filename="mainwindow.ui" line="14148"/>
        <location filename="mainwindow.ui" line="14451"/>
        <location filename="mainwindow.ui" line="14508"/>
        <location filename="mainwindow.ui" line="14515"/>
        <location filename="mainwindow.ui" line="14790"/>
        <location filename="mainwindow.ui" line="14873"/>
        <location filename="mainwindow.ui" line="15039"/>
        <source>TextLabel</source>
        <translation type="unfinished">label</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="13487"/>
        <source>N-Verfügbarkeit %</source>
        <translation type="unfinished">N-availability %</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="13570"/>
        <source>N-Ausbringungsverluste %</source>
        <translation type="unfinished">N-loss %</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="13933"/>
        <source>K-Gehalt kg/t FM</source>
        <translation type="unfinished">K-content in kg K/ t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14400"/>
        <source>Trockenmasse</source>
        <translation type="unfinished">Dry mass</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14566"/>
        <source>P-Gehalt kg/t FM</source>
        <translation type="unfinished">P-content in kg/t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14764"/>
        <source>Standardwerte nutzen</source>
        <translation type="unfinished">Use standardvalues</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14880"/>
        <source>N-Gehalt in kg/t FM</source>
        <translation type="unfinished">N-content in kg/t dm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15196"/>
        <source>Umweltschutzmaßnahmen</source>
        <translation type="unfinished">Encironmental protection measures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15238"/>
        <location filename="mainwindow.ui" line="15310"/>
        <location filename="mainwindow.ui" line="15396"/>
        <location filename="mainwindow.ui" line="15482"/>
        <location filename="mainwindow.ui" line="15568"/>
        <location filename="mainwindow.ui" line="15654"/>
        <location filename="mainwindow.ui" line="15740"/>
        <location filename="mainwindow.ui" line="15826"/>
        <location filename="mainwindow.ui" line="15906"/>
        <location filename="mainwindow.ui" line="15978"/>
        <source>Kultur1</source>
        <translation type="unfinished">crop1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="15247"/>
        <location filename="mainwindow.ui" line="15326"/>
        <location filename="mainwindow.ui" line="15412"/>
        <location filename="mainwindow.ui" line="15520"/>
        <location filename="mainwindow.ui" line="15606"/>
        <location filename="mainwindow.ui" line="15670"/>
        <location filename="mainwindow.ui" line="15756"/>
        <location filename="mainwindow.ui" line="15864"/>
        <location filename="mainwindow.ui" line="15915"/>
        <location filename="mainwindow.ui" line="16016"/>
        <source>Umweltschutzmaßnahme</source>
        <translation type="unfinished">Environmental protection measure</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16046"/>
        <source>Bericht</source>
        <translation type="unfinished">Report</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16142"/>
        <source>New Column</source>
        <translation type="unfinished">New column</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16147"/>
        <source>Saldo</source>
        <translation type="unfinished">Balance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16198"/>
        <source>Tab zurücksetzen</source>
        <translation type="unfinished">Reset tab</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16211"/>
        <source>alles zurücksetzen</source>
        <translation type="unfinished">Reset all</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16278"/>
        <location filename="mainwindow.ui" line="16337"/>
        <source>Speichern</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16300"/>
        <source>Datei</source>
        <translation type="unfinished">File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16310"/>
        <source>Sprache</source>
        <translation type="unfinished">Language</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16318"/>
        <source>Hilfe</source>
        <translation type="unfinished">Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16324"/>
        <source>Über Rotor</source>
        <translation type="unfinished">About Rotor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16345"/>
        <source>Speichern als</source>
        <translation type="unfinished">Save as</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16350"/>
        <source>Deutsch</source>
        <translation type="unfinished">German</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16355"/>
        <source>English</source>
        <translation type="unfinished">English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16360"/>
        <source>Francais</source>
        <translation type="unfinished">French</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16365"/>
        <source>Handbuch</source>
        <translation type="unfinished">Manual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16370"/>
        <source>Projekt öffnen</source>
        <translation type="unfinished">Open project</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16375"/>
        <source>Projekt exportieren</source>
        <translation type="unfinished">Export project</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16380"/>
        <source>Lizenz</source>
        <translation type="unfinished">License</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16385"/>
        <source>Versionen</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="16390"/>
        <source>Projekt importieren</source>
        <translation type="unfinished">Import project</translation>
    </message>
</context>
<context>
    <name>OpenProjectWindow</name>
    <message>
        <location filename="openprojectwindow.ui" line="14"/>
        <source>Projekt öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="67"/>
        <source>New Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="72"/>
        <source>Projekt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="77"/>
        <source>Letzte Änderung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="100"/>
        <source>Öffnen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="openprojectwindow.ui" line="107"/>
        <source>Abbrechen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReportWindow</name>
    <message>
        <location filename="reportwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="30"/>
        <source>Fruchtfolgebilanzen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="59"/>
        <source>Beenden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="79"/>
        <source>Datei</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="90"/>
        <source>PDF Exportieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="95"/>
        <source>Textdatei exportieren</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="reportwindow.ui" line="100"/>
        <source>Drucken</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
