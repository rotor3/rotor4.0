import os

from PySide2.QtCore import QFileInfo
from PySide2 import QtWidgets

from project_handling.project_handling import ProjectHandling


class ProjectExport:
    def project_export(self):
        user_path = os.path.expanduser('~')
        documents_path = os.path.join(user_path, "Documents")

        export_name = QtWidgets.QFileDialog.getSaveFileName(self, "Export Rotor project", documents_path,
                                                            "ROTOR file (*.rotor)")
        print("JSON_EXPORT", export_name[0], "\n")
        print("Checkpoint 1")
        file = open(export_name[0], 'w')
        # get project name from file dialog and set as projectname
        filename = QFileInfo(export_name[0]).fileName()
        self.project.project_name = filename.split(".")[0]
        json_project = ProjectHandling.json_export(self.project)

        file.write(str(json_project))
        file.close()
        print("FILE CLOSED")