from datetime import datetime
# from qtpy import QtWidgets
from PySide2 import QtCore, QtGui, QtWidgets
import logging
import pandas as pd
import numpy as np



class Project:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        # Timestamp is used for unique project name, in case no other name is assigned
        self.project_name = str(datetime.now())
        self.project_last_changed = datetime.now()
        self.project_id = None
        self.logger.info("Project created: Name: ")
        self.user_id = 1

        # TODO all lists --> Cython list comprehension

        # these are default settings of the interface. These settings are also used for reset and reset all
        self.tab_general = {"production_system_id": 2,
                            "rotation_duration": 3,
                            "precipitation": 400,
                            "precipitation_winter_selected": False,
                            "precipitation_winter": 0,
                            "soil_index": 30,
                            "soil_quality_by_soil_index": True,
                            "assessment_method": 1,
                            "soil_advanced_parameters_selected": False,
                            "soil_mineralisation_rate": 2.0,
                            "soil_organic_matter": .9,
                            "soil_c_n_ratio": 11,
                            "soil_topsoil_depth": 1.5,
                            "soil_stoneratio": 0,
                            "soil_bulk_density": 30,
                            "atmospheric_n_input": 12,
                            "red_area": False,
                            "overall_yield_adjustment": 0,
                            "fine_soil": 0,
                            "n_mineralisation_per_year": 0
                            }

        self.tab_rotation = {"crop_id": [None, None, None, None, None, None, None, None, None, None],
                             "crop_name": [None, None, None, None, None, None, None, None, None, None],
                             "early_tillage": [False, False, False, False, False, False, False, False, False,
                                                   False],
                             "main_legume_ratio": [50, 50, 50, 50, 50, 50, 50, 50, 50, 50],
                             "green_fodder": [100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
                             "hay": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             "silage": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             "crop_type_id": [None, None, None, None, None, None, None, None, None, None],
                             "tillage_id": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

                             "strawharvest": [False, False, False, False, False, False, False, False, False, False],
                             "undersowing": [False, False, False, False, False, False, False, False, False, False],
                             "undersowing_winterhardiness": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             "undersowing_legume_ratio": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                             "covercrop": [False, False, False, False, False, False, False, False, False, False],
                             "covercrop_id": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                             "covercrop_winterhardiness": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             "covercrop_legume_ratio": [10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
                             "covercrop_harvest": [False, False, False, False, False, False, False, False, False,
                                                   False],
                             # TODO the same as ["manure_used][index][0] !=0
                             "manure": [False, False, False, False, False, False, False, False,
                                        False, False],  # gb_manure is checked
                             "m_n_manures_id": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                                [0, 0, 0, 0],
                                                [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             # 0 does not exist in the database, it means no manure selected
                             "manure_type": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                             [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             "manure_source": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                               [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             "manure_specification": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                                      [0, 0, 0, 0],
                                                      [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                                      [0, 0, 0, 0]],
                             # "manure_specification": np.zeros((10, 4)),
                             "manure_amount": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                               [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             "dry_mass_minimum": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                                  [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             "dry_mass_maximum": [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0],
                                                  [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             "manure_used": [[False, False, False, False], [False, False, False, False],
                                              [False, False, False, False], [False, False, False, False],
                                              [False, False, False, False], [False, False, False, False],
                                              [False, False, False, False], [False, False, False, False],
                                              [False, False, False, False], [False, False, False, False]]
                             }

        self.tab_free_generation = {"crop": [None, None, None, None, None, None, None, None, None, None],
                                    "crop_id": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                    "crop_type_id": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                    "crop_type_selection": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                    "cb_generate_crop_recordset_1": [],
                                    "cb_generate_crop_recordset": [],
                                    "cb_generate_crop_type_1": [],
                                    "cb_generate_crop_type": [],
                                    "livestock_farming": False}

        self.tab_manure = {"m_n_manures_ids": [],
                           "m_n_manure_label": [],
                           "df_manure_labels": None,
                           "manure_standard_values": [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
                           "manure_values": [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
                           }

        self.tab_yield = {"yield_crop_coefficients": {},
                          "yield_amount_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          "yield_amount_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          "yield_legume_green_fodder_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          "yield_legume_hay_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          "yield_legume_silage_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          "yield_amount_catch_crop_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}


        self.tab_epm = {"environmental_protection_measures_id": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # standard as default
                        "tillage_id": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "yield_correction_factor": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}

        self.all_tabs_combobox_items = {  # all comboboxes in tab_generation
            "cb_soil_type": [],
            "cb_winterhardiness": [],
            # tab manure
            "cb_manure_type": [],
            "cb_manure_source": [[[], [], [], []], [[], [], [], []], [[], [], [], []],
                                 [[], [], [], []], [[], [], [], []], [[], [], [], []],
                                 [[], [], [], []], [[], [], [], []], [[], [], [], []],
                                 [[], [], [], []]],
            "cb_manure_specification": [[[], [], [], []], [[], [], [], []], [[], [], [], []],
                                        [[], [], [], []], [[], [], [], []], [[], [], [], []],
                                        [[], [], [], []], [[], [], [], []], [[], [], [], []],
                                        [[], [], [], []]],
            "cb_manure_solid_rotting": [],
            # tab epm
            "cb_epm": [[], [], [], [], [], [], [], [], [], []],
            "cb_tillage": [],
            "cb_manure_type_old": []}

        self.rotation_temp = {"crop_type_id": [None, None, None, None, None, None, None, None, None, None],
                              "cb_crop_recordsets": [[], [], [], [], [], [], [], [], [], []],
                              "undersowing": [[], [], [], [], [], [], [], [], [], []],
                              "covercrop_ids": [[], [], [], [], [], [], [], [], [], []]}

        self.free_generation_temp = {"crop_type_id": [None, None, None, None, None, None, None, None, None, None],
                                     "cb_crop_recordsets": [[], [], [], [], [], [], [], [], [], []],
                                     "cb_crop_type_recordsets": [[], [], [], [], [], [], [], [], [], []],

                                     }

        self.result_general = {"whc_rz": 0,  # comes from soil_quality_tb according to soil_raing_index
                               "soil_rinsing": 0,  # Durchwaschungshäufigkeit
                               "precipitation_level": 0,  # probably not necessary
                               "precipitation_factor": 0,  # is the precipitation_level/ 10
                               "n_mineralisation_per_year": 0, # nminpa
                                }

        self.results_crop_rotations_free_generation = {"list_of_crop_rotations": [],
                                                        "results": []  # the dictionaries self.results are stored in this list
                                                       }
        self.db_data = {"key_figures": dict(),
                        "n_values": dict(),
                        "weed_infestation": dict()

        }

        self.results = {"crop_rotation": [],  # contains all cpas, crop info and relevant inputs from GUI #TODO get index of crop rotation in entire
                        # "winter_summer_annual": [None, None, None, None, None, None, None, None, None, None],
                        #"yield_cpa_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # yield according to manure and crop
                        #"yield_cpa_dt_organic_manure": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        # was only used in F_NLeach and has no effect
                        # former XXX_N_OD
                        "used_crop_ids": set(), # is a set of all used crop_ids generated in GetCropData
                        "yield_amount_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_amount_byproduct_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_amount_dm_main_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_amount_dm_byproduct_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_legume_green_fodder_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_legume_hay_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_legume_silage_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_amount_catch_crop_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_amount_dm_catch_crop_t": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_available_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # former XXX_N_dfOD
                        "p_available_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # stems from manure
                        "k_available_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_uptake_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # former XXX_N_dfOD
                        "p_uptake_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # stems from manure
                        "k_uptake_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "delta_n_cpa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # equation 4
                        "delta_p_cpa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # equation 4
                        "delta_k_cpa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # equation 4
                        "n_in_main_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_in_byproduct_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "p_in_main_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "p_in_byproduct_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "k_in_main_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "k_in_byproduct_kg_per_ha": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_fixed": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_mineralisation_reduction_tillage": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "n_from_manure": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_from_seeds": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_removed_b_harvest": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_leached": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_volatilized": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_need_crop_mainproduct_per_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_need_crop_byproduct_per_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_need_crop_per_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "n_level": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "n_mineraliseable_per_year" : [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "yield_correction_factor_undersowing": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "yield_cpa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_cpa_manured": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "yield_tot": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        # TODO seems to only be relevant with hay and silage. There are no categories implemented yet (there should be a harvest method)
                        "som_organic_manure_leithold": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "ndfa_reduction": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        "n_level": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # 1-3 from NLevel, for manure is raised by 1

                        "n_mineralisation_som_pa": 0,
                        "n_level_mineralisation_pa": 0,
                        "ndf_soil": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # TODO needs to be done
                        "coefficient_n_min": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],

                        "n_need_per_dt": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # from NNeedCrop get_n_need_per_dt
                        "weed_infestation_index_q": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "weed_infestation_index_sa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "weed_infestation_index_wa": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        "est_probability_cc_us": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}

        self.report = {"item_list": []}
