import json

from dialog_window import DialogWindow
from project_handling.project import Project

from ui_logic.ui_status import UiStatus


def json_encode_project(project):
    print("function json_encode_project")
    if isinstance(project, Project):
        print("is Instance")
        # project_id is not included, since it could interfere with an existing id at project import
        # all_combobox_items is not included, since it is recreated at project load
        json_encoded_project = {"project_name": project.project_name,
                                "project_last_changed": project.project_last_changed,
                                "tab_general": project.tab_general,
                                "tab_rotation": project.tab_rotation,
                                "tab_free_generation": project.tab_free_generation,

                                "tab_manure": project.tab_manure,
                                "tab_epm": project.tab_epm,
                                "rotation_temp": project.rotation_temp,
                                "result_general": project.result_general,
                                "results_crop_rotations_free_generation": project.results_crop_rotations_free_generation,
                                "results": project.results,
                                "report": project.report
                }  # ID does not need/ should not be exported
        print("Json_encoded_project", json_encoded_project)
        return json_encoded_project
    else:
        print(f"Object {project} is not a Rotor project.")

def json_decode_project(json_file):
    print("entering json_decode_project")
    try:
        json_project = json.loads(json_file)
        project = Project()

        project.project_name = json_project["project_name"]
        project.project_last_changed = json_project["project_last_changed"]
        project.tab_general = json_project["dict_tab_general"]
        project.tab_rotation = json_project["dict_tab_rotation"]
        project.tab_free_generation = json_project["dict_tab_free_generation"]
        project.tab_manure = json_project["dict_tab_manure"]
        project.tab_epm = json_project["dict_tab_epm"]
        project.rotation_temp = json_project["rotation_temp"]
        project.result_general = json_project["result_general"]
        project.results_crop_rotations_free_generation = json_project["results_crop_rotations_free_generation"]
        project.results = json_project["results"]
        project.report = json_project["report"]

    except:
        DialogWindow().warning_of_import_error()

    return project

def json_encode_ui_status(ui_status):
    print("function json_encode_project")
    if isinstance(ui_status, UiStatus):
        print("is Instance")
        # project_id is not included, since it could interfere with an existing id at project import
        # all_combobox_items is not included, since it is recreated at project load
        json_encoded_ui_status = {"tabs": ui_status.tabs,
                                    "tabs_state": ui_status.tabs_state,
                                    "calculation_state": ui_status.calculation_state
                                    }  # ID does not need/ should not be exported
        print("Json_encoded_ui_status", json_encoded_ui_status)
        return json_encoded_ui_status
    else:
        print(f"An error occurred encoding the Ui-Status.")

def json_decode_ui_status(json_file):
    print("entering json_decode_project")
    try:
        json_project = json.loads(json_file)
        ui_status = UiStatus()
        ui_status.tabs = json_project["tabs"]
        ui_status.tabs_state = json_project["tabs_state"]
        ui_status.calculation_state = json_project["calculation_state"]


    except:
        DialogWindow().warning_of_import_error()

    return ui_status

