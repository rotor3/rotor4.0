import copy

from db_connect.db_connect import execute_query, execute_query_get_dictionary_list, execute_query_get_one
from PySide2.QtWidgets import QWidget
import json
from project_handling.json_serialize import json_encode_project, json_encode_ui_status
from datetime import datetime

from ui_logic.ui_status import UiStatus


class ProjectHandling(QWidget):


    @staticmethod
    def save_project_to_db(project, ui_status):
        if project.project_id == None:
            # sql_query = """INSERT INTO projects_tb (projects_name) VALUES ('%(project_name)s');""" % {"project_name": project.project_name}
            # project.project_id = execute_query(sql_query)

            # sql_query = """INSERT INTO projects_tb (projects_name) VALUES ('%(project_name)s')
            #                                     RETURNING projects_id;""" % {"project_name": project.project_name}
            # project.project_id = execute_query_get_one(sql_query)[0]

            sql_query = """INSERT INTO projects_tb (projects_name) VALUES ('%(project_name)s');""" % {
                "project_name": project.project_name}
            execute_query(sql_query)
            # Returning did not work in SQLite, therefore the current id is obtained by asking for the latest
            sql_query = """SELECT max(projects_id) FROM projects_tb;"""
            project.project_id = execute_query_get_one(sql_query)[0]

        # TODO change to save all into database-column project_data (project and ui_status)

        # tab_general_json = json.dumps(project.tab_general, default=json_encode_project)
        # tab_rotation_json = json.dumps(project.tab_rotation, default=json_encode_project)
        # tab_free_generation_json = json.dumps(project.tab_free_generation, default=json_encode_project)
        # tab_epm_json = json.dumps(project.tab_epm, default=json_encode_project)
        # tab_manure_json = json.dumps(project.dict_tab_manure_2, default=json_encode_project)
        #
        # result_general_json = json.dumps(project.result_general, default=json_encode_project)
        # results_json = json.dumps(project.results, default=json_encode_project)
        # results_crop_rotations_free_generation_json = json.dumps(project.results_crop_rotations_free_generation, default=json_encode_project)


        project_json = json.dumps(project, default=json_encode_project)
        ui_status_json = json.dumps(ui_status, default=json_encode_ui_status)
        # ui_status_tabs_json = json.dumps(ui_status.tabs, default=json_encode_project)

        # json_project_dictionary = {"dict_tab_general_json": tab_general_json,
        #                            "dict_tab_rotation_json": tab_rotation_json,
        #                            "dict_tab_free_generation_json": tab_free_generation_json,
        #                            "dict_tab_epm_json": tab_epm_json,
        #                            "dict_tab_manure_json": tab_manure_json,
        #                            "result_general_json": result_general_json,
        #                            "results_json": results_json,
        #                            "project_id": project.project_id,
        #                            # "project_last_changed": datetime.now().timestamp(),
        #                            "project_last_changed": int(datetime.now().timestamp()),
        #                            "ui_status_tabs_json": ui_status_tabs_json}
        # print("TIMESTAMP: ", json_project_dictionary["project_last_changed"])

        # json_sql = """UPDATE projects_tb
        #               SET dict_tab_general = '%(dict_tab_general_json)s',
        #               dict_tab_rotation = '%(dict_tab_rotation_json)s',
        #               dict_tab_epm = '%(dict_tab_epm_json)s',
        #               dict_tab_manure = '%(dict_tab_manure_json)s',
        #               dict_tab_free_generation = '%(dict_tab_free_generation_json)s',
        #               result_general = '%(result_general_json)s',
        #               results = '%(results_json)s',
        #               project_last_changed = %(project_last_changed)s,
        #               ui_status_tabs = '%(ui_status_tabs_json)s'
        #               WHERE projects_id =  %(project_id)s;""" % json_project_dictionary

        project_last_changed = datetime.now().timestamp()
        json_sql = """UPDATE projects_tb
                              SET project_data = '%(project_json)s',
                              ui_status = '%(ui_status_json)s',
                              project_last_changed = %(project_last_changed)s                              
                              WHERE projects_id =  %(project_id)s;""" \
                   % {"project_json": project_json, "ui_status_json": ui_status_json, "project_id": project.project_id,\
                        "project_last_changed": project_last_changed}

        print(json_sql)
        # execute_query(json_sql)
        #
        try:
            execute_query(json_sql)
        except Exception as e:

            ui_status.error["error"] = True
            ui_status.error["errormessage"] = str(e)

        return project, ui_status

    @staticmethod
    def load_project_from_db(project):
        # project, ui_status = LogicSidebar.reset_all(project)

        ui_status = UiStatus()


        print("project from db")

        json_sql = """SELECT *
                        FROM projects_tb WHERE projects_id = %(project_id)s""" % {"project_id": project.project_id}

        db_project = execute_query_get_dictionary_list(json_sql)[0]


        # This is a basic try-construct for future updates/upgrades,
        # in order for ROTOR to load projects from previous versions
        try:
            project.project_name = db_project["project_data"]["projects_name"]
            project.user_id = db_project["project_data"]["projects_user"]

            project.tab_general = json.loads(db_project["project_data"]["tab_general"])
            project.tab_rotation = json.loads(db_project["project_data"]["tab_rotation"])
            project.tab_free_generation = json.loads(db_project["project_data"]["tab_free_generation"])
            project.tab_manure = copy.deepcopy(json.loads(db_project["project_data"]["dict_tab_manure"]))
            # The following is necessary, because the keys of the manure dictionaries are integers.
            # JSON converts the to strings so they have to be converted back
            project.tab_manure["manure_values"] = {}
            project.tab_manure["manure_standard_values"] = {}
            for manure_key, manure_values in db_project["project_data"]["tab_manure"]["manure_values"].items():
                project.tab_manure["manure_values"][int(manure_key)] = copy.deepcopy(manure_values)
            for manure_key, manure_values in db_project["project_data"]["tab_manure"]["manure_standard_values"].items():
                project.tab_manure["manure_standard_values"][int(manure_key)] = copy.deepcopy(manure_values)
            project.result_general["manure_values"] = {}
            for manure_key, manure_values in db_project["project_data"]["result_general"]["manure_standard_values"].items():
                project.tab_manure["manure_standard_values"][int(manure_key)] = copy.deepcopy(manure_values)
            project.tab_epm = json.loads(db_project["project_data"]["dict_tab_epm"])

            project.results = json.loads(db_project["results"])
            project.results_crop_rotations_free_generation = json.loads(db_project["project_data"]["results_crop_rotations_free_generation"])

            project.report = json.loads(db_project["project_data"]["report"])

            ui_status.tabs = json.loads(db_project["ui_status"]["tabs"])


            ui_status.tabs = json.loads(db_project["ui_status_tabs"])
        except Exception as e:
            ui_status.error["error"] = True
            ui_status.error["errormessage"] = str(e)

        finally:
            return project, ui_status

    # @staticmethod
    # def load_project_from_db(project):
    #     # project, ui_status = LogicSidebar.reset_all(project)
    #
    #     ui_status = UiStatus()
    #
    #
    #     print("project from db")
    #
    #     json_sql = """SELECT *
    #                     FROM projects_tb WHERE projects_id = %(project_id)s""" % {"project_id": project.project_id}
    #
    #     db_project = execute_query_get_dictionary_list(json_sql)[0]
    #
    #
    #     # This is a basic try-construct for future updates/upgrades,
    #     # in order for ROTOR to load projects from previous versions
    #     try:
    #         project.project_name = db_project["projects_name"]
    #         project.user_id = db_project["projects_user"]
    #
    #         project.tab_general = json.loads(db_project["dict_tab_general"])
    #         project.tab_rotation = json.loads(db_project["dict_tab_rotation"])
    #         project.tab_free_generation = json.loads(db_project["dict_tab_free_generation"])
    #         project.dict_tab_manure_2 = json.loads(db_project["dict_tab_manure"])
    #         project.tab_epm = json.loads(db_project["dict_tab_epm"])
    #
    #         project.result_general = json.loads(db_project["result_general"])
    #         project.results = json.loads(db_project["results"])
    #
    #         ui_status.tabs = json.loads(db_project["ui_status_tabs"])
    #
    #
    #         project.results = json.loads(db_project["results"])
    #
    #         ui_status.tabs = json.loads(db_project["ui_status_tabs"])
    #     except Exception as e:
    #         ui_status.error["error"] = True
    #         ui_status.error["errormessage"] = str(e)
    #
    #     finally:
    #         return project, ui_status


    @staticmethod
    def json_export(project):
        print("function json_export")
        json_project = json.dumps(project, default=json_encode_project)
        print("json_project", json_project)
        return json_project




