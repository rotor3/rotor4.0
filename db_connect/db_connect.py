###################################################################################
#                                                                                 #
# the first function, set_db_connection, is for PostgreSQL, the other for SQLite  #
#                                                                                 #
###################################################################################

import psycopg
import pandas as pd
#from sqlalchemy import create_engine

import sqlite3

def set_pandas_connection():
    POSTGRES_ADDRESS = '127.0.0.1'
    POSTGRES_PORT = '5432'
    POSTGRES_USERNAME = 'postgis'
    POSTGRES_PASSWORD = 'postgis'
    POSTGRES_DBNAME = 'Rotor40_V4'
    # A long string that contains the necessary Postgres login information
    postgres_str = ('postgresql://{username}:{password}@{ipaddress}:{port}/{dbname}'
                    .format(username=POSTGRES_USERNAME,
                            password=POSTGRES_PASSWORD,
                            ipaddress=POSTGRES_ADDRESS,
                            port=POSTGRES_PORT,
                            dbname=POSTGRES_DBNAME))

    pandas_connection = create_engine(postgres_str)
    return pandas_connection
# TODO refactor: index_column as parameter and add df.set_index
def get_dataframe_from_db(sql_query, index_column=None, remove_duplicates=False):
    pandas_connection = set_db_connection()
    df = pd.read_sql_query(sql_query, con=pandas_connection)
    if remove_duplicates:
        df = df.loc[:, ~df.columns.duplicated()]
    if index_column:
        df.set_index(index_column, inplace=True)

    return df

def get_table_from_db(table_name):
    pandas_connection = set_db_connection()
    df = pd.read_sql_table(table_name, con=pandas_connection, schema=None, index_col=None, coerce_float=True, parse_dates=None, columns=None, chunksize=None)

    return df
def get_table_psycopg(table_name):
    pg_connection = set_db_connection()
    df = pd.read_sql_table(table_name, con=pg_connection)

    return df

def set_db_connection():
    try:


        connection = psycopg.connect(user="postgis",
                                      password="postgis",
                                      host="127.0.0.1",
                                      port="5432",
                                      dbname="Rotor40_V4",
                                     autocommit=True)
        # commits automatically after query- instead of connection.commit()
        #connection.set_session(autocommit=True)

        return connection

    except (Exception, psycopg.Error) as error:
        print("Error while connecting to PostgreSQL", error)

# def set_db_connection():
#     connection = sqlite3.connect("./database/rotor_40_V3.db")
#
#     return connection


def execute_query_get_one(query):
    print("get one: \n", query)
    connection = set_db_connection()
    cursor = connection.cursor()
    cursor.execute(query)
    record = cursor.fetchone()

    cursor.close()
    connection.commit()
    connection.close()

    return record


def execute_query_get_recordset(query):
    print("get recordset: \n", query)

    connection = set_db_connection()
    cursor = connection.cursor()
    cursor.execute(query)
    recordset = cursor.fetchall()

    cursor.close()
    connection.commit()
    connection.close()

    return recordset




# def execute_query_get_dictionary_list(query):
#     print("get dictionary: \n", query)
#     dictionary_list = []
#     connection = set_db_connection()
#     connection.row_factory = sqlite3.Row
#
#     cursor = connection.cursor()
#     cursor.execute(query)
#     recordset = cursor.fetchall()
#     print("List of recordset", recordset)
#     try: # postgreSQL
#         print("Check 01")
#         dictionary_list = convert_to_dict(cursor.description, recordset)
#         print("Check 02")
#     except: #sqlite
#         print("Check 03")
#
#         for row in recordset:
#             try:
#                 print("Check 04")
#                 print(dict(row))
#                 print("Check 05")
#                 dictionary_list.append(dict(row))
#             except:
#                 print("Check 06")
#     print("Check 07")
#     cursor.close()
#     connection.commit()
#     connection.close()
#     print("List of dictionaries", dictionary_list)
#     return dictionary_list

# for SQLITE
# def execute_query_get_dictionary_list(query):
#     print("get dictionary: \n", query)
#     dictionary_list = []
#     connection = set_db_connection()
#
#     connection.row_factory = sqlite3.Row
#
#     cursor = connection.cursor()
#     cursor.execute(query)
#     recordset = cursor.fetchall()
#
#     for row in recordset:
#         try:
#             print("Check 04")
#             print(dict(row))
#             print("Check 05")
#             dictionary_list.append(dict(row))
#         except:
#             pass
#     print("Check 07")
#     cursor.close()
#     connection.commit()
#     connection.close()
#     print("List of dictionaries", dictionary_list)
#     return dictionary_list

# for PostgreSQL
def execute_query_get_dictionary_list(query):
    print("get dictionary: \n", query)
    dictionary_list = []
    connection = set_db_connection()

    cursor = connection.cursor()
    cursor.execute(query)
    recordset = cursor.fetchall()
    dictionary_list = convert_to_dict(cursor.description, recordset)

    cursor.close()
    connection.close()

    return dictionary_list

def execute_query(query):
    print("get nothing: \n", query)
    connection = set_db_connection()
    cursor = connection.cursor()
    cursor.execute(query)
    cursor.close()
    connection.commit()
    connection.close()
    print("DB INSERT")


def convert_to_dict(columns, recordset):
    all_results = []
    columns = [col.name for col in columns]
    if type(recordset) is list:
        for value in recordset:
            all_results.append(dict(zip(columns, value)))
        return all_results
    elif type(recordset) is tuple:
        all_results.append(dict(zip(columns, recordset)))
        return all_results
