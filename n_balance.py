from db_connect.db_connect import execute_query_get_one, execute_query_get_dictionary_list

# this module calculates all Nitrogen related parameters
from project_handling.project import Project


class NBalance:
    def __init__(self, project):
        self.project = project








    def get_n_hp(self, index):
        # TODO if crop_type... auslagern
        crop_id = self.project.tab_rotation["crop"][index]
        n_hp_sql_query = """SELECT n_hp FROM crop_key_figures_tb WHERE crop_id = %(crop_id)s 
        AND production_system_id = %(production_system_id)s""" \
                % {"crop_id": crop_id, "production_system_id": self.project.tab_general["production_system_id"]}

        if self.project.tab_rotation["crop_type"][index] == 4:   # TODO introduce crop_type 5 for "Stillegung"
            # ((([NFIXFakt].[NGTM_LEG]*[NFixUS].[LEGAnt]+[NFIXFakt].[NGTM_NLEG]*(1-[NFixUS].[LEGAnt]))-((4*[NFixUS].[LEGAnt]+3.5
    # 	*(1-[NFixUS].[LEGAnt]))*(1-[KW].[Broesel])))/[KW].[Broesel])

            sql_query = """SELECT * FROM n_fixation_factor_tb nf WHERE crop_id = %(crop_id)s 
                        JOIN n_fixation_us_tb nu on nf.crop_id = nu.crop_id nf 
                        JOIN crop_key_figures_tb kw ON nf.crop_id = kw.crop_id WHERE kw.production_system_id = 2""" \
                        % {"crop_id": crop_id}
            #select DISTINCT on (nf.crop_id) FROM n_fixation_factor_tb nf
             #           inner JOIN n_fixation_us_tb nu on nf.crop_id = nu.crop_id
              #          inner JOIN crop_key_figures_tb kw ON nf.crop_id = kw.crop_id WHERE kw.production_system_id = 2 and nf.crop_id = 26'

            n_fix_values = execute_query_get_dictionary_list(sql_query)[0] # returns a dictionary of n_fix values for the selected crop

            # TODO rename variables
            try:
                ngtm_leg = n_fix_values["n_in_dry_mass_legumes"]
                leg_ant = n_fix_values["leg_ant"]
                ngtm_nleg = n_fix_values["n_in_dry_mass_non_legumes"]
                broesel = n_fix_values["harvestlosses"]
            except:
                msg = "nHP konnte nicht bestimmt werden, da die benötigten Werte nicht in der Datenbank vorliegen"

            n_hp = ((ngtm_leg * leg_ant + ngtm_nleg * (1 - leg_ant)) - ((4 * leg_ant + 3.5 *(1 - leg_ant))) * (1 - broesel)) \
                / broesel

        return n_hp


    def get_yield_n_fix(self, index):
        # Yielddt.yielddt.... Quatsch
        pass

    def get_n_uptake(self, index):
        # F_NEntzugSproß([yielddt],[KW].[TM],[NHP],[fNP],[NNP],[Broesel]) FROM yielddt
        sql_query_tm = """SELECT dry_mass FROM crop_key_figures_tb WHERE crop_id = %(crop_id)s 
        AND production_system_id = 2"""
        N_EntzugHP = yield_dt * TM * NHP
        N_EntzugNP = yield_dt * TM * FNP * NNP
        N_EntzugBr = yield_dt * TM * (1 - Broesel) / Broesel * 4.1
        # ??? 4.1 könnte der N-Gehalt in Brösel sein?? Ba. 19.05.03

        # Sieht so aus,als wenn der Leguminosenanteil bei Getreide/Leguminosenmischungen nicht berücksichtigt wird! Muss noch eingebaut werden

        F_NEntzugSproß = N_EntzugHP + N_EntzugNP + N_EntzugBr
        pass

    def get_n_removal(self, crop_id, index):
        # (IIf([TPVListe_NatschKF_Tab].[kultur]="st1",0,IIf([TPVListe_NatschKF_Tab].[kultur]="st0",0,1))*F_NEntz_HPuNP([yielddt],[KW].[TM],[NHP],[fNP],[NNP],
        # 	[TPVListe_NatschKF_Tab].[strawharvest]))+[N_removal_Aut_harv_LG]+[Nuptake_US] AS N_removal
        if crop_id == 31 or crop_id == 32:
            factor = 0
        else:
            factor = 1
        # F_NEntz_HPuNP
        if self.project.tab_rotation["strawharvest"][index] is True:
            enpf = 1
        else:
            enpf = 0
        # TODO was ist die Funktion Nz(yield_dt, 0)
        # F_NEntz_HPuNP = Nz(yield_dt, 0) * (TM * NHP + TM * FNP * NNP * ENPf)
        # f_n_removal_hp_np =



        # n_removal = factor *
if __name__ == "__main__":

    project = Project()
    project.tab_general["soil_index"] = 47
    project.tab_rotation["crop"][0] = 26
    project.tab_rotation["crop_type"][0] = 4




    n_balance = NBalance(project)

    print("WHC_RZ: ", n_balance.get_whc_rz())
    print("nHP for LZH, Leguminosen Heu-Heu Silage: ", n_balance.get_n_hp(0))


