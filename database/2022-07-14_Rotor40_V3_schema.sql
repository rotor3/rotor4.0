--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.0

-- Started on 2022-07-13 08:35:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 100687)
-- Name: covercrop_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.covercrop_tb (
    covercrop_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.covercrop_tb OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 100693)
-- Name: covercrop_tb_covercrop_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.covercrop_tb_covercrop_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.covercrop_tb_covercrop_id_seq OWNER TO postgres;

--
-- TOC entry 3405 (class 0 OID 0)
-- Dependencies: 201
-- Name: covercrop_tb_covercrop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.covercrop_tb_covercrop_id_seq OWNED BY public.covercrop_tb.covercrop_id;


--
-- TOC entry 202 (class 1259 OID 100695)
-- Name: crop_key_figures_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.crop_key_figures_tb (
    kult character varying(5),
    production_system_id integer,
    beschreibung character varying(50),
    seed_quantity double precision DEFAULT 0,
    dry_mass double precision,
    dry_mass_byproduct double precision,
    ratio_byproduct_main_product double precision,
    n2_fixation_crop double precision,
    n2_fixation_covercrop double precision,
    n_uptake_autumn double precision,
    liming double precision,
    broeckel double precision,
    reduction_byproduct_for_undersowing double precision,
    n_mineralisation_crop double precision,
    ratio_n_uptake_covercrop double precision,
    n_uptake_undersowing_from_previous_crop double precision,
    n_uptake_undersowing_from_current_crop double precision,
    erpiorr numeric(100,7),
    bemerkung character varying(100),
    crop_key_figures_id integer NOT NULL,
    crop_id integer,
    n_main_product_per_t double precision,
    n_byproduct_per_t double precision,
    p_main_product_per_t double precision,
    p_byproduct_per_t double precision,
    k_main_product_per_t double precision,
    k_byproduct_per_t double precision
);


ALTER TABLE public.crop_key_figures_tb OWNER TO postgres;

--
-- TOC entry 3406 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.seed_quantity; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.seed_quantity IS 'Saatmenge in kg/ha';


--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.dry_mass; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.dry_mass IS 'TM; Trockenmasse bezogen auf Erntegut (Abfuhr vom Feld) Daten nach Biermann1996';


--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.dry_mass_byproduct; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.dry_mass_byproduct IS 'TMNP; bezogen auf Ernteprodukt';


--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.ratio_byproduct_main_product; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.ratio_byproduct_main_product IS 'fNP; Faktor Nebenprodukt der Trockenmassen (Nebenprodukt/Hauptprodukt)';


--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n2_fixation_crop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n2_fixation_crop IS 'n2fix; N2-Fixierung der Leguminosen';


--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n2_fixation_covercrop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n2_fixation_covercrop IS 'n2fixzf; N2-Fixierung der Leguminosen (Zwischenfrucht)';


--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_uptake_autumn; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_uptake_autumn IS 'N_H; N-Aufnahme im Herbst';


--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.liming; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.liming IS 'kalkung; 1: Kultur wird bevorzugt gekalkt; 0: i.d.R. keine Kalkung';


--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.broeckel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.broeckel IS 'broesel; Faktor zur Berechnung des Ertrags nach Abzug der Bröckelverluste;Hans Bachinger Juli.99';


--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.reduction_byproduct_for_undersowing; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.reduction_byproduct_for_undersowing IS 'Wenn Untersaat, dann höherer Schnitt; abgeerntetes Nebenprodukt ist weniger, mehr verbleibt auf dem Acker. Bei Faktor 0,7 wird 30% höher geschnitten, d.h. Strohernte * 0,7.';


--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_mineralisation_crop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_mineralisation_crop IS 'altes N_Kult_Min
Fruchtartenabhängiger Mineralisierungsfaktor (Mineralisierung abzüglich der N-Aufnahme ergibt den N-Überhang);Hans Bachinger Juli.99

Relevant in der N-Auswaschung';


--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.ratio_n_uptake_covercrop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.ratio_n_uptake_covercrop IS 'n_aufn_ant_zwifr; Fruchtartenabhängiger Anteil der N-Aufnahme der Zwischenfrucht vom N-Überhang;Hans Bachinger Juli.99
Anteil des mineralisierten N der Ernterückstände aus der Vorfrucht durch die Zwischenfrucht';


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_uptake_undersowing_from_previous_crop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_uptake_undersowing_from_previous_crop IS 'n_aufn_ant_us_vor

Fruchtartenabhängiger Anteil der N-Aufnahme der Untersaat aus Vorfrucht vom N-Überhang der Prüffrucht vor Aussaat;Hans Bachinger Juli.99
Untersaat wird gleichzeitig mit Kultur ausgesät. Dieser Faktor ist der Anteil des mineralisierten N aus der Vorfrucht, der durch die Untersaat aufgenommen wird. Wenn also Untersaat == True, dann ist dieser Faktor relevant. Berechnet die N-Aufnahme aus der Mineralisierung der Ernterückständer der vorherigen Frucht';


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_uptake_undersowing_from_current_crop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_uptake_undersowing_from_current_crop IS 'N_Aufn_Ant_US_nach
Fruchtartenabhängiger Anteil der N-Aufnahme der Untersaat in der Prüffrucht vom N-Überhang der Prüffrucht nach Ernte;Hans Bachinger Juli.99

';


--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_main_product_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_main_product_per_t IS 'former nHP * .1
N-content of main product''s dry mass kg/t';


--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.n_byproduct_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.n_byproduct_per_t IS 'former nNP;
N-content of byproduct''s dry mass in kg/t';


--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.p_main_product_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.p_main_product_per_t IS 'P content in kg/t; converted from P2O5 to P, 2021-12-13 ck.
former pHP; P2O5-content of main product in kg/t';


--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.p_byproduct_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.p_byproduct_per_t IS 'P content in kg/t; converted from P2O5 to P, 2021-12-13 ck.
former pNP; P2O5-content of byproduct in kg/t';


--
-- TOC entry 3424 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.k_main_product_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.k_main_product_per_t IS 'K content in kg/t; converted from K2O to K, 2021-12-13 ck.
kNP; K2O-content of main product''s dry mass in kg/t';


--
-- TOC entry 3425 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN crop_key_figures_tb.k_byproduct_per_t; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.crop_key_figures_tb.k_byproduct_per_t IS 'K content in kg/t; converted from K2O to K, 2021-12-13 ck.
kNP; K2O-content of byproduct''s dry mass in kg/t';


--
-- TOC entry 203 (class 1259 OID 100699)
-- Name: crop_key_figures_tb_crop_key_figures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.crop_key_figures_tb_crop_key_figures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crop_key_figures_tb_crop_key_figures_id_seq OWNER TO postgres;

--
-- TOC entry 3426 (class 0 OID 0)
-- Dependencies: 203
-- Name: crop_key_figures_tb_crop_key_figures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.crop_key_figures_tb_crop_key_figures_id_seq OWNED BY public.crop_key_figures_tb.crop_key_figures_id;


--
-- TOC entry 204 (class 1259 OID 100701)
-- Name: crop_production_activities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.crop_production_activities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crop_production_activities_id_seq OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 100703)
-- Name: crop_production_activities_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.crop_production_activities_tb (
    crop_production_activities_id integer DEFAULT nextval('public.crop_production_activities_id_seq'::regclass) NOT NULL,
    crop_id integer,
    acronym character varying(5),
    previous_crop_type_id integer,
    previous_crop_n_delivery integer,
    n_delivery integer,
    undersowing boolean
);


ALTER TABLE public.crop_production_activities_tb OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 100707)
-- Name: crop_type_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.crop_type_tb (
    crop_type_id integer NOT NULL,
    name_de character varying(55),
    name_en character varying(55),
    name_fr character varying(55),
    old_name character varying(55)
);


ALTER TABLE public.crop_type_tb OWNER TO postgres;

--
-- TOC entry 3427 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE crop_type_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.crop_type_tb IS 'Fruchtgruppe';


--
-- TOC entry 207 (class 1259 OID 100710)
-- Name: crops_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.crops_tb (
    crop_id integer NOT NULL,
    acronym character varying(3) NOT NULL,
    name_de text,
    name_en text,
    name_fr text,
    harvest_time_id integer,
    sowing_time_id integer,
    crop_type_id integer,
    winter_summer_crop_id integer,
    undersowing boolean
);


ALTER TABLE public.crops_tb OWNER TO postgres;

--
-- TOC entry 3428 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE crops_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.crops_tb IS 'Kultur-Nutzarten,

crops from KW added';


--
-- TOC entry 208 (class 1259 OID 100716)
-- Name: crops_tb_crops_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.crops_tb_crops_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crops_tb_crops_id_seq OWNER TO postgres;

--
-- TOC entry 3429 (class 0 OID 0)
-- Dependencies: 208
-- Name: crops_tb_crops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.crops_tb_crops_id_seq OWNED BY public.crops_tb.crop_id;


--
-- TOC entry 254 (class 1259 OID 101200)
-- Name: delete_oek_bb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_bb (
    id integer NOT NULL,
    kult_code character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.delete_oek_bb OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 101206)
-- Name: delete_oek_bestellung; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_bestellung (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    nutzung character varying(255)
);


ALTER TABLE public.delete_oek_bestellung OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 101327)
-- Name: delete_oek_bodenbearbeitung_zwischenfrucht; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_bodenbearbeitung_zwischenfrucht (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung integer DEFAULT 0,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.delete_oek_bodenbearbeitung_zwischenfrucht OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 101229)
-- Name: delete_oek_ernte; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_ernte (
    id integer NOT NULL,
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    production_system character varying(255),
    anzahl_einsetzung double precision
);


ALTER TABLE public.delete_oek_ernte OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 101241)
-- Name: delete_oek_lagern; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_lagern (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.delete_oek_lagern OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 101247)
-- Name: delete_oek_maschinen_besonderheiten; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_maschinen_besonderheiten (
    geraet character varying(255),
    geraet_code character varying(255),
    index_var integer DEFAULT 0
);


ALTER TABLE public.delete_oek_maschinen_besonderheiten OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 101254)
-- Name: delete_oek_maschinen_stammdaten; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delete_oek_maschinen_stammdaten (
    id integer NOT NULL,
    geraet character varying(255),
    geraet_code character varying(255),
    lbg character varying(255),
    tractor_kw_min double precision,
    preis double precision,
    normative_nutzungsdauer_a double precision,
    normative_nutzungsdauer_ha double precision,
    abschreibungsschwelle double precision,
    jaehrlicher_einsatzumfang double precision,
    reparaturkosten double precision,
    versicherung_festkosten integer DEFAULT 0,
    dk_verbrauch_geraet integer DEFAULT 0,
    zuschlag_zu_dk_normverbrauch double precision,
    menge double precision DEFAULT 0,
    index_menge character varying(255),
    zeitbedarf double precision,
    diff_index double precision DEFAULT 0,
    geraetevarkosten double precision,
    geraetefestkosten double precision,
    geraete_maschinenkosten_incl_kombinationsgeraet_schlepp_var double precision,
    geraete_maschinenkosten_incl_kombinationsgeraet_schlepp_fest double precision,
    geraete_maschinenkosten_incl_kombinationsgeraet_schlepp_gesamt double precision,
    zinsansatz double precision,
    arbeitsbreite_nominell_werkzeug double precision,
    arbeitsbreite_nominell double precision,
    arbeitsbreite_nutzbar double precision,
    anzahl_werkzeuge double precision,
    netzverbrauch_ballen double precision DEFAULT 0,
    feldarbeits_geschwindigkeit_max double precision,
    feldarbeits_geschwindigkeit_limit double precision,
    leistung_absolut double precision DEFAULT 0,
    leistung_spezifisch double precision DEFAULT 0,
    schlaggroesse double precision,
    schlaggeometrie double precision,
    schlaggeometrie_breite double precision,
    schlaggeometrie_laenge double precision,
    vorgewendebreite double precision,
    taglaenge double precision,
    grundzeit double precision,
    wendenzeit_min double precision,
    wendenzeit_h double precision,
    silozeit_min double precision,
    silozeit_h double precision,
    ladekapazitaet_nomi double precision DEFAULT 0,
    ladekapazitaet_nutz double precision DEFAULT 0,
    ladekapazitaet_leistung double precision DEFAULT 0,
    ausbringmenge double precision DEFAULT 0,
    sonst_zeitbedarf_min double precision DEFAULT 0,
    sonst_zeitbedarf_h double precision DEFAULT 0,
    verlustzeit_min double precision,
    verlustzeit_ double precision,
    ruestzeit_min double precision,
    stoerungszeit_min double precision,
    erholungszeit_min double precision DEFAULT 0,
    hof_schlag_entfernung double precision,
    fahr_strasse double precision,
    wegezeit_arbeitsort double precision,
    fahr_feld double precision,
    schlag_schlag_entfernung double precision,
    wegezeit_zwischen_arbeitsorten double precision,
    wartungzeit_maschine_min double precision,
    operativzeit double precision,
    stueckzeit_tag double precision,
    stueckzeit_ha double precision,
    einsatzzeit_tag double precision,
    einsatzzeit_ha double precision,
    arbeitsleistung_tag double precision,
    arbeitsleistung_ha double precision,
    anteil_grundzeit_an_einsatzzeit double precision,
    index_verfahren character varying(255),
    index_listenauswahl double precision,
    dk_verbrauch double precision,
    besonderheit character varying(255)
);


ALTER TABLE public.delete_oek_maschinen_stammdaten OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 101417)
-- Name: harvest_methods_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.harvest_methods_tb (
    harvest_methods_id bigint NOT NULL,
    name_de character varying,
    name_en character varying,
    name_fr character varying
);


ALTER TABLE public.harvest_methods_tb OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 101415)
-- Name: harvest_methods_tb_harvest_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.harvest_methods_tb_harvest_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.harvest_methods_tb_harvest_methods_id_seq OWNER TO postgres;

--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 274
-- Name: harvest_methods_tb_harvest_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.harvest_methods_tb_harvest_methods_id_seq OWNED BY public.harvest_methods_tb.harvest_methods_id;


--
-- TOC entry 209 (class 1259 OID 100718)
-- Name: humus_repro_crops_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.humus_repro_crops_tb (
    humus_factors_id integer NOT NULL,
    production_system_id integer,
    crop_id integer,
    humus_repro_crop integer,
    humus_repro_for_era double precision,
    annotation text
);


ALTER TABLE public.humus_repro_crops_tb OWNER TO postgres;

--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE humus_repro_crops_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.humus_repro_crops_tb IS 'former Humusfakt';


--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN humus_repro_crops_tb.humus_repro_crop; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.humus_repro_crops_tb.humus_repro_crop IS 'Humusbedarf in Humusfakt;

kg Humuskohlenstoff / ha Jahr (nach Bundesgesetzblatt 2004 Teil I Nr. 58)';


--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN humus_repro_crops_tb.humus_repro_for_era; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.humus_repro_crops_tb.humus_repro_for_era IS 'HEOL in Humusfakt;

HE/ha Humuseinheiten für ÖL (nach Leithold & Hülsbergen, 1998)';


--
-- TOC entry 210 (class 1259 OID 100724)
-- Name: humus_factors_tb_humus_factors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.humus_factors_tb_humus_factors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.humus_factors_tb_humus_factors_id_seq OWNER TO postgres;

--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 210
-- Name: humus_factors_tb_humus_factors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.humus_factors_tb_humus_factors_id_seq OWNED BY public.humus_repro_crops_tb.humus_factors_id;


--
-- TOC entry 211 (class 1259 OID 100726)
-- Name: humus_vdlufa_crops_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.humus_vdlufa_crops_tb (
    humus_vdlufa_crops_id integer NOT NULL,
    crop_id integer NOT NULL,
    humus_vdlufa_crop integer,
    acronym character(3),
    production_system_id integer,
    ratio_main_by_product double precision,
    humus_vdlufa_byproduct double precision
);


ALTER TABLE public.humus_vdlufa_crops_tb OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN humus_vdlufa_crops_tb.ratio_main_by_product; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.humus_vdlufa_crops_tb.ratio_main_by_product IS 'this is also in crop_key_figures!!! check and delete';


--
-- TOC entry 212 (class 1259 OID 100729)
-- Name: humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq OWNER TO postgres;

--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 212
-- Name: humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq OWNED BY public.humus_vdlufa_crops_tb.humus_vdlufa_crops_id;


--
-- TOC entry 253 (class 1259 OID 101185)
-- Name: kw_delete; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kw_delete (
    kult character varying(5),
    production_system_id integer,
    opr boolean,
    beschreibung character varying(50),
    seed_quantity_delete double precision DEFAULT 0,
    dry_mass double precision,
    dry_mass_byproduct double precision,
    ratio_byproduct_main_product double precision,
    n2_fixation double precision,
    n2_fixation_covercrop double precision,
    n_uptake_autumn double precision,
    liming double precision,
    broeckel double precision,
    red_np_us double precision,
    n_mineralisation_crop double precision,
    n_uptake_covercrop double precision,
    n_uptake_undersowing_pre_seeding double precision,
    n_uptake_undersowing_post_harvest double precision,
    erpiorr numeric(100,7),
    bemerkung character varying(100),
    crop_key_figures_id integer NOT NULL,
    crop_id integer,
    seed_quantity_from_sq_tb integer,
    n_main_product_per_t double precision,
    n_byproduct_per_t double precision,
    p_main_product_per_t double precision,
    p_byproduct_per_t double precision,
    k_main_product_per_t double precision,
    k_byproduct_per_t double precision
);


ALTER TABLE public.kw_delete OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 101391)
-- Name: m_n_crop_harvest_disintegration_losses_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_n_crop_harvest_disintegration_losses_tb (
    m_n_crop_harvest_disintegration_losses_id integer NOT NULL,
    crop_id integer,
    acronym character varying,
    harvest_method_id integer,
    disintegration_loss double precision,
    dry_mass double precision
);


ALTER TABLE public.m_n_crop_harvest_disintegration_losses_tb OWNER TO postgres;

--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 273
-- Name: TABLE m_n_crop_harvest_disintegration_losses_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.m_n_crop_harvest_disintegration_losses_tb IS 'Bröckelverluste abhängig von Kultur und Erntemethode';


--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 273
-- Name: COLUMN m_n_crop_harvest_disintegration_losses_tb.acronym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.m_n_crop_harvest_disintegration_losses_tb.acronym IS 'only for better readability. This column has no effect.';


--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 273
-- Name: COLUMN m_n_crop_harvest_disintegration_losses_tb.disintegration_loss; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.m_n_crop_harvest_disintegration_losses_tb.disintegration_loss IS 'Bröckelverlust';


--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 273
-- Name: COLUMN m_n_crop_harvest_disintegration_losses_tb.dry_mass; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.m_n_crop_harvest_disintegration_losses_tb.dry_mass IS 'altered dry mass due to harvest method';


--
-- TOC entry 272 (class 1259 OID 101389)
-- Name: m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq OWNER TO postgres;

--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 272
-- Name: m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq OWNED BY public.m_n_crop_harvest_disintegration_losses_tb.m_n_crop_harvest_disintegration_losses_id;


--
-- TOC entry 213 (class 1259 OID 100731)
-- Name: m_n_crops_covercrop_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_n_crops_covercrop_tb (
    m_n_crops_covercrop_id integer NOT NULL,
    kultur character varying,
    crop_id integer,
    covercrop_id integer
);


ALTER TABLE public.m_n_crops_covercrop_tb OWNER TO postgres;

--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN m_n_crops_covercrop_tb.kultur; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.m_n_crops_covercrop_tb.kultur IS 'can be deleted, only for better readability';


--
-- TOC entry 214 (class 1259 OID 100737)
-- Name: m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq OWNER TO postgres;

--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 214
-- Name: m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq OWNED BY public.m_n_crops_covercrop_tb.m_n_crops_covercrop_id;


--
-- TOC entry 215 (class 1259 OID 100739)
-- Name: m_n_manures_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_n_manures_tb (
    manure_type_id integer,
    manure_source_id integer,
    manure_specification_id integer,
    description text,
    dry_mass double precision,
    n_content_per_unit double precision,
    n_availability_percent double precision,
    n_loss_percent double precision,
    p_content_kg_per_unit double precision,
    k_content_kg_per_unit double precision,
    organic_substance_percent double precision,
    dry_mass_minimum double precision,
    dry_mass_maximum double precision,
    vdlufa_humus_units_factor_a double precision,
    vdlufa_humus_units_factor_b double precision,
    m_n_manures_id integer NOT NULL,
    repro_humus_units double precision
);


ALTER TABLE public.m_n_manures_tb OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 100745)
-- Name: m_n_manures_tb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.m_n_manures_tb ALTER COLUMN m_n_manures_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.m_n_manures_tb_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 100747)
-- Name: m_n_production_systems_manure_types_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_n_production_systems_manure_types_tb (
    m_n_production_systems_manure_types_id integer NOT NULL,
    production_system_id integer,
    manure_type_id integer
);


ALTER TABLE public.m_n_production_systems_manure_types_tb OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 100750)
-- Name: m_n_production_systems_manure_m_n_production_systems_manure_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_n_production_systems_manure_m_n_production_systems_manure_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_n_production_systems_manure_m_n_production_systems_manure_seq OWNER TO postgres;

--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 218
-- Name: m_n_production_systems_manure_m_n_production_systems_manure_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.m_n_production_systems_manure_m_n_production_systems_manure_seq OWNED BY public.m_n_production_systems_manure_types_tb.m_n_production_systems_manure_types_id;


--
-- TOC entry 219 (class 1259 OID 100752)
-- Name: manure_source_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manure_source_tb (
    manure_source_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.manure_source_tb OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 100758)
-- Name: manure_source_tb_manure_source_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.manure_source_tb_manure_source_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manure_source_tb_manure_source_id_seq OWNER TO postgres;

--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 220
-- Name: manure_source_tb_manure_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.manure_source_tb_manure_source_id_seq OWNED BY public.manure_source_tb.manure_source_id;


--
-- TOC entry 221 (class 1259 OID 100760)
-- Name: manure_specification_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manure_specification_tb (
    manure_specification_id bigint NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.manure_specification_tb OWNER TO postgres;

--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE manure_specification_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.manure_specification_tb IS 'from VDLUFA 2014; ';


--
-- TOC entry 222 (class 1259 OID 100766)
-- Name: manure_specification_tb_manure_specification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.manure_specification_tb_manure_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manure_specification_tb_manure_specification_id_seq OWNER TO postgres;

--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 222
-- Name: manure_specification_tb_manure_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.manure_specification_tb_manure_specification_id_seq OWNED BY public.manure_specification_tb.manure_specification_id;


--
-- TOC entry 223 (class 1259 OID 100768)
-- Name: manure_type_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manure_type_tb (
    manure_type_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text,
    manure_unit_ton boolean,
    to_be_deleted boolean
);


ALTER TABLE public.manure_type_tb OWNER TO postgres;

--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN manure_type_tb.manure_unit_ton; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.manure_type_tb.manure_unit_ton IS 'This bool is for changing the label in the GUI';


--
-- TOC entry 224 (class 1259 OID 100774)
-- Name: manure_types_new_tb_manure_types_new_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.manure_types_new_tb_manure_types_new_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manure_types_new_tb_manure_types_new_id_seq OWNER TO postgres;

--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 224
-- Name: manure_types_new_tb_manure_types_new_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.manure_types_new_tb_manure_types_new_id_seq OWNED BY public.manure_type_tb.manure_type_id;


--
-- TOC entry 225 (class 1259 OID 100776)
-- Name: n_delivery_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.n_delivery_tb (
    n_delivery_id integer NOT NULL,
    name_de character varying,
    name_en character varying,
    name_fr character varying
);


ALTER TABLE public.n_delivery_tb OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 100782)
-- Name: n_delivery_tb_n_delivery_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.n_delivery_tb_n_delivery_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.n_delivery_tb_n_delivery_id_seq OWNER TO postgres;

--
-- TOC entry 3450 (class 0 OID 0)
-- Dependencies: 226
-- Name: n_delivery_tb_n_delivery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.n_delivery_tb_n_delivery_id_seq OWNED BY public.n_delivery_tb.n_delivery_id;


--
-- TOC entry 252 (class 1259 OID 101176)
-- Name: n_fixing_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.n_fixing_tb (
    kultur text,
    kev double precision,
    ndfa double precision,
    legume_ratio double precision,
    ngtm_leg double precision,
    ndfa_leg_a double precision,
    ndfa_leg_b double precision,
    ngtm_n_leg double precision,
    ndfa_n_leg_a double precision,
    ndfa_n_leg_b double precision,
    ndfa_n_leg_c double precision,
    tmewr double precision,
    ngewr double precision,
    anmerkungen text,
    crop_id integer,
    n_fixing_id integer NOT NULL
);


ALTER TABLE public.n_fixing_tb OWNER TO postgres;

--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 252
-- Name: COLUMN n_fixing_tb.legume_ratio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.n_fixing_tb.legume_ratio IS 'ratio of legume in the mix';


--
-- TOC entry 283 (class 1259 OID 109431)
-- Name: nfixfakt_tb_n_fixing_tb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nfixfakt_tb_n_fixing_tb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nfixfakt_tb_n_fixing_tb_seq OWNER TO postgres;

--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 283
-- Name: nfixfakt_tb_n_fixing_tb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nfixfakt_tb_n_fixing_tb_seq OWNED BY public.n_fixing_tb.n_fixing_id;


--
-- TOC entry 271 (class 1259 OID 101344)
-- Name: oek_economy_machines_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_economy_machines_tb (
    economy_machines_id bigint NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.oek_economy_machines_tb OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 101342)
-- Name: oek_economy_machines_tb_economy_machines_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oek_economy_machines_tb_economy_machines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oek_economy_machines_tb_economy_machines_id_seq OWNER TO postgres;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 270
-- Name: oek_economy_machines_tb_economy_machines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oek_economy_machines_tb_economy_machines_id_seq OWNED BY public.oek_economy_machines_tb.economy_machines_id;


--
-- TOC entry 282 (class 1259 OID 109375)
-- Name: oek_field_shape_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_field_shape_tb (
    field_shape_id bigint NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.oek_field_shape_tb OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 109373)
-- Name: oek_field_shape_tb_field_shape_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oek_field_shape_tb_field_shape_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oek_field_shape_tb_field_shape_id_seq OWNER TO postgres;

--
-- TOC entry 3454 (class 0 OID 0)
-- Dependencies: 281
-- Name: oek_field_shape_tb_field_shape_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oek_field_shape_tb_field_shape_id_seq OWNED BY public.oek_field_shape_tb.field_shape_id;


--
-- TOC entry 279 (class 1259 OID 101442)
-- Name: oek_neu_kultur_tillage_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_neu_kultur_tillage_tb (
    id integer NOT NULL,
    tillage_id integer,
    crop_id integer,
    acronym character varying
);


ALTER TABLE public.oek_neu_kultur_tillage_tb OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 101440)
-- Name: oek_neu_kultur_tillage_tb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oek_neu_kultur_tillage_tb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oek_neu_kultur_tillage_tb_id_seq OWNER TO postgres;

--
-- TOC entry 3455 (class 0 OID 0)
-- Dependencies: 278
-- Name: oek_neu_kultur_tillage_tb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oek_neu_kultur_tillage_tb_id_seq OWNED BY public.oek_neu_kultur_tillage_tb.id;


--
-- TOC entry 277 (class 1259 OID 101431)
-- Name: oek_neu_maschinenart_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_neu_maschinenart_tb (
    id integer NOT NULL,
    name_de character varying,
    name_en character varying,
    name_fr character varying,
    procedure_id integer
);


ALTER TABLE public.oek_neu_maschinenart_tb OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 101429)
-- Name: oek_neu_maschinenart_tb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oek_neu_maschinenart_tb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oek_neu_maschinenart_tb_id_seq OWNER TO postgres;

--
-- TOC entry 3456 (class 0 OID 0)
-- Dependencies: 276
-- Name: oek_neu_maschinenart_tb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oek_neu_maschinenart_tb_id_seq OWNED BY public.oek_neu_maschinenart_tb.id;


--
-- TOC entry 261 (class 1259 OID 101274)
-- Name: oek_od; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_od (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.oek_od OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 101280)
-- Name: oek_pflege; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_pflege (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    typ character varying(255)
);


ALTER TABLE public.oek_pflege OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 101212)
-- Name: oek_procedures_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_procedures_tb (
    procedure_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.oek_procedures_tb OWNER TO postgres;

--
-- TOC entry 3457 (class 0 OID 0)
-- Dependencies: 256
-- Name: TABLE oek_procedures_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.oek_procedures_tb IS 'croppingmeasuring';


--
-- TOC entry 280 (class 1259 OID 109362)
-- Name: oek_procedures_tb_procedure_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oek_procedures_tb_procedure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oek_procedures_tb_procedure_id_seq OWNER TO postgres;

--
-- TOC entry 3458 (class 0 OID 0)
-- Dependencies: 280
-- Name: oek_procedures_tb_procedure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oek_procedures_tb_procedure_id_seq OWNED BY public.oek_procedures_tb.procedure_id;


--
-- TOC entry 263 (class 1259 OID 101294)
-- Name: oek_schlaggroesse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_schlaggroesse (
    schlag_groesse double precision DEFAULT 0,
    betriebgr character varying(255)
);


ALTER TABLE public.oek_schlaggroesse OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 101298)
-- Name: oek_stroh; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_stroh (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge double precision,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.oek_stroh OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 101304)
-- Name: oek_transport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_transport (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge integer DEFAULT 0,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system character varying(255),
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.oek_transport OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 101311)
-- Name: oek_us; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_us (
    id integer NOT NULL,
    kult_group character varying(255),
    kultur character varying(255),
    reinfolge double precision,
    geraet_folge integer DEFAULT 0,
    geraet character varying(255),
    geraet_code character varying(255),
    betriebgr character varying(255),
    system boolean DEFAULT false,
    anzahl_einsetzung double precision,
    nutzung character varying(255),
    typ character varying(255)
);


ALTER TABLE public.oek_us OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 101319)
-- Name: oek_vornachkult; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_vornachkult (
    kultur character varying(255),
    np character varying(255),
    od character varying(255),
    us character varying(255),
    bb character varying(255),
    zw character varying(255),
    festmist numeric(100,7) DEFAULT 0,
    "gülle" numeric(100,7) DEFAULT 0,
    "gülle_zeit" character varying(255)
);


ALTER TABLE public.oek_vornachkult OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 101334)
-- Name: oek_zwischenfruch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oek_zwischenfruch (
    zw character varying(255),
    optionen character varying(255)
);


ALTER TABLE public.oek_zwischenfruch OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 100805)
-- Name: production_system_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.production_system_tb (
    production_system_id integer NOT NULL,
    production_system_name_de character varying(55),
    production_system_name_en character varying(55),
    production_system_name_fr character varying(55)
);


ALTER TABLE public.production_system_tb OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 100808)
-- Name: projects_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects_tb (
    projects_id integer NOT NULL,
    projects_user character varying(32),
    projects_name character(56),
    project_last_changed bigint,
    dict_tab_general text,
    dict_tab_manure text,
    dict_tab_rotation text,
    results text,
    result_temp text,
    dict_tab_free_generation text,
    dict_tab_epm text,
    ui_status_tabs text,
    result_general text,
    project_data text,
    ui_status text
);


ALTER TABLE public.projects_tb OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 100814)
-- Name: projects_tb_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_tb_projects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_tb_projects_id_seq OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 100816)
-- Name: projects_tb_projects_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_tb_projects_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_tb_projects_id_seq1 OWNER TO postgres;

--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 230
-- Name: projects_tb_projects_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projects_tb_projects_id_seq1 OWNED BY public.projects_tb.projects_id;


--
-- TOC entry 231 (class 1259 OID 100818)
-- Name: report_rows_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.report_rows_tb (
    report_rows_id integer NOT NULL,
    project_key character varying,
    name_de character varying,
    name_en character varying,
    name_fr character varying,
    "position" integer
);


ALTER TABLE public.report_rows_tb OWNER TO postgres;

--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN report_rows_tb."position"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.report_rows_tb."position" IS 'Position orders the items in the report display. It is initially incremented by five to leave space for additional items.
Set to 0 if this item should not be diplayed.';


--
-- TOC entry 232 (class 1259 OID 100824)
-- Name: report_items_tb_report_row_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.report_items_tb_report_row_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_items_tb_report_row_id_seq OWNER TO postgres;

--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 232
-- Name: report_items_tb_report_row_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.report_items_tb_report_row_id_seq OWNED BY public.report_rows_tb.report_rows_id;


--
-- TOC entry 233 (class 1259 OID 100826)
-- Name: rotation_restrictions_crop_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rotation_restrictions_crop_tb (
    rotation_restrictions_crop_id integer DEFAULT 0 NOT NULL,
    description character varying(50),
    ff_ant double precision DEFAULT 0,
    ff_ant_roth double precision DEFAULT 0,
    ff_ant_original double precision DEFAULT 0,
    ffa_year_1 double precision DEFAULT 0,
    ffa_year_2 double precision DEFAULT 0,
    ffa_year_3 double precision DEFAULT 0,
    ffa_year_4 double precision DEFAULT 0,
    ffa_year_5 double precision DEFAULT 0,
    years_of_cultivation double precision DEFAULT 0,
    crop_id integer
);


ALTER TABLE public.rotation_restrictions_crop_tb OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 100840)
-- Name: soil_quality_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.soil_quality_tb (
    soil_quality_id integer NOT NULL,
    soil_index integer,
    name_de character varying,
    whc_rz integer,
    name_en character varying,
    name_fr character varying,
    particle_01micro_m double precision
);


ALTER TABLE public.soil_quality_tb OWNER TO postgres;

--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN soil_quality_tb.soil_index; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.soil_quality_tb.soil_index IS 'Ackerzahl transferred from the soil quality typer for internal calcuation in ROTOR (yield, N-balance)';


--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN soil_quality_tb.whc_rz; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.soil_quality_tb.whc_rz IS 'water holding capacity in root zone [mm H2O] preliminary values!!!';


--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN soil_quality_tb.particle_01micro_m; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.soil_quality_tb.particle_01micro_m IS 'aus Lehrbuch der Bodenkunde, Amelung, Blume et al. 2018, die Werte sind Mittelwerte nach Bodenart. Für sandig toniger Lehm liegt in der Literatur kein Wert vor, er ist gemittelt
';


--
-- TOC entry 235 (class 1259 OID 100846)
-- Name: soil_quality_tb_soil_quality_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.soil_quality_tb_soil_quality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.soil_quality_tb_soil_quality_id_seq OWNER TO postgres;

--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 235
-- Name: soil_quality_tb_soil_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.soil_quality_tb_soil_quality_id_seq OWNED BY public.soil_quality_tb.soil_quality_id;


--
-- TOC entry 236 (class 1259 OID 100848)
-- Name: sowing_harvest_time_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sowing_harvest_time_tb (
    sowing_harvest_time_id integer NOT NULL,
    name_de character varying(55),
    name_en character varying(55),
    name_fr character varying(55)
);


ALTER TABLE public.sowing_harvest_time_tb OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 100851)
-- Name: tillage_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tillage_tb (
    tillage_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.tillage_tb OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 100857)
-- Name: tillage_tb_tillage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tillage_tb_tillage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tillage_tb_tillage_id_seq OWNER TO postgres;

--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 238
-- Name: tillage_tb_tillage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tillage_tb_tillage_id_seq OWNED BY public.tillage_tb.tillage_id;


--
-- TOC entry 239 (class 1259 OID 100859)
-- Name: undersowing_new; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.undersowing_new (
    id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text
);


ALTER TABLE public.undersowing_new OWNER TO postgres;

--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE undersowing_new; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.undersowing_new IS 'für mock';


--
-- TOC entry 240 (class 1259 OID 100865)
-- Name: undersowing_new_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.undersowing_new_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.undersowing_new_id_seq OWNER TO postgres;

--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 240
-- Name: undersowing_new_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.undersowing_new_id_seq OWNED BY public.undersowing_new.id;


--
-- TOC entry 241 (class 1259 OID 100867)
-- Name: users_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_tb (
    user_id integer NOT NULL,
    name character varying,
    last_login date
);


ALTER TABLE public.users_tb OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 100873)
-- Name: users_tb_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_tb_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_tb_user_id_seq OWNER TO postgres;

--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 242
-- Name: users_tb_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_tb_user_id_seq OWNED BY public.users_tb.user_id;


--
-- TOC entry 243 (class 1259 OID 100875)
-- Name: weed_infestation_index_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.weed_infestation_index_tb (
    weed_infestation_index_id integer NOT NULL,
    crop_id integer,
    acronym character varying(3) NOT NULL,
    summer_annual_infestation_risk integer,
    winter_summer_crop_id integer,
    summer_winter_annual_id integer,
    winter_annual_infestation_risk integer,
    couch_grass_infestation_risk integer
);


ALTER TABLE public.weed_infestation_index_tb OWNER TO postgres;

--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN weed_infestation_index_tb.acronym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.weed_infestation_index_tb.acronym IS 'can be deleted- only for better readability';


--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN weed_infestation_index_tb.summer_annual_infestation_risk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.weed_infestation_index_tb.summer_annual_infestation_risk IS 'summer annual weeds';


--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN weed_infestation_index_tb.summer_winter_annual_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.weed_infestation_index_tb.summer_winter_annual_id IS 'should be like winter_summer_crop_id? This column stems from Weed Infestation risk functions, but key is from winter_summer_crop_tb';


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN weed_infestation_index_tb.winter_annual_infestation_risk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.weed_infestation_index_tb.winter_annual_infestation_risk IS 'winter annual weeds';


--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN weed_infestation_index_tb.couch_grass_infestation_risk; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.weed_infestation_index_tb.couch_grass_infestation_risk IS 'quack grass';


--
-- TOC entry 244 (class 1259 OID 100878)
-- Name: weed_infestation_index_tb_weed_infestation_index_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.weed_infestation_index_tb_weed_infestation_index_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.weed_infestation_index_tb_weed_infestation_index_id_seq OWNER TO postgres;

--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 244
-- Name: weed_infestation_index_tb_weed_infestation_index_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.weed_infestation_index_tb_weed_infestation_index_id_seq OWNED BY public.weed_infestation_index_tb.weed_infestation_index_id;


--
-- TOC entry 245 (class 1259 OID 100880)
-- Name: winter_summer_crop_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.winter_summer_crop_tb (
    winter_summer_crop_id integer NOT NULL,
    name_en character varying
);


ALTER TABLE public.winter_summer_crop_tb OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 100886)
-- Name: winter_summer_crop_tb_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.winter_summer_crop_tb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.winter_summer_crop_tb_id_seq OWNER TO postgres;

--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 246
-- Name: winter_summer_crop_tb_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.winter_summer_crop_tb_id_seq OWNED BY public.winter_summer_crop_tb.winter_summer_crop_id;


--
-- TOC entry 247 (class 1259 OID 100888)
-- Name: winterhardiness_values_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.winterhardiness_values_tb (
    winterhardiness_values_id integer NOT NULL,
    name_de text,
    name_en text,
    name_fr text,
    winterhardiness_value double precision
);


ALTER TABLE public.winterhardiness_values_tb OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 100894)
-- Name: winterhardiness_values_tb_winterhardiness_values_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.winterhardiness_values_tb_winterhardiness_values_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.winterhardiness_values_tb_winterhardiness_values_id_seq OWNER TO postgres;

--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 248
-- Name: winterhardiness_values_tb_winterhardiness_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.winterhardiness_values_tb_winterhardiness_values_id_seq OWNED BY public.winterhardiness_values_tb.winterhardiness_values_id;


--
-- TOC entry 249 (class 1259 OID 100896)
-- Name: yield_crop_coefficients_bachinger_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.yield_crop_coefficients_bachinger_tb (
    a_1 double precision,
    a_2 double precision,
    a_3 double precision,
    acronym text NOT NULL,
    b_1 double precision,
    b_2 double precision,
    b_3 double precision,
    c_1 double precision,
    c_2 double precision,
    c_3 double precision,
    crop_id integer,
    yield_crop_coefficients_bachinger_id integer NOT NULL
);


ALTER TABLE public.yield_crop_coefficients_bachinger_tb OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 100902)
-- Name: yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq OWNER TO postgres;

--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 250
-- Name: yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq OWNED BY public.yield_crop_coefficients_bachinger_tb.yield_crop_coefficients_bachinger_id;


--
-- TOC entry 251 (class 1259 OID 100904)
-- Name: yield_crop_coefficients_tb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.yield_crop_coefficients_tb (
    acronym text NOT NULL,
    yield_precrop_comment text,
    yield_crop_coefficients_id integer NOT NULL,
    crop_id integer,
    a_1 double precision,
    a_2 double precision,
    a_3 double precision,
    b_1 double precision,
    b_2 double precision,
    b_3 double precision,
    c_1 double precision,
    c_2 double precision,
    c_3 double precision
);


ALTER TABLE public.yield_crop_coefficients_tb OWNER TO postgres;

--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE yield_crop_coefficients_tb; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.yield_crop_coefficients_tb IS 'All coefficients a, b and c are adapted to yield calculations in metric tons istead of dt.
';


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN yield_crop_coefficients_tb.acronym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.yield_crop_coefficients_tb.acronym IS 'only for better readability';


--
-- TOC entry 3110 (class 2604 OID 100910)
-- Name: covercrop_tb covercrop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.covercrop_tb ALTER COLUMN covercrop_id SET DEFAULT nextval('public.covercrop_tb_covercrop_id_seq'::regclass);


--
-- TOC entry 3112 (class 2604 OID 100911)
-- Name: crop_key_figures_tb crop_key_figures_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_key_figures_tb ALTER COLUMN crop_key_figures_id SET DEFAULT nextval('public.crop_key_figures_tb_crop_key_figures_id_seq'::regclass);


--
-- TOC entry 3114 (class 2604 OID 100912)
-- Name: crops_tb crop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb ALTER COLUMN crop_id SET DEFAULT nextval('public.crops_tb_crops_id_seq'::regclass);


--
-- TOC entry 3170 (class 2604 OID 101420)
-- Name: harvest_methods_tb harvest_methods_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.harvest_methods_tb ALTER COLUMN harvest_methods_id SET DEFAULT nextval('public.harvest_methods_tb_harvest_methods_id_seq'::regclass);


--
-- TOC entry 3115 (class 2604 OID 100913)
-- Name: humus_repro_crops_tb humus_factors_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_repro_crops_tb ALTER COLUMN humus_factors_id SET DEFAULT nextval('public.humus_factors_tb_humus_factors_id_seq'::regclass);


--
-- TOC entry 3116 (class 2604 OID 100914)
-- Name: humus_vdlufa_crops_tb humus_vdlufa_crops_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_vdlufa_crops_tb ALTER COLUMN humus_vdlufa_crops_id SET DEFAULT nextval('public.humus_vdlufa_crops_tb_humus_vdlufa_crops_id_seq'::regclass);


--
-- TOC entry 3169 (class 2604 OID 101394)
-- Name: m_n_crop_harvest_disintegration_losses_tb m_n_crop_harvest_disintegration_losses_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_crop_harvest_disintegration_losses_tb ALTER COLUMN m_n_crop_harvest_disintegration_losses_id SET DEFAULT nextval('public.m_n_crop_harvest_disintegrati_m_n_crop_harvest_disintegrati_seq'::regclass);


--
-- TOC entry 3117 (class 2604 OID 100915)
-- Name: m_n_crops_covercrop_tb m_n_crops_covercrop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_crops_covercrop_tb ALTER COLUMN m_n_crops_covercrop_id SET DEFAULT nextval('public.m_n_crops_covercrop_tb_m_n_crops_covercrop_tb_seq'::regclass);


--
-- TOC entry 3118 (class 2604 OID 100916)
-- Name: m_n_production_systems_manure_types_tb m_n_production_systems_manure_types_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_production_systems_manure_types_tb ALTER COLUMN m_n_production_systems_manure_types_id SET DEFAULT nextval('public.m_n_production_systems_manure_m_n_production_systems_manure_seq'::regclass);


--
-- TOC entry 3119 (class 2604 OID 100917)
-- Name: manure_source_tb manure_source_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_source_tb ALTER COLUMN manure_source_id SET DEFAULT nextval('public.manure_source_tb_manure_source_id_seq'::regclass);


--
-- TOC entry 3120 (class 2604 OID 100918)
-- Name: manure_specification_tb manure_specification_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_specification_tb ALTER COLUMN manure_specification_id SET DEFAULT nextval('public.manure_specification_tb_manure_specification_id_seq'::regclass);


--
-- TOC entry 3121 (class 2604 OID 100919)
-- Name: manure_type_tb manure_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_type_tb ALTER COLUMN manure_type_id SET DEFAULT nextval('public.manure_types_new_tb_manure_types_new_id_seq'::regclass);


--
-- TOC entry 3122 (class 2604 OID 100920)
-- Name: n_delivery_tb n_delivery_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.n_delivery_tb ALTER COLUMN n_delivery_id SET DEFAULT nextval('public.n_delivery_tb_n_delivery_id_seq'::regclass);


--
-- TOC entry 3143 (class 2604 OID 109433)
-- Name: n_fixing_tb n_fixing_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.n_fixing_tb ALTER COLUMN n_fixing_id SET DEFAULT nextval('public.nfixfakt_tb_n_fixing_tb_seq'::regclass);


--
-- TOC entry 3168 (class 2604 OID 101347)
-- Name: oek_economy_machines_tb economy_machines_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_economy_machines_tb ALTER COLUMN economy_machines_id SET DEFAULT nextval('public.oek_economy_machines_tb_economy_machines_id_seq'::regclass);


--
-- TOC entry 3173 (class 2604 OID 109378)
-- Name: oek_field_shape_tb field_shape_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_field_shape_tb ALTER COLUMN field_shape_id SET DEFAULT nextval('public.oek_field_shape_tb_field_shape_id_seq'::regclass);


--
-- TOC entry 3172 (class 2604 OID 101445)
-- Name: oek_neu_kultur_tillage_tb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_neu_kultur_tillage_tb ALTER COLUMN id SET DEFAULT nextval('public.oek_neu_kultur_tillage_tb_id_seq'::regclass);


--
-- TOC entry 3171 (class 2604 OID 101434)
-- Name: oek_neu_maschinenart_tb id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_neu_maschinenart_tb ALTER COLUMN id SET DEFAULT nextval('public.oek_neu_maschinenart_tb_id_seq'::regclass);


--
-- TOC entry 3145 (class 2604 OID 109364)
-- Name: oek_procedures_tb procedure_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_procedures_tb ALTER COLUMN procedure_id SET DEFAULT nextval('public.oek_procedures_tb_procedure_id_seq'::regclass);


--
-- TOC entry 3123 (class 2604 OID 100924)
-- Name: projects_tb projects_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects_tb ALTER COLUMN projects_id SET DEFAULT nextval('public.projects_tb_projects_id_seq1'::regclass);


--
-- TOC entry 3124 (class 2604 OID 100925)
-- Name: report_rows_tb report_rows_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_rows_tb ALTER COLUMN report_rows_id SET DEFAULT nextval('public.report_items_tb_report_row_id_seq'::regclass);


--
-- TOC entry 3135 (class 2604 OID 100926)
-- Name: soil_quality_tb soil_quality_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.soil_quality_tb ALTER COLUMN soil_quality_id SET DEFAULT nextval('public.soil_quality_tb_soil_quality_id_seq'::regclass);


--
-- TOC entry 3136 (class 2604 OID 100927)
-- Name: tillage_tb tillage_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tillage_tb ALTER COLUMN tillage_id SET DEFAULT nextval('public.tillage_tb_tillage_id_seq'::regclass);


--
-- TOC entry 3137 (class 2604 OID 100928)
-- Name: undersowing_new id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.undersowing_new ALTER COLUMN id SET DEFAULT nextval('public.undersowing_new_id_seq'::regclass);


--
-- TOC entry 3138 (class 2604 OID 100929)
-- Name: users_tb user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_tb ALTER COLUMN user_id SET DEFAULT nextval('public.users_tb_user_id_seq'::regclass);


--
-- TOC entry 3139 (class 2604 OID 100930)
-- Name: weed_infestation_index_tb weed_infestation_index_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.weed_infestation_index_tb ALTER COLUMN weed_infestation_index_id SET DEFAULT nextval('public.weed_infestation_index_tb_weed_infestation_index_id_seq'::regclass);


--
-- TOC entry 3140 (class 2604 OID 100931)
-- Name: winter_summer_crop_tb winter_summer_crop_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.winter_summer_crop_tb ALTER COLUMN winter_summer_crop_id SET DEFAULT nextval('public.winter_summer_crop_tb_id_seq'::regclass);


--
-- TOC entry 3141 (class 2604 OID 100932)
-- Name: winterhardiness_values_tb winterhardiness_values_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.winterhardiness_values_tb ALTER COLUMN winterhardiness_values_id SET DEFAULT nextval('public.winterhardiness_values_tb_winterhardiness_values_id_seq'::regclass);


--
-- TOC entry 3142 (class 2604 OID 100933)
-- Name: yield_crop_coefficients_bachinger_tb yield_crop_coefficients_bachinger_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_bachinger_tb ALTER COLUMN yield_crop_coefficients_bachinger_id SET DEFAULT nextval('public.yield_crop_coefficients_bachi_yield_crop_coefficients_bachi_seq'::regclass);


--
-- TOC entry 3175 (class 2606 OID 100935)
-- Name: covercrop_tb covercrop_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.covercrop_tb
    ADD CONSTRAINT covercrop_tb_pk PRIMARY KEY (covercrop_id);


--
-- TOC entry 3177 (class 2606 OID 100937)
-- Name: crop_key_figures_tb crop_key_figures_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_key_figures_tb
    ADD CONSTRAINT crop_key_figures_tb_pk PRIMARY KEY (crop_key_figures_id);


--
-- TOC entry 3179 (class 2606 OID 100939)
-- Name: crop_production_activities_tb crop_production_activities_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_production_activities_tb
    ADD CONSTRAINT crop_production_activities_tb_pk PRIMARY KEY (crop_production_activities_id);


--
-- TOC entry 3181 (class 2606 OID 100941)
-- Name: crop_type_tb crop_type_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_type_tb
    ADD CONSTRAINT crop_type_tb_pk PRIMARY KEY (crop_type_id);


--
-- TOC entry 3183 (class 2606 OID 100943)
-- Name: crops_tb crops_acronym_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_acronym_unique UNIQUE (acronym);


--
-- TOC entry 3185 (class 2606 OID 100945)
-- Name: crops_tb crops_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_tb_pk PRIMARY KEY (crop_id);


--
-- TOC entry 3245 (class 2606 OID 101425)
-- Name: harvest_methods_tb harvest_methods_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.harvest_methods_tb
    ADD CONSTRAINT harvest_methods_tb_pk PRIMARY KEY (harvest_methods_id);


--
-- TOC entry 3217 (class 2606 OID 100947)
-- Name: sowing_harvest_time_tb harvest_time_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sowing_harvest_time_tb
    ADD CONSTRAINT harvest_time_tb_pk PRIMARY KEY (sowing_harvest_time_id);


--
-- TOC entry 3187 (class 2606 OID 100949)
-- Name: humus_repro_crops_tb humus_factors_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_repro_crops_tb
    ADD CONSTRAINT humus_factors_tb_pk PRIMARY KEY (humus_factors_id);


--
-- TOC entry 3189 (class 2606 OID 100951)
-- Name: humus_vdlufa_crops_tb humus_vdlufa_crops_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_vdlufa_crops_tb
    ADD CONSTRAINT humus_vdlufa_crops_pk PRIMARY KEY (humus_vdlufa_crops_id);


--
-- TOC entry 3243 (class 2606 OID 101396)
-- Name: m_n_crop_harvest_disintegration_losses_tb m_n_crop_harvest_disintegration_losses_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_crop_harvest_disintegration_losses_tb
    ADD CONSTRAINT m_n_crop_harvest_disintegration_losses_tb_pk PRIMARY KEY (m_n_crop_harvest_disintegration_losses_id) INCLUDE (m_n_crop_harvest_disintegration_losses_id);


--
-- TOC entry 3191 (class 2606 OID 100953)
-- Name: m_n_crops_covercrop_tb m_n_crops_covercrop_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_crops_covercrop_tb
    ADD CONSTRAINT m_n_crops_covercrop_tb_pk PRIMARY KEY (m_n_crops_covercrop_id);


--
-- TOC entry 3193 (class 2606 OID 100955)
-- Name: m_n_manures_tb m_n_manures_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_manures_tb
    ADD CONSTRAINT m_n_manures_tb_pk PRIMARY KEY (m_n_manures_id);


--
-- TOC entry 3197 (class 2606 OID 100957)
-- Name: m_n_production_systems_manure_types_tb m_n_production_system_manure_types_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_production_systems_manure_types_tb
    ADD CONSTRAINT m_n_production_system_manure_types_pk PRIMARY KEY (m_n_production_systems_manure_types_id);


--
-- TOC entry 3199 (class 2606 OID 100959)
-- Name: manure_source_tb manure_source_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_source_tb
    ADD CONSTRAINT manure_source_tb_pkey PRIMARY KEY (manure_source_id);


--
-- TOC entry 3201 (class 2606 OID 100961)
-- Name: manure_specification_tb manure_specification_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_specification_tb
    ADD CONSTRAINT manure_specification_tb_pkey PRIMARY KEY (manure_specification_id);


--
-- TOC entry 3203 (class 2606 OID 100963)
-- Name: manure_type_tb manure_types_new_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manure_type_tb
    ADD CONSTRAINT manure_types_new_tb_pkey PRIMARY KEY (manure_type_id);


--
-- TOC entry 3205 (class 2606 OID 100965)
-- Name: n_delivery_tb n_delivery_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.n_delivery_tb
    ADD CONSTRAINT n_delivery_tb_pkey PRIMARY KEY (n_delivery_id);


--
-- TOC entry 3237 (class 2606 OID 109441)
-- Name: n_fixing_tb n_fixing_id_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.n_fixing_tb
    ADD CONSTRAINT n_fixing_id_pk PRIMARY KEY (n_fixing_id);


--
-- TOC entry 3241 (class 2606 OID 101352)
-- Name: oek_economy_machines_tb oek_economy_machines_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_economy_machines_tb
    ADD CONSTRAINT oek_economy_machines_tb_pkey PRIMARY KEY (economy_machines_id);


--
-- TOC entry 3251 (class 2606 OID 109383)
-- Name: oek_field_shape_tb oek_field_shape_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_field_shape_tb
    ADD CONSTRAINT oek_field_shape_tb_pkey PRIMARY KEY (field_shape_id);


--
-- TOC entry 3249 (class 2606 OID 101450)
-- Name: oek_neu_kultur_tillage_tb oek_neu_kultur_tillage_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_neu_kultur_tillage_tb
    ADD CONSTRAINT oek_neu_kultur_tillage_tb_pkey PRIMARY KEY (id);


--
-- TOC entry 3247 (class 2606 OID 101439)
-- Name: oek_neu_maschinenart_tb oek_neu_maschinenart_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_neu_maschinenart_tb
    ADD CONSTRAINT oek_neu_maschinenart_tb_pkey PRIMARY KEY (id);


--
-- TOC entry 3239 (class 2606 OID 109372)
-- Name: oek_procedures_tb procedures_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oek_procedures_tb
    ADD CONSTRAINT procedures_tb_pk PRIMARY KEY (procedure_id);


--
-- TOC entry 3207 (class 2606 OID 100975)
-- Name: production_system_tb production_system_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.production_system_tb
    ADD CONSTRAINT production_system_tb_pkey PRIMARY KEY (production_system_id);


--
-- TOC entry 3209 (class 2606 OID 100977)
-- Name: projects_tb projects_tb_pk_1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects_tb
    ADD CONSTRAINT projects_tb_pk_1 PRIMARY KEY (projects_id);


--
-- TOC entry 3211 (class 2606 OID 100979)
-- Name: report_rows_tb report_items_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report_rows_tb
    ADD CONSTRAINT report_items_tb_pkey PRIMARY KEY (report_rows_id);


--
-- TOC entry 3213 (class 2606 OID 100981)
-- Name: rotation_restrictions_crop_tb rotation_restrictions_crop_id_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rotation_restrictions_crop_tb
    ADD CONSTRAINT rotation_restrictions_crop_id_pk PRIMARY KEY (rotation_restrictions_crop_id);


--
-- TOC entry 3215 (class 2606 OID 100983)
-- Name: soil_quality_tb soil_quality_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.soil_quality_tb
    ADD CONSTRAINT soil_quality_tb_pk PRIMARY KEY (soil_quality_id);


--
-- TOC entry 3219 (class 2606 OID 100985)
-- Name: tillage_tb tillage_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tillage_tb
    ADD CONSTRAINT tillage_tb_pk PRIMARY KEY (tillage_id);


--
-- TOC entry 3221 (class 2606 OID 100987)
-- Name: undersowing_new undersowing_new_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.undersowing_new
    ADD CONSTRAINT undersowing_new_pkey PRIMARY KEY (id);


--
-- TOC entry 3223 (class 2606 OID 100989)
-- Name: users_tb users_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_tb
    ADD CONSTRAINT users_tb_pk PRIMARY KEY (user_id);


--
-- TOC entry 3225 (class 2606 OID 109397)
-- Name: weed_infestation_index_tb weed_infestation_index_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.weed_infestation_index_tb
    ADD CONSTRAINT weed_infestation_index_tb_pk PRIMARY KEY (weed_infestation_index_id);


--
-- TOC entry 3227 (class 2606 OID 100993)
-- Name: winter_summer_crop_tb winter_summer_crop_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.winter_summer_crop_tb
    ADD CONSTRAINT winter_summer_crop_tb_pkey PRIMARY KEY (winter_summer_crop_id);


--
-- TOC entry 3229 (class 2606 OID 100995)
-- Name: winterhardiness_values_tb winterhardiness_values_tb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.winterhardiness_values_tb
    ADD CONSTRAINT winterhardiness_values_tb_pkey PRIMARY KEY (winterhardiness_values_id);


--
-- TOC entry 3233 (class 2606 OID 100997)
-- Name: yield_crop_coefficients_tb yiel_crop_coefficients_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_tb
    ADD CONSTRAINT yiel_crop_coefficients_tb_pk PRIMARY KEY (yield_crop_coefficients_id);


--
-- TOC entry 3231 (class 2606 OID 100999)
-- Name: yield_crop_coefficients_bachinger_tb yield_crop_coefficients_bachinger_tb_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_bachinger_tb
    ADD CONSTRAINT yield_crop_coefficients_bachinger_tb_pk PRIMARY KEY (yield_crop_coefficients_bachinger_id);


--
-- TOC entry 3235 (class 2606 OID 109411)
-- Name: yield_crop_coefficients_tb yield_crop_coefficients_crop_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_tb
    ADD CONSTRAINT yield_crop_coefficients_crop_id_unique UNIQUE (crop_id);


--
-- TOC entry 3194 (class 1259 OID 101000)
-- Name: fki_m_n_production_system_manure_types_manure_type_id_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_m_n_production_system_manure_types_manure_type_id_fk ON public.m_n_production_systems_manure_types_tb USING btree (manure_type_id);


--
-- TOC entry 3195 (class 1259 OID 101001)
-- Name: fki_m_n_production_system_manure_types_production_system_id_fk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_m_n_production_system_manure_types_production_system_id_fk ON public.m_n_production_systems_manure_types_tb USING btree (production_system_id);


--
-- TOC entry 3252 (class 2606 OID 101003)
-- Name: crop_key_figures_tb crop_key_figures_crop_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_key_figures_tb
    ADD CONSTRAINT crop_key_figures_crop_id FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) NOT VALID;


--
-- TOC entry 3253 (class 2606 OID 101008)
-- Name: crop_key_figures_tb crop_key_figures_production_system_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crop_key_figures_tb
    ADD CONSTRAINT crop_key_figures_production_system_id FOREIGN KEY (production_system_id) REFERENCES public.production_system_tb(production_system_id) NOT VALID;


--
-- TOC entry 3254 (class 2606 OID 101013)
-- Name: crops_tb crops_crop_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_crop_type_id_fk FOREIGN KEY (crop_type_id) REFERENCES public.crop_type_tb(crop_type_id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 3255 (class 2606 OID 101018)
-- Name: crops_tb crops_harvest_time_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_harvest_time_id_fk FOREIGN KEY (harvest_time_id) REFERENCES public.sowing_harvest_time_tb(sowing_harvest_time_id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 3256 (class 2606 OID 101023)
-- Name: crops_tb crops_sowing_time_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_sowing_time_id_fk FOREIGN KEY (sowing_time_id) REFERENCES public.sowing_harvest_time_tb(sowing_harvest_time_id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 3257 (class 2606 OID 101028)
-- Name: crops_tb crops_winter_summer_crop_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.crops_tb
    ADD CONSTRAINT crops_winter_summer_crop_id_fk FOREIGN KEY (winter_summer_crop_id) REFERENCES public.winter_summer_crop_tb(winter_summer_crop_id);


--
-- TOC entry 3258 (class 2606 OID 101033)
-- Name: humus_repro_crops_tb humus_factors_crop_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_repro_crops_tb
    ADD CONSTRAINT humus_factors_crop_id_fk FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 3259 (class 2606 OID 101038)
-- Name: humus_repro_crops_tb humus_factors_production_system_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_repro_crops_tb
    ADD CONSTRAINT humus_factors_production_system_id_fk FOREIGN KEY (production_system_id) REFERENCES public.production_system_tb(production_system_id) ON UPDATE CASCADE ON DELETE RESTRICT NOT VALID;


--
-- TOC entry 3260 (class 2606 OID 101134)
-- Name: humus_vdlufa_crops_tb humus_vdlufa_crops_tb_production_system_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.humus_vdlufa_crops_tb
    ADD CONSTRAINT humus_vdlufa_crops_tb_production_system_id_fk FOREIGN KEY (humus_vdlufa_crops_id) REFERENCES public.production_system_tb(production_system_id) NOT VALID;


--
-- TOC entry 3263 (class 2606 OID 101149)
-- Name: m_n_manures_tb m_n_manures_tb_manure_source_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_manures_tb
    ADD CONSTRAINT m_n_manures_tb_manure_source_id_fk FOREIGN KEY (manure_type_id) REFERENCES public.manure_type_tb(manure_type_id) NOT VALID;


--
-- TOC entry 3262 (class 2606 OID 101144)
-- Name: m_n_manures_tb m_n_manures_tb_manure_specification_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_manures_tb
    ADD CONSTRAINT m_n_manures_tb_manure_specification_id FOREIGN KEY (manure_type_id) REFERENCES public.manure_specification_tb(manure_specification_id) NOT VALID;


--
-- TOC entry 3261 (class 2606 OID 101139)
-- Name: m_n_manures_tb m_n_manures_tb_manure_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_manures_tb
    ADD CONSTRAINT m_n_manures_tb_manure_type_id_fk FOREIGN KEY (manure_type_id) REFERENCES public.manure_type_tb(manure_type_id) NOT VALID;


--
-- TOC entry 3264 (class 2606 OID 101053)
-- Name: m_n_production_systems_manure_types_tb m_n_production_system_manure_types_manure_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_production_systems_manure_types_tb
    ADD CONSTRAINT m_n_production_system_manure_types_manure_type_id_fk FOREIGN KEY (manure_type_id) REFERENCES public.manure_type_tb(manure_type_id) NOT VALID;


--
-- TOC entry 3265 (class 2606 OID 101058)
-- Name: m_n_production_systems_manure_types_tb m_n_production_system_manure_types_production_system_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_n_production_systems_manure_types_tb
    ADD CONSTRAINT m_n_production_system_manure_types_production_system_id_fk FOREIGN KEY (production_system_id) REFERENCES public.production_system_tb(production_system_id) NOT VALID;


--
-- TOC entry 3269 (class 2606 OID 109442)
-- Name: n_fixing_tb n_fixing_crop_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.n_fixing_tb
    ADD CONSTRAINT n_fixing_crop_id_fk FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) NOT VALID;


--
-- TOC entry 3266 (class 2606 OID 109398)
-- Name: rotation_restrictions_crop_tb rotation_restriction_cropd_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rotation_restrictions_crop_tb
    ADD CONSTRAINT rotation_restriction_cropd_id_fk FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) NOT VALID;


--
-- TOC entry 3267 (class 2606 OID 101103)
-- Name: yield_crop_coefficients_bachinger_tb yield_crop_coefficients_bachinger_crop_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_bachinger_tb
    ADD CONSTRAINT yield_crop_coefficients_bachinger_crop_id_fk FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) NOT VALID;


--
-- TOC entry 3268 (class 2606 OID 109391)
-- Name: yield_crop_coefficients_tb yield_crop_coefficients_crop_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.yield_crop_coefficients_tb
    ADD CONSTRAINT yield_crop_coefficients_crop_id_fk FOREIGN KEY (crop_id) REFERENCES public.crops_tb(crop_id) NOT VALID;


-- Completed on 2022-07-13 08:35:49

--
-- PostgreSQL database dump complete
--

