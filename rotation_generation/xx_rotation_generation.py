"""
This is the old GenerationClass and also the FUTURE FREE ROTATIONGENERATION
"""



# from db_connect.db_connect import execute_query_get_one, execute_query_get_dictionary_list
# from dialog_window import DialogWindow
#
#
# class RotationGeneration:
#     def __init__(self, project):
#         self.project = project
#         self.crop_selection_list = [[], [], [], [], [], [], [], [], [], []]
#         self.rotation_duration = project.dict_tab_general["rotation_duration"]
#         self.proceed = True
#         # self.generate_covercrop_id()
#
#     def adjust_crop_selection_to_rotation_principles(self):
#
#         for i in range(0, self.rotation_duration):
#             sql_query = "SELECT crop_type_id FROM crops_tb WHERE crop_id = %(crop_id)s" \
#                         % {"crop_id": self.project.dict_tab_generator["crop_id"][i]}
#             crop_type_id = execute_query_get_one[sql_query][0]
#             self.project.generation_temp["crop_type_id"][i] = crop_type_id
#
#         # first crop has to be legume
#
#
#         # TODO tbc
#         pass
#
#     def generate_rotation(self):  # old assess
#         print("------------ASSESS----------------------------------------")
#         print("Covercrop IDs: ", self.project.dict_tab_crop["covercrop_id"])
#         print("DICT_TAB_CROP", self.project.dict_tab_crop)
#
#         # rotation_generation_pass = 0
#
#         for i in range(0, self.rotation_duration):
#             print("i = ", i)
#             x = 1
#             print("--------CROP INDEX ", i, "---------------")
#             print("self.crop_selection_list", self.crop_selection_list)
#             crop_id = self.project.dict_tab_crop["crop_id"][i]
#             next_crop_id = self.project.dict_tab_crop["crop_id"][(i + 1) % self.rotation_duration]
#             previous_crop_id = self.project.dict_tab_crop["crop_id"][(i - 1) % self.rotation_duration]
#
#             parameter_dict = {"crop_id": crop_id, "next_crop_id": next_crop_id,
#                               "previous_crop_id": previous_crop_id,
#                               "undersowing": self.project.dict_tab_crop["undersowing"][i],
#                               "covercrop_id": self.project.dict_tab_crop["covercrop_id"][i],
#                               "strawharvest": self.project.dict_tab_crop["strawharvest"][i],
#                               "manure_liquid_solid_type_id": self.project.dict_tab_crop["manure_liquid_solid_type_id"][
#                                   i],
#                               "previous_tpvliste_id": 0,
#                               "first_tpvliste_id": 0}
#
#             if i == 0:  # and rotation_generation_pass == 0:
#
#                 sql_query = """select f1.tpvliste_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery, f1.n_delivery_plus,
#                         f1.n_delivery_suffix, f1.previous_crop_type_id, f1.previous_crop_n_delivery,
#                         f1.previous_crop_n_delivery_suffix, f1.ef, f1.corrfact_yield, f1.undersowing, f1.covercrop_id,
#                         f1.strawharvest, f1.tillage_id, f1.anbauw, f1.sonstige, f1.manure_liquid_solid_type_id, f1.manure_level,
#                         f1.additional_parameters_id, f1.kultur from (SELECT DISTINCT nextnone.tpvliste_id,
#                         nextnone.crop_id, nextnone.n_delivery, nextnone.n_delivery_plus, nextnone.n_delivery_suffix,
#                         nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery,
#                         nextnone.previous_crop_n_delivery_suffix, nextnone.covercrop_id, nextnone.ef,
#                         nextnone.corrfact_yield, nextnone.undersowing, nextnone.tillage_id, nextnone.strawharvest,
#                         nextnone.anbauw, nextnone.sonstige, nextnone.manure_liquid_solid_type_id, nextnone.manure_level,
#                         nextnone.additional_parameters_id, nextnone.kultur
#                         FROM (SELECT DISTINCT t1.tpvliste_id, t1.crop_id, t1.kultur, t1.n_delivery, t1.n_delivery_plus,
#                             t1.n_delivery_suffix, t1.previous_crop_type_id, t1.anbauw, t1.sonstige,
#                             t1.previous_crop_n_delivery, t1.covercrop_id, t1.ef, t1.corrfact_yield, t1.undersowing,
#                             t1.tillage_id, t1.strawharvest, t1.previous_crop_n_delivery_suffix, t1.manure_liquid_solid_type_id,
#                             t1.manure_level, t1.additional_parameters_id
#                             FROM tpvliste t1 INNER JOIN tpvliste t2
#                                 ON t1.previous_crop_n_delivery = t2.n_delivery
#                                 AND t1.previous_crop_n_delivery_suffix = t2.n_delivery_suffix
#                                 AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct
#                                     WHERE ct.crop_id = t2.crop_id)
#                                 WHERE t2.crop_id = %(previous_crop_id)s) nextnone
#                                 INNER JOIN tpvliste p2
#                                 ON nextnone.n_delivery = p2.previous_crop_n_delivery
#                                 AND nextnone.n_delivery_suffix = p2.previous_crop_n_delivery_suffix
#                                 AND (SELECT ct.crop_type_id from crops_tb ct
#                                 WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id
#                                 WHERE p2.crop_id = %(next_crop_id)s AND nextnone.crop_id = %(crop_id)s) f1
#                                 JOIN crops_tb ct2 on f1.crop_id = ct2.crop_id
#                                 WHERE f1.covercrop_id = %(covercrop_id)s
#                                 AND f1.undersowing = %(undersowing)s and f1.strawharvest = %(strawharvest)s""" \
#                             % parameter_dict
#                 list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
#                 # for p in range(0, len(list_of_dictionaries)):
#                 #     self.crop_selection_list.insert[0][0](p, list_of_dictionaries[p])
#                 print("list_of_dictionaries ", list_of_dictionaries)
#                 for crop in list_of_dictionaries:
#                     list = [crop]
#
#                     self.crop_selection_list[0].append(list)
#                 print("Iteration 0 crop_selection_list:", self.crop_selection_list)
#             # TODO elif i < (self.rotation_duration - 1)
#             elif i == (self.rotation_duration - 1):  # last set of rotations
#                 for crop_rotation in self.crop_selection_list[i - 1]:
#                     parameter_dict["previous_tpvliste_id"] = crop_rotation[-1]["tpvliste_id"]
#                     parameter_dict["first_tpvliste_id"] = crop_rotation[0]["tpvliste_id"]
#
#                     sql_query = """select f1.tpvliste_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery, f1.n_delivery_plus,
#                             f1.n_delivery_suffix, f1.previous_crop_type_id, f1.previous_crop_n_delivery,
#                             f1.previous_crop_n_delivery_suffix, f1.ef, f1.corrfact_yield, f1.undersowing, f1.covercrop_id,
#                             f1.strawharvest, f1.tillage_id, f1.anbauw, f1.sonstige, f1.manure_liquid_solid_type_id, f1.manure_level,
#                             f1.additional_parameters_id, f1.kultur from (SELECT DISTINCT nextnone.tpvliste_id,
#                             nextnone.crop_id, nextnone.n_delivery, nextnone.n_delivery_plus, nextnone.n_delivery_suffix,
#                             nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery,
#                             nextnone.previous_crop_n_delivery_suffix, nextnone.covercrop_id, nextnone.ef,
#                             nextnone.corrfact_yield, nextnone.undersowing, nextnone.tillage_id, nextnone.strawharvest,
#                             nextnone.anbauw, nextnone.sonstige, nextnone.manure_liquid_solid_type_id, nextnone.manure_level,
#                             nextnone.additional_parameters_id, nextnone.kultur
#                             FROM (SELECT DISTINCT t1.tpvliste_id, t1.crop_id, t1.kultur, t1.n_delivery, t1.n_delivery_plus,
#                                 t1.n_delivery_suffix, t1.previous_crop_type_id, t1.anbauw, t1.sonstige,
#                                 t1.previous_crop_n_delivery, t1.covercrop_id, t1.ef, t1.corrfact_yield, t1.undersowing,
#                                 t1.tillage_id, t1.strawharvest, t1.previous_crop_n_delivery_suffix, t1.manure_liquid_solid_type_id,
#                                 t1.manure_level, t1.additional_parameters_id
#                                 FROM tpvliste t1 INNER JOIN tpvliste t2
#                                     ON t1.previous_crop_n_delivery = t2.n_delivery
#                                     AND t1.previous_crop_n_delivery_suffix = t2.n_delivery_suffix
#                                     AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct
#                                         WHERE ct.crop_id = t2.crop_id)
#                                     WHERE t2.tpvliste_id = %(previous_tpvliste_id)s) nextnone
#                                     INNER JOIN tpvliste p2
#                                     ON nextnone.n_delivery = p2.previous_crop_n_delivery
#                                     AND nextnone.n_delivery_suffix = p2.previous_crop_n_delivery_suffix
#                                     AND (SELECT ct.crop_type_id from crops_tb ct
#                                     WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id
#                                     WHERE p2.tpvliste_id = %(first_tpvliste_id)s AND nextnone.crop_id = %(crop_id)s) f1
#                                     JOIN crops_tb ct2 on f1.crop_id = ct2.crop_id
#                                     WHERE f1.covercrop_id = %(covercrop_id)s
#                                         AND f1.undersowing = %(undersowing)s and f1.strawharvest = %(strawharvest)s""" \
#                                 % parameter_dict
#
#                     list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
#                     print("list_of_dictionaries: ", list_of_dictionaries)
#                     y = 1
#
#                     for crop in list_of_dictionaries:
#                         print("crop_rotation 1", crop_rotation)
#                         print("COUNTER CROP ", y)
#                         y += 1
#                         print("CROP: ", crop)
#                         list_of_rotations = crop_rotation.copy()
#                         print("crop_rotation 2: ", crop_rotation, "\nlist_of_rotations: ", list_of_rotations)
#                         print("Type of crop rotation", type(crop_rotation))
#                         list_of_rotations.append(crop)
#
#                         print("list, crop APPENDED: ", list_of_rotations)
#
#                         print("Iteration", i, "self.crop_selection_list[i] before append", self.crop_selection_list[i])
#                         self.crop_selection_list[i].append(list_of_rotations)
#                         print("Iteration", i, "self.crop_selection_list[i] after append", self.crop_selection_list[i])
#                     print("Ende innerstes for")
#             else:
#                 for crop_rotation in self.crop_selection_list[i - 1]:
#
#                     print("crop_rotation 1", crop_rotation)
#                     print("COUNTER CROP_ROTATION ", x)
#                     x += 1
#                     parameter_dict["previous_tpvliste_id"] = crop_rotation[-1]["tpvliste_id"]
#
#                     print("crop['tpvliste_id'] ", crop_rotation[-1]["tpvliste_id"])
#
#                     print("else: if i < (self.rotation_duration - 1):")
#                     sql_query = """select f1.tpvliste_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery, f1.n_delivery_plus,
#                         f1.n_delivery_suffix, f1.previous_crop_type_id, f1.previous_crop_n_delivery,
#                         f1.previous_crop_n_delivery_suffix, f1.ef, f1.corrfact_yield, f1.undersowing, f1.covercrop_id,
#                         f1.strawharvest, f1.tillage_id, f1.anbauw, f1.sonstige, f1.manure_liquid_solid_type_id, f1.manure_level,
#                         f1.additional_parameters_id, f1.kultur from (SELECT DISTINCT nextnone.tpvliste_id,
#                         nextnone.crop_id, nextnone.n_delivery, nextnone.n_delivery_plus, nextnone.n_delivery_suffix,
#                         nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery,
#                         nextnone.previous_crop_n_delivery_suffix, nextnone.covercrop_id, nextnone.ef,
#                         nextnone.corrfact_yield, nextnone.undersowing, nextnone.tillage_id, nextnone.strawharvest,
#                         nextnone.anbauw, nextnone.sonstige, nextnone.manure_liquid_solid_type_id, nextnone.manure_level,
#                         nextnone.additional_parameters_id, nextnone.kultur
#                         FROM (SELECT DISTINCT t1.tpvliste_id, t1.crop_id, t1.kultur, t1.n_delivery, t1.n_delivery_plus,
#                             t1.n_delivery_suffix, t1.previous_crop_type_id, t1.anbauw, t1.sonstige,
#                             t1.previous_crop_n_delivery, t1.covercrop_id, t1.ef, t1.corrfact_yield, t1.undersowing,
#                             t1.tillage_id, t1.strawharvest, t1.previous_crop_n_delivery_suffix, t1.manure_liquid_solid_type_id,
#                             t1.manure_level, t1.additional_parameters_id
#                             FROM tpvliste t1 INNER JOIN tpvliste t2
#                                 ON t1.previous_crop_n_delivery = t2.n_delivery
#                                 AND t1.previous_crop_n_delivery_suffix = t2.n_delivery_suffix
#                                 AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct
#                                     WHERE ct.crop_id = t2.crop_id)
#                                 WHERE t2.tpvliste_id = %(previous_tpvliste_id)s) nextnone
#                                 INNER JOIN tpvliste p2
#                                 ON nextnone.n_delivery = p2.previous_crop_n_delivery
#                                 AND nextnone.n_delivery_suffix = p2.previous_crop_n_delivery_suffix
#                                 AND (SELECT ct.crop_type_id from crops_tb ct
#                                 WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id
#                                 WHERE p2.crop_id = %(next_crop_id)s AND nextnone.crop_id = %(crop_id)s) f1
#                                 JOIN crops_tb ct2 on f1.crop_id = ct2.crop_id
#                                 WHERE f1.covercrop_id = %(covercrop_id)s
#                                 AND f1.undersowing = %(undersowing)s and f1.strawharvest = %(strawharvest)s""" \
#                                 % parameter_dict
#
#                     list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
#                     print("list_of_dictionaries: ", list_of_dictionaries)
#                     y = 1
#
#                     for crop in list_of_dictionaries:
#                         print("crop_rotation 1", crop_rotation)
#                         print("COUNTER CROP ", y)
#                         y += 1
#                         print("CROP: ", crop)
#                         list_of_rotations = crop_rotation.copy()
#                         print("crop_rotation 2: ", crop_rotation, "\nlist_of_rotations: ", list_of_rotations)
#                         print("Type of crop rotation", type(crop_rotation))
#                         list_of_rotations.append(crop)
#
#                         print("list, crop APPENDED: ", list_of_rotations)
#
#                         print("Iteration", i, "self.crop_selection_list[i] before append", self.crop_selection_list[i])
#                         self.crop_selection_list[i].append(list_of_rotations)
#                         print("Iteration", i, "self.crop_selection_list[i] after append", self.crop_selection_list[i])
#                     print("Ende innerstes for")
#                 print("Ende for tpv_crop_rotation in self.crop_selection_list[", i, "]")
#
#             print("Crop selection list Iteration", str(i), self.crop_selection_list)
#
#         for i in range(0, self.rotation_duration):
#             if len(self.crop_selection_list[i]) == 0:
#                 DialogWindow().warning_no_entry_in_db(i)
#
