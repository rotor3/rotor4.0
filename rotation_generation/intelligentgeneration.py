from db_connect.db_connect import execute_query_get_dictionary_list
from dialog_window import DialogWindow


class IntelligentGeneration:
    def __init__(self, project, ui_status):
        self.project = project
        self.ui_status = ui_status
        '''this is a temporary list that contains all possible CPAs'''
        self.crop_selection_list = [[], [], [], [], [], [], [], [], [], []]
        self.rotation_duration = project.tab_general["rotation_duration"]
        self.get_result()


    def get_result(self):
        try:
            self.get_intelli_generation()

        except:
            self.ui_status.error["error"] = True
            for list in self.crop_selection_list:
                if list == []:
                    index = self.crop_selection_list.index(list)
                    break
            self.ui_status.error["errormessage"] = f"""Please change your selection for year {index + 1}, or the following, since this combination is not possible!"""

        self.add_parameters_to_crop_rotation()


    def get_intelli_generation(self):
        print("------------INTELLIGENT GENERATION----------------------------------------")
        print("Covercrop IDs: ", self.project.tab_rotation["covercrop_id"])
        print("DICT_TAB_CROP", self.project.tab_rotation)

        # rotation_generation_pass = 0

        index: int
        for index in range(0, self.rotation_duration):
            x = 1
            print("--------CROP INDEX ", index, "---------------")
            print("self.crop_selection_list at", self.project.tab_rotation["crop_id"])
            crop_id = self.project.tab_rotation["crop_id"][index]
            next_crop_id = self.project.tab_rotation["crop_id"][(index + 1) % self.rotation_duration]
            previous_crop_id = self.project.tab_rotation["crop_id"][(index - 1) % self.rotation_duration]
            crop_type_id = self.project.tab_rotation["crop_type_id"][index]


            parameter_dict = {"crop_id": crop_id, "next_crop_id": next_crop_id, "previous_crop_id": previous_crop_id,
                              "undersowing": self.project.tab_rotation["undersowing"][index], "crop_type_id": self.project.tab_rotation["crop_type_id"][index]}

            parameter_dict = {"crop_id": crop_id, "crop_type_id": crop_type_id, "next_crop_id": next_crop_id,
                              "previous_crop_id": previous_crop_id,
                              "undersowing": self.project.tab_rotation["undersowing"][index],
                              "previous_crop_production_activities_id": 0,
                              "first_crop_production_activities_id": 0}

            '''In this first step, there can be two results. Previous_crop_type_id is fix, but not the previous n_level'''
            if index == 0:  # and rotation_generation_pass == 0:

                sql_query = """select f1.crop_production_activities_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery, 
                        f1.previous_crop_type_id, f1.previous_crop_n_delivery, 
                        f1.undersowing, f1.acronym 
                        FROM (SELECT DISTINCT nextnone.crop_production_activities_id, nextnone.crop_id, 
                        nextnone.n_delivery, nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery, 
                        nextnone.undersowing, nextnone.acronym
                        FROM (SELECT DISTINCT t1.crop_production_activities_id, t1.crop_id, t1.acronym, t1.n_delivery, 
                            t1.previous_crop_type_id,
                            t1.previous_crop_n_delivery, t1.undersowing
                            FROM crop_production_activities_tb t1 INNER JOIN crop_production_activities_tb t2 
                                ON t1.previous_crop_n_delivery = t2.n_delivery 
                                AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct 
                                    WHERE ct.crop_id = t2.crop_id)
                                WHERE t2.crop_id = %(previous_crop_id)s) nextnone
                                INNER JOIN crop_production_activities_tb p2 
                                ON nextnone.n_delivery = p2.previous_crop_n_delivery 
                                AND (SELECT ct.crop_type_id from crops_tb ct 
                                WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id 
                                WHERE p2.crop_id = %(next_crop_id)s AND nextnone.crop_id = %(crop_id)s) f1
                                JOIN crops_tb ct2 ON f1.crop_id = ct2.crop_id
                                WHERE f1.undersowing = %(undersowing)s;"""  \
                            % parameter_dict

                list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
                print("List of dictionaries", list_of_dictionaries)

                print("list_of_dictionaries ", list_of_dictionaries)
                for crop in list_of_dictionaries:
                    list = [crop]

                    self.crop_selection_list[0].append(list)
                print("Iteration 0 crop_selection_list:", self.crop_selection_list)
            # TODO elif i < (self.rotation_duration - 1)
            elif index == (self.rotation_duration - 1):  # last set of rotations
                for crop_rotation in self.crop_selection_list[index - 1]:
                    parameter_dict["previous_crop_production_activities_id"] = crop_rotation[-1][
                        "crop_production_activities_id"]
                    parameter_dict["first_crop_production_activities_id"] = crop_rotation[0][
                        "crop_production_activities_id"]

                    sql_query = """SELECT f1.crop_production_activities_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery,
                                f1.previous_crop_type_id, f1.previous_crop_n_delivery, f1.undersowing, f1.acronym 
                                FROM (SELECT DISTINCT nextnone.crop_production_activities_id,
                                nextnone.crop_id, nextnone.n_delivery,   
                                nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery, 
                                nextnone.undersowing,  
                                nextnone.acronym
                                FROM (SELECT DISTINCT t1.crop_production_activities_id, t1.crop_id, t1.acronym, 
                                t1.n_delivery, t1.previous_crop_type_id, t1.previous_crop_n_delivery, t1.undersowing
                                    FROM crop_production_activities_tb t1 INNER JOIN crop_production_activities_tb t2 
                                        ON t1.previous_crop_n_delivery = t2.n_delivery 
                                        AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct 
                                            WHERE ct.crop_id = t2.crop_id)
                                        WHERE t2.crop_production_activities_id = %(previous_crop_production_activities_id)s) nextnone
                                        INNER JOIN crop_production_activities_tb p2 
                                        ON nextnone.n_delivery = p2.previous_crop_n_delivery 
                                        AND (SELECT ct.crop_type_id from crops_tb ct 
                                        WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id 
                                        WHERE p2.crop_production_activities_id = %(first_crop_production_activities_id)s AND nextnone.crop_id = %(crop_id)s) f1
                                        JOIN crops_tb ct2 on f1.crop_id = ct2.crop_id
                                        WHERE f1.undersowing = %(undersowing)s;""" \
                                % parameter_dict

                    list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
                    print("list_of_dictionaries: ", list_of_dictionaries)
                    y = 1

                    for crop in list_of_dictionaries:
                        print("crop_rotation 1", crop_rotation)
                        print("COUNTER CROP ", y)
                        y += 1
                        print("CROP: ", crop)
                        list_of_rotations = crop_rotation.copy()
                        print("crop_rotation 2: ", crop_rotation, "\nlist_of_rotations: ", list_of_rotations)
                        print("Type of crop rotation", type(crop_rotation))
                        list_of_rotations.append(crop)

                        print("list, crop APPENDED: ", list_of_rotations)

                        print("Iteration", index, "self.crop_selection_list[i] before append", self.crop_selection_list[index])
                        self.crop_selection_list[index].append(list_of_rotations)
                        print("Iteration", index, "self.crop_selection_list[i] after append", self.crop_selection_list[index])
                    print("Ende innerstes for")

                # write list of results into project
                self.project.results["crop_rotation"] = self.crop_selection_list[index][0]


            else:
                for crop_rotation in self.crop_selection_list[index - 1]:

                    print("crop_rotation 1", crop_rotation)
                    print("COUNTER CROP_ROTATION ", x)
                    x += 1
                    parameter_dict["previous_crop_production_activities_id"] = crop_rotation[-1][
                        "crop_production_activities_id"]

                    print("crop['crop_production_activities_id'] ", crop_rotation[-1]["crop_production_activities_id"])

                    print("else: if index < (self.rotation_duration - 1):")
                    sql_query = """select f1.crop_production_activities_id, f1.crop_id, ct2.crop_type_id, f1.n_delivery, 
                            f1.previous_crop_type_id, f1.previous_crop_n_delivery, f1.undersowing, 
                            f1.acronym from (SELECT DISTINCT nextnone.crop_production_activities_id,
                            nextnone.crop_id, nextnone.n_delivery, 
                            nextnone.previous_crop_type_id, nextnone.previous_crop_n_delivery, 
                            nextnone.undersowing, nextnone.acronym
                            FROM (SELECT DISTINCT t1.crop_production_activities_id, t1.crop_id, t1.acronym, t1.n_delivery, 
                                t1.previous_crop_type_id, 
                                t1.previous_crop_n_delivery, t1.undersowing
                                FROM crop_production_activities_tb t1 INNER JOIN crop_production_activities_tb t2 
                                    ON t1.previous_crop_n_delivery = t2.n_delivery 
                                    AND t1.previous_crop_type_id = (select ct.crop_type_id from crops_tb ct 
                                        WHERE ct.crop_id = t2.crop_id)
                                    WHERE t2.crop_production_activities_id = %(previous_crop_production_activities_id)s) nextnone
                                    INNER JOIN crop_production_activities_tb p2 
                                    ON nextnone.n_delivery = p2.previous_crop_n_delivery 
                                    AND (SELECT ct.crop_type_id from crops_tb ct 
                                    WHERE ct.crop_id = nextnone.crop_id)= p2.previous_crop_type_id 
                                    WHERE p2.crop_id = %(next_crop_id)s AND nextnone.crop_id = %(crop_id)s) f1
                                    JOIN crops_tb ct2 on f1.crop_id = ct2.crop_id
                                    WHERE f1.undersowing = %(undersowing)s;""" \
                                % parameter_dict

                    list_of_dictionaries = execute_query_get_dictionary_list(sql_query)
                    print("list_of_dictionaries: ", list_of_dictionaries)
                    y = 1

                    for crop in list_of_dictionaries:
                        print("crop_rotation 1", crop_rotation)
                        print("COUNTER CROP ", y)
                        y += 1
                        print("CROP: ", crop)
                        list_of_rotations = crop_rotation.copy()
                        print("crop_rotation 2: ", crop_rotation, "\nlist_of_rotations: ", list_of_rotations)
                        print("Type of crop rotation", type(crop_rotation))
                        list_of_rotations.append(crop)

                        print("list, crop APPENDED: ", list_of_rotations)

                        print("Iteration", index, "self.crop_selection_list[i] before append", self.crop_selection_list[index])
                        self.crop_selection_list[index].append(list_of_rotations)
                        print("Iteration", index, "self.crop_selection_list[i] after append", self.crop_selection_list[index])
                    print("Ende innerstes for")
                print("Ende for tpv_crop_rotation in self.crop_selection_list[", index, "]")

            print("Crop selection list Iteration", str(index), self.crop_selection_list)

        # if no match is found within the range of the rotation duration, the length of the list of results is 0
        for index in range(0, self.rotation_duration):
            if len(self.crop_selection_list[index]) == 0:
                self.ui_status.error["error"] = True
                self.ui_status.error["errormessage"] = f"""There are no entries in the database. The generation of a crop rotation got stuck in year {(index + 1)} """
                DialogWindow().warning_no_entry_in_db(index)

                break
            else:
                self.ui_status.error["error"] = False

    # TODO complete
    def add_parameters_to_crop_rotation(self):
        if self.ui_status.error["error"] is False:
            for index in range(0, self.rotation_duration):
                self.project.results["crop_rotation"][index]["early_tillage"] = \
                    self.project.tab_rotation["early_tillage"][index]
                self.project.results["crop_rotation"][index]["strawharvest"] = \
                    self.project.tab_rotation["strawharvest"][index]
                self.project.results["crop_rotation"][index]["main_legume_ratio"] = \
                    self.project.tab_rotation["main_legume_ratio"][index]
                self.project.results["crop_rotation"][index]["silage"] = \
                    self.project.tab_rotation["silage"][index]
                self.project.results["crop_rotation"][index]["hay"] = \
                    self.project.tab_rotation["hay"][index]
                self.project.results["crop_rotation"][index]["green_fodder"] = \
                    self.project.tab_rotation["green_fodder"][index]
                self.project.results["crop_rotation"][index]["undersowing_winterhardiness"] = \
                    self.project.tab_rotation["undersowing_winterhardiness"][index]
                self.project.results["crop_rotation"][index]["undersowing_legume_ratio"] = \
                    self.project.tab_rotation["undersowing_legume_ratio"][index]
                self.project.results["crop_rotation"][index]["covercrop"] = \
                    self.project.tab_rotation["covercrop"][index]
                self.project.results["crop_rotation"][index]["covercrop_id"] = \
                    self.project.tab_rotation["covercrop_id"][index]
                self.project.results["crop_rotation"][index]["covercrop_winterhardiness"] = \
                    self.project.tab_rotation["covercrop_winterhardiness"][index]
                self.project.results["crop_rotation"][index]["covercrop_legume_ratio"] = \
                    self.project.tab_rotation["covercrop_legume_ratio"][index]
                self.project.results["crop_rotation"][index]["covercrop_harvest"] = \
                    self.project.tab_rotation["covercrop_harvest"][index]
                self.project.results["crop_rotation"][index]["m_n_manures_id"] = \
                    self.project.tab_rotation["m_n_manures_id"][index]
                self.project.results["crop_rotation"][index]["manure_amount"] = \
                    self.project.tab_rotation["manure_amount"][index]
                self.project.result_general["manure_values"] = self.project.tab_manure["manure_values"]

