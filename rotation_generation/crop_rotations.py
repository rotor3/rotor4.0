from rotation_generation.assessment import AssessmentGeneration
from rotation_generation.free_generation import FreeGeneration
from rotation_generation.intelligentgeneration import IntelligentGeneration

from project_handling.project import Project


class CropRotation:
    def __init__(self, project, ui_status):
        self.project = project
        self.ui_status = ui_status

        self.rotation_duration = project.tab_general["rotation_duration"]

        self.get_results()

    def get_results(self):
        # reset results if they have been calculated before
        temp_project = Project()
        self.project.result_general = temp_project.result_general
        self.project.results_crop_rotations_free_generation = temp_project.results_crop_rotations_free_generation
        self.project.results = temp_project.results

        '''For assessment method 1 and 3, there is one set of CPAs, for 2 there are several solutions'''
        if self.project.tab_general["assessment_method"] == 1:
            result = IntelligentGeneration(self.project, self.ui_status)

        elif self.project.tab_general["assessment_method"] == 2:
            result = FreeGeneration(self.project, self.ui_status)
        else:
            result = AssessmentGeneration(self.project, self.ui_status)

        self.project = result.project
        # self.ui_status = result.ui_status

