from db_connect.db_connect import execute_query_get_dictionary_list

result = []
for index in range(1, 34):
    results = {"crop_id": index, "n_need_crop_per_t_fresh_mass_yield_main": 0}
    parameter_dict = {"crop_id": index, "production_system_id": 2}

    sql_query = """SELECT * FROM crop_key_figures_tb 
                 WHERE crop_id = %(crop_id)s AND production_system_id = %(production_system_id)s""" \
                % parameter_dict

    npk_need_values = execute_query_get_dictionary_list(sql_query)[0]


    # ([TM]*[nHP]+[TMNP]*[fNP]*[NNP]) AS N_Need_per_dt,
    # N-need of main product of crop the per t of main product's fresh mass (project.tab_yield["yield_amount_t"]
    results["n_need_main_product_per_t_fresh_mass_yield_main"] = \
        npk_need_values["dry_mass"] * npk_need_values["n_main_product_per_t"]
    # N-need of byproduct of crop the per t of main product
    results["n_need_byproduct_per_t_fresh_mass_yield_main"] = \
        npk_need_values["ratio_byproduct_main_product"] * npk_need_values["dry_mass_byproduct"] * \
        npk_need_values["n_byproduct_per_t"]
    # total N-need of the per t of main product
    results["n_need_crop_per_t_fresh_mass_yield_main"] = \
        results["n_need_main_product_per_t_fresh_mass_yield_main"] + \
        results["n_need_byproduct_per_t_fresh_mass_yield_main"]

    print(results)