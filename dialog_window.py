import sys
from PySide2.QtWidgets import QApplication, QWidget, QInputDialog,QTableWidget, QLineEdit, QTableWidgetItem, QPushButton, \
    QMessageBox
# from PySide6.ui.properties import QtWidgets

from project_handling.project import Project


class DialogWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Save Project Dialog'
        self.left = 100
        self.top = 100
        self.width = 1640
        self.height = 480
        # self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)


    def save_project_window(self, rotor_project):
        text, ok_pressed = QInputDialog.getText(self, "SAVE PROJECT", "Projectname:", QLineEdit.Normal,
                                                rotor_project.project_name)

        # TODO: it NEEDS TO BE VERIFIED THAT Project Name does not yet exist (Unique Restraint)
        if ok_pressed and text != '':
            rotor_project.project_name = text
            print("Textboxtext: ", rotor_project.project_name)
        return rotor_project

    def load_project_window(self, rotor_project):
        # layout = QVBoxLayout()
        self.lw_bt_load = QPushButton("Load Button")
        self.lw_bt_load.clicked.connect(self.load_project)
        # layout.addWidget(self.lw_bt_load)

        # self.lehello = QLabel("Hello")
        # layout.addWidget(self.lehello)

        self.lw_bt_sort_by_name = QPushButton("QFileDialog object")
        self.lw_bt_sort_by_name.clicked.connect(self.sort_by_name)
        # layout.addWidget(self.lw_bt_sort_by_name)

        self.recordset_projects = self.get_record()
        print("RECORDSET PROJECT: ", self.recordset_projects)

        self.projects_count = len(self.recordset_projects)
        print("Länge: ", self.projects_count)

        self.load_project_table_view = QTableWidget(self)
        # self.tableWidget_test.setGeometry(QtCore.QRect(30, 40, 451, 191))

        self.load_project_table_view.setObjectName("load_project_table_view")
        self.load_project_table_view.setColumnCount(3)
        self.load_project_table_view.hideColumn(0)
        self.load_project_table_view.setHorizontalHeaderItem(1, QTableWidgetItem("Name"))
        self.load_project_table_view.setHorizontalHeaderItem(2, QTableWidgetItem("Date Changed"))

        text, load_pressed = QInputDialog.getText(self, "LOAD PROJECT", "Projectname:")

        # NEEDS TO BE VERIFIED THAT Project Name does not yet exist (Unique Restraint)
        if load_pressed and text != '':
            rotor_project.project_name = text
            print("Textboxtext: ", rotor_project.project_name)
        return rotor_project

    # TODO translate
    def warning_no_entry_in_db(self, index):
        QMessageBox.warning(self, f"Warning 2, year "
                                            f"{str(index + 1)}",
                                      "There is no entry in the Rotor database for this combination of crop, "
                                      "undersowing and covercrop."
                                      "\nPlease change at least one of those parameters, or the undersowig of the previous year! ",
                                      QMessageBox.Ok, QMessageBox.Ok)

    def warning_of_error(self, error):
        QMessageBox.warning(self, 'Error', 'The project could not be loaded from '
                                            'the database. \nThe following error '
                                            f'occurred:\n{error}')

    def warning_of_import_error(self):
        QMessageBox.warning(self, 'Error', 'The project could not be imported. \n'
                                           'The format of the .rotor file is incorrect.')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = DialogWindow()
    y = ex.save_project_window(Project())
    x = ex.warning_no_entry_in_db(5)
    z = ex.warning_of_error("Some Errormessage")
    sys.exit(app.exec_())
