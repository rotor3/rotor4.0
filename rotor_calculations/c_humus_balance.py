from rotor_calculations.rotor_calculation import RotorCalculation


class CHumusBalance(RotorCalculation):

    def get_result(self):
        pass



    def get_vdlufa_humus_units_factor(self,  m_n_manures_id):
        print("Manure.get_vdlufa_humus_units")
        """The humus units are calculated via liear regression from humus_units_factor_a and b and dry mass: dm * A + B"""
        self.project.result_general["manure_values"][m_n_manures_id]["vdlufa_humus_units"] = \
            self.project.result_general["manure_values"][m_n_manures_id]["dry_mass"] * \
            self.project.result_general["manure_values"][m_n_manures_id]["vdlufa_humus_units_factor_a"] + \
            self.project.result_general["manure_values"][m_n_manures_id]["vdlufa_humus_units_factor_b"]

    # TODO ???
    def get_humus_vdlufa_and_repro_from_manure(self, index):
        self.project.results["crop_rotation"][index]["vdlufa_humus_units_from_manure"] = 0
        self.project.results["crop_rotation"][index]["repro_humus_units_from_manure"] = 0
        cpa = self.project.results["crop_rotation"][index]
        vdlufa_humus_units_from_manure = 0
        repro_humus_units_from_manure = 0
        print("cpa", cpa)
        print("cpa['m_n_manures_id']", cpa["m_n_manures_id"])

        # iterate through all manures (index_2, the index of m_n_manures_id) per crop (cpa)
        for index_2, m_n_manures_id in enumerate(cpa["m_n_manures_id"]):
            if m_n_manures_id != 0:
                # VDLUFA all manures per crop added
                vdlufa_humus_units_from_manure = vdlufa_humus_units_from_manure + cpa["manure_amount"][index_2] * self.project.result_general["manure_values"][m_n_manures_id]["vdlufa_humus_units"]
                repro_humus_units_from_manure = repro_humus_units_from_manure + cpa["manure_amount"][index_2] * self.project.result_general["manure_values"][m_n_manures_id]["repro_humus_units"]

