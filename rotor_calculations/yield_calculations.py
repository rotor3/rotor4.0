from db_connect.db_connect import execute_query_get_dictionary_list, ex
from rotor_calculations.rotor_calculation import RotorCalculation


class Yield(RotorCalculation):

    def get_result(self):

        self.get_amount_byproduct()
        self.get_amount_dry_masses()
        self.get_n_in_main()
        self.get_n_in_byproduct()
        self.get_p_in_main()
        self.get_p_in_byproduct()
        self.get_k_in_main()
        self.get_k_in_byproduct()

    def get_amount_byproduct(self):
        pass
    def get_amount_dry_masses(self):
        pass

    def get_n_in_main(self):
        pass
    def get_n_in_byproduct(self):
        pass
    def get_p_in_main(self):
        pass
    def get_p_in_byproduct(self):
        pass
    def get_k_in_main(self):
        pass
    def get_k_in_byproduct(self):
        pass
