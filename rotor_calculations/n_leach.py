

"""
Public Function F_Nleach(output, K, AZ, N, Tillage, yield_dt, Ndfak, EF, N_Kult_Min, covercrop, _
            undersowing, Manuring, N_Aufn_Ant_ZwiFr, N_Aufn_Ant_US_vor, N_Aufn_Ant_US_nach, VorFrGr, VorfrTyp, FruchtTyp, WinSom, _
            N_Need_per_dt, NHP, NNP, FNP, TM, Broesel, WinNN, yielddtoOD, Diff_NUeb, WHCrz) As Double

output String der die Art des Outputs bestimmt
K = Kultur
Az Ackerzahl
N N Precipitation  FROM NFixUS but from GUI / self.project
Tillage
yield_dt
Ndfak  ===???
EF aus Yielddt, also erhöhte EF
N_Kult_Min   FROM KW crop_key_figures_tb.n_mineralisation_crop
N_Aufn_Ant_ZwiFr [N_Aufn_Ant_ZwiFr]  FROM KW crop_key_figures_tb.n_uptake_covercrop
N_Aufn_Ant_US_vor [N_Aufn_Ant_US_vor]  FROM KW crop_key_figures_tb.n_uptake_undersowing_pre_seeding
N_Aufn_Ant_US_nach [N_Aufn_Ant_US_nach]  FROM KW crop_key_figures_tb.n_uptake_undersowing_post_harvest
NHP = [KW].[NHP]  FROM KW crop_key_figures_tb.n_in_main_product
NNP = [KW].[NNP]  FROM KW crop_key_figures_tb.n_in_byproduct
FNP = [KW].[fNP]  FROM KW crop_key_figures_tb.ratio_byproduct_mainproduct
TM = [KW].[TM]   FROM KW crop_key_figures_tb.dry_mass
Broesel = [Broesel] FROM KW crop_key_figures_tb.broeckel
WinNN = [WinNN]
yielddtoOD = [Yielddt].[yielddtoOD]
Diff_NUeb = 1
WHCrz = [WHCrz]

'sri  =   AZ         Ackerzahl
'yield_dt  = Ertrag     Im Verfahren errechneter Ertrag des Hauptproduktes
'ndf_soil  = Ndfs       N der aus dem Boden aufgenommen wurde (nicht gleich abgefahrenem N)
'n_level   = EF         Ertragsfunktion 1,2 oder 3  #TODO Achtung, welcher?
'ndf_soil  = NT         Total N in top soil (kg/ha)
'n_level_min_pa = N_Kult_Min             Kulturartenspezifischer Mineralisationsfaktor von efminpa`
'Durchwaschh_ZF         Halbierung der Durchwaschungshäufigkeit aufgrund der covercrop
'N_Aufn_Ant_ZwiFr       Fruchtartenabhängiger Anteil der N-Aufnahme der covercrop vom N-Überhang
'N_Aufn_Ant_US_vor      Fruchtartenabhängiger Anteil der N-Aufnahme der undersowing aus
'                           Vorfrucht vom N-Überhang der Prüffrucht vor Aussaat
'N_Aufn_Ant_US_nach     Fruchtartenabhängiger Anteil der N-Aufnahme der undersowing
'                           in der Prüffrucht vom N-Überhang der Prüffrucht nach Ernte
'WHCrz = whc_rz              water holding capacity in root zone [mm H2O]

"""
from db_connect.db_connect import execute_query_get_dictionary_list
from rotor_calculations.rotor_calculation import RotorCalculation


class NLeach(RotorCalculation):

    def get_result(self):
        self.get_n_leach()



    def get_n_leach(self):

        sri = self.project.tab_general["soil_rating_index"]
        whc_rz = self.project.results["whc_rz"]

        for index in range (0, self.rotation_duration):
            crop_id = self.project.results["crop_rotation"][index]["crop_id"]
            sql_query = """SELECT n_mineralisation_crop, n_uptake_covercrop, n_uptake_undersowing_pre_seeding, 
                    n_uptake_undersowing_post_harvest, n_in_main_product, n_in_byproduct, ratio_byproduct_mainproduct, 
                    dry_mass, broeckel FROM crop_key_figures_tb 
                    WHERE production_system_id = 2 AND crop_id = %(crop_id)s""" \
                    % {"crop_id": crop_id}
            crop_key_figures = execute_query_get_dictionary_list(sql_query)
            yield_dt = self.project.results["yield_dt"][index]
            # TODO ndf soil
            ndf_soil = self.project.results["ndf_soil"][index]
            n_level = self.project.results["n_level"][index]
            n_level_min_pa = self.project.result["n_level_mineralisation_pa"][index]
            covercrop_id = self.project.results["crop_rotation"][index]["covercrop_id"] # 1 is none 2 is
            undersowing = self.project.results["crop_rotation"][index]["undersowing"]

            if covercrop_id == 3: # from old code: covercrop is undersowing of previous year.
                # Since all functions GetEtab_US_Cer(),-Kleg(), Zwifr() in the end do the same the prabability cc_us is taken prom previous year
                if self.project.results["crop_rotation"][index]["covercrop_id"] == 3:
                    ew_us_vor = self.project.results["est_probability_cc_us"][(index - 1) % self.rotation_duration]
                else:
                    ew_us_nach = self.project.results["est_probability_cc_us"][index]
                    ew_zf = self.project.results["est_probability_cc_us"][index]

            # Nminp = Nminpy(AZ)
            # Nminpa = Nminp * EfMinpa * N_Kult_Min * (0.8 + NF) - Diff_NUeb (Diff_NUeb ist fest 1)
            # fester Mineraliserungsfaktor =2,0% nach Scheffer Schachtschabel (1979) Bodenkunde 10. Auflage

            n_level_min_pa = self.project.result["n_level_mineralisation_pa"][index]

            # F_ReMinAntK
            list_of_lucernes = [24, 25, 26, 27, 28, 29, 30]
            if crop_id not in list_of_lucernes:
                frosthardiness_undersowing = 3
                frosthardiness_covercrop = 3
            else:
                frosthardiness_undersowing = self.project.tab_general["frosthardiness_undersowing"]
                frosthardiness_covercrop = self.project.tab_general["frosthardiness_covercrop"]

            # Durchwaschungshäufigkeit


            # wird nicht benutzt
            # NdfOD = F_NdfOD(K, Manuring) 'newly implemented Ba: 6.02.2017


        # ------------------------------------------------------  Korrektur
