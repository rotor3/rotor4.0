from rotor_calculations.rotor_calculation import RotorCalculation


class CovercropEstablishment(RotorCalculation):

    def get_result(self):
        self.get_est_probability_cc_cereal()
        """
        Does not need any input other than sri and crop_rotation with covercrop choices
        """




        """
        EW_US_vor und -nach sind dieselbe Berechnung. Durch die Verwendung von Listen in ROTOR 4 kann einfach nach der
        vorangegangenen Etablierungswahrscheinlichkeit gefragt werden
        Etablierungswahrscheinlichkeiten sind abhängig vom Fruchttyp un Ackerzahl.
        ES HANDELT SICH UM EINEN STANDORTPARAMETER der für Unterschiedliche Fruchtgruppen variiert!

        Aus Public Function F_Nleach()
            F_ZwiFr = 0                                             '
            F_US_vor = 0                                            '
            F_US_nach = 0  
            
            N_Aufn_Ant_ZwiFr, N_Aufn_Ant_US_vor, N_Aufn_Ant_US_nach = 
            
        """


    def get_est_probability_cc_cereal(self):

        sri = self.project.tab_general["soil_rating_index"]
        for index in range(0, self.project.tab_general["rotation_duration"]):
            covercrop_id = self.project.results["crop_rotation"][index]["covercrop_id"]
            winsom_id = self.project.results["winter_summer_annual"][index]
            if covercrop_id == 1: # no covercrop
                est_probability_cc = 0
            elif covercrop_id == 2: # covercrop
                if self.project.results["crop_rotation"][index]["previous_crop_type_id"] == 1:
                    if winsom_id == 0:
                        est_probability_cc = 0.000002 * sri ** 2 - 0.0018 * sri + 0.7338
                    elif winsom_id == 1:
                        est_probability_cc = .00003 * sri ** 2 - 0.0054 * sri + 1.0173
                    else:
                        est_probability_cc = 1
                elif self.project.results["crop_rotation"][index]["previous_crop_type_id"] == 2:
                    pass  # no values in ROTOR 3.1

                elif self.project.results["crop_rotation"][index]["previous_crop_type_id"] == 3:
                    est_probability_cc = -0.000009 * sri ** 2 - 0.0026 * sri + 0.8648
                else:  # crop_type 4
                    est_probability_cc = 1
            else: # covercrop as undersowing
                est_probability_cc = 0.00007 * sri**2 - 0.0093 * sri + 0.5726

            self.project.results["est_probability_cc_us"][index] = est_probability_cc


