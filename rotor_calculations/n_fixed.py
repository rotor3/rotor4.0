from db_connect.db_connect import execute_query_get_dictionary_list
from rotor_calculations.rotor_calculation import RotorCalculation


class NFixed(RotorCalculation):

    def get_result(self):
        self.get_n_fixed()

    def get_n_fixed(self):
        print("NFixed.get_n_fix")
        for index in range(0, self.rotation_duration):
            if self.project.results["crop_rotation"][index]["crop_type_id"] == 3:
                n_fixed = self.get_n_fixed_grain_legume(self.project.results["crop_rotation"][index]["crop_id"])

            elif self.project.results["crop_rotation"][index]["crop_type_id"] == 4:
                n_fixed = self.get_n_fixed_legume_gras(self.project.results["crop_rotation"][index]["crop_id"])

            else:
                n_fixed = 0
            self.project.results["n_fixed_main"][index] = n_fixed

            if self.project.results["crop_rotation"][index]["covercrop"]:
                self.get_n_fixed_catch_crop(index)


    def get_n_fixed_grain_legume(self, index):
        print("NFixed.get_n_fixed_grain_legume")
        crop_id = self.project.results["crop_rotation"][index]["crop_id"]

        pass

    def get_n_fixed_legume_gras(self, index):


        yield_cpa = self.project.results["yield_amount_t"][index]
        yield_cpa_dm = self.project.results["yield_amount_dm_main_t"][index]
        yield_amount_byproduct_t = self.project.results["yield_amount_byproduct_t"]
        yield_amount_dm_byproduct_t = self.project.results["yield_amount_dm_byproduct_t"][index]

        legume_ratio = self.project.results["crop_rotation"][index]["main_legume_ratio"]
        pass

    def get_n_fixed_catch_crop(self, index):
        print("NFixed.get_n_fixed_catch_crop")
        # TODO catch crop crop_id hardcoded to 120
        crop_id = 120
        n_fixing_values = execute_query_get_dictionary_list(sql_query)[0]
        legume_ratio = self.project.results["crop_rotation"][index]["covercrop_legume_ratio"]
        pass