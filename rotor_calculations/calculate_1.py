from rotor_calculations.get_data_from_db import GetData
from rotor_calculations.manure import Manure
from rotor_calculations.n_level import NLevel
from rotor_calculations.precipitation import Precipitation
# from rotor_calculations.som_leithold_vdlufa import SOMLeitholdVDLUFA
from rotor_calculations.soil_parameters import SoilParameters
from rotor_calculations.whc_rz import WHCRZ

from db_connect.db_connect import execute_query_get_dictionary_list
from dialog_window import DialogWindow

# from rotor_calculations.yield_in_t import YieldInT
from rotor_calculations.npk_need_crop import NPKNeedCrop

'''All calculations are executed from this class. 
The input and output of each Calculation class is the project and the ui_status.
FOR THE IMPLEMENTATION OF A NEW CALCULATION, 
1. create a new class in this folder inheriting from RotorCalculation.
2. Write functions and add them to their function get result.
3. Add the class to the get_results function in this class, according to the existing ones.
4. CHECK IF YOU ADDED THE CLASS AT THE RIGHT POSITION, according to which results stored in the project are used in your class, as well as what wil be needed afterwards.
    The best position will generally be at the end. 
'''

class Calculate1:
    def __init__(self, project, ui_status):
        self.project = project
        self.ui_status = ui_status

        self.get_results()


    def get_results(self):
        print("rotor_calculations.get_results")
        '''If no crop rotation could be generated, the calculations are stopped'''
        if self.ui_status.error["error"]:
            print("error is true")
            return
        '''For Free Generation, the loops are in the calculations'''

        # get all necessary data from db
        crop_data = GetData(self.project, self.ui_status)
        self.project.results = crop_data.project.results
        self.project.db_data = crop_data.project.db_data

        # ------------all calculations that are independent of assessment or evaluation
        # get precipitation levels

        precipitation = Precipitation(self.project, self.ui_status)
        self.project = precipitation.project
        self.ui_status = precipitation.ui_status

        soil_parameters = SoilParameters(self.project, self.ui_status)
        self.project = soil_parameters.project

        # calculate WHC_rz
        whc_rz = WHCRZ(self.project, self.ui_status)
        self.project = whc_rz.project
        self.ui_status = whc_rz.ui_status

        # calculate N-levels
        n_levels = NLevel(self.project, self.ui_status)
        self.project = n_levels.project
        self.ui_status = n_levels.ui_status

        # calculate manure for rotation N, P, K
        manure = Manure(self.project, self.ui_status)
        self.project = manure.project
        self.ui_status = manure.ui_status

        #calculate N-needs per crop by main- and byproduct
        n_need = NPKNeedCrop(self.project, self.ui_status)
        self.project.results = n_need.project.results

        # after these calculations, the yields can be estimated


