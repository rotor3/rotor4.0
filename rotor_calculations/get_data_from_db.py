from db_connect.db_connect import get_dataframe_from_db
from rotor_calculations.rotor_calculation import RotorCalculation


class GetData(RotorCalculation):
    def get_result(self):
        self.get_used_crop_ids()

        self.load_all_crop_data_from_db()
        self.load_all_n_data_from_db()
        self.load_all_weed_infestation_data_from_db()

        for key in self.project.db_data:
            print(f"KEY_FIGURES: {key}\n", self.project.db_data[key])
        #self.write_all_calculation_data_into_results()

    def get_used_crop_ids(self):
        for cpa in self.project.results["crop_rotation"]:
            if cpa["crop_id"] > 0:
                self.project.results["used_crop_ids"].add(cpa["crop_id"])



    def load_all_crop_data_from_db(self):
        print("GetCropData.load_all_data_from_db")
        parameter_dict = {"crop_ids": tuple(self.project.results["used_crop_ids"]), "production_system": self.project.tab_general["production_system_id"]}
        sql_query = """SELECT DISTINCT t1.*, t2.*, t3.*, t4.*
                        FROM crops_tb t1 
                            INNER JOIN crop_key_figures_tb t2
                             ON t1.crop_id = t2.crop_id
                             INNER JOIN humus_repro_crops_tb t3
                             ON t1.crop_id = t3.crop_id and t2.production_system_id = t3.production_system_id
                             INNER JOIN humus_vdlufa_crops_tb t4 
                             ON t1.crop_id = t4.crop_id and t2.production_system_id = t4.production_system_id                         
                        WHERE t1.crop_id IN %(crop_ids)s and t2.production_system_id = %(production_system)s;""" % parameter_dict

        key_figures = get_dataframe_from_db(sql_query, index_column='crop_id', remove_duplicates=True).T.to_dict()
        self.project.db_data["key_figures"] = key_figures
        print("KEY FIGURES", key_figures)

    def load_all_n_data_from_db(self):
        print("GetCropData.load_all_data_from_db")
        parameter_dict = {"crop_ids": tuple(self.project.results["used_crop_ids"])}
        sql_query = """SELECT * FROM n_fixing_tb WHERE crop_id IN %(crop_ids)s;""" % parameter_dict

        n_values = get_dataframe_from_db(sql_query, index_column='crop_id').T.to_dict()
        self.project.db_data["n_values"] = n_values
        print("n_values", n_values)

    def load_all_weed_infestation_data_from_db(self):
        print("GetCropData.load_all_data_from_db")
        parameter_dict = {"crop_ids": tuple(self.project.results["used_crop_ids"])}
        sql_query = """SELECT * FROM weed_infestation_index_tb WHERE crop_id IN %(crop_ids)s;""" % parameter_dict

        weed_infestation = get_dataframe_from_db(sql_query, index_column='crop_id').T.to_dict()
        self.project.db_data["weed_infestation"] = weed_infestation
        print("weed_infestation", weed_infestation)

    # TODO this seems obsolete INDEX MISSING in results
    # def write_all_calculation_data_into_results(self):
    #     if self.project.tab_general["assessment_method"] in (1, 3): # intelligent or evaluation
    #         self.project.results["yield_amount_t"] = self.project.tab_yield["yield_amount_t"]
    #         self.project.results["yield_amount_catch_crop_t"] = self.project.tab_yield["yield_amount_catch_crop_t"]
    #         self.project.results["yield_legume_green_fodder_t"] = self.project.tab_yield["yield_legume_green_fodder_t"]
    #         self.project.results["yield_legume_hay_t"] = self.project.tab_yield["yield_legume_hay_t"]
    #         self.project.results["yield_legume_silage_t"] = self.project.tab_yield["yield_legume_silage_t"]

    def get_manure_data(self):
        pass