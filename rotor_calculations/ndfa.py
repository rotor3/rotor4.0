# NdfA is the Nitrogen derved from atmosphere.
# In Rotor 3.11 the F_Ndfa_Red is the reduction of the Ndfa depending on the previous crop and covercrop
from rotor_calculations.rotor_calculation import RotorCalculation


class Ndfa(RotorCalculation):

    def get_result(self):
        pass


    # formerly F_Ndfa_Red, the reduction of the Ndfa depending on the previous crop
    # covercrop_ids: former none: 1; ZwiFr = 2; undersowing: 3; Having 3 equals having undersowing in the previous year,
    # thus VorFrAns V__G, V__L
    def ndfa_reduction(self):

        for index in range(0, self.project.tab_general["rotation_duration"]):
            if self.project.result["crop_rotation"][index]["crop_type_id"] == 3:
                # in TPVList, where previous crop type is 2, there is only 21a or 22a, thus asking for previous crop_type is enough
                if self.project.result["crop_rotation"][index]["previous_crop_type_id"] == 2:
                    ndfa_factor = .85
                elif self.project.result["crop_rotation"][index]["previous_crop_type_id"] == 1:
                    # VorFrAns = V12a or 12L- nothing else exists
                    if self.project.result["crop_rotation"][index]["previous_crop_n_delivery"] == 2:
                        ndfa_factor = .85
                        #VorFrAns = V12L Then F = 0.85 * (-0.2 * GetLegUS() + 0.9)
                        if self.project.result["crop_rotation"][index]["covercrop_id"] == 2:
                            ndfa_factor = ndfa_factor *(-.2 * \
                                                        self.project.tab_rotation["undersowing_legume_ratio"][(index - 1) % self.rotation_duration] + .9)
                        elif self.project.result["crop_rotation"][index]["previous_crop_n_delivery"] == 1 and \
                         self.project.result["crop_rotation"][index]["covercrop_id"] == 2:
                            # If VorFrAns = "V11L" And FruchtTyp = "V03" Then F = -0.2 * GetLegUS() + 0.9
                            # TODO The undersowing_legume_ratio is the same as the current CC Legume-ratio, right?
                            # ndfa_factor = -.2 * self.project.dict_tab_crop["undersowing_legume_ratio"]\
                            #     [(index - 1) % self.rotation_duration] + .9
                            ndfa_factor = -.2 * self.project.tab_rotation["covercrop_legume_ratio"][index] + .9
                else:
                    # TODO What about VorFrAns V11a?
                    pass

                # If Zwischenfrucht = "ZwiFr" And FruchtTyp = "V03" Then F = -0.2 * GetLegCC() + 0.9 in Rotor 3.11 overwrites the above
                    # TODO is that correct?
                if self.project.result["crop_rotation"][index]["covercrop_id"] == 2:
                            ndfa_factor = -.2 * self.project.tab_rotation["covercrop_legume_ratio"][index] + .9
            else:
                ndfa_factor = 1

            self.project.result["ndfa_reduction"][index] = ndfa_factor

