from rotor_calculations.rotor_calculation import RotorCalculation
import numpy as np
import math

class Economy(RotorCalculation):
    def get_result(self):
        self.get_values_tractor()

    def get_values_tractor(self):
        tractor = dict()
        # The inital price
        tractor["price"] = 770.9 * tractor["kW"] - 4319

        # diesel useage in €/h
        tractor["diesel_usage"] = 1 + math.e **(-.01923373 * (tractor["kW"] - 113.74756884))

        # repair costs per year in €
        tractor["repair_costs"] = 4.0989284854 + .0372160179 * tractor["kW"] + -.000051751 * tractor["kW"] ** 2

        # insurance costs per year in €
        tractor["insurance_costs"] = 5.804 * tractor["kW"] - 164.1
        tractor["insurance_costs"] = max(165, (min(tractor["insurance_costs"], 435)))

        # TODO is age not relevant?? if older than 12 years no more Abschreibung!

    def get_fuel_consumption(self):
        pass

    def get_machine_costs(self):
        pass

    def get_variable_machine_costs(self):
        pass



