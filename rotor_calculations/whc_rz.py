from db_connect.db_connect import execute_query_get_one
from rotor_calculations.rotor_calculation import RotorCalculation
"""
This class retrieves the waterholding capacity from the db according to tghe soil rating inderx.
The probability of rinsing is calculated (soil_rinsing).
"""
class WHCRZ(RotorCalculation):


    def get_result(self):
        self.get_whc_rz()
        self.get_soil_rinsing()
        print("WHCRZ: whc_rz", self.project.result_general["whc_rz"], "soil_rinsing", self.project.result_general["soil_rinsing"])

    def get_whc_rz(self):
        soil_index = self.project.tab_general["soil_index"]
        # to get values for whc_rz from db the soil index needs to be rounded/ set
        if soil_index < 25:
            soil_index = 25
        elif soil_index > 80:
            soil_index = 80
        else:
            soil_index = round(soil_index/5) * 5

        sql_query = """SELECT whc_rz FROM soil_quality_tb WHERE soil_index = %(soil_index)s""" % {"soil_index": soil_index}
        whc_rz = execute_query_get_one(sql_query)[0]

        self.project.result_general["whc_rz"] = whc_rz


    def get_soil_rinsing(self):
        rinsing = .5 * self.project.tab_general["precipitation_winter"] / self.project.result_general["whc_rz"]
        rinsing = min(rinsing, 1)

        self.project.result_general["soil_rinsing"] = rinsing