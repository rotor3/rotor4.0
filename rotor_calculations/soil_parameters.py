from rotor_calculations.rotor_calculation import RotorCalculation

"""If no advanced soil parameters have been selected, they are calculated according to the soil rating index (as in ROTOR 3). 
The fine soil is currently derived from the soil rating index. It should be derived from the soil type.
"""
#TODO get fine soil from soil type instead of the soil rating index

class SoilParameters(RotorCalculation):

    def get_result(self):
        self.get_fine_soil()
        if not self.project.tab_general["soil_advanced_parameters_selected"]:
            self.get_fine_soil()
            self.get_soil_organic_matter()
            self.get_topsoil()




    def get_fine_soil(self):
        self.project.tab_general["fine_soil"] = 0.0077 * self.project.tab_general["soil_index"] ** 2 + 0.0548 * \
                                                self.project.tab_general["soil_index"]

    def get_soil_organic_matter(self):
        self.project.tab_general["soil_organic_matter"] = self.project.tab_general["fine_soil"] * .023 + .41

    def get_topsoil(self):
        self.project.tab_general["soil_topsoil_depth"] = -0.0012 * self.project.tab_general["soil_index"] ** 2 + 0.2182 \
                                                         * self.project.tab_general["soil_index"] + 22.273






