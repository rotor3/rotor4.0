from rotor_calculations.get_data_from_db import GetData
from rotor_calculations.manure import Manure
from rotor_calculations.n_level import NLevel
from rotor_calculations.precipitation import Precipitation
# from rotor_calculations.som_leithold_vdlufa import SOMLeitholdVDLUFA
from rotor_calculations.soil_parameters import SoilParameters
from rotor_calculations.whc_rz import WHCRZ

from db_connect.db_connect import execute_query_get_dictionary_list
from dialog_window import DialogWindow

# from rotor_calculations.yield_in_t import YieldInT
from rotor_calculations.npk_need_crop import NPKNeedCrop

'''All calculations are executed from this class. 
The input and output of each Calculation class is the project and the ui_status.
FOR THE IMPLEMENTATION OF A NEW CALCULATION, 
1. create a new class  inheriting from RotorCalculation in this folder.
2. Write functions and add them to their function get_results.
3. Add the class to the get_results function in this class, according to the existing ones.
4. CHECK THE ORDER OF THE CLASSES, according to which results stored in the project are used in your class,
   as well as what will be needed afterwards.
    
'''

class Calculate2:
    def __init__(self, project, ui_status):
        self.project = project
        self.ui_status = ui_status

        self.get_results()


    def get_results(self):
        print("rotor_calculations.get_results")
        '''If no crop rotation could be generated, the calculations are stopped'''
        if self.ui_status.error["error"]:
            print("error is true")
            return
        '''For Free Generation, the loops are in the calculations'''

        # ------------all calculations that are independent of assessment or evaluation
        # get precipitation levels
        print("calculating...")
        print("Result_general", self.project.result_general)



        # --------------- for one crop rotation---------------------


        manure = Manure(self.project, self.ui_status)
        self.project.results = manure.project.results





        #
        # # establishment prababilities of undersowing and covercrop
        # covercrop_establishment = CovercropEstablishment(self.project, self.ui_status)
        # self.project.results = covercrop_establishment.project.results
        #

        #
        # n_mineralisation_som_pa = NMinSOMPA(self.project, self.ui_status)
        # self.project.results = n_mineralisation_som_pa.project.results
        #
        # n_level_mineralisation_pa = NLevelMinPA(self.project, self.ui_status)
        # self.project.results = n_level_mineralisation_pa.project.results
        #
        # # Humus according to Leithold and VDLUFA, TODO in Leithold liquid there is an option missing
        # som_leithold_vdlufa = SOMLeitholdVDLUFA(self.project)
        # self.project.results = som_leithold_vdlufa