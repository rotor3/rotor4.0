from dev.load_dummy import Dummy
#TODO MANURE TESTING!!!
from project_handling.project import Project
from rotor_calculations.rotor_calculation import RotorCalculation
import pandas as pd

"""manure values https://www.lwk-niedersachsen.de/index.cfm/portal/96/nav/2280/article/32460.html?&page=print"""

class Manure(RotorCalculation):


    def get_result(self):
        # TODO move this to intelligenereation
        # self.project.result_general["manure_values"] = self.project.tab_manure["manure_values"]
        for m_n_manures_id in self.project.tab_manure["m_n_manures_ids"]:
            self.get_n_amount_available(m_n_manures_id)
            self.get_vdlufa_humus_units_factor(m_n_manures_id)
        # TODO only for intelligent and free Generation, the necessary manure amount has to be calculated;
        # TODO for free generation, the calculations have to be the other way around

        if self.project.tab_general["assessment_method"] in (1, 3):
            for index in range(0, self.rotation_duration):
                self.get_n_p_k_from_manure_per_crop(index)
                #self.get_humus_vdlufa_and_repro_from_manure(index)

    def get_n_amount_available(self, m_n_manures_id):
        print("Manure.get_n_amount_available")
        """calculating the n_availability for all manures selected"""

        # N in manure
        print("m_n_manures_id", m_n_manures_id)
        print("self.project.tab_manure['manure_values']", self.project.tab_manure['manure_values'])
        print("self.project.result_general['manure_values']", self.project.result_general["manure_values"])
        manure = self.project.tab_manure["manure_values"][m_n_manures_id]

        self.project.result_general["manure_values"][m_n_manures_id]["n_available"] = manure["n_content_per_unit"] * (1 - manure["n_loss_percent"]) * \
                                manure["n_availability_percent"]
        # VDLUFA humusfactors for manure
        # TODO CHECK IF CORRECT!!
        # TODO check if regression coefficients a and b are correctly named and if the dry mass is percentage or percent
        self.project.tab_manure["manure_values"][m_n_manures_id]["vdlufa_humus_units"] = manure["vdlufa_humus_units_factor_a"] * manure["dry_mass"] + manure["vdlufa_humus_units_factor_b"]
    # self.project.result_general["manure_values"] = self.project.tab_manure["manure_values"]




    def get_n_p_k_from_manure_per_crop(self, index):
        # TODO add manures as list to CPA
        cpa = self.project.results["crop_rotation"][index]
        n_amount_from_manure = 0
        p_amount_from_manure = 0
        k_amount_from_manure = 0
        print("cpa",cpa)
        print("cpa['m_n_manures_id']", cpa["m_n_manures_id"])

        # iterate through all manures (index_2, the index of m_n_manures_id) per crop (cpa)
        for index_2, m_n_manures_id in enumerate(cpa["m_n_manures_id"]):

            if m_n_manures_id != 0:

                n_amount_from_manure = n_amount_from_manure + cpa["manure_amount"][index_2] * self.project.result_general["manure_values"][m_n_manures_id]["n_available"]
                p_amount_from_manure = p_amount_from_manure + cpa["manure_amount"][index_2] * self.project.result_general["manure_values"][m_n_manures_id]["p_content_kg_per_unit"]
                k_amount_from_manure = k_amount_from_manure + cpa["manure_amount"][index_2] * self.project.result_general["manure_values"][m_n_manures_id]["p_content_kg_per_unit"]
                print("self.project.result_general['manure_values'][m_n_manures_id]",
                            self.project.result_general['manure_values'][m_n_manures_id])
            print("cpa['manure_amount']", cpa["manure_amount"])
            print("cpa['manure_amount'][index_2]", cpa["manure_amount"][index_2])
            self.project.results["crop_rotation"][index]["n_amount_from_manure"] = n_amount_from_manure
            self.project.results["crop_rotation"][index]["p_amount_from_manure"] = p_amount_from_manure
            self.project.results["crop_rotation"][index]["k_amount_from_manure"] = k_amount_from_manure
            print("n_amount_from_manure at index", index, self.project.results["crop_rotation"][index]["n_amount_from_manure"])
            print("p_amount_from_manure at index", index,
                  self.project.results["crop_rotation"][index]["p_amount_from_manure"])
            print("k_amount_from_manure at index", index,
                  self.project.results["crop_rotation"][index]["k_amount_from_manure"])


        # TODO Fehler im alten Code? die Trockenmasse muss weg, da die Angaben sich immer auf die Frischmasse beziehen.
        # TODO Aber die Ausbringungsverluste müssten auch bei p und k mit rein!!!
        '''N-amount in kg/ha is:
            amount of manure liquid/solid[t/ha; m3/ha] * DM [%] * N-content in DM [kg/t] * N-availability [%] * (1 - N-loss [%]'''

        '''P-amount in manure in kg/ha:
            manure-amount [t/ha; m3/ha] * DM [%] * P-content in DM [kg / t;kg/m3]'''


        '''K-amount in manure in kg/ha:
                    manure-amount [t/ha; m3/ha] * DM [%] * K-content in DM [kg / t;kg/m3]'''





if __name__ == "__main__":
    dummy = Dummy(60)
    # project = dummy.load_project_from_db(122)
    # dummy.results = Project().results

    manure_test = Manure(dummy.project, dummy.ui_status)
    print("Manure Test results: ", manure_test.project.results)

