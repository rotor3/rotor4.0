from db_connect.db_connect import execute_query_get_dictionary_list, execute_query_get_one
from dev.load_dummy import Dummy
from project_handling.project import Project
from rotor_calculations.rotor_calculation import RotorCalculation


class PKBalance(RotorCalculation):


    def get_result(self):
        for index in range(0, self.rotation_duration):
            self.get_p_k_removed_by_main_and_by_product(index)
            self.get_p_k_in_seeds(index)
            # self.get_p_k_removed_covercrop(index)
            # self.p_k_removal_autumn_harvest_silage(index)
            self.get_p_k_uptake_undersowing(index)

        self.get_p_k_balance()



    #TODO refactor
    def get_p_k_balance(self):
        p_total = 0
        k_total = 0
        for index in range(0, self.rotation_duration):

            p_organic_manure = self.project.results["crop_rotation"][index]["p_amount_from_manure"] # calculated in rotor_calculations.Manure
            k_organic_manure = self.project.results["crop_rotation"][index]["k_amount_from_manure"] # calculated in rotor_calculations.Manure
            p_seed = self.project.results["crop_rotation"][index]["p_in_seeds"]
            k_seed = self.project.results["crop_rotation"][index]["k_in_seeds"]
            p_removed = self.project.results["crop_rotation"][index]["p_removed_by_main_and_byproduct"]
            k_removed = self.project.results["crop_rotation"][index]["k_removed_by_main_and_byproduct"]

            p_removal_catch_crop_harvest = self.project.results["crop_rotation"][index]["p_removal_covercrop_harvest"]
            k_removal_catch_crop_harvest = self.project.results["crop_rotation"][index]["k_removal_covercrop_harvest"]

            # TODO ist p_uptake_us nicht eine doppelung von
            p_balance = p_organic_manure + p_seed - p_removed - p_removal_catch_crop_harvest
            k_balance = k_organic_manure + k_seed - k_removed - k_removal_catch_crop_harvest


            p_total = p_total + p_balance
            k_total = k_total + k_balance

        self.project.results["balances"]["p_balance"] = p_total
        self.project.results["balances"]["k_balance"] = k_total



    def get_p_k_in_seeds(self, index):
        p_seed = self.project.results["crop_rotation"][index]["seed_quantity"] * self.project.results["crop_rotation"][index]["p_main_product"] * \
                 self.project.results["crop_rotation"][index]["dry_mass"]
        k_seed = self.project.results["crop_rotation"][index]["seed_quantity"] * self.project.results["crop_rotation"][index]["k_main_product"] * \
                 self.project.results["crop_rotation"][index]["dry_mass"]

        self.project.results["crop_rotation"][index]["p_in_seeds"] = p_seed
        self.project.results["crop_rotation"][index]["k_in_seeds"] = k_seed

    def get_p_k_uptake_undersowing(self, index):
        print("PKBalance.get_p_k_uptake_undersowing")
        """With the harvest of the main product, some of the undersowing gets inevitably also harvested. 
        Therefore P and K has to be subtracted for undersowing, independent of the catch crop harvest.
                
        IIf([TPVListe].[sonstige]="o" Or Left([TPVListe].[Kultur],2)="LZ",[Yield_dt_US]*0.85*[pHP]*0.436*[TM],0) AS Puptake_US, 
	    IIf([TPVListe].[sonstige]="o" Or Left([TPVListe].[Kultur],2)="LZ",[Yield_dt_US]*0.85*[kHP]*0.436*[TM],0) AS Kuptake_US, 
	    
	    == p_k_removed_by_catch_crop"""



    def get_p_k_removed_by_main_and_by_product(self, index): # F_PEntz_HPuNP

        crop_id = self.project.results["crop_rotation"][index]["crop_id"]

        # TODO wahrscheinlich muss der umrechnungsfaktor nur auf die Düngung!
        """Changed from ROTOR 3.1 to 4: The database entries in the table "KW" for P and K used to be saved as K2O and P2O5.
        These entries were converted to the values of pure P and K with the factors .436 for P and .83 in the 
        database of ROTOR 4.0, table crop_key_figures_tb """
        p_removed_main_and_byproduct = self.project.results["crop_rotation"][index]["yield_amount_t"] * \
              (self.project.results["crop_rotation"][index]["p_need_main_product_per_t_fresh_mass_yield_main"] +
              self.project.results["crop_rotation"][index]["p_need_byproduct_per_t_fresh_mass_yield_main"] *
              (int(self.project.results["crop_rotation"][index]["strawharvest"])))

        k_removed_main_and_byproduct = self.project.results["crop_rotation"][index]["yield_amount_t"] * \
              (self.project.results["crop_rotation"][index]["p_need_main_product_per_t_fresh_mass_yield_main"] +
              self.project.results["crop_rotation"][index]["p_need_byproduct_per_t_fresh_mass_yield_main"] *
              (int(self.project.results["crop_rotation"][index]["strawharvest"])))

        self.project.results["crop_rotation"][index]["p_removed_by_main_and_byproduct"] = p_removed_main_and_byproduct
        self.project.results["crop_rotation"][index]["k_removed_by_main_and_byproduct"] = k_removed_main_and_byproduct

    # TODO verify;

    """ => K-Entzug geerntetes Haupt- und Nebenprodukt

    Public Function F_PEntz_HPuNP(yield_dt, TM, PHP, FNP, PNP, strawharvest) As Double
    ---------------BEI ST0, ST1------------------------
    Dim ENPf As Single
    If strawharvest = "yes" Then ENPf = 1 Else ENPf = 0

    F_PEntz_HPuNP = Nz(yield_dt, 0) * (TM * PHP * 0.436 + TM * FNP * PNP * 0.436 * ENPf)

    End Function
    
    Public Function F_KEntz_HPuNP(yield_dt, TM, KHP, FNP, KNP, strawharvest) As Double
    Dim ENPf As Single
    If strawharvest = "yes" Then ENPf = 1 Else  ENPf = 0
    F_KEntz_HPuNP = Nz(yield_dt, 0) * (TM * KHP * 0.83 + TM * FNP * KNP * 0.83 * ENPf)
    
    End  Function
    -----------------------------------------------------------------
    
    ' => P-Entzug Sproß incl. Bröckelverluste
    Public Function F_PEntzugSproß(yield_dt, TM, PHP, FNP, PNP, Broesel) As
    Double Dim P_EntzugHP As Double
    Dim P_EntzugNP As Double
    Dim P_EntzugBr As Double
    
    P_EntzugHP = yield_dt * TM * PHP * 0.436
    P_EntzugNP = yield_dt * TM * FNP * PNP * 0.436
    P_EntzugBr = yield_dt * TM * (1 - Broesel) / Broesel * 1
    'Ist der P-Gehalt in Brösel von 0.76 Grünschnitt auf 1.0
    
    F_PEntzugSproß = P_EntzugHP + P_EntzugNP + P_EntzugBr
    
    End Function
    ------------------P K uptake of crop---------------------------------------------------------
    ' => K-Entzug Sproß incl. Bröckelverluste
    Public Function F_KEntzugSproß(yield_dt, TM, KHP, FNP, KNP, Broesel) As Double 
    Dim K_EntzugHP As Double
    Dim K_EntzugNP As Double
    Dim K_EntzugBr As Double
    
    K_EntzugHP = yield_dt * TM * KHP * 0.83
    K_EntzugNP = yield_dt * TM * FNP * KNP * 0.83
    K_EntzugBr = yield_dt * TM * (1 - Broesel) / Broesel * 4
    'Ist der K-Gehalt in Brösel von 3.01 Grünschnitt auf 4.0
    
    F_KEntzugSproß = K_EntzugHP + K_EntzugNP + K_EntzugBr
    
    End Function
    ----------------------------------------------------------------------------------"""
    def get_p_k_removed_by_catch_crop_harvest(self, index):
        """Legumegras can be harvested in autumn if it has been undersown into the previous crop"""
        # undersowing_crop_id = # LKG


        # TODO siehe Faktoren oben! Die müssen wahrscheinlich nur auf die Düngung
        if self.project.results["crop_rotation"][index]["covercrop_harvest"]:
            p_removal_covercrop_harvest = self.project.results["crop_rotation"][index]["yield_amount_covercrop_t"] * \
                self.project.results["crop_rotation"][index]["p_need_catch_crop"]

            k_removal_covercrop_harvest = self.project.results["crop_rotation"][index]["yield_amount_covercrop_t"] * \
                self.project.results["crop_rotation"][index]["k_need_catch_crop"]

        else:
            p_removal_covercrop_harvest = 0
            k_removal_covercrop_harvest = 0

        self.project.results["crop_rotation"][index]["k_removal_covercrop_harvest"] = p_removal_covercrop_harvest
        self.project.results["crop_rotation"][index]["k_removal_covercrop_harvest"] = k_removal_covercrop_harvest


    # ist komplett Quatsch
    # def p_k_removal_autumn_harvest(self, index):
    #     #TODO Silage ist über Crop_id geregelt, 22, 23, 26, 27 haben Silage, aber welche sind im Herbst?

    #     yield_correction = 1  # TODO placeholder
    #     crop_id = self.project.results["crop_rotation"][index]["crop_id"]
    #     sri = self.project.tab_general["soil_rating_index"]
    #     prev_crop_type_id = self.project.results["crop_rotation"][index]["previous_crop_type_id"]
    #     prev_crop_n_delivery = self.project.results["crop_rotation"][index]["previous_crop_n_delivery"]
    #     prev_undersowing = self.project.results["crop_rotation"][(index - 1) % self.rotation_duration]["undersowing"]
    #     # TODO Untersaat und Zwischenfrucht art ermitteln und danach berechnen! in Tab Yield! Hier dann P und K aus yield ziehen
    #
    #     # TODO Herbsternte Switch
    #     # getaut_harv_lg = True
    #     self.project.tab_rotation["covercrop_harvest"]
    #     if self.project.tab_rotation["crop_id"][index] in [22, 23, 26, 27]: # Leguminosengras silage
    #         # Yield_Aut_harv_LG:
    #         # IIf(GetAut_harv_LG() = "silage", Yield_dt("LKS", 0, 35, 1, 0, 85, 1, "none", 1, 1),
    #         # Yield_dt("LKU", 0, 25, 1, 1, 1,"none", 1, 1))
    #         # *IIf(GetAut_harv_LG() = "no", 0, (IIf([TPVListe_NatschKF_Tab].[kultur]="st1"
    #         # And[TPVListe_NatschKF_Tab].[VorFrAns]="V11F", 0, IIf([TPVListe_NatschKF_Tab].[kultur]="LZ0", 0,
    #         # IIf(Left([TPVListe_NatschKF_Tab].[kultur], 1) = "L"
    #         # And[TPVListe_NatschKF_Tab].[VorFrAns] = "V11F", 0, 2, IIf(Left([TPVListe_NatschKF_Tab].[kultur], 1) = "L"
    #         # And[TPVListe_NatschKF_Tab].[VorFrAns] = "V11a", 0, 15, IIf(Left([TPVListe_NatschKF_Tab].[kultur], 1) = "L"
    #         # And[TPVListe_NatschKF_Tab].[VorFrAns] = "V12a", 0, 15, 0))))))) AS
    #         #
    #
    #         #-------------------------------
    #         # Yield_dt("LKS", 0.35, 1, 0.85, 1, "none", 1, 1):
    #         # PELeggrass = (-0.125 * AZ ^ 2 + 16.9 * AZ - 62.7) * 0.25
    #         # E = (PELeggrass + GetYield_corr_LZ) * 0.85 * 0.9 / TM * Broesel
    #         # also:
    #
    #
    #         pe_leggras = ((-0.125 * sri**2 + 16.9 * sri - 62.7) * 0.25 * yield_correction) * .85 / .35 * .85
    #     else:
    #         # Yield_dt("LKU", 0, 25, 1, 1, 1,"none", 1, 1):
    #         # PELeggrass = (-0.125 * AZ ^ 2 + 16.9 * AZ - 62.7) * 0.25
    #         # E = (PELeggrass + GetYield_corr_LZ) * 0.85 / TM * Broesel
    #         pe_leggras = ((-0.125 * sri ** 2 + 16.9 * sri - 62.7) * 0.25 * yield_correction) * .85 / .25
    #
    #     if getaut_harv_lg is False:
    #         pe_leggras = pe_leggras * 0
    #     else:
    #         #[TPVListe_NatschKF_Tab].[kultur] = "st1" And[TPVListe_NatschKF_Tab].[VorFrAns] = "V11F"
    #         if crop_id == 32 and prev_undersowing and prev_crop_n_delivery == 1 and prev_crop_type_id == 1: # ST1
    #             yield_aut_harv_lg = 0
    #         #----------------HIER HAB ICH AUFGEGEBEN---------Blödsinn
    #     #     else:
    #     #         yield_aut_harv_lg = 2
    #     #     if crop_id == 24: # LZ0
    #     #         yield_aut_harv_lg = 0
    #     #     elif
    #     #     # NUR EINE VERMUTUNG!!! könnte es sein, dass es aus NFixUS pHP, Faktor aus F_KEntzHPuNP
    #     #     # DAS IST KW: aber was? und wo ist das +?
    #     #     P_removal_Aut_harv_LG = 0.76 * 0.436 * 0.35 * 0.76 * 0.436 * 0.25 * yield_aut_harv_lg
    #     #     # kHP
    #     #     k_removal_aut_harv_lg = 3.01 * 0.83 * 0.35 * 3.01 * 0.83 * 0.25 * yield_aut_harv_lg
    #     #
    #     # else:
    #     return 1









if __name__ == "__main__":
    print("Dummy...")
    dummy = Dummy(13)
    print("Dummy results...")
    # project = dummy.load_project_from_db(122)
    dummy.results = Project().results
    print("Dummy calculations", dummy.results)

    pk_balance_test = PKBalance(dummy.project)
    print(pk_balance_test.project.results["n_need_per_dt"])

































