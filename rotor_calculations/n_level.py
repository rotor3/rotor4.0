from rotor_calculations.rotor_calculation import RotorCalculation

##################### DONE DONE DONE #########################
class NLevel(RotorCalculation):

    def get_result(self):
        self.get_n_level()

    # this function assigns the n_level to each crop in the list of results, former "EF" if manure is applied, it gets raised
    def get_n_level(self):
        for index in range(0, self.rotation_duration):
            # TODO should be n_delivery of index-1 for Assessment!
            index_prev = (index - 1) % self.rotation_duration
            n_level = self.project.results["crop_rotation"][index_prev]["n_delivery"]
            # Bachinger Zander 2007 p. 134
            if self.project.results["crop_rotation"][index_prev]["crop_type_id"] != 1:
                n_level = n_level + 1


            # if manuring is applied, the n_level gets raised
            if self.project.tab_rotation["manure"][index]:
                n_level = n_level + 1
            # TODO check! Wirklich nur +1 für covercrop, oder auch cc_id = 3?
            if self.project.results["crop_rotation"][index]["covercrop_id"] == 2:  # Zwischenfrucht geänderter EF #
                n_level = n_level + 1

            if n_level > 3:
                n_level = 3

            self.project.results["crop_rotation"][index]["n_level"] = n_level



