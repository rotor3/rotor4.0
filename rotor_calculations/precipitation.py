from rotor_calculations.rotor_calculation import RotorCalculation


class Precipitation(RotorCalculation):

    def get_result(self):
        self.get_precipitation_level()
        self.get_winter_precipitation()
        print("precipitation_factor", self.project.result_general["precipitation_factor"], "precipitation_level",
              self.project.result_general["precipitation_level"], "precipitation_winter", self.project.tab_general["precipitation_winter"])

    """
    the precipitation level of Rotor 3.1 is calculated as a linear regression instead of static classes
    """
    def get_precipitation_level(self):
        precipitation_factor = .017 * self.project.tab_general["precipitation"] - 6.19

        # the precipitation factor must be between 0 and 0.5
        self.project.result_general["precipitation_factor"] = max(min(precipitation_factor, .5), 0)

    def get_winter_precipitation(self):
        if self.project.tab_general["precipitation_winter_selected"] is False:
            self.project.tab_general["precipitation_winter"] = self.project.tab_general["precipitation"] / 2

