from rotor_calculations.rotor_calculation import RotorCalculation
######################## DONE DONE DONE #############################################
"""get_soil_mineralisation_per_year gets the annual N mineralisation from the soil organic matter in kg/ha.
get_mineraliseable_n_per_year gets the adjustment factor according to the n_level of each crop.

Former EfMinpa and Nminpy """
class Manure(RotorCalculation):

    def get_result(self):
        self.get_soil_mineralisation_per_year()
        self.get_mineraliseable_n_per_year()




    def get_soil_mineralisation_per_year(self):
        """The former Nminpy"""
        if self.project.tab_general["soil_advanced_parameters_selected"]:
            self.project.result_general["n_mineralisation_per_year"] = self.project.tab_general["soil_organic_matter"] /\
                                                                    self.project.tab_general["soil_c_n_ratio"] * \
                                                                    self.project.tab_general["soil_topsoil_depth"] * \
                                                                    (1 - self.project.tab_general["soil_stoneratio"]) * \
                                                                    self.project.tab_general["soil_bulk_density"] * \
                                                                    self.project.tab_general["soil_mineralisation_rate"] \
                                                                    * 1000
        else:
            self.project.result_general["n_mineralisation_per_year"] = self.project.tab_general["soil_topsoil_depth"] * \
                                                                    1.5 * 90.9 * \
                                                                    self.project.tab_general["soil_organic_matter"] * \
                                                                    self.project.tab_general["soil_mineralisation_rate"]


    def get_mineraliseable_n_per_year(self):
        """former EfMinpa"""
        self.project.result_general["n_mineraliseable_per_year_n_level_1"] = 1
        self.project.result_general["n_mineraliseable_per_year_n_level_2"] = 1.5126 - .0056 * self.project.tab_general["soil_index"]
        self.project.result_general["n_mineraliseable_per_year_n_level_3"] = 2.0339 - .0114 * self.project.tab_general["soil_index"]

