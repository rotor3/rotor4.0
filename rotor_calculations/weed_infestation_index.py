from db_connect.db_connect import execute_query_get_dictionary_list
from dev.load_dummy import Dummy
#TODO MANURE TESTING!!!
from project_handling.project import Project

# this class is used for the generation of the weed infestation indices at input in the database
from rotor_calculations.calculate_2 import RotorCalculation


class WeedInfestationIndex(RotorCalculation):


    def get_result(self):
        self.get_weed_infestation_index()

    def get_weed_infestation_index(self):
        for index in range(0, self.project.tab_general["rotation_duration"]):
            crop_id = self.project.tab_rotation["crop_id"][index]
            epm_id = self.project.dict_tab_epm_selection["environmental_protection_measures_id"]

            sql_query = """SELECT summer_annual_weed_infestation_risk, winter_annual_weed_infestation_risk, 
                        couch_grass_infestation_risk, winter_summer_crop_id, winter_summer_crop_id
                        FROM crops_tb WHERE crop_id = %(crop_id)s""" \
                        % {"crop_id": crop_id}

            weed_infestation_indices = execute_query_get_dictionary_list(sql_query)[0]

            sa_index = weed_infestation_indices["summer_annual_weed_infestation_risk"]
            wa_index = weed_infestation_indices["winter_annual_weed_infestation_risk"]
            cg_index = weed_infestation_indices["couch_grass_infestation_risk"]


            # TODO klären, welches Sommer/winterannuell gültig ist -- in allCode Z. 3208-3215 sind z.B. eine Angaben
            # TODO klären, ob das wirklich alle Ausnahmen sind

            # modify summer_annual_index

            if self.project.tab_rotation["undersowing"][index]:
                if weed_infestation_indices["winter_summer_crop_id"] == 2: # winter- and summerannual crop
                    sa_index = sa_index - 1

            if self.project.dict_tab_epm_selection["tillage_id"][index] == 1: # no tillage
                sa_index = sa_index - 1

            if epm_id == 2: # former 'ce', 3facher Reihenabstand, gesamt
                if crop_id == 9 or crop_id == 7 or crop_id == 8:  # WRO, TRI, WGE
                    sa_index = sa_index + .5
                elif crop_id == 2:  # HAF
                    sa_index = sa_index + 2
                else:
                    pass
            elif epm_id == 33: # former 'ga'
                if crop_id == 10: # WWE
                    sa_index = sa_index - .5
                elif crop_id == 19 or crop_id == 18: # GLU, FER
                    sa_index = sa_index + 2
                elif crop_id == 2 or crop_id == 6:  # HAF, SWE
                    sa_index = sa_index + .5
                elif crop_id == 4:  # SGE
                    sa_index = sa_index + 1
                else:
                    pass

            elif epm_id == 22:  # former 'gc'
                if crop_id == 10: # WWE
                    sa_index = sa_index - .5
                elif crop_id == 19 or crop_id == 18: # GLU, FER
                    sa_index = sa_index + 1
                elif  crop_id == 6 or crop_id == 4:  # SWE, SGE
                    sa_index = sa_index + .5
                else:
                    pass

            self.project.results["weed_infestation_index_sa"][index] = sa_index




            # modify winter_annual_index
            if self.project.tab_rotation["covercrop_id"] == 2:
                wa_index = wa_index - 1

            if self.project.tab_rotation["undersowing"]:
                wa_index = wa_index - 1

            if self.project.dict_tab_epm_selection["tillage_id"][index] == 1: # no tillage
                wa_index = wa_index + 2


            if self.project.dict_tab_epm_selection["environmental_protection_measures_id"] == 16:  # 'q'
                wa_index = wa_index - 1

            if self.project.dict_tab_epm_selection["environmental_protection_measures_id"] == 2:  # 'ce'
                if crop_id == 9 or crop_id == 7 or crop_id == 8:  # WRO, TRI, WGE
                    wa_index = wa_index + 2
                elif crop_id == 2:  # HAF
                    wa_index = wa_index + 1
                else:
                    pass
            elif self.project.dict_tab_epm_selection["environmental_protection_measures_id"] == 33:  # 'ga'
                if crop_id == 9 or crop_id == 7 or crop_id == 8 or crop_id == 10 or crop_id == 2 \
                        or crop_id == 4 or crop_id == 6:  # WRO, TRI, WGE, WWE, HAF, SGE, SWE
                    wa_index = wa_index + .5

            self.project.results["weed_infestation_index_wa"][index] = wa_index

            # couch grass
            if self.project.tab_rotation["covercrop_id"] == 2:
                q_index = q_index - 1

            if self.project.tab_rotation["undersowing"]:
                if weed_infestation_indices["winter_summer_crop_id"] == 0 or \
                        weed_infestation_indices["winter_summer_crop_id"] == 1: # sommer or winter annual
                    q_index = q_index + 1

            if self.project.dict_tab_epm_selection["tillage_id"][index] == 1: # no tillage
                q_index = q_index + 3

            if self.project.dict_tab_epm_selection["environmental_protection_measures_id"] == 2:  # 'ce'
                if crop_id == 9 or crop_id == 7 or crop_id == 8 or crop_id == 2:  # WRO, TRI, WGE, HAF
                    q_index = q_index + 2

            self.project.results["weed_infestation_index_q"] = q_index

            # DO_NOTHING FUNCTIONS in ROTOR 3.11
            # q_index
            # If K = "LZH" Or K = "LZS" Then
            #     If sonstige = "aa" Then F = F   'aa = Hochschnitt-14cm (nur beim 1.Schnitt), gesamt
            #     If sonstige = "bc" Then F = F   'bc = Spätschnitt, (2.Schn - 8Wo., 3.Schn- 7Wo.), gesamt
            #     If sonstige = "ma" Then F = F   'ma = ungemähte Streifen, überjährig

            # If sonstige = "gc"Then 'gc = Reduktion Striegeleinsatz, gesamt
            # If K = "WWE" Then F = F + 0
            # If K = "GLU" Then F = F + 0
            # If K = "FER" Then F = F + 0
            # If K = "HAF" Then F = F
            # If K = "SGE" Then F = F + 0
            # If K = "SWE" Then F = F
            # If sonstige = "ga" Then 'ga = ohne Striegel, gesamt
            # If K = "WRO" Then  F = F + 0
            # If K = "TRI" Then F = F + 0
            # If K = "WGE" Then F = F + 0
            # If K = "WWE" Then F = F + 0
            # If K = "GLU" Then  F = F + 0
            # If K = "FER" Then F = F + 0
            # If K = "HAF" Then F = F + 0
            # If K = "SGE" Then F = F + 0
            # If K = "SWE" Then F = F + 0



if __name__ == "__main__":
    dummy = Dummy(123)

    dummy.results = Project().results

    weed_test = WeedInfestationIndex(dummy.project)
    print("Weed Infestation risk Test results: ", weed_test.project.results)