from db_connect.db_connect import execute_query_get_recordset, execute_query_get_dictionary_list
from rotor_calculations.rotor_calculation import RotorCalculation


class NBalance(RotorCalculation):


    """needs c-humus_balance first"""

    def get_result(self):
        self.get_n_fix()
        self.get_n_balance()

    def get_n_balance(self):
        # project.get_ndfa_reduction(....)
        pass

    # TODO umschreiben zu for-loop
    def get_n_fix(self):
        """
        ROTOR 3.1: F_NFIX into in Query Yield_US_CC

         F_NFIX(GetLegUS(),... AS NFixUS,
         F_NFIX(GetLegUS_Kleg(),... AS NFixUS_Köleg,
         F_NFIX(GetLegCC(),... AS NFixCC
            FROM [Soil quality], (KW INNER JOIN NFIXFakt ON KW.Kult = NFIXFakt.Kult) INNER JOIN TP11
                    ON NFIXFakt.Kult = TP11.Kult
                    WHERE ((([Soil quality].Ackerzahl)=GetSoil_quality()) AND ((NFIXFakt.Kult)="LKU"));

         Public Function F_NFIX(LegAnt, Kult, Reinbest, yield_dt, KEV, TS, NGTM_LEG, Ndfa_LEGa, _
                        Ndfa_LEGb, NGTM_NLEG, Ndfa_NLEGa, Ndfa_NLEGb, Ndfa_NLEGc, Ndfa, TMEWR, NGEWR) As Double


            Formel von Hülsbergen und Biermann
            Yield_dt     = Ertragschätzung (in FM) , korrigiert für ST0 (Faktor 0,3 im ErtragsModul)
                         und für VorFrLie=V42K (Faktor 0,8 in Abfrage N-Saldo Spalte: ErtrNFix:
                         Wenn([tpvliste].[kultur]="st0";1;Wenn([tpvliste].[VorFrLie]="V42k";0,8;1))*ErtragBachinger
            KEV        = Ertragskorrekturfaktor zur Eliminierung des Ertragabzuges durch Ernteverluste
            TS         = dm   = Trockensubstanz-Gehalt
            LEGAnt     = leg_portion     = Ertagsanteil der Leguminosen im Gemenge
            NGTM_LEG   = n_in_dry_mass_legumes = N-Gehalt in der TM der Leguminosen
            ndfa_lega  = Faktor a der lin.Regression zur bestandesanteilsabhängigen Schätzung des Ndfa der Leguminosen
            ndfa_legb  = Faktor b der lin.Regression zur bestandesanteilsabhängigen Schätzung des Ndfa der Leguminosen
            ngtm_nleg  = n_total_non_legume = N-Gehalt in der TM der Nicht-Leguminosen  ---->
            ndfa_nlega = Faktor a der pol. Regression (2.G.) zur bestandesanteilsabhängigen Schätzung des Ndfa der Nichtleg.
            ndfa_nlegb = Faktor b der pol. Regression (2.G.) zur bestandesanteilsabhängigen Schätzung des Ndfa der Nichtleg.
            ndfa_nlegc = Faktor c der pol. Regression (2.G.) zur bestandesanteilsabhängigen Schätzung des Ndfa der Nichtleg.
            ndfa       = kulturartenspezifischen Ndfa bei Körner-Leguminosenreinbeständen
                        (Anteil des symb. fixierten N am N in der Pflanze(Nitrogen derived from the atmosphere))
       dm_ratio_yield_root_residues     tmewr      = Verhältnis von TM der Ernte- und Wurzelrückst. zu TM des Ertrages
        n_content_residues  ngewr      = N-Gehalte in Ernte und Wurzelrückstände

            LegAnt = GUI
            Kult = crop_id
            Reinbest =
            yield_dt = [Yield_dt_US]
            KEV = [NFIXFakt]![KEV] = n_fixation_factor_tb.correction_factor_harvest_losses
            TS =                  = n_fixation_factor_tb.dry_substance
            NGTM_LEG = [NFIXFakt]![NGTM_LEG] = n_fixation_factor_tb.n_in_dry_mass_legumes
            Ndfa_LEGa = [NFIXFakt]![Ndfa_LEGa] = n_fixation_factor_tb.ndfa_legumes_a
            Ndfa_LEGb = [NFIXFakt]![Ndfa_LEGa] = n_fixation_factor_tb.ndfa_legumes_a
            NGTM_NLEG = n_fixation_factor_tb.n_in_dry_mass_non_legumes
            Ndfa_NLEGa    n_fixation_factor_tb.ndfa_non_legumes_a
            Ndfa_NLEGb 0 n_fixation_factor_tb.ndfa_non_legumes_b
            Ndfa_NLEGc = n_fixation_factor_tb.ndfa_non_legumes_c
            Ndfa = n_fixation_factor_tb.ndfa
            TMEWR = n_fixation_factor_tb.ratio_yield_root_residues
            NGEWR = n_fixation_factor_tb.n_in_residues
            LKU = crops_tb.crop_id = 34

            NLegSpross = n_sprout   = N in legume sprout
            NLegWurzel = n_root     = N in Legume root
            NnonLegtot = n_total_non_legume = N in biomass of non-legume
            NdfaLegmix = ndfa_legume = N portion from fixation in legume biomass
            NdfaNonLeg = ndfa_portion_non_legume   = N portion from N-fixation of non-legume biomass
            Reinbest   = pure_stand  = boolean

        """

        # TODO investigate... crop_id = 34, the table does not exist etc.
        for index in range(0, self.rotation_duration):
            sql_query = """SELECT crop_id, n_fixation_factor_tb.pure_stand, 
                        n_fixation_factor_tb.correction_factor_harvest_losses, 
                        n_fixation_factor_tb.dry_substance, n_fixation_factor_tb.n_in_dry_mass_legumes, 
                        n_fixation_factor_tb.n_in_dry_mass_legumes, n_fixation_factor_tb.ndfa_legumes_a, 
                        n_fixation_factor_tb.ndfa_legumes_a, n_fixation_factor_tb.n_in_dry_mass_non_legumes, 
                        n_fixation_factor_tb.ndfa_non_legumes_a, n_fixation_factor_tb.ndfa_non_legumes_b, 
                        n_fixation_factor_tb.ndfa_non_legumes_c, n_fixation_factor_tb.ndfa, 
                        n_fixation_factor_tb.ratio_yield_root_residues, n_fixation_factor_tb.n_in_residues
                        FROM n_fixation_factor_tb 
                        INNER JOIN 
                        WHERE crop_id = 34
                        """
            crop_values = execute_query_get_dictionary_list(sql_query)
            # TODO Wann wird was berechnet? Untersaat wir zu CC !!!!
            if self.project.results["crop_rotation"][index]["undersowing"] == True:
                legume_portion = self.project.results["crop_rotation"][index]["undersowing_legume_ratio"]
            if self.project.results["crop_rotation"][index]["covercrop_id"] in (2,3):
                legume_portion = self.project.results["crop_rotation"][index]["covercrop_legume_ratio"]

            if crop_values["n_in_dry_mass_legumes"]:
                n_temp = crop_values["yield_dt"] * crop_values["correction_factor_harvest_losses"] * crop_values["dry_substance"]
                n_sprout = n_temp * crop_values["n_in_dry_mass_legumes"]
                n_root = n_temp * crop_values["ratio_yield_root_residues"] * crop_values["n_in_residues"]
                n_total_non_legume = n_temp * crop_values["n_in_dry_mass_non_legumes"]
                # TODO überprüfen, das hier kann nicht stimmen!!!! Müssen nicht die Faktoren addiert werden????
                # alt
                ndfa_legume = crop_values["ndfa_legumes_a"] * legume_portion + crop_values["ndfa_legumes_b"]
                # neu
                ndfa_legume = (crop_values["ndfa_legumes_a"]  + crop_values["ndfa_legumes_a"])* legume_portion

                ndfa_portion_non_legume = crop_values["ndfa_non_legumes_a"] * legume_portion**2 * \
                                          crop_values["ndfa_non_legumes_b"] * legume_portion + crop_values["ndfa_non_legumes_c"]

            if crop_values["pure_stand"]:
                n_fix = (n_sprout + n_root) * crop_values["ndfa"]
            elif crop_values["pure_stand"] is False:
                # TODO GetLegmix()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                # n_fix = (n_sprout + n_root) * (crop_values["ndfa_legumes_a"] * GetLegmix() + crop_values["ndfa_legumes_b"] )
                pass
            else:
                n_fix = 0

        return n_fix

    def get_ndfa_reduction(self, rotation_duration, rotation):
        """
        Calculates the reduction of the ndfa of grain-legumes regarding the previous crop

        """
        for index in range(0, rotation_duration): # TODO in staticfunction get_n auslagern
            if rotation[index]["crop_type_id"] == 3:
                ndfa_reduction = .85 # for V12a, V21a, V22a, V12L
                if rotation[index]["covercrop_id"] == 2:
                    ndfa_reduction = -0.2 * self.project.tab_rotation["covercrop_legume_ratio"] + 0.9
                if rotation[(index-1) % rotation_duration]["undersowing"]:
                    if rotation_duration[index]["prev_crop_n_delivery"] == 2:
                        """
                        V12L
                        it used to be 0.85 * (-0.2 * GetLegUS() + 0.9),
                        GetLegUS was general, now it is specific for year; Leg of undersowing (of previous year is the same
                        as covercrop leg% of current year
                        """
                        ndfa_reduction = 0.85 * (-0.2 * self.project.tab_rotation["covercrop_legume_ratio"] + 0.9)
                    else:
                        ndfa_reduction = -0.2 * self.project.tab_rotation["covercrop_legume_ratio"] + 0.9

                pass
            else:
                ndfa_reduction = 1

        return ndfa_reduction








