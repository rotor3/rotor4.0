from db_connect.db_connect import execute_query_get_dictionary_list
from dev.load_dummy import Dummy

from project_handling.project import Project
from rotor_calculations.rotor_calculation import RotorCalculation
####################################################################################################
#                                                                                                  #
# These calculations are only dependent on the selected main crops. All values are calculated      #
# per ton of MAIN PRODUCT! The result are kg/t of main product - not the sum of main and           #
# by product!                                                                                      #
# The calculation of n_need (n_need_crop_per_t_fresh_mass_yield_main) used to be table N_need_dt_t #
#                                                                                                  #
####################################################################################################

class NPKNeedCrop(RotorCalculation):

    def get_result(self):
        self.get_npk_need_per_crop()
# TODO "crop_id" muss Variable sein!!!
    # ersetzt die alte Tabelle n_need_dt_t
    def get_npk_need_per_crop(self):
        for index in range(0, self.rotation_duration):
            crop_id = self.project.results["crop_rotation"][index]["crop_id"]
            # ([TM]*[nHP]+[TMNP]*[fNP]*[NNP]) AS N_Need_per_dt,
            # N-need of main product of crop the per t of main product's fresh mass (project.tab_yield["yield_amount_t"]
            self.project.results["crop_rotation"][index]["n_need_main_product_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["dry_mass"] * \
                self.project.db_data["key_figures"][crop_id]["n_main_product_per_t"]

            # N-need of byproduct of crop the per t of main product
            self.project.results["crop_rotation"][index]["n_need_byproduct_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["ratio_byproduct_main_product"] * \
                self.project.db_data["key_figures"][crop_id]["dry_mass_byproduct"] * \
                self.project.db_data["key_figures"][crop_id]["n_byproduct_per_t"]

            # total N-need of the per t of main product
            self.project.results["crop_rotation"][index]["n_need_crop_per_t_fresh_mass_yield_main"] = \
                self.project.results["crop_rotation"][index]["n_need_main_product_per_t_fresh_mass_yield_main"] + \
                self.project.results["crop_rotation"][index]["n_need_byproduct_per_t_fresh_mass_yield_main"]

            # P-need of main product of crop the per t of main product
            self.project.results["crop_rotation"][index]["p_need_main_product_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["dry_mass"] * \
                self.project.db_data["key_figures"][crop_id]["p_main_product_per_t"]

            # P-need of byproduct of crop the per t of main product
            self.project.results["crop_rotation"][index]["p_need_byproduct_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["ratio_byproduct_main_product"] * \
                self.project.db_data["key_figures"][crop_id]["dry_mass_byproduct"] * \
                self.project.db_data["key_figures"][crop_id]["p_byproduct_per_t"]

            # total P-need of the per t of main product
            self.project.results["crop_rotation"][index]["p_need_crop_t_fresh_mass_yield_main"] = \
                self.project.results["crop_rotation"][index]["p_need_main_product_per_t_fresh_mass_yield_main"] + \
                self.project.results["crop_rotation"][index]["p_need_byproduct_per_t_fresh_mass_yield_main"]

            # K-need of main product of crop the per t of main product
            self.project.results["crop_rotation"][index]["k_need_main_product_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["dry_mass"] * \
                self.project.db_data["key_figures"][crop_id]["k_main_product_per_t"]

            # K-need of byproduct of crop the per t of main product
            self.project.results["crop_rotation"][index]["k_need_byproduct_per_t_fresh_mass_yield_main"] = \
                self.project.db_data["key_figures"][crop_id]["ratio_byproduct_main_product"] * \
                self.project.db_data["key_figures"][crop_id]["dry_mass_byproduct"] * \
                self.project.db_data["key_figures"][crop_id]["k_byproduct_per_t"]

            # total K-need of the per t of main product
            self.project.results["crop_rotation"][index]["k_need_crop_per_t_fresh_mass_yield_main"] = \
                self.project.results["crop_rotation"][index]["k_need_main_product_per_t_fresh_mass_yield_main"] + \
                self.project.results["crop_rotation"][index]["k_need_byproduct_per_t_fresh_mass_yield_main"]

            # write database entries into results to avoid later queries
            if self.project.db_data["key_figures"][crop_id]["seed_quantity"]:
                self.project.results["crop_rotation"][index]["seed_quantity"] = \
                    self.project.db_data["key_figures"][crop_id]["seed_quantity"]
            else:
                self.project.results["crop_rotation"][index]["seed_quantity"] = 0
            self.project.results["crop_rotation"][index]["dry_mass"] = \
                self.project.db_data["key_figures"][crop_id]["dry_mass"]

            self.project.results["crop_rotation"][index]["dry_mass_byproduct"] = \
                self.project.db_data["key_figures"][crop_id]["dry_mass_byproduct"]

            self.project.results["crop_rotation"][index]["ratio_byproduct_main_product"] = \
                self.project.db_data["key_figures"][crop_id]["ratio_byproduct_main_product"]

            self.project.results["crop_rotation"][index]["n_main_product_per_t"] = \
                self.project.db_data["key_figures"][crop_id]["n_main_product_per_t"]

            self.project.results["crop_rotation"][index]["n_byproduct_per_t"] = \
                self.project.db_data["key_figures"][crop_id]["n_byproduct_per_t"]

            # self.project.results["crop_rotation"][index]["p_main_product_per_t"] = npk_need_values[
            #     "p_main_product_per_t"]
            # self.project.results["crop_rotation"][index]["k_main_product_per_t"] = npk_need_values[
            #     "k_main_product_per_t"]







if __name__ == "__main__":
    print("Dummy...")
    dummy = Dummy(13)
    print("Dummy results...")
    # project = dummy.load_project_from_db(122)
    dummy.results = Project().results
    print("Dummy calculations", dummy.results)

    n_need_test = NPKNeedCrop(dummy.project)
    print(n_need_test.project.results["n_need_per_dt"])