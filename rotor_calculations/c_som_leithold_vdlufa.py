from db_connect.db_connect import execute_query_get_one
# from rotor_calculations.rotor_calculation import RotorCalculation
from rotor_calculations.rotor_calculation import RotorCalculation


class SOMLeitholdVDLUFA(RotorCalculation):

    def get_result(self):
        self.get_humus_balance_leithold()
        self.get_humus_balance_vdlufa()

    def get_humus_balance_vdlufa(self): #HE_OEL_SOC
        if self.project.dict_tab_manure_2["manure_solid_rotting"] == 1: # fresh
            som_solid = 1.193 * self.project.dict_tab_manure_2["manure_solid_dry_matter"] + 4.207
        elif self.project.dict_tab_manure_2["manure_solid_rotting"] == 2: # rotted
            som_solid = 1.609 * self.project.dict_tab_manure_2["manure_solid_dry_matter"] - 0.273
        else:
            som_solid = 1.691 * self.project.dict_tab_manure_2["manure_solid_dry_matter"] + 3.053


        if self.project.dict_tab_manure_2["manure_liquid_type"] == 2: # pig
            som_liquid = self.project.dict_tab_manure_2["manure_liquid_dry_matter"]

        elif self.project.dict_tab_manure_2["manure_liquid_type"] == 1: # cattle
            som_liquid = 0.966 * self.project.dict_tab_manure_2["manure_liquid_dry_matter"] + 1.8045

        elif self.project.dict_tab_manure_2["manure_liquid_type"] == 3: # poultry
            som_liquid = 0.881 * self.project.dict_tab_manure_2["manure_liquid_dry_matter"] - 0.9762

        else: # TODO implement in GUI separated slurry
            som_liquid = 1.6091 * self.project.dict_tab_manure_2["manure_liquid_dry_matter"] - 0.2727


        for index in range(0, self.project.tab_general["rotation_duration"]):
            som_organic_manure = self.project.tab_rotation["manure_amount_solid"][index] * som_solid + \
                                 self.project.tab_rotation["manure_amount_liquid"][index] * som_liquid

            self.project.results["som_organic_manure_vdlufa"][index] = som_organic_manure

    def get_humus_balance_leithold(self): # HumusRep
        for index in range(0, self.project.tab_general["rotation_duration"]):
            if self.project.dict_tab_manure_2["manure_solid_rotting"] == 1:  # fresh
                som_solid = .05
            elif self.project.dict_tab_manure_2["manure_solid_rotting"] == 2:  # rotted
                som_solid = .07
            else:
                som_solid = .01

            if self.project.dict_tab_manure_2["manure_liquid_type"] == 2:  # pig
                som_liquid = self.project.dict_tab_manure_2["manure_liquid_dry_matter"] * .0018

            elif self.project.dict_tab_manure_2["manure_liquid_type"] == 1:  # cattle
                som_liquid = 0.966 * self.project.dict_tab_manure_2["manure_liquid_dry_matter"] * .0022

            else:  # TODO implement message
                som_liquid = 0

            for index in range(0, self.project.tab_general["rotation_duration"]):
                som_organic_manure = self.project.tab_rotation["manure_amount_solid"][index] * som_solid + \
                                     self.project.tab_rotation["manure_amount_liquid"][index] * som_liquid

                self.project.results["som_organic_manure_leithold"][index] = som_organic_manure


        # Leithold:
        #  HE_OEL_SOC = [HE_OEL]*580
        #  HE_OEL = [HE_B]+[HE_E]
        #  HE_E = [HM_Kult]+[HM_NP]+[HM_ZF_US]+[HM_OD]
        #  HE_B = IIf([HEOL]<0,[HEOL],0)
            # TODO hardcoded prodsys
            sql_query = """SELECT humus_units_for_oil FROM humus_factors_tb WHERE production_system_id = %(prod_sys_id)s 
                            AND %(crop_id)s""" % {"crop_id": self.project.tab_rotation["crop_id"][index], "prod_sys_id": 2}
            humus_units = execute_query_get_one(sql_query)[0]


            if humus_units < 0:
                he_b = humus_units
            else: he_b = 0

            # HM_NP
            if self.project.tab_rotation["crop_type_id"][index] == 1: #WRA
                if self.project.tab_rotation["crop_id"][index] == 33 and self.project.tab_rotation["strawharvest"][index] is False:
                    #hm_np = self.project.results["yield_dt"][index] * kw:fnp *.012
                    pass
            else:
                hm_np = 0
