"""
Crop specific mineralisation depending on soil parameters and precipitation
"""
from db_connect.db_connect import execute_query_get_one
from rotor_calculations.rotor_calculation import RotorCalculation


class NMineralisationPerYear(RotorCalculation):

    def get_result(self):
        self.get_n_mineralisation_per_year()

    def get_n_mineralisation_per_year(self):
        for index in (0, self.project.tab_general["rotation_duration"]):
            if self.project.results["n_level"][index] == 1:
                self.project.results["n_mineraliseable_per_year"][index] = self.project.result_general[
                    "n_mineraliseable_per_year_n_level_1"]
            elif self.project.results["n_level"][index] == 2:
                self.project.results["n_mineraliseable_per_year"][index] = self.project.result_general[
                    "n_mineraliseable_per_year_n_level_2"]
            else:
                self.project.results["n_mineraliseable_per_year"][index] = self.project.result_general[
                    "n_mineraliseable_per_year_n_level_3"]

            sql_query = """SELECT n_mineralisation_crop FROM crop_key_figures_tb WHERE crop_id = %(crop_id)s""" % {
                "crop_id": self.project.results["crop_rotation"][index]["crop_id"]}
            n_mineralisation_crop = execute_query_get_one(sql_query)

            self.project.results["n_mineralised_per_crop"][index] = self.project.result_general[
                                                                        "n_mineralisation_per_year"] * \
                                                                    self.project.results["n_mineraliseable_per_year"][
                                                                        index] * n_mineralisation_crop * \
                                                                    (.8 + self.project.result_general[
                                                                        "precipitation_factor"]) - \
                                                                    self.project.results[
                                                                        "n_mineralisation_reduction_tillage"]
            # TODO self.project.results["n_mineralisation_reduction_tillage"] ist Diff_NUeb, das muss noch eingearbeitet werden, da immer 1!!!

        """
        Old code to new code:
        Nminpa = Nminp * EfMinpa * N_Kult_Min * (0.8 + NF) - Diff_NUeb
        nminp = self.project.result_general["n_mineralisation_per_year"]
        Efminpa = self.project.result_general["n_mineraliseable_per_year_n_level_1"]
        N_Kult_Min = KW.N_Kult_Min = db.crop_key_figures_tb.n_mineralsation_crop
        NF = self.project.result_general["precipitation_factor"]
        Diff_NUeb ist immer 1! Siehe SQL Code F_Nleach 'Diff_NUeb dient zur Reduktion der N-Mineralisierung bei                                                              'reduz. Striegel- und Bodenbearbeitung Ba: 22.03.06
            'fester Mineraliserungsfaktor =2,0% nach Scheffer Schachtschabel (1979) Bodenkunde 10. Auflage
        """
