from rotor_calculations.rotor_calculation import RotorCalculation


class NMinSOMPA(RotorCalculation):
    """
    Function Nminpy(), variable Nminp
    N Mineralisation from top soil (SOM) per year in kg/ha
    """

    def get_result(self):
        self.n_mineralisation_som()


    def get_n_mineralisation_som_pa(self):

        soil_mineralisation = self.project.tab_general["soil_mineralisation"]
        som = self.project.tab_general["soil_organic_matter"]
        cn = self.project.tab_general["soil_c_n_ratio"]
        topsoil = self.project.tab_general["soil_topsoil_depth"]
        stoneratio = self.project.tab_general["soil_stoneratio"]
        bulk_density = self.project.tab_general["soil_bulk_density"]
        sri = self.project.tab_general["soil_rating_index"]


        fines_content = 0.0077 * sri**2 + 0.0548 * sri
        c_org = fines_content * 0.023 + 0.41
        ap = -0.0012 * sri**2 + 0.2182 * sri + 22.273  # TODO WHATEVER THAT IS!?

        if self.project.tab_general["soil_parameters_manually_modified"]:
            n_min_som_pa = som / cn * topsoil * (1 - stoneratio / 100) * bulk_density * soil_mineralisation * 10 # war GetAMR/100 * 1000
        else:
            n_min_som_pa = ap * 1.5 * 90.9 * c_org * soil_mineralisation / 100


        self.project.result["n_level_mineralisation_pa"] = n_min_som_pa