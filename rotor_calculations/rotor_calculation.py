'''This is the parent-class to all calculations'''

class RotorCalculation:
    def __init__(self, project, ui_status):
        self.project = project
        self.ui_status = ui_status

        self.rotation_duration = project.tab_general["rotation_duration"]
        self.error_name = ""
        self.get_result()


    def get_result(self):
        pass