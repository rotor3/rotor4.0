from datetime import datetime

from PySide2 import QtWidgets
from PySide2.QtWidgets import QTableWidgetItem

from project_handling.project_handling import ProjectHandling
from project_handling.project import Project
from ui_logic.logic_open_project import LogicOpenProjectWindow
from ui_logic.ui_status import UiStatus
from qtui_rotor.openprojectwindow import Ui_OpenProjectWindow


class OpenProjectWindow(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.open_project_window = Ui_OpenProjectWindow()

        self.open_project_window.setupUi(self)
        self.open_project_window.tw_project_selection.setColumnHidden(0, True)
        self.open_project_window.tw_project_selection.setColumnWidth(1, 300)

        self.open_project_window.tw_project_selection.cellClicked.connect(self.select_project_id)
        self.open_project_window.tw_project_selection.cellActivated.connect(self.select_project_id)
        self.open_project_window.tw_project_selection.cellDoubleClicked.connect(self.open_project)

        self.open_project_window.bt_open_project.clicked.connect(self.open_project)
        self.open_project_window.bt_cancel.clicked.connect(self.close)
        self.display_info()

        self.project = Project()
        self.ui_status = UiStatus()

        # self.open_project_window.bt_open_project.clicked.connect(self.open_project)

    def select_project_id(self, row, column):
        self.project.project_id = int(self.open_project_window.tw_project_selection.item(row, 0).text())

    def open_project(self):
        if self.project.project_id is not None:
            self.project, self.ui_status = ProjectHandling.load_project_from_db(self.project)
            self.close()
        else:
            pass


    def display_info(self):
        print("entering open_project_window.display_info")
        # Data into table
        # recordset_projects = self.get_project_list()
        recordset_projects = LogicOpenProjectWindow.get_project_list()
        print("Open Project recordset", recordset_projects)
        projects_count = len(recordset_projects)


        self.open_project_window.tw_project_selection.setRowCount(projects_count)



        while True:
            i = 0
            for row in recordset_projects:
                # TODO if not null...
                # the first row with the project_id is hidden
                self.open_project_window.tw_project_selection.setItem(i, 0, QTableWidgetItem(str(row[0])))
                self.open_project_window.tw_project_selection.setItem(i, 1, QTableWidgetItem(row[1]))
                # self.open_project_window.tw_project_selection.setItem(i, 2, QTableWidgetItem(str(datetime(row[2]).strftime('%d/%m/%y %H:%M'))))
                self.open_project_window.tw_project_selection.setItem(i, 2, QTableWidgetItem(
                    str(datetime.fromtimestamp(row[2]).strftime('%d/%m/%y %H:%M'))))

                # self.open_project_window.tw_project_selection.setItem(i, 2, QTableWidgetItem(row[2]).strftime('%d/%m/%y %I:%M %S %p'))
                i += 1
            break


if __name__ == '__main__':
    #recordset = OpenProjectWindow.get_project_list(False, False, False)
    window = OpenProjectWindow()
    window.show()
    # print(recordset)