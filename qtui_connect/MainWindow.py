import copy
import json
import os



from PySide2.QtWidgets import QButtonGroup, QTableWidgetItem, QMessageBox, QMainWindow, QFileDialog, QApplication
from PySide2.QtGui import QDesktopServices
from PySide2.QtCore import QFileInfo, QEvent, QCoreApplication, QUrl

from qtui_rotor.translator import Translator
from qtui_rotor.mainwindow import Ui_MainWindow
from qtui_connect.OpenProjectWindow import OpenProjectWindow
from rotation_generation.crop_rotations import CropRotation
from rotor_calculations.calculate_2 import Calculate2
from rotor_calculations.calculate_1 import Calculate1

from ui_logic.logic_free_generation import LogicFreeRotationGeneration
from ui_logic.logic_general import LogicGeneral
from ui_logic.logic_sidebar import LogicSidebar
from ui_logic.logic_tab_yield import LogicTabYield
from ui_logic.logic_tab_general import LogicTabGeneral
from ui_logic.logic_tab_manure import LogicTabManure
from ui_logic.logic_tab_report import LogicTabReport
from ui_logic.logic_tab_rotation import LogicTabRotation
from ui_logic.ui_status import UiStatus
import logging

# logging.basicConfig(level=logging.DEBUG)

# logging.basicConfig(filename='../logging/rotor.log', level=logging.ERROR, format='%(asctime)s:%(levelname)s:%(message)s')

from lincence import Licence
from rotor_version import Version
from project_handling.project import Project
from project_handling.project_handling import ProjectHandling
from dialog_window import DialogWindow
from project_handling.json_serialize import json_decode_project, json_encode_project



# from rotor_log import Logger


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.QTUI_rotor = Ui_MainWindow()
        #uic.loadUi("./qtui_rotor/mainwindow.ui", self)




        # self.setWindowTitle("ROTOR 4.0  --  {}".format(self.project.project_name))

        # translator specifies available translations
        #self.gui_translator = Translator()
        # generated qt window is shown in python window


        self.QTUI_rotor.setupUi(self)

        # constants
        self.MAX_DURATION = 10
        self.MIN_DURATION = 3

        self.project = Project()
        self.ui_status = UiStatus()
        # GUI default values
        self.ui_status.settings["language"] = "de"

        self.ui_status.load_project = True
        self.generation_repopulating = False
        self.setting_up_tab_manure = False
        # boolean for if the generation has already been calculated
        self.intelligent_rotation_calculated = True
        # tab general is the first tab displayed
        self.QTUI_rotor.tabs_rotor.setCurrentIndex(0)
        # default values for crop rotation
        self.rotation_duration = self.project.tab_general["rotation_duration"]

        # ----------------------TABS-------------------------------------

        self.QTUI_rotor.tabs_rotor.currentChanged.connect(self.tab_changed)
        # -------------MENUBAR---------------------------------------------
        self.QTUI_rotor.action_save.triggered.connect(self.save_project)
        self.QTUI_rotor.action_save_as.triggered.connect(self.save_project_as)
        self.QTUI_rotor.action_open_project.triggered.connect(self.open_project)
        self.QTUI_rotor.action_export_project.triggered.connect(self.export_project)
        self.QTUI_rotor.action_import_project.triggered.connect(self.import_project)

        self.QTUI_rotor.action_deutsch.triggered.connect(lambda: self.set_language("de"))
        self.QTUI_rotor.action_english.triggered.connect(lambda: self.set_language("en"))
        self.QTUI_rotor.action_francais.triggered.connect(lambda: self.set_language("fr"))

        self.QTUI_rotor.action_francais.triggered.connect(self.show_manual)

        self.QTUI_rotor.action_licence.triggered.connect(lambda: Licence.show_licence(self, self.ui_status))
        self.QTUI_rotor.action_version.triggered.connect(lambda: Version.show_versions(self, self.ui_status))

        # ------------- GENERAL BUTTONS --------------------------------
        self.QTUI_rotor.bt_reset_all.clicked.connect(self.reset_all)
        self.QTUI_rotor.bt_back.clicked.connect(self.tab_back)
        self.QTUI_rotor.bt_forward.clicked.connect(self.tab_forward)
        self.QTUI_rotor.bt_reset_current_tab.clicked.connect(self.reset_current_tab)
        self.QTUI_rotor.bt_calculate.clicked.connect(self.bt_calculate_generate_rotations)

        self.QTUI_rotor.bt_save.clicked.connect(self.save_project)

        # --------------TAB 1: GENERAL------------------------------------------------------------
        # ----------rotation duration changed-----------------------------------------------------
        self.QTUI_rotor.cb_rotation_duration.currentIndexChanged.connect(self.get_rotation_duration)

        # Precipitation sliders' connection to spinbox
        self.QTUI_rotor.hslider_precipitation.valueChanged['int'].connect(self.QTUI_rotor.sb_precipitation.setValue)
        self.QTUI_rotor.hslider_precipitation_winter.valueChanged['int'].connect(
            self.QTUI_rotor.sb_precipitation_winter.setValue)
        self.QTUI_rotor.gb_precipitation_winter.toggled.connect(self.gb_precipitation_winter_toggled)

        # Radiobuttons for assessment method
        # Grouping, IDs are assigned
        self.rb_assessment_method_group = QButtonGroup()
        self.rb_assessment_method_group.addButton(self.QTUI_rotor.rb_rotation_intelligent_generation, 1)
        self.rb_assessment_method_group.addButton(self.QTUI_rotor.rb_rotation_free_genereration, 2)
        self.rb_assessment_method_group.addButton(self.QTUI_rotor.rb_rotation_assessment, 3)

        self.rb_assessment_method_group.buttonClicked[int].connect(self.rb_assessment_selection)

        self.QTUI_rotor.sb_precipitation.valueChanged.connect(self.sb_precipitation_value_changed)
        self.QTUI_rotor.sb_precipitation_winter.valueChanged.connect(self.sb_precipitation_winter_value_changed)

        # sync soil type radiobuttons activate comboxes
        self.QTUI_rotor.rb_soil_index.toggled['bool'].connect(self.QTUI_rotor.sb_soil_index.setEnabled)
        self.QTUI_rotor.rb_soil_type.toggled['bool'].connect(self.QTUI_rotor.cb_soil_type.setEnabled)
        self.QTUI_rotor.rb_soil_index.toggled.connect(self.rb_soil_index_toggled)
        self.QTUI_rotor.sb_soil_index.valueChanged.connect(self.set_cb_soil_type_from_soil_index)
        self.QTUI_rotor.cb_soil_type.currentIndexChanged.connect(self.set_sb_soil_index_from_soil_type)

        # atmospheric nitrogen
        self.QTUI_rotor.sb_atmospheric_n_input.valueChanged.connect(self.sb_atmospheric_n_input_value_changed)
        self.QTUI_rotor.clb_uba_nitrogen_map.clicked.connect(self.open_url)
        self.QTUI_rotor.xb_red_area.toggled['bool'].connect(self.xb_red_area_toggled)

        # soil parameters
        self.QTUI_rotor.gb_soil_parameters_advanced.toggled.connect(self.gb_soil_parameters_advanced_toggled)
        self.QTUI_rotor.sb_soil_adv_mineralisation.valueChanged.connect(self.sb_soil_mineralisation_value_changed)
        self.QTUI_rotor.sb_soil_adv_organic_matter.valueChanged.connect(self.sb_soil_organic_matter_value_changed)
        self.QTUI_rotor.sb_soil_adv_c_n_ratio.valueChanged.connect(self.sb_soil_c_n_ratio_value_changed)
        self.QTUI_rotor.sb_soil_adv_dry_density.valueChanged.connect(self.sb_soil_dry_density_value_changed)
        self.QTUI_rotor.sb_soil_adv_stoneratio.valueChanged.connect(self.sb_soil_stoneratio_value_changed)
        self.QTUI_rotor.sb_soil_adv_topsoil_depth.valueChanged.connect(self.sb_soil_topsoil_depth_value_changed)

        # --------------TAB 2: CROP ROTATION------------------------------------------------------------
        # Visibility of crop rotation GroupBoxes in rotation tab
        self.gb_rotation_list = [self.QTUI_rotor.gb_rotation_year_1, self.QTUI_rotor.gb_rotation_year_2,
                                 self.QTUI_rotor.gb_rotation_year_3, self.QTUI_rotor.gb_rotation_year_4,
                                 self.QTUI_rotor.gb_rotation_year_5, self.QTUI_rotor.gb_rotation_year_6,
                                 self.QTUI_rotor.gb_rotation_year_7, self.QTUI_rotor.gb_rotation_year_8,
                                 self.QTUI_rotor.gb_rotation_year_9, self.QTUI_rotor.gb_rotation_year_10]

        # ------------List of all crop selection comboboxes
        self.cb_crop_year_list = [self.QTUI_rotor.cb_crop_year_1, self.QTUI_rotor.cb_crop_year_2,
                                  self.QTUI_rotor.cb_crop_year_3, self.QTUI_rotor.cb_crop_year_4,
                                  self.QTUI_rotor.cb_crop_year_5, self.QTUI_rotor.cb_crop_year_6,
                                  self.QTUI_rotor.cb_crop_year_7, self.QTUI_rotor.cb_crop_year_8,
                                  self.QTUI_rotor.cb_crop_year_9, self.QTUI_rotor.cb_crop_year_10]

        # ------------Legumegras extras
        self.qf_covercrop_list = [self.QTUI_rotor.qf_covercrop_year_1,
                                  self.QTUI_rotor.qf_covercrop_year_2,
                                  self.QTUI_rotor.qf_covercrop_year_3,
                                  self.QTUI_rotor.qf_covercrop_year_4,
                                  self.QTUI_rotor.qf_covercrop_year_5,
                                  self.QTUI_rotor.qf_covercrop_year_6,
                                  self.QTUI_rotor.qf_covercrop_year_7,
                                  self.QTUI_rotor.qf_covercrop_year_8,
                                  self.QTUI_rotor.qf_covercrop_year_9,
                                  self.QTUI_rotor.qf_covercrop_year_10]

        self.qw_legumegras_list = [self.QTUI_rotor.qw_legumegras_1,
                                   self.QTUI_rotor.qw_legumegras_2,
                                   self.QTUI_rotor.qw_legumegras_3,
                                   self.QTUI_rotor.qw_legumegras_4,
                                   self.QTUI_rotor.qw_legumegras_5,
                                   self.QTUI_rotor.qw_legumegras_6,
                                   self.QTUI_rotor.qw_legumegras_7,
                                   self.QTUI_rotor.qw_legumegras_8,
                                   self.QTUI_rotor.qw_legumegras_9,
                                   self.QTUI_rotor.qw_legumegras_10]

        self.xb_early_tillage_list = [self.QTUI_rotor.xb_early_tillage_1,
                                      self.QTUI_rotor.xb_early_tillage_2,
                                      self.QTUI_rotor.xb_early_tillage_3,
                                      self.QTUI_rotor.xb_early_tillage_4,
                                      self.QTUI_rotor.xb_early_tillage_5,
                                      self.QTUI_rotor.xb_early_tillage_6,
                                      self.QTUI_rotor.xb_early_tillage_7,
                                      self.QTUI_rotor.xb_early_tillage_8,
                                      self.QTUI_rotor.xb_early_tillage_9,
                                      self.QTUI_rotor.xb_early_tillage_10]

        self.sb_main_legume_ratio_list = [self.QTUI_rotor.sb_main_legume_ratio_1,
                                      self.QTUI_rotor.sb_main_legume_ratio_2,
                                      self.QTUI_rotor.sb_main_legume_ratio_3,
                                      self.QTUI_rotor.sb_main_legume_ratio_4,
                                      self.QTUI_rotor.sb_main_legume_ratio_5,
                                      self.QTUI_rotor.sb_main_legume_ratio_6,
                                      self.QTUI_rotor.sb_main_legume_ratio_7,
                                      self.QTUI_rotor.sb_main_legume_ratio_8,
                                      self.QTUI_rotor.sb_main_legume_ratio_9,
                                      self.QTUI_rotor.sb_main_legume_ratio_10]

        self.sb_green_fodder_list = [self.QTUI_rotor.sb_green_fodder_1,
                                      self.QTUI_rotor.sb_green_fodder_2,
                                      self.QTUI_rotor.sb_green_fodder_3,
                                      self.QTUI_rotor.sb_green_fodder_4,
                                      self.QTUI_rotor.sb_green_fodder_5,
                                      self.QTUI_rotor.sb_green_fodder_6,
                                      self.QTUI_rotor.sb_green_fodder_7,
                                      self.QTUI_rotor.sb_green_fodder_8,
                                      self.QTUI_rotor.sb_green_fodder_9,
                                      self.QTUI_rotor.sb_green_fodder_10]

        self.sb_hay_list = [self.QTUI_rotor.sb_hay_1,
                                      self.QTUI_rotor.sb_hay_2,
                                      self.QTUI_rotor.sb_hay_3,
                                      self.QTUI_rotor.sb_hay_4,
                                      self.QTUI_rotor.sb_hay_5,
                                      self.QTUI_rotor.sb_hay_6,
                                      self.QTUI_rotor.sb_hay_7,
                                      self.QTUI_rotor.sb_hay_8,
                                      self.QTUI_rotor.sb_hay_9,
                                      self.QTUI_rotor.sb_hay_10]

        self.sb_silage_list = [self.QTUI_rotor.sb_silage_1,
                                      self.QTUI_rotor.sb_silage_2,
                                      self.QTUI_rotor.sb_silage_3,
                                      self.QTUI_rotor.sb_silage_4,
                                      self.QTUI_rotor.sb_silage_5,
                                      self.QTUI_rotor.sb_silage_6,
                                      self.QTUI_rotor.sb_silage_7,
                                      self.QTUI_rotor.sb_silage_8,
                                      self.QTUI_rotor.sb_silage_9,
                                      self.QTUI_rotor.sb_silage_10]

        self.sb_main_legume_ratio_list[0].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(0))
        self.sb_main_legume_ratio_list[1].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(1))
        self.sb_main_legume_ratio_list[2].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(2))
        self.sb_main_legume_ratio_list[3].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(3))
        self.sb_main_legume_ratio_list[4].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(4))
        self.sb_main_legume_ratio_list[5].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(5))
        self.sb_main_legume_ratio_list[6].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(6))
        self.sb_main_legume_ratio_list[7].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(7))
        self.sb_main_legume_ratio_list[8].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(8))
        self.sb_main_legume_ratio_list[9].valueChanged.connect(lambda: self.sb_main_legume_ratio_changed(9))



        self.sb_green_fodder_list[0].valueChanged.connect(lambda: self.sb_green_fodder_changed(0))
        self.sb_green_fodder_list[1].valueChanged.connect(lambda: self.sb_green_fodder_changed(1))
        self.sb_green_fodder_list[2].valueChanged.connect(lambda: self.sb_green_fodder_changed(2))
        self.sb_green_fodder_list[3].valueChanged.connect(lambda: self.sb_green_fodder_changed(3))
        self.sb_green_fodder_list[4].valueChanged.connect(lambda: self.sb_green_fodder_changed(4))
        self.sb_green_fodder_list[5].valueChanged.connect(lambda: self.sb_green_fodder_changed(5))
        self.sb_green_fodder_list[6].valueChanged.connect(lambda: self.sb_green_fodder_changed(6))
        self.sb_green_fodder_list[7].valueChanged.connect(lambda: self.sb_green_fodder_changed(7))
        self.sb_green_fodder_list[8].valueChanged.connect(lambda: self.sb_green_fodder_changed(8))
        self.sb_green_fodder_list[9].valueChanged.connect(lambda: self.sb_green_fodder_changed(9))

        self.sb_hay_list[0].valueChanged.connect(lambda: self.sb_hay_changed(0))
        self.sb_hay_list[1].valueChanged.connect(lambda: self.sb_hay_changed(1))
        self.sb_hay_list[2].valueChanged.connect(lambda: self.sb_hay_changed(2))
        self.sb_hay_list[3].valueChanged.connect(lambda: self.sb_hay_changed(3))
        self.sb_hay_list[4].valueChanged.connect(lambda: self.sb_hay_changed(4))
        self.sb_hay_list[5].valueChanged.connect(lambda: self.sb_hay_changed(5))
        self.sb_hay_list[6].valueChanged.connect(lambda: self.sb_hay_changed(6))
        self.sb_hay_list[7].valueChanged.connect(lambda: self.sb_hay_changed(7))
        self.sb_hay_list[8].valueChanged.connect(lambda: self.sb_hay_changed(8))
        self.sb_hay_list[9].valueChanged.connect(lambda: self.sb_hay_changed(9))

        self.sb_silage_list[0].valueChanged.connect(lambda: self.sb_silage_changed(0))
        self.sb_silage_list[1].valueChanged.connect(lambda: self.sb_silage_changed(1))
        self.sb_silage_list[2].valueChanged.connect(lambda: self.sb_silage_changed(2))
        self.sb_silage_list[3].valueChanged.connect(lambda: self.sb_silage_changed(3))
        self.sb_silage_list[4].valueChanged.connect(lambda: self.sb_silage_changed(4))
        self.sb_silage_list[5].valueChanged.connect(lambda: self.sb_silage_changed(5))
        self.sb_silage_list[6].valueChanged.connect(lambda: self.sb_silage_changed(6))
        self.sb_silage_list[7].valueChanged.connect(lambda: self.sb_silage_changed(7))
        self.sb_silage_list[8].valueChanged.connect(lambda: self.sb_silage_changed(8))
        self.sb_silage_list[9].valueChanged.connect(lambda: self.sb_silage_changed(9))

        # --------------------------------Tab Rotation: MANURE------------------------------------------

        self.gb_manure_list = [self.QTUI_rotor.gb_manure_1, self.QTUI_rotor.gb_manure_2,
                               self.QTUI_rotor.gb_manure_3, self.QTUI_rotor.gb_manure_4,
                               self.QTUI_rotor.gb_manure_5, self.QTUI_rotor.gb_manure_6, self.QTUI_rotor.gb_manure_7,
                               self.QTUI_rotor.gb_manure_8, self.QTUI_rotor.gb_manure_9, self.QTUI_rotor.gb_manure_10]

        self.qw_manure_list = [[self.QTUI_rotor.qw_manure_1_1, self.QTUI_rotor.qw_manure_1_2,
                                self.QTUI_rotor.qw_manure_1_3, self.QTUI_rotor.qw_manure_1_4],
                               [self.QTUI_rotor.qw_manure_2_1, self.QTUI_rotor.qw_manure_2_2,
                                self.QTUI_rotor.qw_manure_2_3, self.QTUI_rotor.qw_manure_2_4],
                               [self.QTUI_rotor.qw_manure_3_1, self.QTUI_rotor.qw_manure_3_2,
                                self.QTUI_rotor.qw_manure_3_3, self.QTUI_rotor.qw_manure_3_4],
                               [self.QTUI_rotor.qw_manure_4_1, self.QTUI_rotor.qw_manure_4_2,
                                self.QTUI_rotor.qw_manure_4_3, self.QTUI_rotor.qw_manure_4_4],
                               [self.QTUI_rotor.qw_manure_5_1, self.QTUI_rotor.qw_manure_5_2,
                                self.QTUI_rotor.qw_manure_5_3, self.QTUI_rotor.qw_manure_5_4],
                               [self.QTUI_rotor.qw_manure_6_1, self.QTUI_rotor.qw_manure_6_2,
                                self.QTUI_rotor.qw_manure_6_3, self.QTUI_rotor.qw_manure_6_4],
                               [self.QTUI_rotor.qw_manure_7_1, self.QTUI_rotor.qw_manure_7_2,
                                self.QTUI_rotor.qw_manure_7_3, self.QTUI_rotor.qw_manure_7_4],
                               [self.QTUI_rotor.qw_manure_8_1, self.QTUI_rotor.qw_manure_8_2,
                                self.QTUI_rotor.qw_manure_8_3, self.QTUI_rotor.qw_manure_8_4],
                               [self.QTUI_rotor.qw_manure_9_1, self.QTUI_rotor.qw_manure_9_2,
                                self.QTUI_rotor.qw_manure_9_3, self.QTUI_rotor.qw_manure_9_4],
                               [self.QTUI_rotor.qw_manure_10_1, self.QTUI_rotor.qw_manure_10_2,
                                self.QTUI_rotor.qw_manure_10_3, self.QTUI_rotor.qw_manure_10_4]]

        self.gb_manure_list[0].toggled.connect(lambda: self.gb_manure_toggled(0))
        self.gb_manure_list[1].toggled.connect(lambda: self.gb_manure_toggled(1))
        self.gb_manure_list[2].toggled.connect(lambda: self.gb_manure_toggled(2))
        self.gb_manure_list[3].toggled.connect(lambda: self.gb_manure_toggled(3))
        self.gb_manure_list[4].toggled.connect(lambda: self.gb_manure_toggled(4))
        self.gb_manure_list[5].toggled.connect(lambda: self.gb_manure_toggled(5))
        self.gb_manure_list[6].toggled.connect(lambda: self.gb_manure_toggled(6))
        self.gb_manure_list[7].toggled.connect(lambda: self.gb_manure_toggled(7))
        self.gb_manure_list[8].toggled.connect(lambda: self.gb_manure_toggled(8))
        self.gb_manure_list[9].toggled.connect(lambda: self.gb_manure_toggled(9))

        self.cb_manure_type_list = [[self.QTUI_rotor.cb_manure_type_1_1, self.QTUI_rotor.cb_manure_type_1_2,
                                     self.QTUI_rotor.cb_manure_type_1_3, self.QTUI_rotor.cb_manure_type_1_4],
                                    [self.QTUI_rotor.cb_manure_type_2_1, self.QTUI_rotor.cb_manure_type_2_2,
                                     self.QTUI_rotor.cb_manure_type_2_3, self.QTUI_rotor.cb_manure_type_2_4],
                                    [self.QTUI_rotor.cb_manure_type_3_1, self.QTUI_rotor.cb_manure_type_3_2,
                                     self.QTUI_rotor.cb_manure_type_3_3, self.QTUI_rotor.cb_manure_type_3_4],
                                    [self.QTUI_rotor.cb_manure_type_4_1, self.QTUI_rotor.cb_manure_type_4_2,
                                     self.QTUI_rotor.cb_manure_type_4_3, self.QTUI_rotor.cb_manure_type_4_4],
                                    [self.QTUI_rotor.cb_manure_type_5_1, self.QTUI_rotor.cb_manure_type_5_2,
                                     self.QTUI_rotor.cb_manure_type_5_3, self.QTUI_rotor.cb_manure_type_5_4],
                                    [self.QTUI_rotor.cb_manure_type_6_1, self.QTUI_rotor.cb_manure_type_6_2,
                                     self.QTUI_rotor.cb_manure_type_6_3, self.QTUI_rotor.cb_manure_type_6_4],
                                    [self.QTUI_rotor.cb_manure_type_7_1, self.QTUI_rotor.cb_manure_type_7_2,
                                     self.QTUI_rotor.cb_manure_type_7_3, self.QTUI_rotor.cb_manure_type_7_4],
                                    [self.QTUI_rotor.cb_manure_type_8_1, self.QTUI_rotor.cb_manure_type_8_2,
                                     self.QTUI_rotor.cb_manure_type_8_3, self.QTUI_rotor.cb_manure_type_8_4],
                                    [self.QTUI_rotor.cb_manure_type_9_1, self.QTUI_rotor.cb_manure_type_9_2,
                                     self.QTUI_rotor.cb_manure_type_9_3, self.QTUI_rotor.cb_manure_type_9_4],
                                    [self.QTUI_rotor.cb_manure_type_10_1, self.QTUI_rotor.cb_manure_type_10_2,
                                     self.QTUI_rotor.cb_manure_type_10_3, self.QTUI_rotor.cb_manure_type_10_4]]

        self.cb_manure_source_list = [[self.QTUI_rotor.cb_manure_source_1_1, self.QTUI_rotor.cb_manure_source_1_2,
                                       self.QTUI_rotor.cb_manure_source_1_3, self.QTUI_rotor.cb_manure_source_1_4],
                                      [self.QTUI_rotor.cb_manure_source_2_1, self.QTUI_rotor.cb_manure_source_2_2,
                                       self.QTUI_rotor.cb_manure_source_2_3, self.QTUI_rotor.cb_manure_source_2_4],
                                      [self.QTUI_rotor.cb_manure_source_3_1, self.QTUI_rotor.cb_manure_source_3_2,
                                       self.QTUI_rotor.cb_manure_source_3_3, self.QTUI_rotor.cb_manure_source_3_4],
                                      [self.QTUI_rotor.cb_manure_source_4_1, self.QTUI_rotor.cb_manure_source_4_2,
                                       self.QTUI_rotor.cb_manure_source_4_3, self.QTUI_rotor.cb_manure_source_4_4],
                                      [self.QTUI_rotor.cb_manure_source_5_1, self.QTUI_rotor.cb_manure_source_5_2,
                                       self.QTUI_rotor.cb_manure_source_5_3, self.QTUI_rotor.cb_manure_source_5_4],
                                      [self.QTUI_rotor.cb_manure_source_6_1, self.QTUI_rotor.cb_manure_source_6_2,
                                       self.QTUI_rotor.cb_manure_source_6_3, self.QTUI_rotor.cb_manure_source_6_4],
                                      [self.QTUI_rotor.cb_manure_source_7_1, self.QTUI_rotor.cb_manure_source_7_2,
                                       self.QTUI_rotor.cb_manure_source_7_3, self.QTUI_rotor.cb_manure_source_7_4],
                                      [self.QTUI_rotor.cb_manure_source_8_1, self.QTUI_rotor.cb_manure_source_8_2,
                                       self.QTUI_rotor.cb_manure_source_8_3, self.QTUI_rotor.cb_manure_source_8_4],
                                      [self.QTUI_rotor.cb_manure_source_9_1, self.QTUI_rotor.cb_manure_source_9_2,
                                       self.QTUI_rotor.cb_manure_source_9_3, self.QTUI_rotor.cb_manure_source_9_4],
                                      [self.QTUI_rotor.cb_manure_source_10_1, self.QTUI_rotor.cb_manure_source_10_2,
                                       self.QTUI_rotor.cb_manure_source_10_3, self.QTUI_rotor.cb_manure_source_10_4]]

        self.cb_manure_specification_list = [[self.QTUI_rotor.cb_manure_spec_1_1, self.QTUI_rotor.cb_manure_spec_1_2,
                                              self.QTUI_rotor.cb_manure_spec_1_3, self.QTUI_rotor.cb_manure_spec_1_4],
                                             [self.QTUI_rotor.cb_manure_spec_2_1, self.QTUI_rotor.cb_manure_spec_2_2,
                                              self.QTUI_rotor.cb_manure_spec_2_3, self.QTUI_rotor.cb_manure_spec_2_4],
                                             [self.QTUI_rotor.cb_manure_spec_3_1, self.QTUI_rotor.cb_manure_spec_3_2,
                                              self.QTUI_rotor.cb_manure_spec_3_3, self.QTUI_rotor.cb_manure_spec_3_4],
                                             [self.QTUI_rotor.cb_manure_spec_4_1, self.QTUI_rotor.cb_manure_spec_4_2,
                                              self.QTUI_rotor.cb_manure_spec_4_3, self.QTUI_rotor.cb_manure_spec_4_4],
                                             [self.QTUI_rotor.cb_manure_spec_5_1, self.QTUI_rotor.cb_manure_spec_5_2,
                                              self.QTUI_rotor.cb_manure_spec_5_3, self.QTUI_rotor.cb_manure_spec_5_4],
                                             [self.QTUI_rotor.cb_manure_spec_6_1, self.QTUI_rotor.cb_manure_spec_6_2,
                                              self.QTUI_rotor.cb_manure_spec_6_3, self.QTUI_rotor.cb_manure_spec_6_4],
                                             [self.QTUI_rotor.cb_manure_spec_7_1, self.QTUI_rotor.cb_manure_spec_7_2,
                                              self.QTUI_rotor.cb_manure_spec_7_3, self.QTUI_rotor.cb_manure_spec_7_4],
                                             [self.QTUI_rotor.cb_manure_spec_8_1, self.QTUI_rotor.cb_manure_spec_8_2,
                                              self.QTUI_rotor.cb_manure_spec_8_3, self.QTUI_rotor.cb_manure_spec_8_4],
                                             [self.QTUI_rotor.cb_manure_spec_9_1, self.QTUI_rotor.cb_manure_spec_9_2,
                                              self.QTUI_rotor.cb_manure_spec_9_3, self.QTUI_rotor.cb_manure_spec_9_4],
                                             [self.QTUI_rotor.cb_manure_spec_10_1, self.QTUI_rotor.cb_manure_spec_10_2,
                                              self.QTUI_rotor.cb_manure_spec_10_3, self.QTUI_rotor.cb_manure_spec_10_4]]

        self.cb_manure_type_list[0][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(0, 0))
        self.cb_manure_type_list[0][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(0, 1))
        self.cb_manure_type_list[0][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(0, 2))
        self.cb_manure_type_list[0][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(0, 3))
        self.cb_manure_type_list[1][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(1, 0))
        self.cb_manure_type_list[1][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(1, 1))
        self.cb_manure_type_list[1][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(1, 2))
        self.cb_manure_type_list[1][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(1, 3))
        self.cb_manure_type_list[2][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(2, 0))
        self.cb_manure_type_list[2][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(2, 1))
        self.cb_manure_type_list[2][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(2, 2))
        self.cb_manure_type_list[2][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(2, 3))
        self.cb_manure_type_list[3][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(3, 0))
        self.cb_manure_type_list[3][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(3, 1))
        self.cb_manure_type_list[3][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(3, 2))
        self.cb_manure_type_list[3][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(3, 3))
        self.cb_manure_type_list[4][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(4, 0))
        self.cb_manure_type_list[4][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(4, 1))
        self.cb_manure_type_list[4][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(4, 2))
        self.cb_manure_type_list[4][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(4, 3))
        self.cb_manure_type_list[5][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(5, 0))
        self.cb_manure_type_list[5][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(5, 1))
        self.cb_manure_type_list[5][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(5, 2))
        self.cb_manure_type_list[5][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(5, 3))
        self.cb_manure_type_list[6][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(6, 0))
        self.cb_manure_type_list[6][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(6, 1))
        self.cb_manure_type_list[6][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(6, 2))
        self.cb_manure_type_list[6][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(6, 3))
        self.cb_manure_type_list[7][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(7, 0))
        self.cb_manure_type_list[7][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(7, 1))
        self.cb_manure_type_list[7][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(7, 2))
        self.cb_manure_type_list[7][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(7, 3))
        self.cb_manure_type_list[8][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(8, 0))
        self.cb_manure_type_list[8][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(8, 1))
        self.cb_manure_type_list[8][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(8, 2))
        self.cb_manure_type_list[8][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(8, 3))
        self.cb_manure_type_list[9][0].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(9, 0))
        self.cb_manure_type_list[9][1].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(9, 1))
        self.cb_manure_type_list[9][2].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(9, 2))
        self.cb_manure_type_list[9][3].currentIndexChanged.connect(lambda: self.cb_manure_type_selected(9, 3))

        self.cb_manure_source_list[0][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(0, 0))
        self.cb_manure_source_list[0][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(0, 1))
        self.cb_manure_source_list[0][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(0, 2))
        self.cb_manure_source_list[0][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(0, 3))
        self.cb_manure_source_list[1][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(1, 0))
        self.cb_manure_source_list[1][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(1, 1))
        self.cb_manure_source_list[1][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(1, 2))
        self.cb_manure_source_list[1][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(1, 3))
        self.cb_manure_source_list[2][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(2, 0))
        self.cb_manure_source_list[2][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(2, 1))
        self.cb_manure_source_list[2][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(2, 2))
        self.cb_manure_source_list[2][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(2, 3))
        self.cb_manure_source_list[3][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(3, 0))
        self.cb_manure_source_list[3][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(3, 1))
        self.cb_manure_source_list[3][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(3, 2))
        self.cb_manure_source_list[3][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(3, 3))
        self.cb_manure_source_list[4][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(4, 0))
        self.cb_manure_source_list[4][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(4, 1))
        self.cb_manure_source_list[4][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(4, 2))
        self.cb_manure_source_list[4][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(4, 3))
        self.cb_manure_source_list[5][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(5, 0))
        self.cb_manure_source_list[5][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(5, 1))
        self.cb_manure_source_list[5][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(5, 2))
        self.cb_manure_source_list[5][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(5, 3))
        self.cb_manure_source_list[6][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(6, 0))
        self.cb_manure_source_list[6][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(6, 1))
        self.cb_manure_source_list[6][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(6, 2))
        self.cb_manure_source_list[6][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(6, 3))
        self.cb_manure_source_list[7][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(7, 0))
        self.cb_manure_source_list[7][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(7, 1))
        self.cb_manure_source_list[7][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(7, 2))
        self.cb_manure_source_list[7][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(7, 3))
        self.cb_manure_source_list[8][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(8, 0))
        self.cb_manure_source_list[8][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(8, 1))
        self.cb_manure_source_list[8][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(8, 2))
        self.cb_manure_source_list[8][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(8, 3))
        self.cb_manure_source_list[9][0].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(9, 0))
        self.cb_manure_source_list[9][1].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(9, 1))
        self.cb_manure_source_list[9][2].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(9, 2))
        self.cb_manure_source_list[9][3].currentIndexChanged.connect(lambda: self.cb_manure_source_selected(9, 3))

        self.cb_manure_specification_list[0][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(0, 0))
        self.cb_manure_specification_list[0][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(0, 1))
        self.cb_manure_specification_list[0][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(0, 2))
        self.cb_manure_specification_list[0][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(0, 3))
        self.cb_manure_specification_list[1][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(1, 0))
        self.cb_manure_specification_list[1][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(1, 1))
        self.cb_manure_specification_list[1][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(1, 2))
        self.cb_manure_specification_list[1][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(1, 3))
        self.cb_manure_specification_list[2][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(2, 0))
        self.cb_manure_specification_list[2][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(2, 1))
        self.cb_manure_specification_list[2][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(2, 2))
        self.cb_manure_specification_list[2][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(2, 3))
        self.cb_manure_specification_list[3][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(3, 0))
        self.cb_manure_specification_list[3][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(3, 1))
        self.cb_manure_specification_list[3][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(3, 2))
        self.cb_manure_specification_list[3][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(3, 3))
        self.cb_manure_specification_list[4][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(4, 0))
        self.cb_manure_specification_list[4][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(4, 1))
        self.cb_manure_specification_list[4][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(4, 2))
        self.cb_manure_specification_list[4][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(4, 3))
        self.cb_manure_specification_list[5][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(5, 0))
        self.cb_manure_specification_list[5][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(5, 1))
        self.cb_manure_specification_list[5][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(5, 2))
        self.cb_manure_specification_list[5][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(5, 3))
        self.cb_manure_specification_list[6][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(6, 0))
        self.cb_manure_specification_list[6][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(6, 1))
        self.cb_manure_specification_list[6][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(6, 2))
        self.cb_manure_specification_list[6][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(6, 3))
        self.cb_manure_specification_list[7][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(7, 0))
        self.cb_manure_specification_list[7][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(7, 1))
        self.cb_manure_specification_list[7][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(7, 2))
        self.cb_manure_specification_list[7][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(7, 3))
        self.cb_manure_specification_list[8][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(8, 0))
        self.cb_manure_specification_list[8][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(8, 1))
        self.cb_manure_specification_list[8][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(8, 2))
        self.cb_manure_specification_list[8][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(8, 3))
        self.cb_manure_specification_list[9][0].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(9, 0))
        self.cb_manure_specification_list[9][1].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(9, 1))
        self.cb_manure_specification_list[9][2].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(9, 2))
        self.cb_manure_specification_list[9][3].currentIndexChanged.connect(
            lambda: self.cb_manure_specification_selected(9, 3))

        self.lb_manure_amount_list = [[self.QTUI_rotor.lb_manure_amount_1_1, self.QTUI_rotor.lb_manure_amount_1_2,
                                       self.QTUI_rotor.lb_manure_amount_1_3, self.QTUI_rotor.lb_manure_amount_1_4],
                                      [self.QTUI_rotor.lb_manure_amount_2_1, self.QTUI_rotor.lb_manure_amount_2_2,
                                       self.QTUI_rotor.lb_manure_amount_2_3, self.QTUI_rotor.lb_manure_amount_2_4],
                                      [self.QTUI_rotor.lb_manure_amount_3_1, self.QTUI_rotor.lb_manure_amount_3_2,
                                       self.QTUI_rotor.lb_manure_amount_3_3, self.QTUI_rotor.lb_manure_amount_3_4],
                                      [self.QTUI_rotor.lb_manure_amount_4_1, self.QTUI_rotor.lb_manure_amount_4_2,
                                       self.QTUI_rotor.lb_manure_amount_4_3, self.QTUI_rotor.lb_manure_amount_4_4],
                                      [self.QTUI_rotor.lb_manure_amount_5_1, self.QTUI_rotor.lb_manure_amount_5_2,
                                       self.QTUI_rotor.lb_manure_amount_5_3, self.QTUI_rotor.lb_manure_amount_5_4],
                                      [self.QTUI_rotor.lb_manure_amount_6_1, self.QTUI_rotor.lb_manure_amount_6_2,
                                       self.QTUI_rotor.lb_manure_amount_6_3, self.QTUI_rotor.lb_manure_amount_6_4],
                                      [self.QTUI_rotor.lb_manure_amount_7_1, self.QTUI_rotor.lb_manure_amount_7_2,
                                       self.QTUI_rotor.lb_manure_amount_7_3, self.QTUI_rotor.lb_manure_amount_7_4],
                                      [self.QTUI_rotor.lb_manure_amount_8_1, self.QTUI_rotor.lb_manure_amount_8_2,
                                       self.QTUI_rotor.lb_manure_amount_8_3, self.QTUI_rotor.lb_manure_amount_8_4],
                                      [self.QTUI_rotor.lb_manure_amount_9_1, self.QTUI_rotor.lb_manure_amount_9_2,
                                       self.QTUI_rotor.lb_manure_amount_9_3, self.QTUI_rotor.lb_manure_amount_9_4],
                                      [self.QTUI_rotor.lb_manure_amount_10_1, self.QTUI_rotor.lb_manure_amount_10_2,
                                       self.QTUI_rotor.lb_manure_amount_10_3, self.QTUI_rotor.lb_manure_amount_10_4]]
        # TODO move
        for index in range(0, self.MAX_DURATION):
            for j in range(0, 4):
                self.cb_manure_source_list[index][j].setHidden(True)
                self.cb_manure_specification_list[index][j].setHidden(True)

        self.bt_manure_delete_list = [[self.QTUI_rotor.bt_manure_delete_1_1, self.QTUI_rotor.bt_manure_delete_1_2,
                                       self.QTUI_rotor.bt_manure_delete_1_3, self.QTUI_rotor.bt_manure_delete_1_4],
                                      [self.QTUI_rotor.bt_manure_delete_2_1, self.QTUI_rotor.bt_manure_delete_2_2,
                                       self.QTUI_rotor.bt_manure_delete_2_3, self.QTUI_rotor.bt_manure_delete_2_4],
                                      [self.QTUI_rotor.bt_manure_delete_3_1, self.QTUI_rotor.bt_manure_delete_3_2,
                                       self.QTUI_rotor.bt_manure_delete_3_3, self.QTUI_rotor.bt_manure_delete_3_4],
                                      [self.QTUI_rotor.bt_manure_delete_4_1, self.QTUI_rotor.bt_manure_delete_4_2,
                                       self.QTUI_rotor.bt_manure_delete_4_3, self.QTUI_rotor.bt_manure_delete_4_4],
                                      [self.QTUI_rotor.bt_manure_delete_5_1, self.QTUI_rotor.bt_manure_delete_5_2,
                                       self.QTUI_rotor.bt_manure_delete_5_3, self.QTUI_rotor.bt_manure_delete_5_4],
                                      [self.QTUI_rotor.bt_manure_delete_6_1, self.QTUI_rotor.bt_manure_delete_6_2,
                                       self.QTUI_rotor.bt_manure_delete_6_3, self.QTUI_rotor.bt_manure_delete_6_4],
                                      [self.QTUI_rotor.bt_manure_delete_7_1, self.QTUI_rotor.bt_manure_delete_7_2,
                                       self.QTUI_rotor.bt_manure_delete_7_3, self.QTUI_rotor.bt_manure_delete_7_4],
                                      [self.QTUI_rotor.bt_manure_delete_8_1, self.QTUI_rotor.bt_manure_delete_8_2,
                                       self.QTUI_rotor.bt_manure_delete_8_3, self.QTUI_rotor.bt_manure_delete_8_4],
                                      [self.QTUI_rotor.bt_manure_delete_9_1, self.QTUI_rotor.bt_manure_delete_9_2,
                                       self.QTUI_rotor.bt_manure_delete_9_3, self.QTUI_rotor.bt_manure_delete_9_4],
                                      [self.QTUI_rotor.bt_manure_delete_10_1, self.QTUI_rotor.bt_manure_delete_10_2,
                                       self.QTUI_rotor.bt_manure_delete_10_3, self.QTUI_rotor.bt_manure_delete_10_4]]

        self.sb_manure_amount_list = [[self.QTUI_rotor.sb_manure_amount_1_1, self.QTUI_rotor.sb_manure_amount_1_2,
                                       self.QTUI_rotor.sb_manure_amount_1_3, self.QTUI_rotor.sb_manure_amount_1_4],
                                      [self.QTUI_rotor.sb_manure_amount_2_1, self.QTUI_rotor.sb_manure_amount_2_2,
                                       self.QTUI_rotor.sb_manure_amount_2_3, self.QTUI_rotor.sb_manure_amount_2_4],
                                      [self.QTUI_rotor.sb_manure_amount_3_1, self.QTUI_rotor.sb_manure_amount_3_2,
                                       self.QTUI_rotor.sb_manure_amount_3_3, self.QTUI_rotor.sb_manure_amount_3_4],
                                      [self.QTUI_rotor.sb_manure_amount_4_1, self.QTUI_rotor.sb_manure_amount_4_2,
                                       self.QTUI_rotor.sb_manure_amount_4_3, self.QTUI_rotor.sb_manure_amount_4_4],
                                      [self.QTUI_rotor.sb_manure_amount_5_1, self.QTUI_rotor.sb_manure_amount_5_2,
                                       self.QTUI_rotor.sb_manure_amount_5_3, self.QTUI_rotor.sb_manure_amount_5_4],
                                      [self.QTUI_rotor.sb_manure_amount_6_1, self.QTUI_rotor.sb_manure_amount_6_2,
                                       self.QTUI_rotor.sb_manure_amount_6_3, self.QTUI_rotor.sb_manure_amount_6_4],
                                      [self.QTUI_rotor.sb_manure_amount_7_1, self.QTUI_rotor.sb_manure_amount_7_2,
                                       self.QTUI_rotor.sb_manure_amount_7_3, self.QTUI_rotor.sb_manure_amount_7_4],
                                      [self.QTUI_rotor.sb_manure_amount_8_1, self.QTUI_rotor.sb_manure_amount_8_2,
                                       self.QTUI_rotor.sb_manure_amount_8_3, self.QTUI_rotor.sb_manure_amount_8_4],
                                      [self.QTUI_rotor.sb_manure_amount_9_1, self.QTUI_rotor.sb_manure_amount_9_2,
                                       self.QTUI_rotor.sb_manure_amount_9_3, self.QTUI_rotor.sb_manure_amount_9_4],
                                      [self.QTUI_rotor.sb_manure_amount_10_1, self.QTUI_rotor.sb_manure_amount_10_2,
                                       self.QTUI_rotor.sb_manure_amount_10_3, self.QTUI_rotor.sb_manure_amount_10_4]]

        self.bt_next_manure_list = [[self.QTUI_rotor.bt_next_manure_1_1, self.QTUI_rotor.bt_next_manure_1_2,
                                     self.QTUI_rotor.bt_next_manure_1_3],
                                    [self.QTUI_rotor.bt_next_manure_2_1, self.QTUI_rotor.bt_next_manure_2_2,
                                     self.QTUI_rotor.bt_next_manure_2_3],
                                    [self.QTUI_rotor.bt_next_manure_3_1, self.QTUI_rotor.bt_next_manure_3_2,
                                     self.QTUI_rotor.bt_next_manure_3_3],
                                    [self.QTUI_rotor.bt_next_manure_4_1, self.QTUI_rotor.bt_next_manure_4_2,
                                     self.QTUI_rotor.bt_next_manure_4_3],
                                    [self.QTUI_rotor.bt_next_manure_5_1, self.QTUI_rotor.bt_next_manure_5_2,
                                     self.QTUI_rotor.bt_next_manure_5_3],
                                    [self.QTUI_rotor.bt_next_manure_6_1, self.QTUI_rotor.bt_next_manure_6_2,
                                     self.QTUI_rotor.bt_next_manure_6_3],
                                    [self.QTUI_rotor.bt_next_manure_7_1, self.QTUI_rotor.bt_next_manure_7_2,
                                     self.QTUI_rotor.bt_next_manure_7_3],
                                    [self.QTUI_rotor.bt_next_manure_8_1, self.QTUI_rotor.bt_next_manure_8_2,
                                     self.QTUI_rotor.bt_next_manure_8_3],
                                    [self.QTUI_rotor.bt_next_manure_9_1, self.QTUI_rotor.bt_next_manure_9_2,
                                     self.QTUI_rotor.bt_next_manure_9_3],
                                    [self.QTUI_rotor.bt_next_manure_10_1, self.QTUI_rotor.bt_next_manure_10_2,
                                     self.QTUI_rotor.bt_next_manure_10_3]]

        self.sb_manure_amount_list[0][0].valueChanged.connect(lambda: self.sb_manure_amount(0, 0))
        self.sb_manure_amount_list[1][0].valueChanged.connect(lambda: self.sb_manure_amount(1, 0))
        self.sb_manure_amount_list[2][0].valueChanged.connect(lambda: self.sb_manure_amount(2, 0))
        self.sb_manure_amount_list[3][0].valueChanged.connect(lambda: self.sb_manure_amount(3, 0))
        self.sb_manure_amount_list[4][0].valueChanged.connect(lambda: self.sb_manure_amount(4, 0))
        self.sb_manure_amount_list[5][0].valueChanged.connect(lambda: self.sb_manure_amount(5, 0))
        self.sb_manure_amount_list[6][0].valueChanged.connect(lambda: self.sb_manure_amount(6, 0))
        self.sb_manure_amount_list[7][0].valueChanged.connect(lambda: self.sb_manure_amount(7, 0))
        self.sb_manure_amount_list[8][0].valueChanged.connect(lambda: self.sb_manure_amount(8, 0))
        self.sb_manure_amount_list[9][0].valueChanged.connect(lambda: self.sb_manure_amount(9, 0))
        self.sb_manure_amount_list[0][1].valueChanged.connect(lambda: self.sb_manure_amount(0, 1))
        self.sb_manure_amount_list[1][1].valueChanged.connect(lambda: self.sb_manure_amount(1, 1))
        self.sb_manure_amount_list[2][1].valueChanged.connect(lambda: self.sb_manure_amount(2, 1))
        self.sb_manure_amount_list[3][1].valueChanged.connect(lambda: self.sb_manure_amount(3, 1))
        self.sb_manure_amount_list[4][1].valueChanged.connect(lambda: self.sb_manure_amount(4, 1))
        self.sb_manure_amount_list[5][1].valueChanged.connect(lambda: self.sb_manure_amount(5, 1))
        self.sb_manure_amount_list[6][1].valueChanged.connect(lambda: self.sb_manure_amount(6, 1))
        self.sb_manure_amount_list[7][1].valueChanged.connect(lambda: self.sb_manure_amount(7, 1))
        self.sb_manure_amount_list[8][1].valueChanged.connect(lambda: self.sb_manure_amount(8, 1))
        self.sb_manure_amount_list[9][1].valueChanged.connect(lambda: self.sb_manure_amount(9, 1))
        self.sb_manure_amount_list[0][2].valueChanged.connect(lambda: self.sb_manure_amount(0, 2))
        self.sb_manure_amount_list[1][2].valueChanged.connect(lambda: self.sb_manure_amount(1, 2))
        self.sb_manure_amount_list[2][2].valueChanged.connect(lambda: self.sb_manure_amount(2, 2))
        self.sb_manure_amount_list[3][2].valueChanged.connect(lambda: self.sb_manure_amount(3, 2))
        self.sb_manure_amount_list[4][2].valueChanged.connect(lambda: self.sb_manure_amount(4, 2))
        self.sb_manure_amount_list[5][2].valueChanged.connect(lambda: self.sb_manure_amount(5, 2))
        self.sb_manure_amount_list[6][2].valueChanged.connect(lambda: self.sb_manure_amount(6, 2))
        self.sb_manure_amount_list[7][2].valueChanged.connect(lambda: self.sb_manure_amount(7, 2))
        self.sb_manure_amount_list[8][2].valueChanged.connect(lambda: self.sb_manure_amount(8, 2))
        self.sb_manure_amount_list[9][2].valueChanged.connect(lambda: self.sb_manure_amount(9, 2))
        self.sb_manure_amount_list[0][3].valueChanged.connect(lambda: self.sb_manure_amount(0, 3))
        self.sb_manure_amount_list[1][3].valueChanged.connect(lambda: self.sb_manure_amount(1, 3))
        self.sb_manure_amount_list[2][3].valueChanged.connect(lambda: self.sb_manure_amount(2, 3))
        self.sb_manure_amount_list[3][3].valueChanged.connect(lambda: self.sb_manure_amount(3, 3))
        self.sb_manure_amount_list[4][3].valueChanged.connect(lambda: self.sb_manure_amount(4, 3))
        self.sb_manure_amount_list[5][3].valueChanged.connect(lambda: self.sb_manure_amount(5, 3))
        self.sb_manure_amount_list[6][3].valueChanged.connect(lambda: self.sb_manure_amount(6, 3))
        self.sb_manure_amount_list[7][3].valueChanged.connect(lambda: self.sb_manure_amount(7, 3))
        self.sb_manure_amount_list[8][3].valueChanged.connect(lambda: self.sb_manure_amount(8, 3))
        self.sb_manure_amount_list[9][3].valueChanged.connect(lambda: self.sb_manure_amount(9, 3))

        self.bt_next_manure_list[0][0].clicked.connect(lambda: self.bt_next_manure(0, 0))
        self.bt_next_manure_list[0][1].clicked.connect(lambda: self.bt_next_manure(0, 1))
        self.bt_next_manure_list[0][2].clicked.connect(lambda: self.bt_next_manure(0, 2))

        self.bt_next_manure_list[1][0].clicked.connect(lambda: self.bt_next_manure(1, 0))
        self.bt_next_manure_list[1][1].clicked.connect(lambda: self.bt_next_manure(1, 1))
        self.bt_next_manure_list[1][2].clicked.connect(lambda: self.bt_next_manure(1, 2))

        self.bt_next_manure_list[2][0].clicked.connect(lambda: self.bt_next_manure(2, 0))
        self.bt_next_manure_list[2][1].clicked.connect(lambda: self.bt_next_manure(2, 1))
        self.bt_next_manure_list[2][2].clicked.connect(lambda: self.bt_next_manure(2, 2))

        self.bt_next_manure_list[3][0].clicked.connect(lambda: self.bt_next_manure(3, 0))
        self.bt_next_manure_list[3][1].clicked.connect(lambda: self.bt_next_manure(3, 1))
        self.bt_next_manure_list[3][2].clicked.connect(lambda: self.bt_next_manure(3, 2))

        self.bt_next_manure_list[4][0].clicked.connect(lambda: self.bt_next_manure(4, 0))
        self.bt_next_manure_list[4][1].clicked.connect(lambda: self.bt_next_manure(4, 1))
        self.bt_next_manure_list[4][2].clicked.connect(lambda: self.bt_next_manure(4, 2))

        self.bt_next_manure_list[5][0].clicked.connect(lambda: self.bt_next_manure(5, 0))
        self.bt_next_manure_list[5][1].clicked.connect(lambda: self.bt_next_manure(5, 1))
        self.bt_next_manure_list[5][2].clicked.connect(lambda: self.bt_next_manure(5, 2))

        self.bt_next_manure_list[6][0].clicked.connect(lambda: self.bt_next_manure(6, 0))
        self.bt_next_manure_list[6][1].clicked.connect(lambda: self.bt_next_manure(6, 1))
        self.bt_next_manure_list[6][2].clicked.connect(lambda: self.bt_next_manure(6, 2))

        self.bt_next_manure_list[7][0].clicked.connect(lambda: self.bt_next_manure(7, 0))
        self.bt_next_manure_list[7][1].clicked.connect(lambda: self.bt_next_manure(7, 1))
        self.bt_next_manure_list[7][2].clicked.connect(lambda: self.bt_next_manure(7, 2))

        self.bt_next_manure_list[8][0].clicked.connect(lambda: self.bt_next_manure(8, 0))
        self.bt_next_manure_list[8][1].clicked.connect(lambda: self.bt_next_manure(8, 1))
        self.bt_next_manure_list[8][2].clicked.connect(lambda: self.bt_next_manure(8, 2))

        self.bt_next_manure_list[9][0].clicked.connect(lambda: self.bt_next_manure(9, 0))
        self.bt_next_manure_list[9][1].clicked.connect(lambda: self.bt_next_manure(9, 1))
        self.bt_next_manure_list[9][2].clicked.connect(lambda: self.bt_next_manure(9, 2))

        self.bt_manure_delete_list[0][0].clicked.connect(lambda: self.bt_manure_delete(0, 0))
        self.bt_manure_delete_list[0][1].clicked.connect(lambda: self.bt_manure_delete(0, 1))
        self.bt_manure_delete_list[0][2].clicked.connect(lambda: self.bt_manure_delete(0, 2))
        self.bt_manure_delete_list[0][3].clicked.connect(lambda: self.bt_manure_delete(0, 3))

        self.bt_manure_delete_list[1][0].clicked.connect(lambda: self.bt_manure_delete(1, 0))
        self.bt_manure_delete_list[1][1].clicked.connect(lambda: self.bt_manure_delete(1, 1))
        self.bt_manure_delete_list[1][2].clicked.connect(lambda: self.bt_manure_delete(1, 2))
        self.bt_manure_delete_list[1][3].clicked.connect(lambda: self.bt_manure_delete(1, 3))

        self.bt_manure_delete_list[2][0].clicked.connect(lambda: self.bt_manure_delete(2, 0))
        self.bt_manure_delete_list[2][1].clicked.connect(lambda: self.bt_manure_delete(2, 1))
        self.bt_manure_delete_list[2][2].clicked.connect(lambda: self.bt_manure_delete(2, 2))
        self.bt_manure_delete_list[2][3].clicked.connect(lambda: self.bt_manure_delete(2, 3))

        self.bt_manure_delete_list[3][0].clicked.connect(lambda: self.bt_manure_delete(3, 0))
        self.bt_manure_delete_list[3][1].clicked.connect(lambda: self.bt_manure_delete(3, 1))
        self.bt_manure_delete_list[3][2].clicked.connect(lambda: self.bt_manure_delete(3, 2))
        self.bt_manure_delete_list[3][3].clicked.connect(lambda: self.bt_manure_delete(3, 3))

        self.bt_manure_delete_list[4][0].clicked.connect(lambda: self.bt_manure_delete(4, 0))
        self.bt_manure_delete_list[4][1].clicked.connect(lambda: self.bt_manure_delete(4, 1))
        self.bt_manure_delete_list[4][2].clicked.connect(lambda: self.bt_manure_delete(4, 2))
        self.bt_manure_delete_list[4][3].clicked.connect(lambda: self.bt_manure_delete(4, 3))

        self.bt_manure_delete_list[5][0].clicked.connect(lambda: self.bt_manure_delete(5, 0))
        self.bt_manure_delete_list[5][1].clicked.connect(lambda: self.bt_manure_delete(5, 1))
        self.bt_manure_delete_list[5][2].clicked.connect(lambda: self.bt_manure_delete(5, 2))
        self.bt_manure_delete_list[5][3].clicked.connect(lambda: self.bt_manure_delete(5, 3))

        self.bt_manure_delete_list[6][0].clicked.connect(lambda: self.bt_manure_delete(6, 0))
        self.bt_manure_delete_list[6][1].clicked.connect(lambda: self.bt_manure_delete(6, 1))
        self.bt_manure_delete_list[6][2].clicked.connect(lambda: self.bt_manure_delete(6, 2))
        self.bt_manure_delete_list[6][3].clicked.connect(lambda: self.bt_manure_delete(6, 3))

        self.bt_manure_delete_list[7][0].clicked.connect(lambda: self.bt_manure_delete(7, 0))
        self.bt_manure_delete_list[7][1].clicked.connect(lambda: self.bt_manure_delete(7, 1))
        self.bt_manure_delete_list[7][2].clicked.connect(lambda: self.bt_manure_delete(7, 2))
        self.bt_manure_delete_list[7][3].clicked.connect(lambda: self.bt_manure_delete(7, 3))

        self.bt_manure_delete_list[8][0].clicked.connect(lambda: self.bt_manure_delete(8, 0))
        self.bt_manure_delete_list[8][1].clicked.connect(lambda: self.bt_manure_delete(8, 1))
        self.bt_manure_delete_list[8][2].clicked.connect(lambda: self.bt_manure_delete(8, 2))
        self.bt_manure_delete_list[8][3].clicked.connect(lambda: self.bt_manure_delete(8, 3))

        self.bt_manure_delete_list[9][0].clicked.connect(lambda: self.bt_manure_delete(9, 0))
        self.bt_manure_delete_list[9][1].clicked.connect(lambda: self.bt_manure_delete(9, 1))
        self.bt_manure_delete_list[9][2].clicked.connect(lambda: self.bt_manure_delete(9, 2))
        self.bt_manure_delete_list[9][3].clicked.connect(lambda: self.bt_manure_delete(9, 3))

        # -------------strawharvest is toggled
        self.QTUI_rotor.xb_strawharvest_year_1.toggled.connect(lambda: self.xb_strawharvest_toggled(0))
        self.QTUI_rotor.xb_strawharvest_year_2.toggled.connect(lambda: self.xb_strawharvest_toggled(1))
        self.QTUI_rotor.xb_strawharvest_year_3.toggled.connect(lambda: self.xb_strawharvest_toggled(2))
        self.QTUI_rotor.xb_strawharvest_year_4.toggled.connect(lambda: self.xb_strawharvest_toggled(3))
        self.QTUI_rotor.xb_strawharvest_year_5.toggled.connect(lambda: self.xb_strawharvest_toggled(4))
        self.QTUI_rotor.xb_strawharvest_year_6.toggled.connect(lambda: self.xb_strawharvest_toggled(5))
        self.QTUI_rotor.xb_strawharvest_year_7.toggled.connect(lambda: self.xb_strawharvest_toggled(6))
        self.QTUI_rotor.xb_strawharvest_year_8.toggled.connect(lambda: self.xb_strawharvest_toggled(7))
        self.QTUI_rotor.xb_strawharvest_year_9.toggled.connect(lambda: self.xb_strawharvest_toggled(8))
        self.QTUI_rotor.xb_strawharvest_year_10.toggled.connect(lambda: self.xb_strawharvest_toggled(9))

        # -------------List of all strawharvest checkboxes
        self.xb_strawharvest_list = [self.QTUI_rotor.xb_strawharvest_year_1,
                                     self.QTUI_rotor.xb_strawharvest_year_2,
                                     self.QTUI_rotor.xb_strawharvest_year_3,
                                     self.QTUI_rotor.xb_strawharvest_year_4,
                                     self.QTUI_rotor.xb_strawharvest_year_5,
                                     self.QTUI_rotor.xb_strawharvest_year_6,
                                     self.QTUI_rotor.xb_strawharvest_year_7,
                                     self.QTUI_rotor.xb_strawharvest_year_8,
                                     self.QTUI_rotor.xb_strawharvest_year_9,
                                     self.QTUI_rotor.xb_strawharvest_year_10]
        # -------------List of all undersowing checkboxes
        self.xb_undersowing_list = [self.QTUI_rotor.xb_undersowing_year_1,
                                    self.QTUI_rotor.xb_undersowing_year_2,
                                    self.QTUI_rotor.xb_undersowing_year_3,
                                    self.QTUI_rotor.xb_undersowing_year_4,
                                    self.QTUI_rotor.xb_undersowing_year_5,
                                    self.QTUI_rotor.xb_undersowing_year_6,
                                    self.QTUI_rotor.xb_undersowing_year_7,
                                    self.QTUI_rotor.xb_undersowing_year_8,
                                    self.QTUI_rotor.xb_undersowing_year_9,
                                    self.QTUI_rotor.xb_undersowing_year_10]

        # -------------List of all undersowing groupboxes
        self.gb_undersowing_list = [self.QTUI_rotor.gb_undersowing_year_1,
                                    self.QTUI_rotor.gb_undersowing_year_2,
                                    self.QTUI_rotor.gb_undersowing_year_3,
                                    self.QTUI_rotor.gb_undersowing_year_4,
                                    self.QTUI_rotor.gb_undersowing_year_5,
                                    self.QTUI_rotor.gb_undersowing_year_6,
                                    self.QTUI_rotor.gb_undersowing_year_7,
                                    self.QTUI_rotor.gb_undersowing_year_8,
                                    self.QTUI_rotor.gb_undersowing_year_9,
                                    self.QTUI_rotor.gb_undersowing_year_10]



        # -------------List of all undersowing legume ratio spinboxes
        self.sb_undersowing_legume_ratio_list = [self.QTUI_rotor.cb_undersowing_legume_ratio_year_1,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_2,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_3,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_4,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_5,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_6,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_7,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_8,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_9,
                                                 self.QTUI_rotor.cb_undersowing_legume_ratio_year_10]

        # -------------List of all covercrop checkboxes
        self.xb_covercrop_list = [self.QTUI_rotor.xb_covercrop_year_1,
                                  self.QTUI_rotor.xb_covercrop_year_2,
                                  self.QTUI_rotor.xb_covercrop_year_3,
                                  self.QTUI_rotor.xb_covercrop_year_4,
                                  self.QTUI_rotor.xb_covercrop_year_5,
                                  self.QTUI_rotor.xb_covercrop_year_6,
                                  self.QTUI_rotor.xb_covercrop_year_7,
                                  self.QTUI_rotor.xb_covercrop_year_8,
                                  self.QTUI_rotor.xb_covercrop_year_9,
                                  self.QTUI_rotor.xb_covercrop_year_10]

        # -------------List of all covercrop groupboxes
        self.gb_covercrop_list = [self.QTUI_rotor.gb_covercrop_year_1,
                                  self.QTUI_rotor.gb_covercrop_year_2,
                                  self.QTUI_rotor.gb_covercrop_year_3,
                                  self.QTUI_rotor.gb_covercrop_year_4,
                                  self.QTUI_rotor.gb_covercrop_year_5,
                                  self.QTUI_rotor.gb_covercrop_year_6,
                                  self.QTUI_rotor.gb_covercrop_year_7,
                                  self.QTUI_rotor.gb_covercrop_year_8,
                                  self.QTUI_rotor.gb_covercrop_year_9,
                                  self.QTUI_rotor.gb_covercrop_year_10]

        # -------------List of all covercrop winterhardiness comboboxes
        self.cb_covercrop_winterhardiness_list = [self.QTUI_rotor.cb_covercrop_winterhardiness_year_1,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_2,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_3,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_4,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_5,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_6,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_7,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_8,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_9,
                                                  self.QTUI_rotor.cb_covercrop_winterhardiness_year_10]

        # -------------List of all covercrop legume ratio spinboxes
        self.sb_covercrop_legume_ratio_list = [self.QTUI_rotor.sb_covercrop_legume_ratio_year_1,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_2,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_3,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_4,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_5,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_6,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_7,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_8,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_9,
                                               self.QTUI_rotor.sb_covercrop_legume_ratio_year_10]

        # -------------List of all covercrop harvest checkboxes
        self.xb_covercrop_harvest_list = [self.QTUI_rotor.xb_covercrop_harvest_year_1,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_2,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_3,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_4,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_5,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_6,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_7,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_8,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_9,
                                          self.QTUI_rotor.xb_covercrop_harvest_year_10]

        self.cb_tillage_list = [self.QTUI_rotor.cb_tillage_1, self.QTUI_rotor.cb_tillage_2,
                                self.QTUI_rotor.cb_tillage_3, self.QTUI_rotor.cb_tillage_4,
                                self.QTUI_rotor.cb_tillage_5, self.QTUI_rotor.cb_tillage_6,
                                self.QTUI_rotor.cb_tillage_7, self.QTUI_rotor.cb_tillage_8,
                                self.QTUI_rotor.cb_tillage_9, self.QTUI_rotor.cb_tillage_10]

        # _________________----------CONNECT----------------__________________________
        # this is connencted to the assessment method so the assessment methods do not interfere with each other
        # crop_year selections
        # if self.project.dict_tab_general["assessment_method"] in (1, 3):
        self.cb_crop_year_list[0].currentIndexChanged.connect(lambda: self.cb_crop_selected(0))
        self.cb_crop_year_list[1].currentIndexChanged.connect(lambda: self.cb_crop_selected(1))
        self.cb_crop_year_list[2].currentIndexChanged.connect(lambda: self.cb_crop_selected(2))
        self.cb_crop_year_list[3].currentIndexChanged.connect(lambda: self.cb_crop_selected(3))
        self.cb_crop_year_list[4].currentIndexChanged.connect(lambda: self.cb_crop_selected(4))
        self.cb_crop_year_list[5].currentIndexChanged.connect(lambda: self.cb_crop_selected(5))
        self.cb_crop_year_list[6].currentIndexChanged.connect(lambda: self.cb_crop_selected(6))
        self.cb_crop_year_list[7].currentIndexChanged.connect(lambda: self.cb_crop_selected(7))
        self.cb_crop_year_list[8].currentIndexChanged.connect(lambda: self.cb_crop_selected(8))
        self.cb_crop_year_list[9].currentIndexChanged.connect(lambda: self.cb_crop_selected(9))
        # self.qtui_rotor.cb_generate_crop_1.currentIndexChanged

        # ----------early tillage ----------------------------
        self.xb_early_tillage_list[0].toggled.connect(lambda: self.xb_early_tillage_toggled(0))
        self.xb_early_tillage_list[1].toggled.connect(lambda: self.xb_early_tillage_toggled(1))
        self.xb_early_tillage_list[2].toggled.connect(lambda: self.xb_early_tillage_toggled(2))
        self.xb_early_tillage_list[3].toggled.connect(lambda: self.xb_early_tillage_toggled(3))
        self.xb_early_tillage_list[4].toggled.connect(lambda: self.xb_early_tillage_toggled(4))
        self.xb_early_tillage_list[5].toggled.connect(lambda: self.xb_early_tillage_toggled(5))
        self.xb_early_tillage_list[6].toggled.connect(lambda: self.xb_early_tillage_toggled(6))
        self.xb_early_tillage_list[7].toggled.connect(lambda: self.xb_early_tillage_toggled(7))
        self.xb_early_tillage_list[8].toggled.connect(lambda: self.xb_early_tillage_toggled(8))
        self.xb_early_tillage_list[9].toggled.connect(lambda: self.xb_early_tillage_toggled(9))

        # ----------undersowing and covercrop-------------------

        self.xb_undersowing_list[0].toggled.connect(lambda: self.xb_undersowing_toggled(0))
        self.xb_undersowing_list[1].toggled.connect(lambda: self.xb_undersowing_toggled(1))
        self.xb_undersowing_list[2].toggled.connect(lambda: self.xb_undersowing_toggled(2))
        self.xb_undersowing_list[3].toggled.connect(lambda: self.xb_undersowing_toggled(3))
        self.xb_undersowing_list[4].toggled.connect(lambda: self.xb_undersowing_toggled(4))
        self.xb_undersowing_list[5].toggled.connect(lambda: self.xb_undersowing_toggled(5))
        self.xb_undersowing_list[6].toggled.connect(lambda: self.xb_undersowing_toggled(6))
        self.xb_undersowing_list[7].toggled.connect(lambda: self.xb_undersowing_toggled(7))
        self.xb_undersowing_list[8].toggled.connect(lambda: self.xb_undersowing_toggled(8))
        self.xb_undersowing_list[9].toggled.connect(lambda: self.xb_undersowing_toggled(9))


        self.sb_undersowing_legume_ratio_list[0].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(0))
        self.sb_undersowing_legume_ratio_list[1].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(1))
        self.sb_undersowing_legume_ratio_list[2].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(2))
        self.sb_undersowing_legume_ratio_list[3].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(3))
        self.sb_undersowing_legume_ratio_list[4].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(4))
        self.sb_undersowing_legume_ratio_list[5].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(5))
        self.sb_undersowing_legume_ratio_list[6].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(6))
        self.sb_undersowing_legume_ratio_list[7].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(7))
        self.sb_undersowing_legume_ratio_list[8].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(8))
        self.sb_undersowing_legume_ratio_list[9].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(9))

        self.cb_covercrop_winterhardiness_list[0].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(0))
        self.cb_covercrop_winterhardiness_list[1].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(1))
        self.cb_covercrop_winterhardiness_list[2].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(2))
        self.cb_covercrop_winterhardiness_list[3].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(3))
        self.cb_covercrop_winterhardiness_list[4].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(4))
        self.cb_covercrop_winterhardiness_list[5].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(5))
        self.cb_covercrop_winterhardiness_list[6].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(6))
        self.cb_covercrop_winterhardiness_list[7].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(7))
        self.cb_covercrop_winterhardiness_list[8].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(8))
        self.cb_covercrop_winterhardiness_list[9].currentIndexChanged.connect(
            lambda: self.sync_gb_undersowing_gb_covercrop(9))

        self.sb_covercrop_legume_ratio_list[0].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(0))
        self.sb_covercrop_legume_ratio_list[1].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(1))
        self.sb_covercrop_legume_ratio_list[2].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(2))
        self.sb_covercrop_legume_ratio_list[3].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(3))
        self.sb_covercrop_legume_ratio_list[4].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(4))
        self.sb_covercrop_legume_ratio_list[5].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(5))
        self.sb_covercrop_legume_ratio_list[6].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(6))
        self.sb_covercrop_legume_ratio_list[7].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(7))
        self.sb_covercrop_legume_ratio_list[8].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(8))
        self.sb_covercrop_legume_ratio_list[9].valueChanged.connect(lambda: self.sync_gb_undersowing_gb_covercrop(9))

        self.xb_covercrop_list[0].toggled.connect(lambda: self.xb_covercrop_toggled(0))
        self.xb_covercrop_list[1].toggled.connect(lambda: self.xb_covercrop_toggled(1))
        self.xb_covercrop_list[2].toggled.connect(lambda: self.xb_covercrop_toggled(2))
        self.xb_covercrop_list[3].toggled.connect(lambda: self.xb_covercrop_toggled(3))
        self.xb_covercrop_list[4].toggled.connect(lambda: self.xb_covercrop_toggled(4))
        self.xb_covercrop_list[5].toggled.connect(lambda: self.xb_covercrop_toggled(5))
        self.xb_covercrop_list[6].toggled.connect(lambda: self.xb_covercrop_toggled(6))
        self.xb_covercrop_list[7].toggled.connect(lambda: self.xb_covercrop_toggled(7))
        self.xb_covercrop_list[8].toggled.connect(lambda: self.xb_covercrop_toggled(8))
        self.xb_covercrop_list[9].toggled.connect(lambda: self.xb_covercrop_toggled(9))

        # ---------------TAB 3: FREE GENERAION--------------------------------------------------------------

        self.cb_free_generation_crop_list = [self.QTUI_rotor.cb_generate_crop_1, self.QTUI_rotor.cb_generate_crop_2,
                                             self.QTUI_rotor.cb_generate_crop_3, self.QTUI_rotor.cb_generate_crop_4,
                                             self.QTUI_rotor.cb_generate_crop_5, self.QTUI_rotor.cb_generate_crop_6,
                                             self.QTUI_rotor.cb_generate_crop_7, self.QTUI_rotor.cb_generate_crop_8,
                                             self.QTUI_rotor.cb_generate_crop_9, self.QTUI_rotor.cb_generate_crop_10]

        self.cb_free_generation_crop_type_list = [self.QTUI_rotor.cb_generate_crop_type_1,
                                                  self.QTUI_rotor.cb_generate_crop_type_2,
                                                  self.QTUI_rotor.cb_generate_crop_type_3,
                                                  self.QTUI_rotor.cb_generate_crop_type_4,
                                                  self.QTUI_rotor.cb_generate_crop_type_5,
                                                  self.QTUI_rotor.cb_generate_crop_type_6,
                                                  self.QTUI_rotor.cb_generate_crop_type_7,
                                                  self.QTUI_rotor.cb_generate_crop_type_8,
                                                  self.QTUI_rotor.cb_generate_crop_type_9,
                                                  self.QTUI_rotor.cb_generate_crop_type_10]

        self.qw_free_generation_crop_list = [self.QTUI_rotor.qw_generate_crop_1, self.QTUI_rotor.qw_generate_crop_2,
                                             self.QTUI_rotor.qw_generate_crop_3, self.QTUI_rotor.qw_generate_crop_4,
                                             self.QTUI_rotor.qw_generate_crop_5, self.QTUI_rotor.qw_generate_crop_6,
                                             self.QTUI_rotor.qw_generate_crop_7, self.QTUI_rotor.qw_generate_crop_8,
                                             self.QTUI_rotor.qw_generate_crop_9, self.QTUI_rotor.qw_generate_crop_10]

        # __________________--------TAB FREE GENERATION CONNECT-----_____________________________________
        self.QTUI_rotor.gb_livestock_farming.toggled.connect(self.set_manure_tab_visible)

        self.cb_free_generation_crop_list[0].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(0))
        self.cb_free_generation_crop_list[1].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(1))
        self.cb_free_generation_crop_list[2].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(2))
        self.cb_free_generation_crop_list[3].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(3))
        self.cb_free_generation_crop_list[4].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(4))
        self.cb_free_generation_crop_list[5].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(5))
        self.cb_free_generation_crop_list[6].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(6))
        self.cb_free_generation_crop_list[7].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(7))
        self.cb_free_generation_crop_list[8].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(8))
        self.cb_free_generation_crop_list[9].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_selected(9))

        self.cb_free_generation_crop_type_list[0].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(0))
        self.cb_free_generation_crop_type_list[1].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(1))
        self.cb_free_generation_crop_type_list[2].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(2))
        self.cb_free_generation_crop_type_list[3].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(3))
        self.cb_free_generation_crop_type_list[4].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(4))
        self.cb_free_generation_crop_type_list[5].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(5))
        self.cb_free_generation_crop_type_list[6].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(6))
        self.cb_free_generation_crop_type_list[7].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(7))
        self.cb_free_generation_crop_type_list[8].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(8))
        self.cb_free_generation_crop_type_list[9].currentIndexChanged.connect(
            lambda: self.cb_free_generation_crop_type_selected(9))

        # -------------------------TAB 3: MANURE--------------------------------------------------------------

        self.lb_manure_list = [self.QTUI_rotor.lb_manure_1, self.QTUI_rotor.lb_manure_2, self.QTUI_rotor.lb_manure_3,
                               self.QTUI_rotor.lb_manure_4, self.QTUI_rotor.lb_manure_5, self.QTUI_rotor.lb_manure_6,
                               self.QTUI_rotor.lb_manure_7, self.QTUI_rotor.lb_manure_8, self.QTUI_rotor.lb_manure_9,
                               self.QTUI_rotor.lb_manure_10, self.QTUI_rotor.lb_manure_11, self.QTUI_rotor.lb_manure_12,
                               self.QTUI_rotor.lb_manure_13, self.QTUI_rotor.lb_manure_14, self.QTUI_rotor.lb_manure_15,
                               self.QTUI_rotor.lb_manure_16, self.QTUI_rotor.lb_manure_17, self.QTUI_rotor.lb_manure_18,
                               self.QTUI_rotor.lb_manure_19, self.QTUI_rotor.lb_manure_20]

        self.bt_manure_default_list = [self.QTUI_rotor.bt_manure_default_values_1,
                                       self.QTUI_rotor.bt_manure_default_values_2,
                                       self.QTUI_rotor.bt_manure_default_values_3,
                                       self.QTUI_rotor.bt_manure_default_values_4,
                                       self.QTUI_rotor.bt_manure_default_values_5,
                                       self.QTUI_rotor.bt_manure_default_values_6,
                                       self.QTUI_rotor.bt_manure_default_values_7,
                                       self.QTUI_rotor.bt_manure_default_values_8,
                                       self.QTUI_rotor.bt_manure_default_values_9,
                                       self.QTUI_rotor.bt_manure_default_values_10,
                                       self.QTUI_rotor.bt_manure_default_values_11,
                                       self.QTUI_rotor.bt_manure_default_values_12,
                                       self.QTUI_rotor.bt_manure_default_values_13,
                                       self.QTUI_rotor.bt_manure_default_values_14,
                                       self.QTUI_rotor.bt_manure_default_values_15,
                                       self.QTUI_rotor.bt_manure_default_values_16,
                                       self.QTUI_rotor.bt_manure_default_values_17,
                                       self.QTUI_rotor.bt_manure_default_values_18,
                                       self.QTUI_rotor.bt_manure_default_values_19,
                                       self.QTUI_rotor.bt_manure_default_values_20]

        self.sb_manure_dry_mass_list = [self.QTUI_rotor.sb_manure_dry_mass_1, self.QTUI_rotor.sb_manure_dry_mass_2,
                                        self.QTUI_rotor.sb_manure_dry_mass_3, self.QTUI_rotor.sb_manure_dry_mass_4,
                                        self.QTUI_rotor.sb_manure_dry_mass_5, self.QTUI_rotor.sb_manure_dry_mass_6,
                                        self.QTUI_rotor.sb_manure_dry_mass_7, self.QTUI_rotor.sb_manure_dry_mass_8,
                                        self.QTUI_rotor.sb_manure_dry_mass_9, self.QTUI_rotor.sb_manure_dry_mass_10,
                                        self.QTUI_rotor.sb_manure_dry_mass_11, self.QTUI_rotor.sb_manure_dry_mass_12,
                                        self.QTUI_rotor.sb_manure_dry_mass_13, self.QTUI_rotor.sb_manure_dry_mass_14,
                                        self.QTUI_rotor.sb_manure_dry_mass_15, self.QTUI_rotor.sb_manure_dry_mass_16,
                                        self.QTUI_rotor.sb_manure_dry_mass_17, self.QTUI_rotor.sb_manure_dry_mass_18,
                                        self.QTUI_rotor.sb_manure_dry_mass_19, self.QTUI_rotor.sb_manure_dry_mass_20]

        self.sb_manure_n_content_list = [self.QTUI_rotor.sb_manure_n_content_1, self.QTUI_rotor.sb_manure_n_content_2,
                                         self.QTUI_rotor.sb_manure_n_content_3, self.QTUI_rotor.sb_manure_n_content_4,
                                         self.QTUI_rotor.sb_manure_n_content_5, self.QTUI_rotor.sb_manure_n_content_6,
                                         self.QTUI_rotor.sb_manure_n_content_7, self.QTUI_rotor.sb_manure_n_content_8,
                                         self.QTUI_rotor.sb_manure_n_content_9, self.QTUI_rotor.sb_manure_n_content_10,
                                         self.QTUI_rotor.sb_manure_n_content_11, self.QTUI_rotor.sb_manure_n_content_12,
                                         self.QTUI_rotor.sb_manure_n_content_13, self.QTUI_rotor.sb_manure_n_content_14,
                                         self.QTUI_rotor.sb_manure_n_content_15, self.QTUI_rotor.sb_manure_n_content_16,
                                         self.QTUI_rotor.sb_manure_n_content_17, self.QTUI_rotor.sb_manure_n_content_18,
                                         self.QTUI_rotor.sb_manure_n_content_19, self.QTUI_rotor.sb_manure_n_content_20]

        self.sb_manure_n_availability_list = [self.QTUI_rotor.sb_manure_n_availability_percent_1,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_2,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_3,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_4,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_5,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_6,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_7,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_8,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_9,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_10,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_11,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_12,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_13,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_14,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_15,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_16,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_17,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_18,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_19,
                                              self.QTUI_rotor.sb_manure_n_availability_percent_20]

        self.sb_manure_n_loss_list = [self.QTUI_rotor.sb_manure_n_loss_percent_1,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_2,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_3,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_4,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_5,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_6,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_7,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_8,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_9,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_10,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_11,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_12,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_13,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_14,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_15,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_16,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_17,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_18,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_19,
                                      self.QTUI_rotor.sb_manure_n_loss_percent_20]

        self.sb_manure_p_content_list = [self.QTUI_rotor.sb_manure_p_content_1, self.QTUI_rotor.sb_manure_p_content_2,
                                         self.QTUI_rotor.sb_manure_p_content_3, self.QTUI_rotor.sb_manure_p_content_4,
                                         self.QTUI_rotor.sb_manure_p_content_5, self.QTUI_rotor.sb_manure_p_content_6,
                                         self.QTUI_rotor.sb_manure_p_content_7, self.QTUI_rotor.sb_manure_p_content_8,
                                         self.QTUI_rotor.sb_manure_p_content_9, self.QTUI_rotor.sb_manure_p_content_10,
                                         self.QTUI_rotor.sb_manure_p_content_11, self.QTUI_rotor.sb_manure_p_content_12,
                                         self.QTUI_rotor.sb_manure_p_content_13, self.QTUI_rotor.sb_manure_p_content_14,
                                         self.QTUI_rotor.sb_manure_p_content_15, self.QTUI_rotor.sb_manure_p_content_16,
                                         self.QTUI_rotor.sb_manure_p_content_17, self.QTUI_rotor.sb_manure_p_content_18,
                                         self.QTUI_rotor.sb_manure_p_content_19, self.QTUI_rotor.sb_manure_p_content_20]

        self.sb_manure_k_content_list = [self.QTUI_rotor.sb_manure_k_content_1, self.QTUI_rotor.sb_manure_k_content_2,
                                         self.QTUI_rotor.sb_manure_k_content_3, self.QTUI_rotor.sb_manure_k_content_4,
                                         self.QTUI_rotor.sb_manure_k_content_5, self.QTUI_rotor.sb_manure_k_content_6,
                                         self.QTUI_rotor.sb_manure_k_content_7, self.QTUI_rotor.sb_manure_k_content_8,
                                         self.QTUI_rotor.sb_manure_k_content_9, self.QTUI_rotor.sb_manure_k_content_10,
                                         self.QTUI_rotor.sb_manure_k_content_11, self.QTUI_rotor.sb_manure_k_content_12,
                                         self.QTUI_rotor.sb_manure_k_content_13, self.QTUI_rotor.sb_manure_k_content_14,
                                         self.QTUI_rotor.sb_manure_k_content_15, self.QTUI_rotor.sb_manure_k_content_16,
                                         self.QTUI_rotor.sb_manure_k_content_17, self.QTUI_rotor.sb_manure_k_content_18,
                                         self.QTUI_rotor.sb_manure_k_content_19, self.QTUI_rotor.sb_manure_k_content_20]

        # -----------------------TAB 3 SLOTS ------------------------------------------------------------------

        self.bt_manure_default_list[0].clicked.connect(lambda: self.bt_manure_default_clicked(0))
        self.bt_manure_default_list[1].clicked.connect(lambda: self.bt_manure_default_clicked(1))
        self.bt_manure_default_list[2].clicked.connect(lambda: self.bt_manure_default_clicked(2))
        self.bt_manure_default_list[3].clicked.connect(lambda: self.bt_manure_default_clicked(3))
        self.bt_manure_default_list[4].clicked.connect(lambda: self.bt_manure_default_clicked(4))
        self.bt_manure_default_list[5].clicked.connect(lambda: self.bt_manure_default_clicked(5))
        self.bt_manure_default_list[6].clicked.connect(lambda: self.bt_manure_default_clicked(6))
        self.bt_manure_default_list[7].clicked.connect(lambda: self.bt_manure_default_clicked(7))
        self.bt_manure_default_list[8].clicked.connect(lambda: self.bt_manure_default_clicked(8))
        self.bt_manure_default_list[9].clicked.connect(lambda: self.bt_manure_default_clicked(9))
        self.bt_manure_default_list[10].clicked.connect(lambda: self.bt_manure_default_clicked(10))
        self.bt_manure_default_list[11].clicked.connect(lambda: self.bt_manure_default_clicked(11))
        self.bt_manure_default_list[12].clicked.connect(lambda: self.bt_manure_default_clicked(12))
        self.bt_manure_default_list[13].clicked.connect(lambda: self.bt_manure_default_clicked(13))
        self.bt_manure_default_list[14].clicked.connect(lambda: self.bt_manure_default_clicked(14))
        self.bt_manure_default_list[15].clicked.connect(lambda: self.bt_manure_default_clicked(15))
        self.bt_manure_default_list[16].clicked.connect(lambda: self.bt_manure_default_clicked(16))
        self.bt_manure_default_list[17].clicked.connect(lambda: self.bt_manure_default_clicked(17))
        self.bt_manure_default_list[18].clicked.connect(lambda: self.bt_manure_default_clicked(18))
        self.bt_manure_default_list[19].clicked.connect(lambda: self.bt_manure_default_clicked(19))

        self.sb_manure_dry_mass_list[0].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(0))
        self.sb_manure_dry_mass_list[1].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(1))
        self.sb_manure_dry_mass_list[2].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(2))
        self.sb_manure_dry_mass_list[3].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(3))
        self.sb_manure_dry_mass_list[4].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(4))
        self.sb_manure_dry_mass_list[5].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(5))
        self.sb_manure_dry_mass_list[6].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(6))
        self.sb_manure_dry_mass_list[7].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(7))
        self.sb_manure_dry_mass_list[8].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(8))
        self.sb_manure_dry_mass_list[9].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(9))
        self.sb_manure_dry_mass_list[10].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(10))
        self.sb_manure_dry_mass_list[11].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(11))
        self.sb_manure_dry_mass_list[12].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(12))
        self.sb_manure_dry_mass_list[13].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(13))
        self.sb_manure_dry_mass_list[14].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(14))
        self.sb_manure_dry_mass_list[15].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(15))
        self.sb_manure_dry_mass_list[16].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(16))
        self.sb_manure_dry_mass_list[17].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(17))
        self.sb_manure_dry_mass_list[18].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(18))
        self.sb_manure_dry_mass_list[19].valueChanged.connect(lambda: self.sb_manure_dry_mass_changed(19))

        self.sb_manure_n_content_list[0].valueChanged.connect(lambda: self.sb_manure_n_content_changed(0))
        self.sb_manure_n_content_list[1].valueChanged.connect(lambda: self.sb_manure_n_content_changed(1))
        self.sb_manure_n_content_list[2].valueChanged.connect(lambda: self.sb_manure_n_content_changed(2))
        self.sb_manure_n_content_list[3].valueChanged.connect(lambda: self.sb_manure_n_content_changed(3))
        self.sb_manure_n_content_list[4].valueChanged.connect(lambda: self.sb_manure_n_content_changed(4))
        self.sb_manure_n_content_list[5].valueChanged.connect(lambda: self.sb_manure_n_content_changed(5))
        self.sb_manure_n_content_list[6].valueChanged.connect(lambda: self.sb_manure_n_content_changed(6))
        self.sb_manure_n_content_list[7].valueChanged.connect(lambda: self.sb_manure_n_content_changed(7))
        self.sb_manure_n_content_list[8].valueChanged.connect(lambda: self.sb_manure_n_content_changed(8))
        self.sb_manure_n_content_list[9].valueChanged.connect(lambda: self.sb_manure_n_content_changed(9))
        self.sb_manure_n_content_list[10].valueChanged.connect(lambda: self.sb_manure_n_content_changed(10))
        self.sb_manure_n_content_list[11].valueChanged.connect(lambda: self.sb_manure_n_content_changed(11))
        self.sb_manure_n_content_list[12].valueChanged.connect(lambda: self.sb_manure_n_content_changed(12))
        self.sb_manure_n_content_list[13].valueChanged.connect(lambda: self.sb_manure_n_content_changed(13))
        self.sb_manure_n_content_list[14].valueChanged.connect(lambda: self.sb_manure_n_content_changed(14))
        self.sb_manure_n_content_list[15].valueChanged.connect(lambda: self.sb_manure_n_content_changed(15))
        self.sb_manure_n_content_list[16].valueChanged.connect(lambda: self.sb_manure_n_content_changed(16))
        self.sb_manure_n_content_list[17].valueChanged.connect(lambda: self.sb_manure_n_content_changed(17))
        self.sb_manure_n_content_list[18].valueChanged.connect(lambda: self.sb_manure_n_content_changed(18))
        self.sb_manure_n_content_list[19].valueChanged.connect(lambda: self.sb_manure_n_content_changed(19))

        self.sb_manure_n_availability_list[0].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(0))
        self.sb_manure_n_availability_list[1].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(1))
        self.sb_manure_n_availability_list[2].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(2))
        self.sb_manure_n_availability_list[3].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(3))
        self.sb_manure_n_availability_list[4].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(4))
        self.sb_manure_n_availability_list[5].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(5))
        self.sb_manure_n_availability_list[6].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(6))
        self.sb_manure_n_availability_list[7].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(7))
        self.sb_manure_n_availability_list[8].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(8))
        self.sb_manure_n_availability_list[9].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(9))
        self.sb_manure_n_availability_list[10].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(10))
        self.sb_manure_n_availability_list[11].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(11))
        self.sb_manure_n_availability_list[12].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(12))
        self.sb_manure_n_availability_list[13].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(13))
        self.sb_manure_n_availability_list[14].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(14))
        self.sb_manure_n_availability_list[15].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(15))
        self.sb_manure_n_availability_list[16].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(16))
        self.sb_manure_n_availability_list[17].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(17))
        self.sb_manure_n_availability_list[18].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(18))
        self.sb_manure_n_availability_list[19].valueChanged.connect(lambda: self.sb_manure_n_availability_changed(19))

        self.sb_manure_n_loss_list[0].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(0))
        self.sb_manure_n_loss_list[1].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(1))
        self.sb_manure_n_loss_list[2].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(2))
        self.sb_manure_n_loss_list[3].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(3))
        self.sb_manure_n_loss_list[4].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(4))
        self.sb_manure_n_loss_list[5].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(5))
        self.sb_manure_n_loss_list[6].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(6))
        self.sb_manure_n_loss_list[7].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(7))
        self.sb_manure_n_loss_list[8].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(8))
        self.sb_manure_n_loss_list[9].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(9))
        self.sb_manure_n_loss_list[10].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(10))
        self.sb_manure_n_loss_list[11].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(11))
        self.sb_manure_n_loss_list[12].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(12))
        self.sb_manure_n_loss_list[13].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(13))
        self.sb_manure_n_loss_list[14].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(14))
        self.sb_manure_n_loss_list[15].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(15))
        self.sb_manure_n_loss_list[16].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(16))
        self.sb_manure_n_loss_list[17].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(17))
        self.sb_manure_n_loss_list[18].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(18))
        self.sb_manure_n_loss_list[19].valueChanged.connect(lambda: self.sb_manure_n_loss_percent_changed(19))

        self.sb_manure_p_content_list[0].valueChanged.connect(lambda: self.sb_manure_p_content_changed(0))
        self.sb_manure_p_content_list[1].valueChanged.connect(lambda: self.sb_manure_p_content_changed(1))
        self.sb_manure_p_content_list[2].valueChanged.connect(lambda: self.sb_manure_p_content_changed(2))
        self.sb_manure_p_content_list[3].valueChanged.connect(lambda: self.sb_manure_p_content_changed(3))
        self.sb_manure_p_content_list[4].valueChanged.connect(lambda: self.sb_manure_p_content_changed(4))
        self.sb_manure_p_content_list[5].valueChanged.connect(lambda: self.sb_manure_p_content_changed(5))
        self.sb_manure_p_content_list[6].valueChanged.connect(lambda: self.sb_manure_p_content_changed(6))
        self.sb_manure_p_content_list[7].valueChanged.connect(lambda: self.sb_manure_p_content_changed(7))
        self.sb_manure_p_content_list[8].valueChanged.connect(lambda: self.sb_manure_p_content_changed(8))
        self.sb_manure_p_content_list[9].valueChanged.connect(lambda: self.sb_manure_p_content_changed(9))
        self.sb_manure_p_content_list[10].valueChanged.connect(lambda: self.sb_manure_p_content_changed(10))
        self.sb_manure_p_content_list[11].valueChanged.connect(lambda: self.sb_manure_p_content_changed(11))
        self.sb_manure_p_content_list[12].valueChanged.connect(lambda: self.sb_manure_p_content_changed(12))
        self.sb_manure_p_content_list[13].valueChanged.connect(lambda: self.sb_manure_p_content_changed(13))
        self.sb_manure_p_content_list[14].valueChanged.connect(lambda: self.sb_manure_p_content_changed(14))
        self.sb_manure_p_content_list[15].valueChanged.connect(lambda: self.sb_manure_p_content_changed(15))
        self.sb_manure_p_content_list[16].valueChanged.connect(lambda: self.sb_manure_p_content_changed(16))
        self.sb_manure_p_content_list[17].valueChanged.connect(lambda: self.sb_manure_p_content_changed(17))
        self.sb_manure_p_content_list[18].valueChanged.connect(lambda: self.sb_manure_p_content_changed(18))
        self.sb_manure_p_content_list[19].valueChanged.connect(lambda: self.sb_manure_p_content_changed(19))

        self.sb_manure_k_content_list[0].valueChanged.connect(lambda: self.sb_manure_k_content_changed(0))
        self.sb_manure_k_content_list[1].valueChanged.connect(lambda: self.sb_manure_k_content_changed(1))
        self.sb_manure_k_content_list[2].valueChanged.connect(lambda: self.sb_manure_k_content_changed(2))
        self.sb_manure_k_content_list[3].valueChanged.connect(lambda: self.sb_manure_k_content_changed(3))
        self.sb_manure_k_content_list[4].valueChanged.connect(lambda: self.sb_manure_k_content_changed(4))
        self.sb_manure_k_content_list[5].valueChanged.connect(lambda: self.sb_manure_k_content_changed(5))
        self.sb_manure_k_content_list[6].valueChanged.connect(lambda: self.sb_manure_k_content_changed(6))
        self.sb_manure_k_content_list[7].valueChanged.connect(lambda: self.sb_manure_k_content_changed(7))
        self.sb_manure_k_content_list[8].valueChanged.connect(lambda: self.sb_manure_k_content_changed(8))
        self.sb_manure_k_content_list[9].valueChanged.connect(lambda: self.sb_manure_k_content_changed(9))
        self.sb_manure_k_content_list[10].valueChanged.connect(lambda: self.sb_manure_k_content_changed(10))
        self.sb_manure_k_content_list[11].valueChanged.connect(lambda: self.sb_manure_k_content_changed(11))
        self.sb_manure_k_content_list[12].valueChanged.connect(lambda: self.sb_manure_k_content_changed(12))
        self.sb_manure_k_content_list[13].valueChanged.connect(lambda: self.sb_manure_k_content_changed(13))
        self.sb_manure_k_content_list[14].valueChanged.connect(lambda: self.sb_manure_k_content_changed(14))
        self.sb_manure_k_content_list[15].valueChanged.connect(lambda: self.sb_manure_k_content_changed(15))
        self.sb_manure_k_content_list[16].valueChanged.connect(lambda: self.sb_manure_k_content_changed(16))
        self.sb_manure_k_content_list[17].valueChanged.connect(lambda: self.sb_manure_k_content_changed(17))
        self.sb_manure_k_content_list[18].valueChanged.connect(lambda: self.sb_manure_k_content_changed(18))
        self.sb_manure_k_content_list[19].valueChanged.connect(lambda: self.sb_manure_k_content_changed(19))

        # --------------TAB 4: TAB YIELD---------------------------------

        self.lb_yield_amount_main_list = [self.QTUI_rotor.lb_yield_1,
                                          self.QTUI_rotor.lb_yield_2,
                                          self.QTUI_rotor.lb_yield_3,
                                          self.QTUI_rotor.lb_yield_4,
                                          self.QTUI_rotor.lb_yield_5,
                                          self.QTUI_rotor.lb_yield_6,
                                          self.QTUI_rotor.lb_yield_7,
                                          self.QTUI_rotor.lb_yield_8,
                                          self.QTUI_rotor.lb_yield_9,
                                          self.QTUI_rotor.lb_yield_10]

        self.sb_yield_amount_main_list = [self.QTUI_rotor.sb_yield_1,
                                          self.QTUI_rotor.sb_yield_2,
                                          self.QTUI_rotor.sb_yield_3,
                                          self.QTUI_rotor.sb_yield_4,
                                          self.QTUI_rotor.sb_yield_5,
                                          self.QTUI_rotor.sb_yield_6,
                                          self.QTUI_rotor.sb_yield_7,
                                          self.QTUI_rotor.sb_yield_8,
                                          self.QTUI_rotor.sb_yield_9,
                                          self.QTUI_rotor.sb_yield_10]

        self.sb_yield_amount_catch_crop_list = [self.QTUI_rotor.sb_yield_catch_crop_1,
                                                self.QTUI_rotor.sb_yield_catch_crop_2,
                                                self.QTUI_rotor.sb_yield_catch_crop_3,
                                                self.QTUI_rotor.sb_yield_catch_crop_4,
                                                self.QTUI_rotor.sb_yield_catch_crop_5,
                                                self.QTUI_rotor.sb_yield_catch_crop_6,
                                                self.QTUI_rotor.sb_yield_catch_crop_7,
                                                self.QTUI_rotor.sb_yield_catch_crop_8,
                                                self.QTUI_rotor.sb_yield_catch_crop_9,
                                                self.QTUI_rotor.sb_yield_catch_crop_10]

        self.bt_yield_reset_list = [self.QTUI_rotor.bt_reset_yield_1,
                                    self.QTUI_rotor.bt_reset_yield_2,
                                    self.QTUI_rotor.bt_reset_yield_3,
                                    self.QTUI_rotor.bt_reset_yield_4,
                                    self.QTUI_rotor.bt_reset_yield_5,
                                    self.QTUI_rotor.bt_reset_yield_6,
                                    self.QTUI_rotor.bt_reset_yield_7,
                                    self.QTUI_rotor.bt_reset_yield_8,
                                    self.QTUI_rotor.bt_reset_yield_9,
                                    self.QTUI_rotor.bt_reset_yield_10]

        self.lb_yield_unit_list = [self.QTUI_rotor.lb_unit_yield_1,
                                   self.QTUI_rotor.lb_unit_yield_2,
                                   self.QTUI_rotor.lb_unit_yield_3,
                                   self.QTUI_rotor.lb_unit_yield_4,
                                   self.QTUI_rotor.lb_unit_yield_5,
                                   self.QTUI_rotor.lb_unit_yield_6,
                                   self.QTUI_rotor.lb_unit_yield_7,
                                   self.QTUI_rotor.lb_unit_yield_8,
                                   self.QTUI_rotor.lb_unit_yield_9,
                                   self.QTUI_rotor.lb_unit_yield_10]

        self.lb_yield_unit_catch_crop_list = [self.QTUI_rotor.lb_unit_yield_catch_crop_1,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_2,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_3,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_4,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_5,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_6,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_7,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_8,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_9,
                                              self.QTUI_rotor.lb_unit_yield_catch_crop_10]

        # --------------------TAB 4 SLOTS --------------------------------------------------------------

        self.sb_yield_amount_main_list[0].valueChanged.connect(lambda: self.sb_yield_projection_changed(0))
        self.sb_yield_amount_main_list[1].valueChanged.connect(lambda: self.sb_yield_projection_changed(1))
        self.sb_yield_amount_main_list[2].valueChanged.connect(lambda: self.sb_yield_projection_changed(2))
        self.sb_yield_amount_main_list[3].valueChanged.connect(lambda: self.sb_yield_projection_changed(3))
        self.sb_yield_amount_main_list[4].valueChanged.connect(lambda: self.sb_yield_projection_changed(4))
        self.sb_yield_amount_main_list[5].valueChanged.connect(lambda: self.sb_yield_projection_changed(5))
        self.sb_yield_amount_main_list[6].valueChanged.connect(lambda: self.sb_yield_projection_changed(6))
        self.sb_yield_amount_main_list[7].valueChanged.connect(lambda: self.sb_yield_projection_changed(7))
        self.sb_yield_amount_main_list[8].valueChanged.connect(lambda: self.sb_yield_projection_changed(8))
        self.sb_yield_amount_main_list[9].valueChanged.connect(lambda: self.sb_yield_projection_changed(9))

        self.sb_yield_amount_catch_crop_list[0].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(0))
        self.sb_yield_amount_catch_crop_list[1].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(1))
        self.sb_yield_amount_catch_crop_list[2].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(2))
        self.sb_yield_amount_catch_crop_list[3].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(3))
        self.sb_yield_amount_catch_crop_list[4].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(4))
        self.sb_yield_amount_catch_crop_list[5].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(5))
        self.sb_yield_amount_catch_crop_list[6].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(6))
        self.sb_yield_amount_catch_crop_list[7].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(7))
        self.sb_yield_amount_catch_crop_list[8].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(8))
        self.sb_yield_amount_catch_crop_list[9].valueChanged.connect(lambda: self.sb_yield_catch_crop_changed(9))

        self.bt_yield_reset_list[0].clicked.connect(lambda: self.bt_yield_reset_clicked(0))
        self.bt_yield_reset_list[1].clicked.connect(lambda: self.bt_yield_reset_clicked(1))
        self.bt_yield_reset_list[2].clicked.connect(lambda: self.bt_yield_reset_clicked(2))
        self.bt_yield_reset_list[3].clicked.connect(lambda: self.bt_yield_reset_clicked(3))
        self.bt_yield_reset_list[4].clicked.connect(lambda: self.bt_yield_reset_clicked(4))
        self.bt_yield_reset_list[5].clicked.connect(lambda: self.bt_yield_reset_clicked(5))
        self.bt_yield_reset_list[6].clicked.connect(lambda: self.bt_yield_reset_clicked(6))
        self.bt_yield_reset_list[7].clicked.connect(lambda: self.bt_yield_reset_clicked(7))
        self.bt_yield_reset_list[8].clicked.connect(lambda: self.bt_yield_reset_clicked(8))
        self.bt_yield_reset_list[9].clicked.connect(lambda: self.bt_yield_reset_clicked(9))

        self.cb_tillage_list[0].currentIndexChanged.connect(lambda: self.cb_tillage_selected(0))
        self.cb_tillage_list[1].currentIndexChanged.connect(lambda: self.cb_tillage_selected(1))
        self.cb_tillage_list[2].currentIndexChanged.connect(lambda: self.cb_tillage_selected(2))
        self.cb_tillage_list[3].currentIndexChanged.connect(lambda: self.cb_tillage_selected(3))
        self.cb_tillage_list[4].currentIndexChanged.connect(lambda: self.cb_tillage_selected(4))
        self.cb_tillage_list[5].currentIndexChanged.connect(lambda: self.cb_tillage_selected(5))
        self.cb_tillage_list[6].currentIndexChanged.connect(lambda: self.cb_tillage_selected(6))
        self.cb_tillage_list[7].currentIndexChanged.connect(lambda: self.cb_tillage_selected(7))
        self.cb_tillage_list[8].currentIndexChanged.connect(lambda: self.cb_tillage_selected(8))
        self.cb_tillage_list[9].currentIndexChanged.connect(lambda: self.cb_tillage_selected(9))

        # --------------TAB 5: REPORT------------------------------------------------------------

        # TEMP TEMP TEMP TEMP TEMP TEMP TEMP

        # END TEMP TEMP

        # --------------miscellaneous------------------------------------------------------------
        # no matter what preference was set in QT creator, it always starts with tab_general
        self.QTUI_rotor.tabs_rotor.setCurrentIndex(0)
        #TODO right?
        self.setup_ui()
        # self.ui.setupUi(self)

        self.load_project_to_ui()

    # -----------------------------END OF __init__-------END OF __init__--------END OF __init__--------------
    # TODO new manure stuff -----------------------------------------------------------------------------------
    def tab_changed(self, tab_index):
        print("MainWindow.tab_changed", tab_index)
        # TODO refactor: create class or move to class...
        if tab_index == 3:
            if self.project.tab_general["assessment_method"] in (1, 3):
                self.project, self.ui_status = LogicTabRotation.get_manure_list(self.project, self.ui_status)
                self.project, self.ui_status = LogicTabManure.update_manure_ids(self.project, self.ui_status)

                print("load_manures", self.ui_status.tab_manure["load_manures"])
            if self.ui_status.tab_manure["load_manures"]:
                self.load_project_to_tab_manure()

            # self.project, self.ui_status = LogicTabManure.get_manure_standardvalues(self.project, self.ui_status)

    def set_ui_status_manure(self, index, index_2):
        print("MainWindow.set_ui_status_manure", index, index_2)
        # changing color and clickeability of the calculate button
        self.cb_manure_source_list[index][index_2].setVisible(
            self.ui_status.tab_rotation["cb_manure_source_visible"][index][index_2])
        self.cb_manure_specification_list[index][index_2].setVisible(
            self.ui_status.tab_rotation["cb_manure_specification_visible"][index][index_2])
        if self.ui_status.tab_rotation["manure_unit_ton"][index][index_2]:
            self.lb_manure_amount_list[index][index_2].setText("t/ha")
        else:
            self.lb_manure_amount_list[index][index_2].setText("m<sup>3</sup>/ha")

    def cb_manure_type_selected(self, index, index_2):
        print("MainWindow.cb_manure_type_selected", index, index_2)
        self.project.tab_rotation["manure_type"][index][index_2] = self.cb_manure_type_list[index][
            index_2].currentData()

        self.project, self.ui_status = LogicTabRotation.get_sb_manure_amount_unit(self.project, self.ui_status, index,
                                                                                  index_2)
        self.project, self.ui_status = LogicTabRotation.get_cb_manure_source(self.project, self.ui_status, index,
                                                                             index_2)
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_source"][index][index_2],
                               self.cb_manure_source_list[index][index_2])

        self.set_ui_status_manure(index, index_2)

    def cb_manure_source_selected(self, index, index_2):
        print("MainWindow.cb_manure_source_selected", index, index_2)
        self.project.tab_rotation["manure_source"][index][index_2] = self.cb_manure_source_list[index][
            index_2].currentData()
        self.project, self.ui_status = LogicTabRotation.get_cb_manure_specification(self.project, self.ui_status, index,
                                                                                    index_2)
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_specification"][index][index_2],
                               self.cb_manure_specification_list[index][index_2])

        # TODO HIER DER FEHLER? ist hier noch ein Fehler? check and delete
        if self.project.tab_rotation["manure_used"][index][index_2]:
            self.project, self.ui_status = LogicTabRotation.get_m_n_manure_id(self.project, self.ui_status, index,
                                                                              index_2)
            # self.project, self.ui_status = LogicTabManure.add_manure_id(self.project, self.ui_status)

        self.set_ui_status_manure(index, index_2)

    def cb_manure_specification_selected(self, index, index_2):
        print("MainWindow.cb_manure_specification_selected", index, index_2)
        self.project.tab_rotation["manure_specification"][index][index_2] = self.cb_manure_specification_list[index][
            index_2].currentData()

        if self.project.tab_rotation["manure_used"][index][index_2]:
            self.project, self.ui_status = LogicTabRotation.get_m_n_manure_id(self.project, self.ui_status, index,
                                                                              index_2)

        self.set_ui_status_manure(index, index_2)

    def bt_manure_delete(self, index, index_2):
        print("MainWindow.bt_manure_delete", index, index_2)
        print("List after pop and append", self.ui_status.tab_rotation["qw_manure_visible"][index])
        self.project, self.ui_status = LogicTabRotation.bt_manure_delete(self.project, self.ui_status, index, index_2)
        project_temp = copy.copy(self.project)

        self.set_visibility_manure_all_widgets(index)
        self.project = project_temp

        for i in range(0, 3):
            self.load_manure_to_qw(index, i)
        # TODO this actually belongs to ui_status
        if not self.project.tab_rotation["manure"][index]:
            self.gb_manure_list[index].setChecked(False)

    def load_manure_to_qw_all(self, index):
        print("MainWindow.load_manure_to_qw_all", index)
        for index_2 in range(0, 4):
            self.load_manure_to_qw(index, index_2)

    def bt_next_manure(self, index, index_2):
        print("MainWindow.bt_next_manure", index, index_2)
        index_2 += 1
        self.ui_status.tab_rotation["qw_manure_visible"][index][index_2] = True
        self.project.tab_rotation["manure_used"][index][index_2] = True
        self.project, self.ui_status = LogicTabRotation.get_m_n_manure_id(self.project, self.ui_status, index, index_2)

        if index_2 < 3:
            self.bt_next_manure_list[index][index_2].setVisible(True)

        print(self.project.tab_rotation["manure_used"])
        self.set_visibility_manure_widgets(index, index_2)

    def gb_manure_toggled(self, index):
        print("MainWindow.gb_manure_toggled", index)

        self.project.tab_rotation["manure"][index] = self.gb_manure_list[index].isChecked()
        print("self.project.tab_rotation['manure'][index]", self.project.tab_rotation["manure"][index])
        # this evaluates, if the tab manure has to be visible
        self.project, self.ui_status = LogicTabRotation.gb_manure_selected(self.project, self.ui_status, index)

        self.set_visibility_manure_widgets(index, 0)
        self.set_ui_status_tabs()

    def sb_manure_amount(self, index, index_2):
        print("MainWindow.sb_manure_amount", index, index_2)

        self.project.tab_rotation["manure_amount"][index][index_2] = self.sb_manure_amount_list[index][index_2].value()

    def setup_tab_manure(self):
        print("MainWindow.setup_tab_manure")
        self.project, self.ui_status = LogicTabManure.get_df_manure_all_standarvalues(self.project, self.ui_status)
        self.ui_status = LogicTabManure.get_df_manure_labels(self.project, self.ui_status)

        # TODO new manure stuff ----------------------------------------------------------------------------THE END

    # ------------------GENERAL--------------------------------------------------------

    def populate_combobox(self, recordset, combobox):
        print("MainWindow.populate_combobox", combobox.objectName())

        combobox.clear()
        if combobox in self.cb_crop_year_list:
            combobox.addItem("----select crop----")
        for row in recordset:
            # row[1] is name displayed, row[0] is item-data
            combobox.addItem(row[1], row[0])

    def remove_item_from_combobox(self, index):
        print("MainWindow.remove_item_from_combobox", index)
        # self.ui_status.error["error"] = False
        # self.ui_status.error["remove_item"][index] = False
        self.project, self.ui_status = LogicTabRotation.reset_values_for_remove_item(self.project, self.ui_status,
                                                                                     index)
        self.cb_crop_year_list[index].removeItem(self.cb_crop_year_list[index].findData(
            self.project.tab_rotation["crop_id"][index]))

    # ----------------SETUP UI: TABS------------------------------------------------------
    def setup_ui(self):
        print("MainWindow.setup_ui")
        # tabs are set in/ visible

        self.set_ui_status_tabs()

        self.setup_tab_general()
        # self.setup_tab_rotation()
        self.setup_tabs_for_rotation_duration()
        '''the tab_rotation and the tab for the free generation are setup in self.setup_tabs_for_rotation_duration()'''
        # self.setup_tab_rotation()
        # self.setup_tab_free_generation()
        self.setup_tab_manure()
        # self.load_project_to_ui()

        self.ui_status.load_project = False



    # sets the chosen number of crop GroupBoxes as active in the tabs Generation and Free Generation and Tab Yield
    def setup_tabs_for_rotation_duration(self):
        print("MainWindow.setup_tabs_for_rotation_duration")
        for index in range(0, self.MAX_DURATION):
            self.gb_rotation_list[index].setHidden(True)
            self.qw_free_generation_crop_list[index].setHidden(True)
            self.qw_legumegras_list[index].setHidden(True)
            self.lb_yield_amount_main_list[index].setHidden(True)
            self.sb_yield_amount_main_list[index].setHidden(True)
            self.lb_yield_unit_list[index].setHidden(True)
            self.sb_yield_amount_catch_crop_list[index].setHidden(True)
            self.lb_yield_unit_catch_crop_list[index].setHidden(True)
            self.bt_yield_reset_list[index].setHidden(True)
        for index in range(0, self.rotation_duration):
            self.gb_rotation_list[index].setHidden(False)
            self.qw_free_generation_crop_list[index].setHidden(False)
            self.lb_yield_amount_main_list[index].setHidden(False)
            self.sb_yield_amount_main_list[index].setHidden(False)
            self.lb_yield_unit_list[index].setHidden(False)
            self.sb_yield_amount_catch_crop_list[index].setHidden(False)
            self.lb_yield_unit_catch_crop_list[index].setHidden(False)
            self.bt_yield_reset_list[index].setHidden(False)
        if self.project.tab_general["assessment_method"] == 2:
            self.setup_tab_free_generation()
        else:
            self.setup_tab_rotation()

    def setup_tab_general(self):
        print("MainWindow.setup_tab_general")
        # selcts the radiobutton for the assessment method
        self.rb_assessment_method_group.button(self.project.tab_general["assessment_method"]).setChecked(True)
        # comboboxes tab_general
        self.project = LogicTabGeneral.get_cb_soil_type(self.project, self.ui_status)
        self.populate_combobox(self.project.all_tabs_combobox_items["cb_soil_type"], self.QTUI_rotor.cb_soil_type)

    def setup_tab_rotation(self):
        print("MainWindow.setup_tab_rotation")
        # the tem_project is needed to avoid values being changed by selections triggered by filling comboboxes
        temp_project = copy.copy(self.project)
        self.project = LogicTabRotation.get_data_winterhardiness(self.project, self.ui_status)
        self.project = LogicTabRotation.get_cb_manure_type(self.project, self.ui_status)
        print(self.project.all_tabs_combobox_items["cb_manure_type"])
        self.project = LogicTabRotation.get_cb_tillage(self.project, self.ui_status)

        for index in range(0, self.rotation_duration):
            # Tab Generation: filling all comboboxes
            self.populate_crop_year_combobox(index)
            self.populate_combobox(self.project.all_tabs_combobox_items["cb_winterhardiness"],
                                   self.cb_covercrop_winterhardiness_list[index])
            self.populate_combobox(self.project.all_tabs_combobox_items["cb_tillage"],
                                   self.cb_tillage_list[index])
            print("setup_tab_rotation 1", self.project.all_tabs_combobox_items["cb_manure_type"])
            self.set_visibility_manure_all_widgets(index)
            for index_2 in range(0, 4):
                self.populate_combobox(self.project.all_tabs_combobox_items["cb_manure_type"],
                                       self.cb_manure_type_list[index][index_2])
            print("setup_tab_rotation 2", self.project.all_tabs_combobox_items["cb_manure_type"])

        self.project = temp_project

    def setup_tab_rotation_crop_year_comboboxes(self):
        print("MainWindow.setup_tab_rotation_crop_year_comboboxes")

        for index in range(0, self.rotation_duration):
            # Tab Generation: filling all comboboxes
            self.populate_crop_year_combobox(index)

    def set_visibility_manure_all_widgets(self, index):
        print("MainWindow.set_visibility_manure_all", index)
        for index_2 in range(0, 4):
            self.set_visibility_manure_widgets(index, index_2)

    # TODO this is bt_manure_next, bt_manure_delete
    def set_visibility_manure_widgets(self, index, index_2):
        print("MainWindow.set_visibility_manure_widgets", index, index_2)
        # turn qwidgets on or off according to status
        qw_visible = self.ui_status.tab_rotation["qw_manure_visible"][index][index_2]

        self.qw_manure_list[index][index_2].setVisible(qw_visible)

    def load_manure_to_qw(self, index, index_2):
        print("MainWindow.load_manure_to_qw", index, index_2)
        self.cb_manure_type_list[index][index_2].setCurrentIndex(
            self.cb_manure_type_list[index][index_2].findData(self.project.tab_rotation["manure_type"][index][index_2]))
        self.cb_manure_source_list[index][index_2].setCurrentIndex(self.cb_manure_source_list[index][index_2].findData(
            self.project.tab_rotation["manure_source"][index][index_2]))
        self.cb_manure_specification_list[index][index_2].setCurrentIndex(
            self.cb_manure_specification_list[index][index_2].findData(
                self.project.tab_rotation["manure_specification"][index][index_2]))

    # Tab Free Generation. filling all comboboxes

    def setup_tab_free_generation(self):
        print("MainWindow.setup_tab_free_generation")
        # self.load_project = True
        for index in range(0, self.rotation_duration):
            self.project = LogicFreeRotationGeneration.get_recordset_cb_crop_types(self.project, self.ui_status, index)
            self.project = LogicFreeRotationGeneration.get_recordset_cb_crops(self.project, self.ui_status, index)
            self.populate_combobox(self.project.free_generation_temp["cb_crop_type_recordsets"][index],
                                   self.cb_free_generation_crop_type_list[index])
            print("Cb name", self.cb_free_generation_crop_type_list[index].objectName())
            print("CURRENT INDEX CROP TYPE", self.cb_free_generation_crop_type_list[index].currentIndex())
            # self.cb_free_generation_crop_type_list[index].setCurrentIndex(2)
            self.populate_free_generation_crop_combobox(index)

        #     self.cb_free_generation_crop_selected(index)
        # # self.load_project = False
        self.load_project_to_tab_free_generation()

    def setup_tab_yield(self):
        print("MainWindow.setup_tab_yield")
        # tillage is the same for any selection
        # self.setting_up_tab_yield = True

        for index in range(0, self.rotation_duration):
            self.project, self.ui_status = LogicTabYield.get_names_tab_yield(self.project, self.ui_status, index)
            # self.populate_combobox(self.project.all_tabs_combobox_items["cb_epm_tillage"],
            #                        self.cb_tillage_2_list[index])
            self.lb_yield_amount_main_list[index].setText(self.project.tab_rotation["crop_name"][index])

    def setup_tab_report(self):
        print("MainWindow.setup_tab_report")
        self.QTUI_rotor.tw_report.clear()
        self.QTUI_rotor.tw_report.verticalHeaderVisible = False
        self.QTUI_rotor.tw_report.horizontalHeaderVisible = False
        for index in range(1, (self.MAX_DURATION + 1)):
            if index <= self.rotation_duration:
                hidden = False
            else:
                hidden = True
            self.QTUI_rotor.tw_report.setColumnHidden(index, hidden)

        # populationg rows and columns
        self.project, self.ui_status = LogicTabReport.setup_report_table(self.project, self.ui_status)

        self.QTUI_rotor.tw_report.setRowCount(len(self.project.report["item_list"]))

        self.QTUI_rotor.tw_report.setItem(2, 2, QTableWidgetItem("test"))
        for row in range(0, len(self.project.report["item_list"])):

            for column in range(0, (self.rotation_duration + 1)):
                print("TABLE ITEM", self.project.report["item_list"][row][column])
                if row == 0:
                    # font = QFont()
                    # font.setBold(True)

                    self.QTUI_rotor.tw_report.setItem(row, column, QTableWidgetItem(
                        str(self.project.report["item_list"][row][column])))
                    # .setFont(font))
                else:
                    self.QTUI_rotor.tw_report.setItem(row, column, QTableWidgetItem(
                        str(self.project.report["item_list"][row][column])))

    # UI STATUS

    # if a project is verified, the "calculate" button turn green and enabled
    def set_ui_status_calculation_possible(self):
        print("MainWindow.set_ui_status_calculation_possible")

        if self.ui_status.calculation_state["possible"]:
            self.QTUI_rotor.bt_calculate.setStyleSheet("background-color: #66dd00")
            self.QTUI_rotor.bt_calculate.setEnabled(True)
        else:
            self.QTUI_rotor.bt_calculate.setStyleSheet("")
            self.QTUI_rotor.bt_calculate.setEnabled(False)

    def set_ui_status_tabs(self):
        print("MainWindow.set_ui_status_tabs")
        # visible_tabs_indices is needed for the previous and next buttons
        index = 0
        self.QTUI_rotor.tabs_rotor.setTabText(1, self.ui_status.tabs_state["rotation_tab_name"])
        visible_tabs_indices = []
        for key in self.ui_status.tabs:

            print("set_ui_status_tabs", index, key, self.ui_status.tabs[key])

            self.QTUI_rotor.tabs_rotor.setTabVisible(index, self.ui_status.tabs[key])
            if self.ui_status.tabs[key]:
                visible_tabs_indices.append(index)
            print("visible_tabs_indices", visible_tabs_indices)
            index += 1
        self.ui_status.tabs_state["update_tabs"] = False
        self.ui_status.tabs_state["visible_tabs_indices"] = visible_tabs_indices

    # in the tab_rotation undersowing and covercrop are interdependant, checkeabilities are set here
    def set_ui_status_undersowing(self, index):
        print("MainWindow.set_ui_status_undersowing", index)
        # print("VORHER")
        #
        # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])

        index_next = (index + 1) % self.rotation_duration
        self.xb_undersowing_list[index].setChecked(self.project.tab_rotation["undersowing"][index])
        self.xb_undersowing_list[index].setEnabled(self.ui_status.tab_rotation["xb_undersowing_checkeable"][index])
        self.gb_undersowing_list[index].setEnabled(self.ui_status.tab_rotation["gb_undersowing_active"][index])

        self.cb_covercrop_winterhardiness_list[index_next].setEnabled(self.ui_status.tab_rotation["cb_covercrop_winterhardiness_active"][index_next])

        self.xb_covercrop_list[index_next].setChecked(self.project.tab_rotation["covercrop"][index_next])
        self.xb_covercrop_list[index_next].setEnabled(
            self.ui_status.tab_rotation["xb_covercrop_checkeable"][index_next])
        self.gb_covercrop_list[index_next].setEnabled(
            self.ui_status.tab_rotation["gb_covercrop_active"][index_next])
        # self.xb_covercrop_harvest_list[index_next].setEnabled(self.ui_status.dict_tab_rotation["xb_covercrop_harvest_checkeable"][index_next])
        # the removal triggers a repopulation of the comboboxes, where remove_item is set to False
        # self.display_ui_status_errormessage(index)
        if self.ui_status.error["remove_item"][index]:
            self.ui_status.error["remove_item"][index] = False
            self.remove_item_from_combobox(index)

        self.display_ui_status_errormessage(index)
        # print("Ende set_ui_status_undersowing")
        #
        # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])

    def set_ui_status_covercrop(self, index):
        print("MainWindow.set_ui_status_covercrop", index)
        # print("VORHER")
        #
        # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])
        index_previous = (index - 1) % self.rotation_duration

        self.xb_covercrop_list[index].setChecked(self.project.tab_rotation["covercrop"][index])
        self.xb_covercrop_list[index].setEnabled(self.ui_status.tab_rotation["xb_covercrop_checkeable"][index])
        self.gb_covercrop_list[index].setEnabled(self.ui_status.tab_rotation["gb_covercrop_active"][index])

        self.xb_undersowing_list[index_previous].setChecked(
            self.project.tab_rotation["undersowing"][index_previous])
        self.xb_undersowing_list[index_previous].setEnabled(
            self.ui_status.tab_rotation["xb_undersowing_checkeable"][index_previous])
        self.gb_undersowing_list[index_previous].setEnabled(
            self.ui_status.tab_rotation["gb_undersowing_active"][index_previous])
        # if an impossible combination occurs, the current crop is removed from the combobox
        # the removal triggers a repopulation of the comboboxes, where remove_item is set to False
        # self.display_ui_status_errormessage(index)
        if self.ui_status.error["remove_item"][index]:
            self.ui_status.error["remove_item"][index] = False
            self.remove_item_from_combobox(index)

        self.xb_covercrop_harvest_list[index].setEnabled(
            self.ui_status.tab_rotation["xb_covercrop_harvest_checkeable"][index])
        self.xb_covercrop_harvest_list[index].setChecked(self.project.tab_rotation["covercrop_harvest"][index])

        self.display_ui_status_errormessage(index)
        # print("Nachher")
        #
        # print("xb_covercrop_checkeable", self.ui_status.dict_tab_rotation["xb_covercrop_checkeable"])
        # print("gb_covercrop_active", self.ui_status.dict_tab_rotation["gb_covercrop_active"])

    def set_ui_status_strawharvest(self, index):
        print("MainWindow.set_ui_status_strawharvest", index)
        self.xb_strawharvest_list[index].setEnabled(
            self.ui_status.tab_rotation["xb_strawharvest_checkeable"][index])

    def set_ui_status_early_tillage(self, index):
        print("MainWindow.set_ui_status_early_tillage", index)
        self.xb_early_tillage_list[index].setChecked(self.project.tab_rotation["early_tillage"][index])
        self.xb_early_tillage_list[index].setEnabled(
            self.ui_status.tab_rotation["xb_early_tillage_checkeable"][index])

    def set_ui_status_legumegras(self, index):
        print("MainWindow.set_ui_status_legumegras", index)
        self.qw_legumegras_list[index].setVisible(self.ui_status.tab_rotation["qw_legumegras_visible"][index])
        self.qf_covercrop_list[index].setVisible(self.ui_status.tab_rotation["qf_covercrop_visible"][index])


    def display_ui_status_errormessage(self, *args):
        print("MainWindow.display_ui_status_errormessage", args)
        # args is the index
        if self.ui_status.error["error"]:
            if args:
                print("args", args[0])
                year = args[0] % self.rotation_duration + 1
                messagebox_header = f"Information year {year}"

            else:
                messagebox_header = "Information"

            QMessageBox.information(self, messagebox_header,
                                              self.ui_status.error["errormessage"], QMessageBox.Ok)
            self.ui_status.error["error"] = False
            self.ui_status.error["errormessage"] = ""
        else:
            pass

    # TODO not in use
    def set_ui_status_tab_generation_yield_adjustment(self, index):
        print("MainWindow.set_ui_status_tab_generation_yield_adjustment", index)
        self.qw_free_generation_crop_list[index].setVisible(self.ui_status.tab_rotation["qf_yield_adjustment_visible"])

    # TODO refactor
    def set_manure_tab_visible(self):
        print("MainWindow.set_manure_tab_visible")

        self.ui_status.tabs["tab_organic_manure_visible"] = self.QTUI_rotor.gb_livestock_farming.isChecked()
        self.set_ui_status_tabs()

    # --------------------MENUBAR FUNCTIONS AND SLOTS--------------------------------------------
    def save_project_as(self):
        print("MainWindow.save_project_as")

        self.project.project_id = None
        self.save_project()

    def save_project(self):
        print("MainWindow.save_project")
        # self.logger.info("SAVING PROJECT...")
        # if self.verify_project() is False:
        #     return

        # project has not yet been saved to the db and therefore does not have an id yet
        if self.project.project_id is None:
            #self.logger.info("ID is None")
            # change default project name and save

            # TODO: NEEDS TO BE VERIFIED THAT Project Name does not yet exist (Unique Constraint)

            save_window = DialogWindow()
            self.project = save_window.save_project_window(self.project)

        self.project, self.ui_status = ProjectHandling.save_project_to_db(self.project, self.ui_status)

        self.setWindowTitle("ROTOR 4.0  --  %(projectname)s" % {"projectname": self.project.project_name})
        self.display_ui_status_errormessage()

    def open_project(self):
        print("MainWindow.open_project")

        open_project_window = OpenProjectWindow()
        # open_project_window.show()
        open_project_window.exec_()

        if open_project_window.project.project_id is not None:
            self.project = open_project_window.project
            self.ui_status = open_project_window.ui_status

            self.display_ui_status_errormessage()
            self.load_project_to_ui()

    def export_project(self):
        print("MainWindow.export_project")

        user_path = os.path.expanduser('~')
        documents_path = os.path.join(user_path, "Documents")

        export_name = QFileDialog.getSaveFileName(self, "Export Rotor project", documents_path,
                                                            "ROTOR file (*.rotor)")
        print("JSON_EXPORT", export_name[0], "\n")
        print("Checkpoint 1")
        # try:
        file = open(export_name[0], 'w')
        # get project name from file dialog and set as projectname
        filename = QFileInfo(export_name[0]).fileName()
        self.project.project_name = filename.split(".")[0]
        print("Export; project", self.project)
        # json_project = ProjectHandling.json_export(self.project)
        json_project = json.dumps(self.project, default=json_encode_project)
        print("Export; json_project", json_project)
        print("Export; str(json_project)", str(json_project))
        file.write(str(json_project))
        file.close()
        # except:
        #     pass
        print("FILE CLOSED")

    # TODO refactor import to project_handling
    def import_project(self):
        print("MainWindow.import_project")
        user_path = os.path.expanduser('~')
        documents_path = os.path.join(user_path, "Documents")

        import_name = QFileDialog.getOpenFileName(self, "Import Rotor project", documents_path,
                                                            "ROTOR file (*.rotor)")
        try:
            print("IMPORT FILENAME", import_name)
            file = open(import_name[0], 'r')
            print("FILE", file)
            project = Project()
            json_import = file.read()
            print("json_import", json_import)

            self.project = json_decode_project(json_import)
            print(project.project_name)
            file.close()

            self.load_project_to_ui()
        except:
            pass

    def changeEvent(self, event):
        print("MainWindow.changeEvent", event)

        if event.type() == QEvent.LanguageChange:
            self.QTUI_rotor.retranslateUi(self)

            super(MainWindow, self).changeEvent(event)
        else:
            pass

    def translate_gui(self, text):
        """simplify translating the gui"""
        print("MainWindow.translate_gui")

        return QCoreApplication.translate('@default', text)

    # TODO tab name generator needs the translation AND gui_translator <> translate_gui

    def set_language(self, language):
        print("MainWindow.set_language", language)
        # self.language = language
        self.ui_status.settings["language"] = language
        self.ui_status.settings["changing_language"] = True
        # self.QTUI_rotor.retranslateUi(self)
        temp_project = copy.copy(self.project)
        temp_ui_status = copy.copy(self.ui_status)

        if self.ui_status.settings["language"] == "en":
            self.gui_translator.load("mainwindow_en", "resources")
            QApplication.instance().installTranslator(self.gui_translator)
        elif self.ui_status.settings["language"] == "fr":
            self.gui_translator.load("mainwindow_fr", "resources")
        else:

            QApplication.instance().removeTranslator(self.gui_translator)

        self.setup_ui()
        self.project = temp_project
        self.ui_status = temp_ui_status
        self.ui_status = LogicTabManure.get_df_manure_labels(self.project, self.ui_status)
        self.set_ui_status_tabs()
        self.load_project_to_ui()

        self.ui_status.settings["changing_language"] = False

    def show_manual(self):
        pass

    # ---------------LOWER BAR FUNCTIONS AND SLOTS---------------------------------------------------------

    def tab_forward(self):
        print("MainWindow.tab_forward")
        current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()
        current_index = self.ui_status.tabs_state["visible_tabs_indices"].index(current_tab)
        next_tab = self.ui_status.tabs_state["visible_tabs_indices"][
            (current_index + 1) % len(self.ui_status.tabs_state["visible_tabs_indices"])]
        self.QTUI_rotor.tabs_rotor.setCurrentIndex(next_tab)

    def tab_back(self):
        print("MainWindow.tab_back")
        current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()

        current_index = self.ui_status.tabs_state["visible_tabs_indices"].index(current_tab)
        next_tab = self.ui_status.tabs_state["visible_tabs_indices"][
            (current_index - 1) % len(self.ui_status.tabs_state["visible_tabs_indices"])]
        self.QTUI_rotor.tabs_rotor.setCurrentIndex(next_tab)

    # ------------------------- RESETS----------------------------------------------------------

    # TODO implement free generation reset

    def reset_current_tab(self):
        print("MainWindow.reset_current_tab")
        current_tab = self.QTUI_rotor.tabs_rotor.currentIndex()

        if current_tab == 0:
            self.reset_tab_general()
        elif current_tab == 1:
            self.reset_tabs_rotation()
        elif current_tab == 2:
            self.reset_tab_free_generation()
        elif current_tab == 3:
            self.reset_tab_manure()
        elif current_tab == 4:
            self.reset_tab_yield()

    def reset_all(self):
        print("MainWindow.reset_all")
        self.project, self.ui_status = LogicSidebar.reset_all(self.project)
        self.set_ui_status_tabs()

        self.load_project_to_ui()

    def reset_tab_general(self):
        print("MainWindow.reset_tab_general")
        self.project, self.ui_status = LogicSidebar.reset_tab_general(self.project, self.ui_status)

        self.load_project_to_tab_general()

    def reset_tabs_rotation(self):
        print("MainWindow.reset_tabs_rotation")
        print(self.project.all_tabs_combobox_items["cb_manure_type"])
        self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)

        self.setup_tab_rotation()
        self.load_project_to_tab_rotation()
        self.set_ui_status_tabs()

    def reset_tab_free_generation(self):
        print("MainWindow.reset_tab_free_generation")

        self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)
        self.setup_tab_free_generation()
        self.load_project_to_tab_free_generation()

    def reset_tab_manure(self):
        print("MainWindow.reset_tab_manure")
        # TODO adjust this function to new manure logic
        self.project, self.ui_status = LogicSidebar.reset_manure(self.project, self.ui_status)
        self.load_project_to_tab_manure()

    def reset_tab_yield(self):
        print("MainWindow.reset_epm_selection")
        self.project, self.ui_status = LogicSidebar.reset_yield(self.project, self.ui_status)

        self.load_project_to_tab_yield()

    # TODO refactor move out to LogicSideBar?
    def bt_calculate_generate_rotations(self):
        print("MainWindow.bt_calculate_generate_rotations")
        '''This function generates the crop rotations and hands them over to the calculations for yield'''
        self.save_project()
        self.project, self.ui_status = LogicTabManure.update_manure_ids(self.project, self.ui_status)
        print("manure before calculation")
        print("project.tab_manure[manure_standard_values]: ", self.project.tab_manure["manure_standard_values"])
        print("project.tab_manure[manure_values]: ", self.project.tab_manure["manure_values"])
        if not self.project.tab_general["assessment_method"] == 1:
            # TODO und von hier noch weiter
            QMessageBox.question(self, "Bis hierhin und nicht weiter...", "Hier fehlt noch ein bißchen was- sorry!",
                                 QMessageBox.Ok | QMessageBox.Cancel)

            return

        if self.ui_status.tabs["tab_yield_visible"] is False:
            print("Calculate: tab_yield_visible is False, yield has to be assessed")
            crop_rotation = CropRotation(self.project, self.ui_status)
            if crop_rotation.ui_status.error["error"]:
                self.ui_status = crop_rotation.ui_status
                print("ERROR in MainWindow.bt_calculate_generate_rotations")
            else:
                self.project = crop_rotation.project
                self.ui_status = crop_rotation.ui_status

                print("Project.results before Calculate1", self.project.results)
                # calculates all location parameters, N-, P- and K-needs of crops and N, P and K in manure
                result = Calculate1(self.project, self.ui_status)
                self.project = result.project
                self.ui_status = result.ui_status
                print("Project.results after Calculate1", self.project.results)

                # calculate all yields
                self.project, self.ui_status = LogicTabYield.get_all_yields(self.project, self.ui_status)

                self.load_project_to_tab_yield()
                self.set_ui_status_tabs()
                self.QTUI_rotor.tabs_rotor.setCurrentIndex(4)
                self.setup_tab_yield()
                self.load_project_to_tab_yield()

        else:
            print("Calculate: tab_yield_visible is True, calculations can start")
            # TODO yields have to be checked for zeros!
            # all user data is present, balances can be calculated
            result = Calculate2(self.project, self.ui_status)
            self.project = result.project
            self.ui_status = result.ui_status

            # TODO move to Calculate2
            self.ui_status.tabs["tab_report_visible"] = True
            self.ui_status.tabs["tab_economy_visible"] = True
            self.ui_status.tabs_state["update_tabs"] = True
            self.set_ui_status_tabs()
            self.QTUI_rotor.tabs_rotor.setCurrentIndex(5)
            self.setup_tab_report()
            self.load_project_to_tab_report()

        self.display_ui_status_errormessage()
        print("Dict_tab_rotation", self.project.tab_rotation)
        print("RESULT", self.project.results)

    # ----------------GENERAL TAB SLOTS--------------------------------------
    def gb_precipitation_winter_toggled(self, value):
        self.project.tab_general["precipitation_winter_selected"] = value

    def sb_precipitation_value_changed(self, value):
        self.project.tab_general["precipitation"] = value
        self.QTUI_rotor.sb_precipitation_winter.setMaximum(value)

    def sb_precipitation_winter_value_changed(self, value):
        self.project.tab_general["precipitation_winter"] = value

    def open_url(self):
        print("MainWindow.open_url")

        try:
            url = QUrl('https://gis.uba.de/website/depo1/')
            QDesktopServices.openUrl(url)

        except Exception as e:
            QMessageBox.warning(self, 'Open Url', 'Could not open URL.\n Error:\n{}'.format(str(e)))

    def xb_red_area_toggled(self, red_area):
        print("MainWindow.xb_red_area_toggled", red_area)

        # TODO §13a DüV, red areas should be considered in the calculations, so far, this bool has no effect
        self.project.tab_general["red_area"] = red_area

    def get_rotation_duration(self):
        print("MainWindow.get_rotation_duration")
        # cast is necessary, because the combobox only knows strings
        self.rotation_duration = int(self.QTUI_rotor.cb_rotation_duration.currentText())
        self.project.tab_general["rotation_duration"] = self.rotation_duration
        self.setup_tabs_for_rotation_duration()

        # self.logger.info("get_rotation_duration(): Rotation_duration changed to", self.rotation_duration)

    def sb_atmospheric_n_input_value_changed(self, value):
        self.project.tab_general["atmospheric_n_input"] = value

    def gb_soil_parameters_advanced_toggled(self, value):
        self.project.tab_general["soil_advanced_parameters_selected"] = value

    def sb_soil_mineralisation_value_changed(self, value):
        self.project.tab_general["soil_mineralisation_rate"] = value

    def sb_soil_organic_matter_value_changed(self, value):
        self.project.tab_general["soil_organic_matter"] = value

    def sb_soil_c_n_ratio_value_changed(self, value):
        self.project.tab_general["soil_c_n_ratio"] = value

    def sb_soil_dry_density_value_changed(self, value):
        self.project.tab_general["soil_bulk_density"] = value

    def sb_soil_stoneratio_value_changed(self, value):
        self.project.tab_general["soil_stoneratio"] = value

    def sb_soil_topsoil_depth_value_changed(self, value):
        self.project.tab_general["soil_topsoil_depth"] = value

    def rb_soil_index_toggled(self, value):
        print("MainWindow.rb_soil_index_toggled", value)

        self.project.tab_general["soil_quality_by_soil_index"] = value

    # sets the soil type according to the soil rating index (Ackerzahl) - can be changed in database soil_quality_tb)
    def set_cb_soil_type_from_soil_index(self, value):
        print("MainWindow.set_cb_soil_type_from_soil_index", value)
        self.project.tab_general["soil_index"] = value
        if self.QTUI_rotor.sb_soil_index.isEnabled():
            LogicTabGeneral.set_soil_type_from_soil_index(self.project)
            self.QTUI_rotor.cb_soil_type.setCurrentIndex(
                self.QTUI_rotor.cb_soil_type.findData(self.project.tab_general["soil_index"]))

    # sets the soil rating index according to the soil type - can be changed in database soil_quality_tb)
    def set_sb_soil_index_from_soil_type(self):
        print("MainWindow.set_sb_soil_index_from_soil_type")

        if self.QTUI_rotor.cb_soil_type.isEnabled():
            # self.logger.info("cb_soil_type enabled")

            self.QTUI_rotor.sb_soil_index.setValue(self.QTUI_rotor.cb_soil_type.currentData())

    def rb_assessment_selection(self, assessment_method):
        print("MainWindow.rb_assessment_selection", assessment_method)
        self.project.tab_general["assessment_method"] = assessment_method
        if self.project.tab_general["assessment_method"] == 1:
            self.project, self.ui_status = LogicSidebar.reset_crop_rotation(self.project, self.ui_status)
            self.setup_tab_rotation_crop_year_comboboxes()
        elif self.project.tab_general["assessment_method"] == 3:
            self.setup_tab_rotation_crop_year_comboboxes()
        else:
            self.setup_tab_free_generation()
        print("ASSESSMENT METHOD ", self.project.tab_general["assessment_method"])
        if self.project.tab_general["assessment_method"] in [1, 3]:
            self.load_project_to_tab_rotation()
        else:
            self.load_project_to_tab_free_generation()

        self.project, self.ui_status = LogicTabGeneral.get_ui_status_assessment(self.project, self.ui_status)

        self.set_ui_status_tabs()

    # -------------------ROTATION TAB FUNCTIONS-(Assessment and Intelligent Generation)-------------------------------

    def cb_crop_selected(self, index):
        print("MainWindow.cb_crop_selected", index)
        '''this function writes the selected crop to the project. In the intelligent generation, every selection 
        requires the crop comboboxes to be repopulated. This leads to a renewed seelction and triggers this function - 
        therefore the bool self.generation_repopulating.''' \
 \
            # is set to false, in case the rotation duration has been change and the verification has to be run again and the calculate button needs a reset
        self.ui_status.calculation_state["possible"] = False

        if self.project.tab_general["assessment_method"] == 1:

            print('self.project.dict_tab_rotation["crop_id"][index]:', index, 'Crop_id:',
                  self.project.tab_rotation["crop_id"][index])
            print("current data: ", self.cb_crop_year_list[index].currentData())

            if self.project.tab_rotation["crop_id"][index] != self.cb_crop_year_list[index].currentData() \
                    and self.generation_repopulating is False:
                self.project.tab_rotation["crop_id"][index] = self.cb_crop_year_list[index].currentData()

                print("REPOPULATING ", index)

                index_range = (copy.copy(index) + 1) % self.rotation_duration
                print("INDEX_RANGE", index_range)

                for i in range(index_range, (index_range + self.rotation_duration)):
                    # self.generation_repopulating = True
                    index_repopulate = i % self.rotation_duration
                    print("Repopulate Loop", index_repopulate)

                    self.populate_crop_year_combobox(index_repopulate)
                    # self.generation_repopulating = False

                if self.project.tab_rotation["crop_id"][index] is None:
                    self.project, self.ui_status = LogicTabRotation.get_ui_status_of_none(self.project, self.ui_status,
                                                                                          index)
                else:
                    self.project, self.ui_status = LogicTabRotation.get_possible_undersowing_covercrop(self.project,
                                                                                                       self.ui_status,
                                                                                                       index)

            # self.set_ui_status_covercrop(index)
            # self.set_ui_status_undersowing(index)
            # self.set_ui_status_strawharvest(index)
            # self.set_ui_status_early_tillage(index)
            # self.set_ui_status_legumegras(index)

        # assessment method assessment
        else:
            self.project.tab_rotation["crop_id"][index] = self.cb_crop_year_list[index].currentData()
            print('self.project.dict_tab_rotation["crop_id"][index]', self.project.tab_rotation["crop_id"][index])

            if self.project.tab_rotation["crop_id"][index] is None:
                self.project, self.ui_status = LogicTabRotation.get_ui_status_of_none(self.project, self.ui_status,
                                                                                      index)
            else:
                self.project, self.ui_status = LogicTabRotation.get_possible_undersowing_covercrop(self.project,
                                                                                                   self.ui_status,
                                                                                                   index)
            # self.set_ui_status_covercrop(index)
            # self.set_ui_status_undersowing(index)
            # self.set_ui_status_strawharvest(index)
            # self.set_ui_status_early_tillage(index)

        self.set_ui_status_covercrop(index)
        self.set_ui_status_undersowing(index)
        self.set_ui_status_strawharvest(index)
        self.set_ui_status_early_tillage(index)
        self.set_ui_status_legumegras(index)

        '''If a calculation is possible, the calculate button turns active and green'''
        self.project, self.ui_status = LogicGeneral.verify_project(self.project, self.ui_status)
        self.set_ui_status_calculation_possible()

    def xb_early_tillage_toggled(self, index):
        print("MainWindow.xb_early_tillage_toggled", index)
        self.project.tab_rotation["early_tillage"][index] = self.xb_early_tillage_list[index].isChecked()

    def sb_main_legume_ratio_changed(self, index):
        print("MainWindow.sb_main_legume_ratio_changed", index)
        self.project.tab_rotation["main_legume_ratio"][index] = self.sb_main_legume_ratio_list[index].value()

    def sync_crop_methods(self, index):
        self.sb_silage_list[index].setValue(self.project.tab_rotation["silage"][index])
        self.sb_hay_list[index].setValue(self.project.tab_rotation["hay"][index])
        self.sb_green_fodder_list[index].setValue(self.project.tab_rotation["green_fodder"][index])

    def sb_green_fodder_changed(self, index):
        print("MainWindow.sb_green_fodder_changed", index)
        self.project.tab_rotation["green_fodder"][index] = self.sb_green_fodder_list[index].value()
        self.project = LogicTabRotation.get_crop_methods_from_green_fodder(self.project, index)
        self.sync_crop_methods(index)

    def sb_hay_changed(self, index):
        print("MainWindow.sb_hay_changed", index)
        self.project.tab_rotation["hay"][index] = self.sb_hay_list[index].value()
        self.project = LogicTabRotation.get_crop_methods_from_hay(self.project, index)
        self.sync_crop_methods(index)

    def sb_silage_changed(self, index):
        print("MainWindow.sb_silage_changed", index)
        self.project.tab_rotation["silage"][index] = self.sb_silage_list[index].value()
        self.project = LogicTabRotation.get_crop_methods_from_silage(self.project, index)
        self.sync_crop_methods(index)

    def xb_strawharvest_toggled(self, index):
        print("MainWindow.xb_strawharvest_toggled", index)
        self.project.tab_rotation["strawharvest"][index] = self.xb_strawharvest_list[index].isChecked()

        # self.logger.info("strawharvest", self.project.tab_rotation["strawharvest"])

    def xb_covercrop_toggled(self, index):
        print("MainWindow.xb_covercrop_toggled", index)
        self.project.tab_rotation["covercrop"][index] = self.xb_covercrop_list[index].isChecked()
        # self.project, self.ui_status = LogicTabRotation.get_ui_status_crop_year(self.project, self.ui_status, index)

        self.project, self.ui_status = LogicTabRotation.get_status_covercrop_toggled(self.project, self.ui_status,
                                                                                     index)

        self.set_ui_status_covercrop(index)

    # this function links the inputs of undersowing and covercrop_id 3, undersowing as covercrop
    def sync_gb_undersowing_gb_covercrop(self, index):
        print("MainWindow.sync_gb_undersowing_gb_covercrop", index)
        index_next = (index + 1) % self.rotation_duration
        index_previous = (index - 1) % self.rotation_duration

        if self.ui_status.tab_rotation["undersowing_sync"][index]:
            self.cb_covercrop_winterhardiness_list[index_next].setCurrentIndex(self.project.tab_rotation["covercrop_winterhardiness"][index_next])
            self.sb_covercrop_legume_ratio_list[index_next].setValue(
                self.sb_undersowing_legume_ratio_list[index].value())
            self.sb_main_legume_ratio_list[index_next].setValue(
                self.sb_undersowing_legume_ratio_list[index].value())
        elif self.ui_status.tab_rotation["covercrop_sync"][index]:
            self.sb_undersowing_legume_ratio_list[index_previous].setValue(
                self.sb_covercrop_legume_ratio_list[index].value())
            self.sb_undersowing_legume_ratio_list[index_previous].setValue(
                self.sb_main_legume_ratio_list[index].value())

    def xb_undersowing_toggled(self, index):
        print("MainWindow.xb_undersowing_toggled", index)
        self.project.tab_rotation["undersowing"][index] = self.xb_undersowing_list[index].isChecked()
        # self.project, self.ui_status = LogicTabRotation.get_ui_status_crop_year(self.project, self.ui_status, index)
        self.project, self.ui_status = LogicTabRotation.get_ui_status_undersowing_toggled(self.project, self.ui_status,
                                                                                          index)

        if self.project.tab_rotation["undersowing"][index]:
            self.sync_gb_undersowing_gb_covercrop(index)

        self.set_ui_status_undersowing(index)
        # if covercrop of the next year has already been adjusted, undersowing is synchronized

    def populate_crop_year_combobox(self, index):
        print("MainWindow.populate_crop_year_combobox", 7)
        # recordsets are created in cb_crop_selected
        self.generation_repopulating = True
        self.project = LogicTabRotation.generate_crop_recordset(self.project, self.ui_status, index)
        recordset = self.project.rotation_temp["cb_crop_recordsets"][index]
        self.populate_combobox(recordset, self.cb_crop_year_list[index])

        # sets the combobox to previously selected crop
        if self.project.tab_rotation["crop_id"][index] is not None:
            print("FROM CROP YEAR CB, crop at current index is not none")
            # asking if crop_id is in the recordset. This is necessary if the assessment method is switched from assessment to Intelligeneration
            if self.project.tab_rotation["crop_id"][index] in (item for sublist in recordset for item in sublist):
                self.cb_crop_year_list[index].setCurrentIndex(
                    self.cb_crop_year_list[index].findData(self.project.tab_rotation["crop_id"][index]))
                print("if index...", index, "crop_id", self.project.tab_rotation["crop_id"][index])
            else:
                self.cb_crop_year_list[index].setCurrentIndex(0)  # Index 0 is ---select crop---

        self.generation_repopulating = False

    # ------------FREE GENERATION TAB-------------------------------------
    def cb_free_generation_crop_selected(self, index):
        print("MainWindow.cb_free_generation_crop_selected", index)
        if self.project.tab_general["assessment_method"] == 2:  # and self.load_project is False:
            if self.cb_free_generation_crop_list[index].currentData() is not None:
                self.project.tab_free_generation["crop_id"][index] = self.cb_free_generation_crop_list[
                    index].currentData()
            print("Current Crop", self.project.tab_free_generation["crop_id"][index])

            self.project, self.ui_status = LogicGeneral.verify_project(self.project, self.ui_status)
            self.set_ui_status_calculation_possible()

    def cb_free_generation_crop_type_selected(self, index):
        print("MainWindow.cb_free_generation_crop_type_selected", index)
        # if self.load_project is False:
        self.project.tab_free_generation["crop_type_selection"][index] = self.cb_free_generation_crop_type_list[
            index].currentData()

        old_crop_id = copy.copy(self.project.tab_free_generation["crop_id"][index])

        self.populate_free_generation_crop_combobox(index)
        # if save crop id is available in the selected crop type, the crop remains, otherwise the first item in combobox is selected
        if old_crop_id in (item for row in self.project.free_generation_temp["cb_crop_recordsets"][index] for item in
                           row):
            print("crop_type try")
            self.cb_free_generation_crop_list[index].setCurrentIndex(
                self.cb_free_generation_crop_list[index].findData(old_crop_id))

    def populate_free_generation_crop_combobox(self, index):
        print("MainWindow.populate_free_generation_crop_combobox", index)
        self.project = LogicFreeRotationGeneration.get_recordset_cb_crops(self.project, self.ui_status, index)
        self.populate_combobox(self.project.free_generation_temp["cb_crop_recordsets"][index],
                               self.cb_free_generation_crop_list[index])

    # --------------------MANURE TAB---------------------------------------------------------------------
    # --------------------------- TAB MANURE
    def set_manure_line_visible(self, index_manure, visible):
        print("MainWindow.set_manure_line_visible")
        self.lb_manure_list[index_manure].setVisible(visible)
        self.bt_manure_default_list[index_manure].setVisible(visible)
        self.sb_manure_dry_mass_list[index_manure].setVisible(visible)
        self.sb_manure_n_content_list[index_manure].setVisible(visible)
        self.sb_manure_n_availability_list[index_manure].setVisible(visible)
        self.sb_manure_n_loss_list[index_manure].setVisible(visible)
        self.sb_manure_p_content_list[index_manure].setVisible(visible)
        self.sb_manure_k_content_list[index_manure].setVisible(visible)

    # -----------------MANURE TAB SLOTS -------------------------------------------------------------------
    def sb_manure_dry_mass_changed(self, index):
        print("MainWindow.sb_manure_dry_mass_changed")

        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        print("dry mass value before", self.project.tab_manure["manure_values"][m_n_manures_id]["dry_mass"])
        print("dry mass standard value before",
              self.project.tab_manure["manure_standard_values"][m_n_manures_id]["dry_mass"])

        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["dry_mass"] = self.sb_manure_dry_mass_list[
                                                                                       index].value() / 100

        print("dry mass value after", self.project.tab_manure["manure_values"][m_n_manures_id]["dry_mass"])
        print("dry mass standard value after",
              self.project.tab_manure["manure_standard_values"][m_n_manures_id]["dry_mass"])

    def sb_manure_n_content_changed(self, index):
        print("MainWindow.sb_manure_n_content_changed")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["n_content_per_unit"] = \
                self.sb_manure_n_content_list[index].value()

    def sb_manure_n_availability_changed(self, index):
        print("MainWindow.sb_manure_n_availability_changed")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["n_availability_percent"] = \
                self.sb_manure_n_availability_list[index].value() / 100

    def sb_manure_n_loss_percent_changed(self, index):
        print("MainWindow.sb_manure_n_loss_percent_changed")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["n_loss_percent"] = self.sb_manure_n_loss_list[
                                                                                             index].value() / 100

    def sb_manure_p_content_changed(self, index):
        print("MainWindow.sb_manure_p_content_changed")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["p_content_kg_per_unit"] = \
                self.sb_manure_p_content_list[index].value()

    def sb_manure_k_content_changed(self, index):
        print("MainWindow.sb_manure_k_content_changed")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]
        if self.setting_up_tab_manure is False:
            self.project.tab_manure["manure_values"][m_n_manures_id]["k_content_kg_per_unit"] = \
                self.sb_manure_k_content_list[index].value()

    def bt_manure_default_clicked(self, index):
        print("MainWindow.bt_manure_default_clicked", index)
        # TODO das stimmt doch noch nicht!
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index]

        self.project, self.ui_status = LogicTabManure.bt_load_standard_values(self.project, self.ui_status, index)

        self.load_manure_value_to_gui(index)
        self.ui_status.tab_manure["load_manures"] = False

    # -----------------TAB EPM----------------------------------------------------------------
    def sb_yield_projection_changed(self, index):
        print("MainWindow.sb_yield_projection_changed")
        if self.ui_status.settings["language"] == "de":
            self.project.tab_yield["yield_amount_t"][index] = self.sb_yield_amount_main_list[index].value() / 10
        else:
            self.project.tab_yield["yield_amount_t"][index] = self.sb_yield_amount_main_list[index].value()

    def sb_yield_catch_crop_changed(self, index):
        if self.ui_status.settings["language"] == "de":
            self.project.tab_yield["yield_amount_catch_crop_t"][index] = self.sb_yield_amount_catch_crop_list[
                                                                             index].value() / 10
        else:
            self.project.tab_yield["yield_amount_catch_crop_t"][index] = self.sb_yield_amount_catch_crop_list[
                index].value()

    def bt_yield_reset_clicked(self, index):
        print("MainWindow.bt_yield_reset_clicked", index)
        self.project, self.ui_status = LogicTabYield.reset_one_yield(self.project, self.ui_status, index)

        self.load_value_to_sb_yield(index)

    def cb_tillage_selected(self, index):
        print("MainWindow.cb_tillage_selected")
        # if self.setting_up_tab_yield is False:
        self.project.tab_rotation["tillage_id"][index] = self.cb_tillage_list[index].currentData()

    # ----------------------LOAD PROJECT---------------------------------------------------------
    def load_project_to_ui(self):
        print("MainWindow.load_project_to_ui")
        #self.logger.info("PROJECT in LOAD_PROJECT_TO_UI", self.project.tab_general, self.project.tab_rotation, self.project.tab_manure)

        # self.load_project = True to keep warnings from popping up
        self.ui_status.load_project = True
        # self.setup_rotation_tab()
        # rotor_project = self.project
        self.setWindowTitle("ROTOR 4.0  --  %(projectname)s" % {"projectname": self.project.project_name})

        self.load_project_to_tab_general()
        if self.project.tab_general["assessment_method"] in [1, 3]:
            self.load_project_to_tab_rotation()
        else:
            self.load_project_to_tab_free_generation()

        if self.ui_status.tabs["tab_yield_visible"]:
            self.setup_tab_yield()
            self.load_project_to_tab_yield()
        if self.ui_status.tabs["tab_organic_manure_visible"]:
            self.load_project_to_tab_manure()
        if self.ui_status.tabs["tab_report_visible"]:
            self.load_project_to_tab_report()
        self.ui_status.load_project = False

    def load_project_to_tab_general(self):
        print("MainWindow.load_project_to_tab_general")

        self.rotation_duration = self.project.tab_general["rotation_duration"]

        # adds all attributes from rotor_project to UI
        # if the duration is changed by reloading, the tabs generation and free generation are automatically setup
        self.QTUI_rotor.cb_rotation_duration.setCurrentText(str(self.project.tab_general["rotation_duration"]))
        self.QTUI_rotor.sb_precipitation.setValue(self.project.tab_general["precipitation"])

        self.QTUI_rotor.gb_precipitation_winter.setChecked(self.project.tab_general["precipitation_winter_selected"])
        self.QTUI_rotor.sb_precipitation_winter.setValue(self.project.tab_general["precipitation_winter"])

        self.rb_assessment_method_group.button(self.project.tab_general["assessment_method"]).setChecked(True)

        self.QTUI_rotor.sb_soil_index.setValue(self.project.tab_general["soil_index"])
        self.QTUI_rotor.rb_soil_index.setChecked(self.project.tab_general["soil_quality_by_soil_index"])
        self.QTUI_rotor.rb_soil_type.setChecked(not self.project.tab_general["soil_quality_by_soil_index"])

        self.QTUI_rotor.gb_soil_parameters_advanced.setChecked(
            self.project.tab_general["soil_advanced_parameters_selected"])
        if self.project.tab_general["soil_advanced_parameters_selected"]:
            self.QTUI_rotor.sb_soil_adv_mineralisation.setValue(self.project.tab_general["soil_mineralisation"])
            self.QTUI_rotor.sb_soil_adv_organic_matter.setValue(self.project.tab_general["soil_organic_matter"])
            self.QTUI_rotor.sb_soil_adv_c_n_ratio.setValue(self.project.tab_general["soil_c_n_ratio"])
            self.QTUI_rotor.sb_soil_adv_topsoil_depth.setValue(self.project.tab_general["soil_topsoil_depth"])
            self.QTUI_rotor.sb_soil_adv_stoneratio.setValue(self.project.tab_general["soil_stoneratio"])
            self.QTUI_rotor.sb_soil_adv_dry_density.setValue(self.project.tab_general["soil_bulk_density"])

        self.QTUI_rotor.sb_atmospheric_n_input.setValue(self.project.tab_general["atmospheric_n_input"])
        self.QTUI_rotor.xb_red_area.setChecked(self.project.tab_general["red_area"])

        # ----------------TAB ROTATION-----------------------------------------

    def load_project_to_tab_rotation(self):
        print("MainWindow.load_project_to_tab_rotation")
        # TODO Check if the rotation tab needs resetting first
        # TODO update!
        # self.setup_tab_rotation()
        # since the project comes with the selected crops, the comboboxes are automatically reloaded
        # if self.project.dict_tab_general["assessment_method"] in (1, 3):

        for index in range(0, self.rotation_duration):
            self.cb_crop_year_list[index].setCurrentIndex((self.cb_crop_year_list[index].findData(
                self.project.tab_rotation["crop_id"][index])))
            self.xb_early_tillage_list[index].setChecked(self.project.tab_rotation["early_tillage"][index])
            self.xb_strawharvest_list[index].setChecked(self.project.tab_rotation["strawharvest"][index])
            self.xb_undersowing_list[index].setChecked(self.project.tab_rotation["undersowing"][index])
            self.sb_undersowing_legume_ratio_list[index].setValue(
                self.project.tab_rotation["undersowing_legume_ratio"][index])
            self.xb_covercrop_list[index].setChecked(self.project.tab_rotation["covercrop"][index])
            self.cb_covercrop_winterhardiness_list[index].setCurrentIndex(self.project.tab_rotation["covercrop_winterhardiness"][index])

            self.sb_covercrop_legume_ratio_list[index].setValue(
                self.project.tab_rotation["covercrop_legume_ratio"][index])
            self.xb_covercrop_harvest_list[index].setChecked(self.project.tab_rotation["covercrop_harvest"][index])
            self.sb_main_legume_ratio_list[index].setValue(self.project.tab_rotation["main_legume_ratio"][index])
            self.sb_green_fodder_list[index].setValue(self.project.tab_rotation["green_fodder"][index])
            self.sb_hay_list[index].setValue(self.project.tab_rotation["hay"][index])
            self.sb_silage_list[index].setValue(
                self.project.tab_rotation["silage"][index])

            if self.project.tab_rotation["manure"][index]:
                for index_2 in range(4):
                    if self.project.tab_rotation["m_n_manures_id"][index_2] == 0:
                        break
                    else:
                        self.cb_manure_type_list[index][index_2].setCurrentIndex(
                            self.cb_manure_type_list[index][index_2].findData(
                                self.project.tab_rotation["manure_type"][index][index_2]))
                        self.cb_manure_source_list[index][index_2].setCurrentIndex(
                            self.cb_manure_source_list[index][index_2].findData(
                                self.project.tab_rotation["manure_source"][index][index_2]))
                        self.cb_manure_specification_list[index][index_2].setCurrentIndex(
                            self.cb_manure_specification_list[index][index_2].findData(
                                self.project.tab_rotation["manure_specification"][index][index_2]))

    def load_project_to_tab_free_generation(self):
        print("MainWindow.load_project_to_tab_free_generation")
        # if self.load_project = True
        # TODO loading project to free rotation
        self.QTUI_rotor.gb_livestock_farming.setChecked(self.project.tab_free_generation["livestock_farming"])
        for index in range(0, self.rotation_duration):
            print("Current index crop_type", self.cb_free_generation_crop_type_list[index].findData(
                self.project.tab_free_generation["crop_type_selection"]))
            self.cb_free_generation_crop_type_list[index].setCurrentIndex(
                self.cb_free_generation_crop_type_list[index].findData(
                    self.project.tab_free_generation["crop_type_selection"][index]))
            if self.project.tab_free_generation["crop_id"][index] is not None:
                self.cb_free_generation_crop_list[index].setCurrentIndex(
                    self.cb_free_generation_crop_list[index].findData(
                        self.project.tab_free_generation["crop_id"][index]))

        # self.load_project = False

    def load_project_to_tab_yield(self):
        print("MainWindow.load_project_to_tab_yield")

        # the tab is only visible, if the project is verified and has proceeded to that selection
        if self.ui_status.tabs["tab_yield_visible"]:
            # self.QTUI_rotor.tab_epm_selection.setVisible(True)
            for index in range(0, self.rotation_duration):
                self.lb_yield_amount_main_list[index].setText(self.project.tab_rotation["crop_name"][index])
                self.load_value_to_sb_yield(index)

    def load_value_to_sb_yield(self, index):
        if self.ui_status.settings["language"] == "de":
            print("Yield: ", self.project.tab_yield["yield_amount_t"][index] * 10)
            self.sb_yield_amount_main_list[index].setValue(self.project.tab_yield["yield_amount_t"][index] * 10)
            self.sb_yield_amount_catch_crop_list[index].setValue(
                self.project.tab_yield["yield_amount_catch_crop_t"][index] * 10)
        else:
            self.sb_yield_amount_main_list[index].setValue(self.project.tab_yield["yield_amount_t"][index])
            self.sb_yield_amount_catch_crop_list[index].setValue(
                self.project.tab_yield["yield_amount_catch_crop_t"][index])

    # -----------------TAB MANURE -----------------------------------------------------

    def load_project_to_tab_manure(self):
        print("MainWindow.load_project_to_tab_manure")
        print("load_manures", self.ui_status.tab_manure["load_manures"])
        if self.ui_status.tab_manure["load_manures"]:

            for index_manure in range(0, len(self.project.tab_manure["m_n_manures_ids"])):
                self.set_manure_line_visible(index_manure, True)
                self.load_manure_value_to_gui(index_manure)

            for index_manure in range(len(self.project.tab_manure["m_n_manures_ids"]), len(self.lb_manure_list)):
                self.set_manure_line_visible(index_manure, False)

            self.ui_status.tab_manure["load_manures"] = False

    def load_project_to_tab_report(self):
        print("MainWindow.load_project_to_tab_report")

        if self.ui_status.tabs["tab_report_visible"]:
            pass

    def set_manure_label(self, index_manure):
        print("MainWindow.set_manure_label")
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index_manure]
        label = self.ui_status.tab_manure["manure_labels"][m_n_manures_id]
        self.lb_manure_list[index_manure].setText(label)

    def load_manure_value_to_gui(self, index_manure):
        print("MainWindow.load_manure_value_to_gui", index_manure)
        m_n_manures_id = self.project.tab_manure["m_n_manures_ids"][index_manure]
        print("m_n_manures_id", m_n_manures_id)
        print("manure_values", self.project.tab_manure["manure_values"][m_n_manures_id])
        print("manure_standard_values", self.project.tab_manure["manure_standard_values"][m_n_manures_id])
        # TODO this does not belong here
        self.set_manure_label(index_manure)

        manure_values = self.project.tab_manure["manure_values"][m_n_manures_id]

        self.sb_manure_dry_mass_list[index_manure].setValue(manure_values["dry_mass"] * 100)
        self.sb_manure_n_content_list[index_manure].setValue(manure_values["n_content_per_unit"])
        self.sb_manure_k_content_list[index_manure].setValue(manure_values["k_content_kg_per_unit"])
        self.sb_manure_n_loss_list[index_manure].setValue(manure_values["n_loss_percent"] * 100)
        self.sb_manure_p_content_list[index_manure].setValue(manure_values["p_content_kg_per_unit"])
        self.sb_manure_n_availability_list[index_manure].setValue(manure_values["n_availability_percent"] * 100)

        self.sb_manure_dry_mass_list[index_manure].setMinimum(manure_values["dry_mass_minimum"] * 100)
        self.sb_manure_dry_mass_list[index_manure].setMaximum(manure_values["dry_mass_maximum"] * 100)
